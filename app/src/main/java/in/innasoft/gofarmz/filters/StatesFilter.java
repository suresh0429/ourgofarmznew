package in.innasoft.gofarmz.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.gofarmz.adapters.StatesAdapter;
import in.innasoft.gofarmz.models.StatesModel;

public class StatesFilter extends Filter {

    StatesAdapter adapter;
    ArrayList<StatesModel> filterList;

    public StatesFilter(ArrayList<StatesModel> filterList, StatesAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<StatesModel> filteredPlayers = new ArrayList<StatesModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.statesModels = (ArrayList<StatesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
