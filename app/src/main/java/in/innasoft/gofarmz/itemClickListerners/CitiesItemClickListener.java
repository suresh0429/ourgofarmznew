package in.innasoft.gofarmz.itemClickListerners;

import android.view.View;

public interface CitiesItemClickListener
{
    void onItemClick(View v, int pos);
}

