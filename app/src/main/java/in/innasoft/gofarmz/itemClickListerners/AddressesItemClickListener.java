package in.innasoft.gofarmz.itemClickListerners;

import android.view.View;

public interface AddressesItemClickListener
{
    void onItemClick(View v, int pos);
}

