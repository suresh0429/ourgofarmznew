package in.innasoft.gofarmz.itemClickListerners;

import android.view.View;

public interface BoxNameItemClickListener
{
    void onItemClick(View v, int pos);
}

