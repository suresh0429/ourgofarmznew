package in.innasoft.gofarmz.activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import java.util.HashMap;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class UpdateProfileActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    EditText mobile_edt, email_edt, username_edt;
    RadioGroup rg;
    RadioButton male_rb, female_rb;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ImageView close_img;
    TextView gender_title;
    String deviceId, token, user_id, email, mobile, gender, name, send_gender;
    Button update_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        checkInternet = NetworkChecking.isConnected(this);

        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("name");
        email = bundle.getString("email");
        mobile = bundle.getString("mobile");
        gender = bundle.getString("gender");
        Log.d("DETAIL", email + "-----" + mobile + "------" + gender + "------" + name);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        update_btn = findViewById(R.id.update_btn);
        update_btn.setOnClickListener(this);

        email_edt = findViewById(R.id.email_edt);
        mobile_edt = findViewById(R.id.mobile_edt);
        username_edt = findViewById(R.id.username_edt);

        rg = findViewById(R.id.rg);
        rg.setOnCheckedChangeListener(this);
        male_rb = findViewById(R.id.male_rb);
        female_rb = findViewById(R.id.female_rb);

        gender_title = findViewById(R.id.gender_title);

        mobile_edt.setText(mobile);
        email_edt.setText(email);
        username_edt.setText(name);
        if (gender != null) {
            send_gender = gender;
            if (gender.equalsIgnoreCase("Male")) {
                male_rb.setChecked(true);
            } else if (gender.equalsIgnoreCase("Female")) {
                female_rb.setChecked(true);
            }
        }
    }

    @Override
    public void onClick(View v) {

        if (v == close_img) {

            finish();
        }
        if (v == update_btn) {
            String moblie = mobile_edt.getText().toString();
            if (moblie == null || moblie.length() == 0) {
                Toast.makeText(this, "Enter Mobile No", Toast.LENGTH_SHORT).show();
            } else if (send_gender == null || send_gender.length() == 0) {
                Toast.makeText(this, "Select Gender", Toast.LENGTH_SHORT).show();
            } else {
                updateProfile();
            }

        }
    }

    private void updateProfile() {
        if (checkInternet) {

            Call<BaseResponse> call = RetrofitClient.getInstance().getApi().updateProfile(token,deviceId,user_id,username_edt.getText().toString(),email_edt.getText().toString(),
                    mobile_edt.getText().toString(),send_gender);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {

                    if (response.isSuccessful()) ;
                    BaseResponse baseResponse = response.body();

                    if (baseResponse.getStatus().equals("10100")) {

                        Intent intent = new Intent(UpdateProfileActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (baseResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.getStatus().equals("10300")) {

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.male_rb) {
            male_rb.setChecked(true);
            send_gender = "Male";
        } else {
            female_rb.setChecked(true);
            send_gender = "Female";
        }
    }
}
