package in.innasoft.gofarmz.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.HashMap;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.WalletgetResponse;
import in.innasoft.gofarmz.adapters.WalletAdapter;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class RazerPayActivity extends AppCompatActivity implements PaymentResultListener {
    private static final String TAG = RazerPayActivity.class.getSimpleName();
    UserSessionManager userSessionManager;
    String activity,name,email,mobile,encryptOrderId,order_ref_no,token,deviceId,user_id;
    int orderId;
    Handler mHandler = new Handler();
    Double totalvalue;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razer_pay);

        pprogressDialog = new ProgressDialog(RazerPayActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        email = userDetails.get(UserSessionManager.USER_EMAIL);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        name = userDetails.get(UserSessionManager.USER_NAME);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        Log.d(TAG, "onCreate: "+email+"  "+mobile);

     /*
         To ensure faster loading of the Checkout form,
          call this method as early as possible in your checkout flow.
         */

        Checkout.preload(getApplicationContext());

        if (getIntent() != null){

             orderId = getIntent().getIntExtra("ORDERID",0);
            // int total = getIntent().getIntExtra("TOTAL",0);
             Double total = getIntent().getDoubleExtra("TOTAL",0.00);
             order_ref_no = getIntent().getStringExtra("ORDER_REF_NO");
             encryptOrderId = getIntent().getStringExtra("ENCRYPTORDERID");
             activity = getIntent().getStringExtra("ACTIVITY");
            // totalvalue = (double) total;
             totalvalue = total * 100;

            // call razorpay
            startPayment(orderId,totalvalue,encryptOrderId);
        }


       // startPayment(orderId);
    }

    public void startPayment(int orderId, Double total, String encryptOrderId) {


        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject data  = new JSONObject();
            data .put("name", "Gofarmz");
            data .put("description", "Demoing Charges");
            //You can omit the image option to fetch the image from dashboard
            data .put("image", "https://www.gofarmz.com/assets-v1/img/gofarmz.png");
            data .put("currency", "INR");
            data .put("amount", total);

            JSONObject notes  = new JSONObject();
            notes.put("email", email);
            notes.put("contact", mobile);
            notes.put("order_id", orderId);
            notes.put("order_ref_no", order_ref_no);
            data .put("notes", notes );
            data .put("prefill", notes );

            co.open(activity, data);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(final String razorpayPaymentID) {
        try {
            //Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            mHandler.post(new Runnable() {
                public void run() {
                    mHandler = null;

                   // if (activity.equalsIgnoreCase("PaymentDetails")) {
                    if (activity.equalsIgnoreCase("PreOrderActivity") || activity.equalsIgnoreCase("OrderActivity")) {

                        //Toast.makeText(getApplicationContext(), "Successfully payment", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(RazerPayActivity.this, SuccessesPage.class);
                        intent.putExtra("ORDERID", orderId);
                        intent.putExtra("ENCRYPTORDERID", encryptOrderId);
                        intent.putExtra("razorpayPaymentID", razorpayPaymentID);
                        Log.d(TAG, "run: " + razorpayPaymentID);
                        intent.putExtra("amount", totalvalue);
                        intent.putExtra("ACTIVITY", activity);
                        startActivity(intent);
                    }
                    else {

                        Double finaltotal = (double) totalvalue;
                        finaltotal = finaltotal / 100;

                        String total = String.valueOf(finaltotal);
                        addMoneyToWallet(razorpayPaymentID,total);
                    }
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Log.d(TAG, "onPaymentError: "+code+"__"+response);
            //Toast.makeText(getApplicationContext(),"Cancel payment" , Toast.LENGTH_SHORT).show();


            if (code==0){
                Toast.makeText(this, response, Toast.LENGTH_SHORT).show();

                if (activity.equalsIgnoreCase("WalletActivity")){
                    Intent intent = new Intent(RazerPayActivity.this, WalletActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else {

                   /* Intent intent = new Intent(RazerPayActivity.this, PaymentDetails.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);*/
                    finish();
                }

            }else {
                Intent intent = new Intent(RazerPayActivity.this, FailurePage.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }




        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }


    private void addMoneyToWallet(String razorpayPaymentID, String total) {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<BaseResponse> call= RetrofitClient.getInstance().getApi().addMoney(token,deviceId,user_id,razorpayPaymentID, total);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                    if (response.isSuccessful()){

                        BaseResponse baseResponse=response.body();

                        if (baseResponse.getStatus().equalsIgnoreCase("10100")){
                            Intent order_detail = new Intent(RazerPayActivity.this, WalletActivity.class);
                            order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(order_detail);
                            finish();
                            Toast.makeText(RazerPayActivity.this,baseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                        if (baseResponse.getStatus().equalsIgnoreCase("10200")){
                            Toast.makeText(RazerPayActivity.this,baseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                        if (baseResponse.getStatus().equalsIgnoreCase("10300")){
                            Toast.makeText(RazerPayActivity.this,baseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                        }


                    }



                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(RazerPayActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}

