package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.AddressListResponse;
import in.innasoft.gofarmz.adapters.AddAreasAdapter;
import in.innasoft.gofarmz.adapters.AddCitiesAdapter;
import in.innasoft.gofarmz.adapters.AddCountriesAdapter;
import in.innasoft.gofarmz.adapters.AddStatesAdapter;
import in.innasoft.gofarmz.adapters.AddressesAdapter;
import in.innasoft.gofarmz.models.AddressesModel;
import in.innasoft.gofarmz.models.AreasModel;
import in.innasoft.gofarmz.models.CitiesModel;
import in.innasoft.gofarmz.models.CountriesModel;
import in.innasoft.gofarmz.models.StatesModel;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class ChooseAddressActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private static final int PLACE_PICKER_REQUEST = 1;
    String latitude, longitude, finaladdress;
    ImageView close_img, search_location, plus_img, no_addresses_img;
    TextView address_txt, add_tittle_txt, prev_txt, default_title;
    ScrollView scrollView;
    RelativeLayout add_form_rl;
    EditText name_edt, address_one_edt, address_two_edt, pincode_edt, mobile_edt, alt_mobile_edt, area_edt, city_edt, state_edt, country_edt;
    TextInputLayout  name_til,address_one_til,address_two_til,pincode_til,mobile_til,alt_mobile_til,area_til,city_til,state_til,country_til;
    Button add_new_address_btn;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile, send_default_value;
    private boolean checkInternet;
    RecyclerView prev_recyclerview;
    AddressesAdapter addressesAdapter;
    ArrayList<AddressesModel> addressesModelArrayList = new ArrayList<AddressesModel>();

    RadioGroup rg_default;
    RadioButton yes_rb, no_rb;

    AlertDialog dialog, dialog2, dialog3, dialog4;
    /*countries*/
    AddCountriesAdapter countriesAdapter;
    ArrayList<CountriesModel> countriesModels = new ArrayList<CountriesModel>();
    ArrayList<String> countriesList = new ArrayList<String>();

    /*states*/
    AddStatesAdapter statesAdapter;
    ArrayList<StatesModel> statesModels = new ArrayList<StatesModel>();
    ArrayList<String> statesList = new ArrayList<String>();

    /*cities*/
    AddCitiesAdapter citiesAdapter;
    ArrayList<CitiesModel> citiesModels = new ArrayList<CitiesModel>();
    ArrayList<String> citiesList = new ArrayList<String>();

    /*areas*/
    AddAreasAdapter areasAdapter;
    ArrayList<AreasModel> areasModels = new ArrayList<AreasModel>();
    ArrayList<String> areasList = new ArrayList<String>();

    String sendCountryName, countryId, sendStateName, stateId, sendCityName,
            cityId, sendAreaName, areaId, activity;
    Geocoder geocoder;
    List<Address> addresses;

    RotateLoading progress_indicator;
    ProgressDialog pprogressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_address);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);

        pprogressDialog = new ProgressDialog(ChooseAddressActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
       // progress_indicator = findViewById(R.id.progress_indicator);
     //   progress_indicator.start();

        activity = getIntent().getStringExtra("activity");

        geocoder = new Geocoder(this, Locale.getDefault());

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        search_location = findViewById(R.id.search_location);
        search_location.setOnClickListener(this);
        plus_img = findViewById(R.id.plus_img);
        plus_img.setOnClickListener(this);
        no_addresses_img = findViewById(R.id.no_addresses_img);

        scrollView = findViewById(R.id.scrollView);
        address_txt = findViewById(R.id.address_txt);

        address_txt.setOnClickListener(this);
        add_tittle_txt = findViewById(R.id.add_tittle_txt);

        add_tittle_txt.setOnClickListener(this);
        prev_txt = findViewById(R.id.prev_txt);

        prev_txt.setOnClickListener(this);
        name_edt = findViewById(R.id.name_edt);

        address_one_edt = findViewById(R.id.address_one_edt);

        address_two_edt = findViewById(R.id.address_two_edt);

        area_edt = findViewById(R.id.area_edt);

        city_edt = findViewById(R.id.city_edt);

        state_edt = findViewById(R.id.state_edt);

        country_edt = findViewById(R.id.country_edt);

        pincode_edt = findViewById(R.id.pincode_edt);

        mobile_edt = findViewById(R.id.mobile_edt);




        name_til = findViewById(R.id.name_til);

        address_one_til = findViewById(R.id.address_one_til);

        address_two_til = findViewById(R.id.address_two_til);

        pincode_til = findViewById(R.id.pincode_til);

        mobile_til = findViewById(R.id.mobile_til);

        alt_mobile_til = findViewById(R.id.alt_mobile_til);

        area_til = findViewById(R.id.area_til);

        city_til = findViewById(R.id.city_til);

        state_til = findViewById(R.id.state_til);

        country_til = findViewById(R.id.country_til);



        default_title = findViewById(R.id.default_title);



        prev_recyclerview = findViewById(R.id.prev_recyclerview);
        prev_recyclerview.setHasFixedSize(true);

        add_new_address_btn = findViewById(R.id.add_new_address_btn);

        add_new_address_btn.setOnClickListener(this);

        rg_default = findViewById(R.id.rg_default);
//        rg_default.setOnCheckedChangeListener(this);
        yes_rb = findViewById(R.id.yes_rb);

        no_rb = findViewById(R.id.no_rb);

        send_default_value = "Yes";
      //  getCountries();
        getAddresses();
    }



    public void getAddresses() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<AddressListResponse> call= RetrofitClient.getInstance().getApi().AddressList(token,deviceId,user_id);
            call.enqueue(new Callback<AddressListResponse>() {
                @Override
                public void onResponse(Call<AddressListResponse> call, retrofit2.Response<AddressListResponse> response) {
                    if (response.isSuccessful());
                    AddressListResponse addressListRespons=response.body();
                    if (addressListRespons.getStatus().equals("10100")){
                        pprogressDialog.dismiss();
                        List<AddressListResponse.DataBean> dataBeanList=addressListRespons.getData();

                        addressesAdapter = new AddressesAdapter(dataBeanList, ChooseAddressActivity.this, R.layout.row_addresses,activity);
                        prev_recyclerview.setLayoutManager(new LinearLayoutManager(ChooseAddressActivity.this, LinearLayoutManager.VERTICAL, false));
                        prev_recyclerview.setItemAnimator(new DefaultItemAnimator());
                        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(ChooseAddressActivity.this, R.drawable.recycler_view_divider));
                        prev_recyclerview.addItemDecoration(dividerItemDecoration);
                        prev_recyclerview.setAdapter(addressesAdapter);
                    }
                    else if (addressListRespons.getStatus().equals("10200")){
                        pprogressDialog.dismiss();
                        Toast.makeText(ChooseAddressActivity.this, addressListRespons.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (addressListRespons.getStatus().equals("10300")){
                        pprogressDialog.dismiss();
                        Toast.makeText(ChooseAddressActivity.this, addressListRespons.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddressListResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(ChooseAddressActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void onClick(View v) {

        if (v == close_img) {

            /*Intent intent = new Intent(ChooseAddressActivity.this, DeliverySlotsActivity.class);
            intent.putExtra("activity", "ChooseAddressActivity");
            startActivity(intent);*/

            if (activity.equalsIgnoreCase("PreOrderCartActivity") || activity.equalsIgnoreCase("PreOrderProductViewActivity")) {
                Intent intent = new Intent(ChooseAddressActivity.this, DeliverySlotsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("activity", "PreOrderCartActivity");
                startActivity(intent);
            }else {
                finish();
                /*Intent intent = new Intent(ChooseAddressActivity.this, DeliverySlotsActivity.class);
                //intent.putExtra("activity", "ChooseAddressActivity");
                intent.putExtra("activity", "ChooseAddressAdapter");
                startActivity(intent);*/
            }
        }



        if (v == add_new_address_btn)
        {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet)
            {
                Intent intent = new Intent(ChooseAddressActivity.this, TestActivity.class);
                //intent.putExtra("activity","ChooseAddress");
                intent.putExtra("activity",activity);
                startActivity(intent);

            }
            else
            {
                Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
            }

        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (activity.equalsIgnoreCase("PreOrderCartActivity") || activity.equalsIgnoreCase("PreOrderProductViewActivity")) {
            Intent intent = new Intent(ChooseAddressActivity.this, DeliverySlotsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("activity", "PreOrderCartActivity");
            startActivity(intent);
        }else {

            finish();
            /*Intent intent = new Intent(ChooseAddressActivity.this, DeliverySlotsActivity.class);
            //intent.putExtra("activity", "ChooseAddressActivity");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("activity", "ChooseAddressAdapter");
            startActivity(intent);*/
        }
    }


    public void setAreaName(String areaName, String area_id) {

        dialog4.dismiss();
        areasAdapter.notifyDataSetChanged();
        sendAreaName = areaName;
        areaId = area_id;
        area_edt.setText(sendAreaName);
        Log.d("COUNTRYDETAIL:", sendCountryName + "::::" + countryId);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {


        if (checkedId == R.id.yes_rb) {
            yes_rb.setChecked(true);
            send_default_value = "Yes";
        } else {
            no_rb.setChecked(true);
            send_default_value = "No";
        }
    }

    public boolean validate()
    {
        boolean result = true;

        String name = name_edt.getText().toString();
        if (name.isEmpty() || name.equals("") || name.equals(null)) {
            name_til.setError("Please Enter Name");
            result = false;
        } else {
          //  name_edt.setError("Please Enter Name");
        }

        String addline_one = address_one_edt.getText().toString();
        if (addline_one.isEmpty() || addline_one.equals("") || addline_one.equals(null)) {
            address_one_til.setError("Please Enter Address 1");
            result = false;
        } else {
         //   address_one_edt.setError("Please Enter Address 1");
        }

        String addline_two = address_two_edt.getText().toString();
        if (addline_two.isEmpty() || addline_two.equals("") || addline_two.equals(null)) {
            address_two_til.setError("Please Enter Address 2");
            result = false;
        } else {
           // address_two_edt.setError("Please Enter Address 2");
        }

        String area = area_edt.getText().toString();
        if (area.isEmpty() || area.equals("") || area.equals(null)) {
            area_til.setError("Please Enter Area");
            result = false;
        } else {
          //  area_edt.setError("Please Enter Area");
        }

        String city = city_edt.getText().toString();
        if (city.isEmpty() || city.equals("") || city.equals(null)) {
            city_til.setError("Please Enter City");
            result = false;
        } else {
           // city_edt.setError("Please Enter City");
        }

        String state = state_edt.getText().toString();
        if (state.isEmpty() || state.equals("") || state.equals(null)) {
            state_til.setError("Please Enter State");
            result = false;
        } else {
           // state_edt.setError("Please Enter State");
        }

        String country = country_edt.getText().toString();
        if (country.isEmpty() || country.equals("") || country.equals(null)) {
            country_til.setError("Please Enter Country");
            result = false;
        } else {
          //  country_edt.setError("Please Enter Country");
        }

        String pincode = pincode_edt.getText().toString();
        if (pincode.isEmpty() || pincode.equals("") || pincode.equals(null)|| pincode.length() != 6) {
            pincode_til.setError("Please Enter Pincode");
            result = false;
        } else {
          //  pincode_edt.setError("Please Enter Pincode");
        }

        String mobile = mobile_edt.getText().toString();
        if (mobile.isEmpty() || mobile.equals("") || mobile.equals(null)) {
            mobile_til.setError("Please Enter Mobile");
            result = false;
        } else {
           // mobile_edt.setError("Please Enter Mobile");
        }
        return result;
    }
}
