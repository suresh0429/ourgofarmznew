package in.innasoft.gofarmz.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.victor.loading.rotate.RotateLoading;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.WebAppInterface;

public class PaymentWebViewActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img;
    TextView tittle_txt;
    WebView pay_url_web;
    RotateLoading progress_indicator;
    String payurl;
    int Order_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_web_view);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        payurl = getIntent().getStringExtra("payurl");

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        Order_id = getIntent().getIntExtra("ORDERID",0);
        Log.d("OSPSO", payurl + "////" + Order_id);
        tittle_txt = findViewById(R.id.tittle_txt);


        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();


        pay_url_web = findViewById(R.id.pay_url_web);
        pay_url_web.setInitialScale(1);
        pay_url_web.getSettings().setJavaScriptEnabled(true);
        pay_url_web.getSettings().setBuiltInZoomControls(true);
        pay_url_web.getSettings().setLoadWithOverviewMode(true);
        pay_url_web.getSettings().setUseWideViewPort(true);
        pay_url_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        pay_url_web.setScrollbarFadingEnabled(false);

        pay_url_web.setWebViewClient(new MyBrowser());
        pay_url_web.loadUrl(payurl);



    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            view.addJavascriptInterface(new Object()
            {
                @JavascriptInterface
                public void performClick(String status)
                {
                    // Deal with a click on the OK button
                    //    Toast.makeText(mContext,"Payment Success",2000).show();
                    //  Log.e("status payment-------->",status);
                    Intent order_detail = new Intent(PaymentWebViewActivity.this, OrderHistoryDetailActivity.class);
                    order_detail.putExtra("ORDERID", Order_id);
                    startActivity(order_detail);
                    finish();

                }
            }, "ok");
            return true;
        }




    }


    @Override
    public void onClick(View v) {

        if (v == close_img) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentWebViewActivity.this);
            alertDialog.setTitle("Do you want to cancel payment?");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent order_detail = new Intent(PaymentWebViewActivity.this, MainActivity.class);
                    startActivity(order_detail);
                    dialog.cancel();
                }
            });

            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            alertDialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentWebViewActivity.this);
        alertDialog.setTitle("Do you want to cancel payment?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent order_detail = new Intent(PaymentWebViewActivity.this, MainActivity.class);
                startActivity(order_detail);
                dialog.cancel();
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }
}
