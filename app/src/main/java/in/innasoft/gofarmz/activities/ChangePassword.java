package in.innasoft.gofarmz.activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.victor.loading.rotate.RotateLoading;

import java.util.HashMap;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class ChangePassword extends AppCompatActivity implements View.OnClickListener {

    EditText old_password_edt,new_password_edt,conf_password_edt;
    Button submit_btn;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId,user_id,token,old_pass,new_pass,confrm_pass;
    RotateLoading progress_indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA",deviceId+"/n"+user_id+"/n"+token);


        progress_indicator = findViewById(R.id.progress_indicator);

        old_password_edt = findViewById(R.id.old_password_edt);

        new_password_edt = findViewById(R.id.new_password_edt);

        conf_password_edt = findViewById(R.id.conf_password_edt);

        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {

        if (v == submit_btn)
        {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {

                    Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userChangePassword(token,deviceId,user_id,old_password_edt.getText().toString(),new_password_edt.getText().toString());

                    call.enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {

                            progress_indicator.stop();
                            BaseResponse baseResponse = response.body();

                            if (response.isSuccessful()) {

                                if (baseResponse.getStatus().equalsIgnoreCase("10100")) {

                                    Toast.makeText(ChangePassword.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(ChangePassword.this, MainActivity.class);
                                    startActivity(intent);

                                } else if (baseResponse.getStatus().equals("10200")){

                                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                                else if (baseResponse.getStatus().equals("10300")){

                                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                                else if (baseResponse.getStatus().equals("10400")){

                                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            progress_indicator.stop();
                        }
                    });


                } else {

                   /* Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                     snackbar.show();*/
                    Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }








    private boolean validate()
    {

        boolean result = true;
        int flag = 0;

        String strCurrentPassword = old_password_edt.getText().toString();

        if (strCurrentPassword == null || strCurrentPassword.equals("") ||strCurrentPassword.length()==0)
        {
            old_password_edt.setError("Please enter old password");
            result = false;
        }


        String strNewPassword = new_password_edt.getText().toString();

        if (strNewPassword == null || strNewPassword.equals("") || strNewPassword.matches("^-\\s")||strNewPassword.length()==0)
        {
            new_password_edt.setError("Please enter new password");
            result = false;

        }

        String strConformPassword = conf_password_edt.getText().toString();

        if (strConformPassword == null || strConformPassword.equals("")|| strConformPassword.matches("^-\\s") ||strConformPassword.length()==0)
        {
            conf_password_edt.setError("Please enter confirm password");
            result = false;

        }

     /*  if(strNewPassword != "" && strConformPassword != "" &&!strConformPassword.equals(strNewPassword) && flag == 0 )
        {
            new_password_edt.setError("New Password and Confirm password mismatch");
            conf_password_edt.setError("New Password and Confirm password mismatch");
            result = false;
        }
       else {

           new_password_edt.setError("New Password and Confirm password mismatch");
           conf_password_edt.setError("New Password and Confirm password mismatch");
       }*/

        return result;

    }
}
