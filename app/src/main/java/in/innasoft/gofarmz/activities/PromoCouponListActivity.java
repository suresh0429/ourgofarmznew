package in.innasoft.gofarmz.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.PromoCodeResponse;
import in.innasoft.gofarmz.Response.ValidateCoupenResponse;
import in.innasoft.gofarmz.adapters.PromoCodeAdapter;
import in.innasoft.gofarmz.models.PromoCodeModel;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class PromoCouponListActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "PromoCouponListActivity";
    TextView done_text, offer_text, toolbar_title;
    ImageView close_img;
    Button applyCoupanyButton;
    EditText edt_coupan_code;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String deviceId, token, user_id, addressid, slotId;
    RecyclerView promocode_recyclerview;
    PromoCodeAdapter promoCodeAdapter;
    ArrayList<PromoCodeModel> promoCodeList;
    LinearLayoutManager layoutManager;
    Double final_amount,payAmount;
    String resultAmount;

    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dummy_activity_promo_coupon_list);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        checkInternet = NetworkChecking.isConnected(this);

        Bundle bundle = getIntent().getExtras();
        final_amount = bundle.getDouble("FINAL_AMOUNT");
        addressid = bundle.getString("ADD_ID");
        slotId = bundle.getString("SLOT_ID");

        promocode_recyclerview = findViewById(R.id.promocode_recyclerview);
        pprogressDialog = new ProgressDialog(PromoCouponListActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);


        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        toolbar_title = findViewById(R.id.toolbar_title);
        done_text = findViewById(R.id.done_text);

        done_text.setOnClickListener(this);
        offer_text = findViewById(R.id.offer_text);


        edt_coupan_code = findViewById(R.id.edt_coupan_code);
        applyCoupanyButton = findViewById(R.id.applyCoupanyButton);
        applyCoupanyButton.setOnClickListener(this);
        getPromoCodes();
    }

    public void getPromoCodes() {
        if (checkInternet) {

            Call<PromoCodeResponse> call = RetrofitClient.getInstance().getApi().getPromoCodes(token,deviceId);
            call.enqueue(new Callback<PromoCodeResponse>() {
                @Override
                public void onResponse(Call<PromoCodeResponse> call, retrofit2.Response<PromoCodeResponse> response) {
                    PromoCodeResponse promoCodeResponse = response.body();

                    if (promoCodeResponse.getStatus().equals("10100")) {

                        pprogressDialog.dismiss();

                        List<PromoCodeResponse.DataBean> dataBeanList = promoCodeResponse.getData();

                        promoCodeAdapter = new PromoCodeAdapter(dataBeanList, PromoCouponListActivity.this, R.layout.dummy_row_promo_codes);
                        layoutManager = new LinearLayoutManager(getApplicationContext());
                        promocode_recyclerview.setNestedScrollingEnabled(false);
                        promocode_recyclerview.setLayoutManager(layoutManager);
                        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(PromoCouponListActivity.this, R.drawable.recycler_view_divider));
                        promocode_recyclerview.addItemDecoration(dividerItemDecoration);

                        promocode_recyclerview.setAdapter(promoCodeAdapter);

                    } else if (promoCodeResponse.getStatus().equals("10200")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(PromoCouponListActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                    }
                    else if (promoCodeResponse.getStatus().equals("10300")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(PromoCouponListActivity.this, "No Coupon Found..!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PromoCodeResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                }
            });

        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onClick(View v) {
        if (v == applyCoupanyButton) {
            String selected_code = promoCodeAdapter.send_code_Value;
            Log.d("CDDCDCD", selected_code);
            validateCouponCode(selected_code);
        }
        if (v == close_img) {
            Intent intent = new Intent(PromoCouponListActivity.this, PaymentDetails.class);
            intent.putExtra("activity", "PromoCouponListActivity");
            intent.putExtra("promoStatus", "0");
            intent.putExtra("ADD_ID", addressid);
            intent.putExtra("SLOT_ID", slotId);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        if (v == done_text) {
            String selected_code = edt_coupan_code.getText().toString();
            Log.d("DOOODONE", selected_code);
            validateCouponCode(selected_code);
        }
    }

    public void validateCouponCode(final String coupon_code) {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {


            Call<ValidateCoupenResponse> call = RetrofitClient.getInstance().getApi().validCoupenCode(token,deviceId,user_id,user_id,coupon_code, String.valueOf(final_amount));
            call.enqueue(new Callback<ValidateCoupenResponse>() {
                @Override
                public void onResponse(Call<ValidateCoupenResponse> call, retrofit2.Response<ValidateCoupenResponse> response) {
                    ValidateCoupenResponse promoCodeResponse = response.body();

                    if (promoCodeResponse.getStatus().equals("10100")) {

                        pprogressDialog.dismiss();

                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", coupon_code);
                        returnIntent.putExtra("applied_amt", String.valueOf(promoCodeResponse.getData()));
                        PromoCouponListActivity.this.setResult(Activity.RESULT_OK, returnIntent);
                        PromoCouponListActivity.this.finish();

                    } else if (promoCodeResponse.getStatus().equals("10200")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(PromoCouponListActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                    }
                    else if (promoCodeResponse.getStatus().equals("10300")) {
                        pprogressDialog.dismiss();
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result","2");

                        PromoCouponListActivity.this.setResult(Activity.RESULT_OK, returnIntent);
                        PromoCouponListActivity.this.finish();
                        Toast.makeText(PromoCouponListActivity.this, "No Coupon Found..!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ValidateCoupenResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Log.d("MESSAGE", "onFailure: "+t.getMessage()+" "+t.toString());
                }
            });



        } else {
            Toast.makeText(PromoCouponListActivity.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PromoCouponListActivity.this, PaymentDetails.class);
        intent.putExtra("activity", "PromoCouponListActivity");
        intent.putExtra("promoStatus", "0");
        intent.putExtra("ADD_ID", addressid);
        intent.putExtra("SLOT_ID", slotId);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void setCode(String code) {
        edt_coupan_code.setText(code);


    }

}
