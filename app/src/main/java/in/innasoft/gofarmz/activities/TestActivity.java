package in.innasoft.gofarmz.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.victor.loading.rotate.RotateLoading;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.AddAddressResponse;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class TestActivity extends FragmentActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener,OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private static String TAG = TestActivity.class.getSimpleName();
    private static final int AUTOCOMPLETE_REQUEST_CODE = 22;
    UserSessionManager session;
    private boolean checkInternet;
    SupportMapFragment mapFragment;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private GoogleMap mMap;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private TextView save_button_txt,textSearch,txtArea,txtaddressline2;
    EditText name_edt,address_1_edt,address_2_edt,area_edt,city_edt,state_edt,country_edt,pincode_edt,mobile_edt,alternate_mob_edt;
    TextInputLayout name_til,address_one_til,address_two_til,pincode_til,mobile_til,alt_mobile_til,area_til,city_til,state_til,country_til;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    private Circle mCircle;
    double radiusInMeters = 100.0,latitute,longitiu;
    int strokeColor = 0xffff0000; //Color Code you want
    int shadeColor = 0x44ff0000;
    String deviceId, user_id, token, user_name,mobile,send_default_value;
    RadioGroup rg_default;
    RadioButton yes_rb, no_rb;
    RotateLoading progress_indicator;
    List<Address> addresses;
    Geocoder geocoder;
    String latitude1, longitude1,activity;
    ProgressDialog pprogressDialog;

    PlacesClient placesClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // places sdk
        String apiKey = getString(R.string.googleapikey);

        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        placesClient = Places.createClient(this);



        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("MMMMMM",mobile);

        pprogressDialog = new ProgressDialog(TestActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        activity = bundle.getString("activity");

        Log.d("TestActivity",activity);

        txtaddressline2 =  findViewById(R.id.txtaddressline2);
        txtArea =  findViewById(R.id.txtArea);
        name_edt =  findViewById(R.id.name_edt);
        address_1_edt =  findViewById(R.id.address_1_edt);
        address_2_edt =  findViewById(R.id.address_2_edt);
        area_edt =  findViewById(R.id.area_edt);
        city_edt =  findViewById(R.id.city_edt);
        state_edt =  findViewById(R.id.state_edt);
        country_edt =  findViewById(R.id.country_edt);
        pincode_edt =  findViewById(R.id.pincode_edt);
        mobile_edt =  findViewById(R.id.mobile_edt);
        alternate_mob_edt =  findViewById(R.id.alternate_mob_edt);
        save_button_txt =  findViewById(R.id.save_button_txt);
        save_button_txt.setOnClickListener(this);

       textSearch =  findViewById(R.id.textSearch);
       textSearch.setOnClickListener(this);

        geocoder = new Geocoder(this, Locale.getDefault());
        name_til = findViewById(R.id.name_til);
        address_one_til = findViewById(R.id.address_one_til);
        address_two_til = findViewById(R.id.address_two_til);
        pincode_til = findViewById(R.id.pincode_til);
        mobile_til = findViewById(R.id.mobile_til);
        alt_mobile_til = findViewById(R.id.alt_mobile_til);
        area_til = findViewById(R.id.area_til);
        city_til = findViewById(R.id.city_til);
        state_til = findViewById(R.id.state_til);
        country_til = findViewById(R.id.country_til);

        rg_default = findViewById(R.id.rg_default);
        rg_default.setOnCheckedChangeListener(this);
        yes_rb = findViewById(R.id.yes_rb);
        no_rb = findViewById(R.id.no_rb);
        send_default_value = "Yes";

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        configureCameraIdle();






    }


    private void configureCameraIdle()
    {
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener()
        {
            @Override
            public void onCameraIdle() {

                LatLng latLng = mMap.getCameraPosition().target;


                try {
                    Geocoder geocoder = new Geocoder(TestActivity.this);
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0)
                    {
                        Address address = addressList.get(0);
                        String locality = addressList.get(0).getAddressLine(0);  //whole address coming in lovality
                        String addr[] = locality.split(",", 2);
                        Log.d("LOClity",""+address);
                        Log.d("ARYA",addr[0]+"////"+addr[1]);


                        String country = addressList.get(0).getCountryName();
                        String state = addressList.get(0).getAdminArea();
                        String city = addressList.get(0).getLocality();
                        String area = addressList.get(0).getSubLocality();
                        String pincode = addressList.get(0).getPostalCode();
                        latitute=addressList.get(0).getLatitude();
                        longitiu=addressList.get(0).getLongitude();
                        if (!locality.isEmpty() && !country.isEmpty())

                            if (area != null){
                                txtArea.setText(area);
                            }
                        else {
                                txtArea.setText("Select Location");
                            }

                            txtaddressline2.setText(addr[1]);
                            name_edt.setText(user_name);
                            //address_1_edt.setText(addr[0]);
                            address_2_edt.setText(addr[1]);
                            area_edt.setText(area);
                            city_edt.setText(city);
                            state_edt.setText(state);
                            country_edt.setText(country);
                            pincode_edt.setText(pincode);

                            if(mobile.equals("null") || mobile.equals(""))
                            {

                                mobile_edt.setText("");
                            }
                            else
                            {

                                mobile_edt.setText(mobile);
                            }


                    }



                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }


    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED)
            {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
            else {
                //Request Location Permission
                //checkLocationPermission();
            }
        }
        else
            {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnCameraIdleListener(onCameraIdleListener);

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location)
    {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
      //  markerOptions.title("Current Position");
       // markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
       // mCurrLocationMarker = mMap.addMarker(markerOptions);

      //  CircleOptions addCircle = new CircleOptions().center(latLng).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
      //  mCircle = mMap.addCircle(addCircle);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(19));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

   /* protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }*/

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {


        if (checkedId == R.id.yes_rb) {
            yes_rb.setChecked(true);
            send_default_value = "Yes";
        } else {
            no_rb.setChecked(true);
            send_default_value = "No";
        }
    }

    @Override
    public void onClick(View v)
    {
        if(v==textSearch)
        {
            /*try {
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                // TODO: Handle the error.
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }*/

            onSearchCalled();
        }

      if(v==save_button_txt)
      {
          checkInternet = NetworkChecking.isConnected(this);
          if (checkInternet)
          {
                if(validate())
                {
                    pprogressDialog.show();

                    Call<AddAddressResponse> call= RetrofitClient.getInstance().getApi().AddAddress(token,deviceId,user_id, name_edt.getText().toString(),
                            address_1_edt.getText().toString(),address_2_edt.getText().toString(),area_edt.getText().toString(),city_edt.getText().toString(),
                            state_edt.getText().toString(),pincode_edt.getText().toString(), mobile_edt.getText().toString(), alternate_mob_edt.getText().toString(),
                            send_default_value,String.valueOf(latitute),String.valueOf(longitiu));
                    call.enqueue(new Callback<AddAddressResponse>() {
                        @Override
                        public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                            if (response.isSuccessful());
                            AddAddressResponse addAddressResponse=response.body();
                            if (addAddressResponse.getStatus().equals("10100")){
                                pprogressDialog.dismiss();
                                Toast.makeText(TestActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                assert activity != null;
                                if (activity.equalsIgnoreCase("ChooseAddressAdapter")) {
                                    Intent intent = new Intent(TestActivity.this, DeliverySlotsActivity.class);
                                    intent.putExtra("activity", "Order");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }

                                if (activity.equalsIgnoreCase("PreOrderCartActivity")) {

                                    Intent intent = new Intent(TestActivity.this, DeliverySlotsActivity.class);
                                    intent.putExtra("activity", "PreOrderCartActivity");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                }

                                if (activity.equalsIgnoreCase("MyCartActivity")) {

                                    Intent intent = new Intent(TestActivity.this, DeliverySlotsActivity.class);
                                    intent.putExtra("activity", "MyCartActivity");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                }

                                if (activity.equalsIgnoreCase("ChooseAddress")) {

                                    Intent intent = new Intent(TestActivity.this, ChooseAddressActivity.class);
                                    intent.putExtra("activity", "ChooseAddress");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                }
                            }
                            else if (addAddressResponse.getStatus().equals("10200")){
                                pprogressDialog.dismiss();
                                Toast.makeText(TestActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            else if (addAddressResponse.getStatus().equals("10300")){
                                pprogressDialog.dismiss();
                                Toast.makeText(TestActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            else if (addAddressResponse.getStatus().equals("10400")){
                                pprogressDialog.dismiss();
                                Toast.makeText(TestActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            else if (addAddressResponse.getStatus().equals("10500")){
                                pprogressDialog.dismiss();
                                Toast.makeText(TestActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                            pprogressDialog.dismiss();
                            Toast.makeText(TestActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
          }
          else
          {
              Toast.makeText(this, "Check your Internet Connection..", Toast.LENGTH_SHORT).show();

          }
      }
    }

    public void onSearchCalled() {
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN") //INDIA
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() + ", " + place.getAddress());
                // Toast.makeText(TestActivity.this, "ID: " + place.getId() + "address:" + place.getAddress() + "Name:" + place.getName() + " latlong: " + place.getLatLng(), Toast.LENGTH_LONG).show();
                // String address = place.getAddress();
                String stringlat = place.getLatLng().toString();
                CharSequence name = place.getName();
                final CharSequence address = place.getAddress();

                if (name.toString().contains("°")) {
                    name = "";
                }
                stringlat = stringlat.substring(stringlat.indexOf("(") + 1);
                stringlat = stringlat.substring(0, stringlat.indexOf(")"));
                String latValue = stringlat.split(",")[0];
                latitude1 = latValue;
                String lngValue = stringlat.split(",")[1];
                longitude1 = lngValue;

                Double lat = Double.valueOf(latitude1);
                Double lng = Double.valueOf(longitude1);
                String fulladd = address.toString();
                String addr[] = fulladd.split(",", 2);
                /*if (name != null && name.length() > 0)
                    address_1_edt.setText(name);
                else
                    address_1_edt.setText(addr[0]);
*/

                //area_edt.setText(addr[5]+","+addr[6]+","+addr[7]);
                ////#2-56/2/19, 3rd Floor, Vijaya Towers, near Meridian School,, Ayyappa Society 100ft Road, Madhapur, Ayyappa Society, Chanda Naik Nagar, Madhapur, Hyderabad, Telangana 500081, India
                try {
                    addresses = geocoder.getFromLocation(lat, lng, 1);
                    //    save_btn.setEnabled(true);
                    Log.d("LOXXX", addresses.get(0).getAddressLine(0));
                    String addLine = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    Log.d(TAG, "onActivityResult: "+city);
                    if (city != null){
                        city_edt.setText(city);
                    }

                    String area = addresses.get(0).getSubLocality();
                    area_edt.setText(area);
                    String state = addresses.get(0).getAdminArea();
                    state_edt.setText(state);
                    String country = addresses.get(0).getCountryName();
                    country_edt.setText(country);

                    String postalCode = addresses.get(0).getPostalCode();
                    pincode_edt.setText(postalCode);
                    String addLineTwo = addresses.get(0).getThoroughfare();
                    // address_two_edt.setText(addLineTwo);
                    String addLineOne = addresses.get(0).getFeatureName();
                    // address_one_edt.setText(addLineOne);

                    if (addr[1].contains(postalCode)) {
                        addr[1] = addr[1].replace(postalCode, " ");
                    }
                    if (addr[1].contains(state)) {
                        addr[1] = addr[1].replace(state, " ");
                    }
                    if (addr[1].contains(country)) {
                        addr[1] = addr[1].replace(country, " ");
                    }

                    if (city != null) {
                        if (addr[1].contains(city)) {
                            addr[1] = addr[1].replace(city, " ");
                        }
                    }
                    if (addr[1] != null && area != null && addr[1].contains(area)) {
                        addr[1] = addr[1].replace(area, " ");
                    }

                    txtArea.setText(area);
                    txtaddressline2.setText(addr[1]);


                    addr[1] = new LinkedHashSet<String>(Arrays.asList(addr[1].split("\\s"))).toString().replaceAll("[\\[\\],]", "");
                    address_2_edt.setText(addr[1]);
                    name_edt.setText(user_name);
                    if (!mobile.equalsIgnoreCase("null"))
                        mobile_edt.setText(mobile);

                    Log.d("ADDRESSES", addLine + "\n" + city + "\n" + state + "\n" + country + "\n" + postalCode + "\n" + address_1_edt);
                    Log.d("REMAININGADDRESS", addresses.get(0).getSubAdminArea() + "\n" + addresses.get(0).getSubLocality() + "\n" + addresses.get(0).getPremises() + "\n" + addresses.get(0).getSubThoroughfare()
                            + "\n" + addresses.get(0).getThoroughfare());
                    Log.v("FFFFFFF", "//" + address.toString());
                    //address_txt.setText(address);
                    // finaladdress = address.toString();
                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }
                    LatLng latLng = new LatLng(Double.parseDouble(latitude1), Double.parseDouble(longitude1));
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    //  markerOptions.title("Current Position");
                    //  markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                    //    mCurrLocationMarker = mMap.addMarker(markerOptions);

                    //  CircleOptions addCircle = new CircleOptions().center(latLng).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
                    //   mCircle = mMap.addCircle(addCircle);

                    //move map camera
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(19));

                    //stop location updates
                    if (mGoogleApiClient != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Toast.makeText(TestActivity.this, "Error: " + status.getStatusMessage(), Toast.LENGTH_LONG).show();
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }




    public boolean validate()
    {
        boolean result = true;

        String name = name_edt.getText().toString();
        if (name.isEmpty() || name.equals("") || name.equals(null)) {
            name_til.setError("Please Enter Name");
            result = false;
        } else {
            //  name_edt.setError("Please Enter Name");
        }

        String addline_one = address_1_edt.getText().toString();
        if (addline_one.isEmpty() || addline_one.equals("") || addline_one.equals(null)) {
            address_one_til.setError("Please Enter HOUSE/FLAT/ BLOCK NO.");
            result = false;
        } else {
            //   address_one_edt.setError("Please Enter Address 1");
        }

        String addline_two = address_2_edt.getText().toString();
        if (addline_two.isEmpty() || addline_two.equals("") || addline_two.equals(null)) {
            area_til.setError("Please Enter AREA/LANDMARK");
            result = false;
        } else {
            // address_two_edt.setError("Please Enter Address 2");
        }

        String area = area_edt.getText().toString();
        if (area.isEmpty() || area.equals("") || area.equals(null)) {
            area_til.setError("Please Enter Area");
            result = false;
        } else {
            //  area_edt.setError("Please Enter Area");
        }

        String city = city_edt.getText().toString();
        if (city.isEmpty() || city.equals("") || city.equals(null)) {
            city_til.setError("Please Enter City");
            result = false;
        } else {
            // city_edt.setError("Please Enter City");
        }

        String state = state_edt.getText().toString();
        if (state.isEmpty() || state.equals("") || state.equals(null)) {
            state_til.setError("Please Enter State");
            result = false;
        } else {
            // state_edt.setError("Please Enter State");
        }

        String country = country_edt.getText().toString();
        if (country.isEmpty() || country.equals("") || country.equals(null)) {
            country_til.setError("Please Enter Country");
            result = false;
        } else {
            //  country_edt.setError("Please Enter Country");
        }

        String pincode = pincode_edt.getText().toString();
        if (pincode.isEmpty() || pincode.equals("") || pincode.equals(null)|| pincode.length() != 6) {
            pincode_til.setError("Please Enter Pincode");
            result = false;
        } else {
            //  pincode_edt.setError("Please Enter Pincode");
        }

        String mobile = mobile_edt.getText().toString();
        if (mobile.isEmpty() || mobile.equals("") || mobile.equals(null)) {
            mobile_til.setError("Please Enter Mobile");
            result = false;
        } else {
            // mobile_edt.setError("Please Enter Mobile");
        }
        return result;
    }


}
