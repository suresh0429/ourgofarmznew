package in.innasoft.gofarmz.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.chaos.view.PinView;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.victor.loading.rotate.RotateLoading;

import java.util.HashMap;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    TextView otp_txt, txtresend;
    PinView pinView;
    EditText password_edt;
    Button verify_btn;
    private boolean checkInternet;
    String mobile, deviceId, send_mobile,senPin;
    UserSessionManager userSessionManager;
    RotateLoading progress_indicator;
    Dialog dialog;
    private Boolean exit = false;
    SmsVerifyCatcher smsVerifyCatcher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        mobile = bundle.getString("mobile"); // need to check

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();

        password_edt = findViewById(R.id.password_edt);
        otp_txt = findViewById(R.id.otp_txt);

        txtresend = findViewById(R.id.txtresend);
        txtresend.setPaintFlags(txtresend.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        txtresend.setOnClickListener(this);

        pinView = findViewById(R.id.pinView);


        verify_btn = findViewById(R.id.verify_btn);

        verify_btn.setOnClickListener(this);


        getAlertBox();
        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = message.replaceAll("[^0-9]", "");
                pinView.setText(code);
                Log.v("message >>> ", message.replaceAll("[^0-9]", ""));
            }
        });
    }

    private void getAlertBox()
    {
        dialog = new Dialog(ForgotPassword.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_mobile);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_layout_dialog);
        TextView dialog_title = dialog.findViewById(R.id.dialog_title);
        final EditText edt_send_mobile = dialog.findViewById(R.id.edt_send_mobile);
        final TextView sendmobileText =  dialog.findViewById(R.id.sendmobileText);
        final ImageView close_dialog =  dialog.findViewById(R.id.close_dialog);


        dialog.setCanceledOnTouchOutside(false);
        sendmobileText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.d("eererere","tdgssdfertdf");
                sendmobileText.setClickable(false);
                sendmobileText.setFocusableInTouchMode(false);
                 send_mobile=edt_send_mobile.getText().toString();

                if(send_mobile == null || send_mobile.equals("") || send_mobile.length() < 0)
                {
                    edt_send_mobile.setError("Please Enter Mobile No...!");
                }
                else
                {
                    dialog.cancel();
                    Log.d("eererere",send_mobile);
                    getOTP(send_mobile);


                }
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
              dialog.dismiss();
              finish();
            }
        });


        dialog.show();

    }
    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void getOTP(final String send_mobile)
    {
        Log.d("fsdafsdf",send_mobile);
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userForgotPassword(deviceId,send_mobile);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {

                    BaseResponse baseResponse = response.body();
                    if (baseResponse.getStatus().equalsIgnoreCase("10100")) {

                        progress_indicator.stop();
                        Toast.makeText(ForgotPassword.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }else if (baseResponse.getStatus().equals("10200")){
                        progress_indicator.stop();
                        Toast.makeText(ForgotPassword.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();                    }
                    else if (baseResponse.getStatus().equals("10300")){
                        progress_indicator.stop();
                        Toast.makeText(ForgotPassword.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();                    }
                    else if (baseResponse.getStatus().equals("10400")){
                        progress_indicator.stop();
                        Toast.makeText(ForgotPassword.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();                    } else {

                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    progress_indicator.stop();

                }
            });

        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {

        if (v == verify_btn) {
            verifyOtp();
        }

        if (v == txtresend)
        {

          //  getOTP();
        }
    }

    private boolean validate() {
        boolean result = true;
        String otp = pinView.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            pinView.setError("Invalid OTP");
            result = false;
        }
        return result;
    }

    public void verifyOtp() {
        checkInternet = NetworkChecking.isConnected(this);
        if (validate()) {

            if (checkInternet) {//716504

                Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userVerifyForgotPassword(deviceId,send_mobile,pinView.getText().toString(),deviceId,password_edt.getText().toString());

                call.enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                        BaseResponse baseResponse = response.body();
                        if (baseResponse.getStatus().equalsIgnoreCase("10100")) {

                            progress_indicator.stop();
                            Intent intent = new Intent(ForgotPassword.this, LoginActivity.class);
                            startActivity(intent);
                            Toast.makeText(ForgotPassword.this,"Password Reset Succesfully..!", Toast.LENGTH_SHORT).show();


                        }else if (baseResponse.getStatus().equals("10200")){
                            progress_indicator.stop();
                            Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        else if (baseResponse.getStatus().equals("10300")){
                            progress_indicator.stop();
                            Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        else if (baseResponse.getStatus().equals("10400")){
                            progress_indicator.stop();
                            Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        progress_indicator.stop();

                    }
                });


            } else {
                Toast.makeText(ForgotPassword.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }



}
