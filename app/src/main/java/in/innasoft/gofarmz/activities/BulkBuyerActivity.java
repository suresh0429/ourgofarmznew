package in.innasoft.gofarmz.activities;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.HomeItem;
import in.innasoft.gofarmz.adapters.Adapter;

public class BulkBuyerActivity extends AppCompatActivity {
    @BindView(R.id.homeRecycler)
    RecyclerView homeRecycler;


    Adapter adapter;

    List<HomeItem> homeItemList = new ArrayList<>();
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.tittle_txt)
    TextView tittleTxt;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk_buyer);
        ButterKnife.bind(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        homeRecycler.setLayoutManager(mLayoutManager);
        adapter = new Adapter(homeItemList, this);
        homeRecycler.setAdapter(adapter);

        homeData();


    }

    private void homeData() {
        HomeItem homeItem = new HomeItem("Tamato", "1000", "https://i.ytimg.com/vi/jGGk2FZ5zSs/hqdefault.jpg", "100kgs", "vegitables", "Organic Farming", "Yes","+918985018104","Ramesh","Gandipet",17.3807,78.3245);
        homeItemList.add(homeItem);

        homeItem = new HomeItem("Apple", "3000", "https://ichef.bbci.co.uk/wwfeatures/live/976_549/images/live/p0/7v/2w/p07v2wjn.jpg", "50kgs", "Fruits", "Organic Farming", "Yes","+917898018104","Somesh","Gandipet",17.3807,78.3245);
        homeItemList.add(homeItem);

        homeItem = new HomeItem("Potato", "2000", "https://cdn-prod.medicalnewstoday.com/content/images/articles/280/280579/potatoes-can-be-healthful.jpg", "100kgs", "vegitables", "Organic Farming", "Yes","+918564018104","Rajesh","Gandipet",17.3807,78.3245);
        homeItemList.add(homeItem);

        homeItem = new HomeItem("Onions", "5000", "https://img.etimg.com/thumb/width-640,height-480,imgsize-876498,resizemode-1,msid-67172581/despite-scheme-onions-plunge-to-rs-1-50-in-maharashtra.jpg", "100kgs", "vegitables", "Organic Farming", "Yes","+917895018104","Ramesh","Gandipet",17.3807,78.3245);
        homeItemList.add(homeItem);

        homeItem = new HomeItem("Brinjal", "1000", "https://countercurrents.org/wp-content/uploads/2019/04/Bt-Brinjal.jpg", "100kgs", "vegitables", "Organic Farming", "Yes","+918985018104","Ramesh","Gandipet",17.3807,78.3245);
        homeItemList.add(homeItem);


        homeItem = new HomeItem("Cauliflower", "3000", "https://i0.wp.com/images-prod.healthline.com/hlcmsresource/images/AN_images/cauliflower-thumb.jpg?w=756&h=567", "100kgs", "vegitables", "Organic Farming", "Yes","+918795018104","Ramesh","Gandipet",17.3807,78.3245);
        homeItemList.add(homeItem);


    }

    @OnClick(R.id.close_img)
    public void onViewClicked() {
        finish();
    }
}
