package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import java.util.HashMap;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.ImageNotificationResponse;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class ImageNotificationActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img,prod_imag;
    TextView tittle_txt,msg_tittle,msg_desp;
    RotateLoading progress_indicator;
    Button purchase_btn;

    String DDDD,deviceId, user_id, token;
    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_notification);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        DDDD = getIntent().getStringExtra("GETDATANOTFY");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        progress_indicator = findViewById(R.id.progress_indicator);
        //progress_indicator.start();

        prod_imag = findViewById(R.id.prod_imag);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        tittle_txt = findViewById(R.id.tittle_txt);


        msg_tittle = findViewById(R.id.msg_tittle);


        purchase_btn = findViewById(R.id.purchase_btn);
        purchase_btn.setOnClickListener(this);

        msg_desp = findViewById(R.id.msg_desp);


        getDataDetail();
    }

    public void getDataDetail()
    {

        Call<ImageNotificationResponse> call = RetrofitClient.getInstance().getApi().imageNotification(token,deviceId,user_id,DDDD);
        call.enqueue(new Callback<ImageNotificationResponse>() {
            @Override
            public void onResponse(Call<ImageNotificationResponse> call, retrofit2.Response<ImageNotificationResponse> response) {
                ImageNotificationResponse promoCodeResponse = response.body();

                if (promoCodeResponse.getStatus().equals("10100")) {

                  //  pprogressDialog.dismiss();

                    msg_tittle.setText(promoCodeResponse.getData().getMsgTitle());
                    msg_desp.setText(promoCodeResponse.getData().getMsgTxt());

                    Picasso.with(ImageNotificationActivity.this)
                            .load(promoCodeResponse.getData().getMsgImage())
                            .placeholder(R.drawable.cart)
                            .into(prod_imag);

                }
            }

            @Override
            public void onFailure(Call<ImageNotificationResponse> call, Throwable t) {
              //  pprogressDialog.dismiss();
                Log.d("MESSAGE", "onFailure: "+t.getMessage()+" "+t.toString());
            }
        });


    }

    @Override
    public void onClick(View v)
    {

        if (v == close_img) {

            Intent intent = new Intent(ImageNotificationActivity.this, NotificationsActivity.class);
            startActivity(intent);
        }
        if (v == purchase_btn)
        {

        }
    }

}
