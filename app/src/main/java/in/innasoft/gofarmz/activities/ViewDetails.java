package in.innasoft.gofarmz.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.ViewDetailsResponse;
import in.innasoft.gofarmz.adapters.CartRecordChild;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class ViewDetails extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView allsponsors_toolbar_title;
    ListView productlist;
    CartRecordChild adapter;
    ArrayList<Object> detailsarr = new ArrayList<Object>();
    String type;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, idd, price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_details);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(ViewDetails.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        close = findViewById(R.id.close_img);
        close.setOnClickListener(this);
        allsponsors_toolbar_title = findViewById(R.id.allsponsors_toolbar_title);

        productlist = findViewById(R.id.listRecordChild);

        type = (String) getIntent().getSerializableExtra("TYPE");
        idd = (String) getIntent().getSerializableExtra("IDD");
        Log.d("LLL", type + "---" + idd);

        getViewDetailChild();

    }

    public void getViewDetailChild() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            Call<ViewDetailsResponse> call= RetrofitClient.getInstance().getApi().ViewDetailsData(token,user_id,deviceId,idd);
            call.enqueue(new Callback<ViewDetailsResponse>() {
                @Override
                public void onResponse(Call<ViewDetailsResponse> call, retrofit2.Response<ViewDetailsResponse> response) {
                    if (response.isSuccessful());
                    ViewDetailsResponse viewDetailsResponse=response.body();
                    if (viewDetailsResponse.getStatus().equals("10100")){
                        ViewDetailsResponse.DataBean dataBean=viewDetailsResponse.getData();
                        List<ViewDetailsResponse.DataBean.RecordsBean> recordsBeanList=dataBean.getRecords();

                        adapter = new CartRecordChild(ViewDetails.this, recordsBeanList, type);
                        productlist.setAdapter(adapter);

                    }
                    else if (viewDetailsResponse.getStatus().equals("10200")){
                        Toast.makeText(ViewDetails.this, viewDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ViewDetailsResponse> call, Throwable t) {
                    Toast.makeText(ViewDetails.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }
}
