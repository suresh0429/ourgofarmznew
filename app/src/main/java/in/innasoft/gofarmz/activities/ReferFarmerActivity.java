package in.innasoft.gofarmz.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.StateResponse;
import in.innasoft.gofarmz.adapters.CityCustomAdapter;
import in.innasoft.gofarmz.adapters.StateCustomAdapter;
import in.innasoft.gofarmz.models.CitiesModel;
import in.innasoft.gofarmz.models.StatesModel;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;


public class ReferFarmerActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener
{
    TextView tittle_txt,note_text_title,refer_descrption;
    ImageView close_img;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, send_farmer_name, send_farmer_mobile, send_farmer_state,send_farmer_location,sendState,sendCity;
    private boolean checkInternet;
    Button submit_btn;
    EditText farmer_name,farmer_mobile_edt,farmer_state,farmer_location;
    String MOBILE_REGEX = "^[6789]\\d{9}$";
    Spinner spinfarmer_state,spinfarmer_city;

    StateCustomAdapter customAdapter;

    ArrayList<StatesModel> statesModels = new ArrayList<StatesModel>();
    ArrayList<String> statesList = new ArrayList<String>();

    /*cities*/
    CityCustomAdapter citiesAdapter;
    ArrayList<CitiesModel> citiesModels = new ArrayList<CitiesModel>();
    ArrayList<String> citiesList = new ArrayList<String>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_farmer);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        checkInternet = NetworkChecking.isConnected(this);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        tittle_txt = findViewById(R.id.tittle_txt);

        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);

        refer_descrption = findViewById(R.id.refer_descrption);
        note_text_title = findViewById(R.id.note_text_title);

        SpannableString content = new SpannableString("Note :");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        note_text_title.setText(content);

        farmer_name = findViewById(R.id.farmer_name);
        farmer_mobile_edt = findViewById(R.id.farmer_mobile_edt);
        farmer_state = findViewById(R.id.farmer_state);
        farmer_location = findViewById(R.id.farmer_location);
        spinfarmer_state = findViewById(R.id.spinfarmer_state);
        spinfarmer_city = findViewById(R.id.spinfarmer_city);
        spinfarmer_state.setOnItemSelectedListener(this);
        spinfarmer_city.setOnItemSelectedListener(this);

        getStateList();


    }

    private void getStateList()
    {
        statesModels.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {


            Call<StateResponse> call = RetrofitClient.getInstance().getApi().stateList(token,"1");
            call.enqueue(new Callback<StateResponse>() {
                @Override
                public void onResponse(Call<StateResponse> call, retrofit2.Response<StateResponse> response) {
                    StateResponse baseResponse = response.body();
                    if (baseResponse.getStatus().equals("10100")) {

                        List<StateResponse.DataBean> dataBeanList = baseResponse.getData();

                        for (StateResponse.DataBean dataBean : dataBeanList){

                            StatesModel sm = new StatesModel();
                            sm.setId(dataBean.getId());
                            sm.setName(dataBean.getName());

                            statesList.add(dataBean.getName());
                            statesModels.add(sm);
                        }

                        customAdapter=new StateCustomAdapter(ReferFarmerActivity.this,R.layout.row_states_list, R.id.state_txt, statesModels);
                        spinfarmer_state.setAdapter(customAdapter);

                        spinfarmer_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                //Populate City list to the second spinner when
                                // a state is chosen from the first spinner
                                StatesModel cityDetails = customAdapter.getItem(position);
                                // List<String> cityList = Collections.singletonList(cityDetails.getId());

                                sendState=customAdapter.getItem(position).getName();
                                Log.d("SSSSS",sendState);
                                String cityList=customAdapter.getItem(position).getId();


                                getCityList(cityList);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });



                    }

                    if (baseResponse.getStatus().equals("10200")) {

                        Toast.makeText(ReferFarmerActivity.this, "Invalid Details.", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<StateResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    // pprogressDialog.dismiss();
                }
            });


        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void getCityList(final String cityList)
    {

        citiesModels.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            Call<StateResponse> call = RetrofitClient.getInstance().getApi().citiesList(token,cityList);
            call.enqueue(new Callback<StateResponse>() {
                @Override
                public void onResponse(Call<StateResponse> call, retrofit2.Response<StateResponse> response) {
                    StateResponse baseResponse = response.body();
                    if (baseResponse.getStatus().equals("10100")) {

                        List<StateResponse.DataBean> dataBeanList = baseResponse.getData();

                        for (StateResponse.DataBean dataBean : dataBeanList){

                            CitiesModel sm = new CitiesModel();
                            sm.setId(dataBean.getId());
                            sm.setName(dataBean.getName());

                            // cityList.add(name);
                            citiesModels.add(sm);


                        }
                        citiesAdapter=new CityCustomAdapter(ReferFarmerActivity.this,R.layout.row_cities_list, R.id.city_txt, citiesModels);
                        spinfarmer_city.setAdapter(citiesAdapter);

                        spinfarmer_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                sendCity=citiesAdapter.getItem(position).getName();
                                Log.d("CCCCCC",sendCity);


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });



                    }

                    if (baseResponse.getStatus().equals("10200")) {

                        Toast.makeText(ReferFarmerActivity.this, "Invalid Details.", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<StateResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    // pprogressDialog.dismiss();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }


        @Override
    public void onClick(View v)
    {

        if (v == close_img)
        {

          finish();
        }
        if (v == submit_btn)
        {
            if (checkInternet)
            {
                send_farmer_name = farmer_name.getText().toString();
                send_farmer_state = farmer_state.getText().toString();
                send_farmer_location = farmer_location.getText().toString();
                send_farmer_mobile = farmer_mobile_edt.getText().toString();
                if(send_farmer_name.length()==0)
                {
                    Toast.makeText(ReferFarmerActivity.this,"Please enter farmer name",Toast.LENGTH_SHORT).show();
                }

                else if(send_farmer_mobile.length()==0 || !send_farmer_mobile.matches(MOBILE_REGEX))
                {
                    Toast.makeText(ReferFarmerActivity.this,"Please enter valid moblie no",Toast.LENGTH_SHORT).show();
                }

                else
                {

                    Call<BaseResponse> call = RetrofitClient.getInstance().getApi().referFarmer(deviceId,user_id,send_farmer_name,sendState,send_farmer_mobile,sendCity);
                    call.enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                           BaseResponse baseResponse = response.body();
                            if (baseResponse.getStatus().equals("10100")) {

                                Toast.makeText(ReferFarmerActivity.this, "Refer Farmer Successfully..!", Toast.LENGTH_SHORT).show();
                                finish();

                            }

                            if (baseResponse.getStatus().equals("10200")) {

                                Toast.makeText(ReferFarmerActivity.this, "Invalid Details.", Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                            // pprogressDialog.dismiss();
                        }
                    });


                  /*  StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.API_URL + AppUrls.REFER_FARMER,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String status = jsonObject.getString("status");

                                        if (status.equals("10100")) {

                                            String message = jsonObject.getString("message");
                                            Toast.makeText(ReferFarmerActivity.this, "Refer Farmer Successfully..!", Toast.LENGTH_SHORT).show();
                                            finish();

                                        }

                                        if (status.equals("10200")) {

                                            Toast.makeText(ReferFarmerActivity.this, "Invalid Details.", Toast.LENGTH_SHORT).show();

                                        }



                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id",user_id);
                            params.put("name", send_farmer_name);
                            params.put("state", sendState);
                            params.put("mobile", send_farmer_mobile);
                            params.put("city", sendCity);
                            Log.d("Params", params.toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Gofarmz-Api-Key",GOFARMZ_API_KEY );
                            headers.put("X-Platform", "ANDROID");
                            headers.put("X-Deviceid", deviceId);
                            Log.d("Headers", deviceId);
                            return headers;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(this);
                    requestQueue.add(stringRequest);*/
                }

            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
