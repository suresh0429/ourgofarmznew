package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.CartResponse;
import in.innasoft.gofarmz.adapters.CartRecordChild;
import in.innasoft.gofarmz.adapters.MyCartAdapter;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class MyCartActivity extends AppCompatActivity implements View.OnClickListener {

    Button total_btn, proceed_btn;
    RecyclerView cart_recyclerview;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    Double finalTotal;
    MyCartAdapter myCartAdapter;
    ImageView close_img;
    ArrayList<Object> recordList = new ArrayList<Object>();
    ArrayList<Object> recordChildList = new ArrayList<Object>();

    CartRecordChild cartRecordChild;
    String from = "", pid = "";

    private boolean checkInternet;
    RotateLoading progress_indicator;
    TextView progress_dialog_txt, former_title_txt;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);


        pprogressDialog = new ProgressDialog(MyCartActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        progress_dialog_txt = findViewById(R.id.progress_dialog_txt);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        former_title_txt = findViewById(R.id.former_title_txt);

        cart_recyclerview=findViewById(R.id.cart_recyclerview);

        total_btn = findViewById(R.id.total_btn);
        //total_btn.setTypeface(bold);
        proceed_btn = findViewById(R.id.proceed_btn);
        //proceed_btn.setTypeface(bold);
        proceed_btn.setOnClickListener(this);
        from = getIntent().getStringExtra("from");
        pid = getIntent().getStringExtra("pid");

        getCartData();

    }

    public void getCartData() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<CartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().CartData(token, user_id, deviceId);
            cartResponseCall.enqueue(new Callback<CartResponse>() {
                @Override
                public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                    if (response.isSuccessful()) ;
                    CartResponse cartResponse = response.body();
                    if (cartResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();
                        progress_dialog_txt.setVisibility(View.GONE);

                        CartResponse.DataBean dataBean = cartResponse.getData();
                        List<CartResponse.DataBean.RecordsBean> recordsBeanList = dataBean.getRecords();

                        myCartAdapter = new MyCartAdapter(MyCartActivity.this, recordsBeanList, R.layout.dummy_new_row_cart, from, pid);
                        cart_recyclerview.setHasFixedSize(true);
                        cart_recyclerview.setLayoutManager(new LinearLayoutManager(MyCartActivity.this, LinearLayoutManager.VERTICAL, false));
                        cart_recyclerview.setItemAnimator(new DefaultItemAnimator());
                        cart_recyclerview.setAdapter(myCartAdapter);

                        total_btn.setText("Total : " + Integer.valueOf(dataBean.getFinalTotal()));

//
                    }else if (cartResponse.getStatus().equals("10300")){
                        pprogressDialog.dismiss();
                        Intent intent = new Intent(MyCartActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(),cartResponse.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CartResponse> call, Throwable t) {

                }
            });

        } else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (from.equalsIgnoreCase("Home")) {
            Intent intent = new Intent(MyCartActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(MyCartActivity.this, CustomBoxProductsActivity.class);
            intent.putExtra("pid", pid);
            intent.putExtra("from", "main");
            startActivity(intent);
            finish();
        }
    }

    public void refreshCartPage() {
        myCartAdapter.notifyDataSetChanged();
        getCartData();
    }

    @Override
    public void onClick(View v) {

        if (v == proceed_btn) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
               /* Intent intent = new Intent(MyCartActivity.this, PaymentDetails.class);
                intent.putExtra("activity", "MyCartActivity");
                startActivity(intent);*/

                String totalprice = total_btn.getText().toString();
                Log.d("TOTOTO", totalprice);
                String[] split = totalprice.split(":");
                int total = Integer.parseInt(split[1].trim());

                if (total >= 300) {
                    Intent intent = new Intent(MyCartActivity.this, DeliverySlotsActivity.class);
                    intent.putExtra("activity", "MyCartActivity");
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Minimun order is 300/-", Toast.LENGTH_SHORT).show();
                }


            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == close_img) {
            if (from.equalsIgnoreCase("Home")) {
                Intent intent = new Intent(MyCartActivity.this, MainActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(MyCartActivity.this, CustomBoxProductsActivity.class);
                intent.putExtra("pid", pid);
                intent.putExtra("from", "main");
                startActivity(intent);
                finish();
            }
        }
        // finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartData();
    }
}
