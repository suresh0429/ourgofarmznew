package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.ChildItemDetailsAdapter;
import in.innasoft.gofarmz.adapters.OrderHisDetailChildADapter;
import in.innasoft.gofarmz.models.ViewChildItemsModel;

public class OrderChildeViewDetails extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView headet_text_category;

    RecyclerView recycler_child;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_record_child_dialog);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        ArrayList<ViewChildItemsModel> viewChildItemsModels = (ArrayList<ViewChildItemsModel>) args.getSerializable("ARRAYLIST");
        String title_text=intent.getStringExtra("TITLE");
//        Bundle b = getIntent().getExtras();
//        String title_text = b.getString("TITLE");

        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        headet_text_category = findViewById(R.id.headet_text_category);
        headet_text_category.setText(title_text);

        recycler_child = findViewById(R.id.recycler_child);

        // ArrayList<ViewChildItemsModel> orderChildHistDetailList = (ArrayList<ViewChildItemsModel>) getIntent().getStringArrayExtra("productlist");
        ChildItemDetailsAdapter adapter = new ChildItemDetailsAdapter(OrderChildeViewDetails.this, viewChildItemsModels);
        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(OrderChildeViewDetails.this);
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_child.setLayoutManager(layoutManager1);
        recycler_child.setNestedScrollingEnabled(false);
        recycler_child.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }

    }
}
