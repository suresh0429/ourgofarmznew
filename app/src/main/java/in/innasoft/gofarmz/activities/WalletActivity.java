package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.AddressGetResponse;
import in.innasoft.gofarmz.Response.WalletAmountResponse;
import in.innasoft.gofarmz.Response.WalletgetResponse;
import in.innasoft.gofarmz.adapters.WalletAdapter;
import in.innasoft.gofarmz.models.PostItem;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.PaginationListener;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;

import static in.innasoft.gofarmz.utils.PaginationListener.PAGE_START;

public class WalletActivity extends AppCompatActivity {

    private static final String TAG = "WalletActivity";

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.former_title_txt)
    TextView formerTitleTxt;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtAddAmount)
    TextView txtAddAmount;
    @BindView(R.id.txtAmount)
    TextView txtAmount;
    @BindView(R.id.transactionsList)
    RecyclerView transactionsList;

    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile;

    private boolean checkInternet;
    ProgressDialog pprogressDialog;

    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private int totalPage = 1;
    private boolean isLoading = false;
    int itemCount = 0;
    WalletAdapter walletAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);

        pprogressDialog = new ProgressDialog(WalletActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);

        txtName.setText("Hi ! " + user_name + " your Wallet Balance");


        transactionsList.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        transactionsList.setLayoutManager(layoutManager);

        walletAdapter = new WalletAdapter(WalletActivity.this, new ArrayList<>());
        transactionsList.setAdapter(walletAdapter);

        doApiCall();
        getWalletAmount();

        /**
         * add scroll listener while user reach in bottom load more will call
         */
        transactionsList.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                doApiCall();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    /**
     * do api call here to fetch data from server
     * In example i'm adding data manually
     */
    private void doApiCall() {
        final ArrayList<PostItem> items = new ArrayList<>();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                getWalletTransactions(items);
                // do this all stuff on Success of APIs response

            }
        }, 1500);
    }



    private void getWalletAmount() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<WalletAmountResponse> call= RetrofitClient.getInstance().getApi().getWalletAmount(token,deviceId,user_id);
            call.enqueue(new Callback<WalletAmountResponse>() {
                @Override
                public void onResponse(Call<WalletAmountResponse> call, retrofit2.Response<WalletAmountResponse> response) {
                    if (response.isSuccessful());
                    WalletAmountResponse addressGetResponse=response.body();
                    if (addressGetResponse.getStatus().equals("10100")){
                        pprogressDialog.dismiss();

                        txtAmount.setText("\u20B9 " + addressGetResponse.getData().getAmount());
                    }

                    if (addressGetResponse.getStatus().equals("10200")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if (addressGetResponse.getStatus().equals("10300")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<WalletAmountResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(WalletActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getWalletTransactions(ArrayList<PostItem> items) {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<WalletgetResponse> call= RetrofitClient.getInstance().getApi().getWalletTransactions(token,deviceId,user_id,0);
            call.enqueue(new Callback<WalletgetResponse>() {
                @Override
                public void onResponse(Call<WalletgetResponse> call, retrofit2.Response<WalletgetResponse> response) {
                    if (response.isSuccessful());
                    WalletgetResponse addressGetResponse=response.body();
                    if (addressGetResponse.getStatus().equals("10100")){
                        pprogressDialog.dismiss();

                        List<WalletgetResponse.DataBean> dataBeanList = addressGetResponse.getData();
                        Log.d(TAG, "onResponse: " + dataBeanList.size());

                        for (int i = 0; i <dataBeanList.size(); i++) {
                            itemCount++;
                            PostItem postItem = new PostItem();
                            postItem.setTitle(dataBeanList.get(i).getTitle());
                            postItem.setAmount(dataBeanList.get(i).getAmount());
                            postItem.setCreated_on(dataBeanList.get(i).getCreatedOn());
                            postItem.setTxt_type(dataBeanList.get(i).getTxnType());
                            postItem.setEvent_flag(dataBeanList.get(i).getEventFlag());
                            postItem.setId(dataBeanList.get(i).getId());
                            postItem.setInternal_ref_no(dataBeanList.get(i).getInternalRefNo());
                            items.add(postItem);
                        }

                        Log.d(TAG, "onResponse: "+addressGetResponse.getData().get(0).getAmount());
                        /**
                         * manage progress view
                         */
                        if (currentPage != PAGE_START) walletAdapter.removeLoading();
                        walletAdapter.addItems(items);


                        // check weather is last page or not
                        if (currentPage < totalPage) {
                            walletAdapter.addLoading();
                        } else {
                            isLastPage = true;
                        }
                        isLoading = false;
                    }

                    else if (addressGetResponse.getStatus().equals("10200")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    else if (addressGetResponse.getStatus().equals("10300")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<WalletgetResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(WalletActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
        }
    }




    @OnClick({R.id.close_img, R.id.txtAddAmount})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                Intent order_detail = new Intent(WalletActivity.this, MainActivity.class);
                //order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(order_detail);
                finish();
                break;
            case R.id.txtAddAmount:
                addmoneyDiloge();
                break;
        }
    }

    private void addmoneyDiloge() {

        final AlertDialog dialogBuilder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);

        final EditText editText = (EditText) dialogView.findViewById(R.id.edt_comment);
        Button button1 = (Button) dialogView.findViewById(R.id.buttonSubmit);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DO SOMETHINGS

                if (!editText.getText().toString().isEmpty()) {

                    String totalvalue = editText.getText().toString();

                    Intent order_detail = new Intent(WalletActivity.this, RazerPayActivity.class);

                    order_detail.putExtra("ORDERID", 0);
                    order_detail.putExtra("TOTAL", Double.parseDouble(totalvalue));
                    order_detail.putExtra("ENCRYPTORDERID", "");
                    order_detail.putExtra("ORDER_REF_NO", "");
                    order_detail.putExtra("ACTIVITY", "WalletActivity");
                    order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(order_detail);

                    dialogBuilder.dismiss();
                }
                else {

                    Toast.makeText(WalletActivity.this,"Please Enter Amount",Toast.LENGTH_SHORT).show();
                }

            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent order_detail = new Intent(WalletActivity.this, MainActivity.class);
        //order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(order_detail);
        finish();
    }
}
