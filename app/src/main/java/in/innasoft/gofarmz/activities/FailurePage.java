package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;


public class FailurePage extends AppCompatActivity implements View.OnClickListener {

    ImageView fail_gotoHomeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_failure_page);

        fail_gotoHomeButton = findViewById(R.id.close_img);
        fail_gotoHomeButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == fail_gotoHomeButton)
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();

    }
}

