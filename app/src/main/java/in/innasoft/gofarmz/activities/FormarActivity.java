package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.FormarResponse;
import in.innasoft.gofarmz.adapters.FormarAdapter;
import in.innasoft.gofarmz.models.FarmerSupplierModel;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;

import static in.innasoft.gofarmz.Api.RetrofitClient.BASEIMAGEURL_70x70;
import static in.innasoft.gofarmz.Api.RetrofitClient.IMAGE_URL;


public class FormarActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView all_formars_recyclerview;
    Button custom_box_btn;
    ImageView product_img, close_img;
    TextView former_title_txt, product_name, formar_name_text_title, farmName_txt_title, all_formars_txt, farm_images;
    FormarAdapter formarAdapter;
    Map<String, Object> productObj = new HashMap<>();
    ArrayList<FarmerSupplierModel> formardetails = new ArrayList<>();
    ArrayList<FarmerSupplierModel> supplierdetails = new ArrayList<>();
    String deviceId, user_id, token;
    UserSessionManager userSessionManager;
    RotateLoading progress_indicator;
    TextView progress_dialog_txt;
    String from;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formar);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);

        pprogressDialog = new ProgressDialog(FormarActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        progress_dialog_txt = findViewById(R.id.progress_dialog_txt);

        former_title_txt = findViewById(R.id.former_title_txt);

        product_img = findViewById(R.id.product_img);
        close_img = findViewById(R.id.close_img);
        product_name = findViewById(R.id.product_name_txt);
        all_formars_txt = findViewById(R.id.all_formars_txt);
        formar_name_text_title = findViewById(R.id.formar_name_text_title);

        farmName_txt_title = findViewById(R.id.farmName_txt_title);

        farm_images = findViewById(R.id.farm_images);

        all_formars_recyclerview = findViewById(R.id.all_formars_recyclerview);
        all_formars_recyclerview.setHasFixedSize(true);
        all_formars_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        all_formars_recyclerview.setItemAnimator(new DefaultItemAnimator());
        custom_box_btn = findViewById(R.id.custom_box_btn);
        custom_box_btn.setOnClickListener(this);
        close_img.setOnClickListener(this);

        from = getIntent().getStringExtra("FROM");
        if(from.equalsIgnoreCase("FROMBROWSE"))
        {

            Picasso.with(this)
                    .load(IMAGE_URL + "70x70/"+getIntent().getStringExtra("Image"))
                    .placeholder(R.drawable.cart)
                    .into(product_img);

            product_name.setText(getIntent().getStringExtra("Name"));
            getFormarDetails(getIntent().getStringExtra("IDD"));
        }
        else
        {
            custom_box_btn.setVisibility(View.VISIBLE);
            productObj = (Map<String, Object>) getIntent().getSerializableExtra("productobj");
            Picasso.with(this)
                    .load(BASEIMAGEURL_70x70 + productObj.get("images").toString())
                    .placeholder(R.drawable.cart)
                    .into(product_img);

            product_name.setText(productObj.get("childPdtName").toString());
            getFormarDetails(productObj.get("pdtId").toString());
        }



    }

    @Override
    public void onClick(View view) {
        if (view == custom_box_btn) {
            Intent it = new Intent(FormarActivity.this, CustomBoxProductsActivity.class);
            it.putExtra("productid", productObj.get("pdtId").toString());
            it.putExtra("pid", productObj.get("pid").toString());
            it.putExtra("from", "farmar");
            startActivity(it);
        }
        if (view == close_img) {
            finish();
        }
    }

    public void getFormarDetails(String id)
    {
        pprogressDialog.show();

        Call<FormarResponse> call = RetrofitClient.getInstance().getApi().getFormar(deviceId,id);
        call.enqueue(new Callback<FormarResponse>() {
            @Override
            public void onResponse(Call<FormarResponse> call, retrofit2.Response<FormarResponse> response) {

                if (response.isSuccessful()) ;
                FormarResponse baseResponse = response.body();

                if (baseResponse.getStatus().equals("10100")) {
                    pprogressDialog.dismiss();

                    List<FormarResponse.DataBean> formerresponse =baseResponse.getData();

                    formarAdapter = new FormarAdapter(FormarActivity.this, formerresponse, R.layout.formar_name_row);
                    RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(FormarActivity.this, R.drawable.recycler_view_divider));
                    all_formars_recyclerview.addItemDecoration(dividerItemDecoration);
                    all_formars_recyclerview.setAdapter(formarAdapter);

                } else if (baseResponse.getStatus().equals("10200")) {
                    pprogressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (baseResponse.getStatus().equals("10300")) {

                    pprogressDialog.dismiss();
                    progress_dialog_txt.setVisibility(View.GONE);
                    Toast.makeText(FormarActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();                } else if (baseResponse.getStatus().equals("10400")) {

                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<FormarResponse> call, Throwable t) {
                pprogressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

       /* String url = AppUrls.API_URL + AppUrls.GET_FORMAR_DETAILS + id;
        Log.d("LISTOFINURL", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            pprogressDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("RESPONSE:", response);

                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equalsIgnoreCase("10100")) {

                                pprogressDialog.dismiss();
                              //  progress_indicator.stop();
                                progress_dialog_txt.setVisibility(View.GONE);

                                JSONArray jsonArray=jsonObject.getJSONArray("data");

                               for (int i = 0; i < jsonArray.length(); i++)
                                {
                                    JSONObject jdataobj = jsonArray.getJSONObject(i);
                                    FarmerSupplierModel allFarmer = new FarmerSupplierModel(jdataobj);
                                    formardetails.add(allFarmer);
                                }
                                formarAdapter = new FormarAdapter(FormarActivity.this, formardetails, R.layout.formar_name_row);
                                all_formars_recyclerview.setAdapter(formarAdapter);
                                RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(FormarActivity.this, R.drawable.recycler_view_divider));
                                all_formars_recyclerview.addItemDecoration(dividerItemDecoration);

                            }

                            if (status.equalsIgnoreCase("10300")) {
                              //  progress_indicator.stop();
                                pprogressDialog.dismiss();
                                progress_dialog_txt.setVisibility(View.GONE);
                                Toast.makeText(FormarActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.getMessage();
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Authorization-Basic", token);
                headers.put("Gofarmz-Api-Key", GOFARMZ_API_KEY);
                headers.put("x-device-id", deviceId);
                headers.put("x-device-platform", "ANDROID");
                Log.d("GETOTPEADER", "HEADDER " + headers.toString());
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(FormarActivity.this);
        requestQueue.add(stringRequest);*/
    }

    public void saveArrayList(ArrayList<String> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


}
