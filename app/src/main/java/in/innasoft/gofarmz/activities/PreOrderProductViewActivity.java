package in.innasoft.gofarmz.activities;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.PreOrderAddCartResponse;
import in.innasoft.gofarmz.Response.PreOrderProductResponse;
import in.innasoft.gofarmz.Response.PreOrderUpdateCartResponse;
import in.innasoft.gofarmz.Response.PreOrderViewResponse;
import in.innasoft.gofarmz.utils.CircleAnimationUtil;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class PreOrderProductViewActivity extends AppCompatActivity {

    float totalAmount = 0;
    float unitPlusminus = 0;
    float maxavaliblUnits = 0;

    @BindView(R.id.product_img)
    ImageView productImg;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.txtDecrease)
    TextView txtDecrease;
    @BindView(R.id.txtQty)
    TextView txtQty;
    @BindView(R.id.txtIncrease)
    TextView txtIncrease;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.txtDate)
    TextView txtDate;
    @BindView(R.id.txtUnits)
    TextView txtUnits;
    @BindView(R.id.btnAddToCart)
    Button btnAddToCart;
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.preorderproductdetails_txt)
    TextView preorderproductdetailsTxt;
    @BindView(R.id.cart_img)
    ImageView cartImg;
    @BindView(R.id.cart_count_txt)
    TextView cartCountTxt;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtAvalibleUnits)
    TextView txtAvalibleUnits;
    @BindView(R.id.btnViewCart)
    Button btnViewCart;
    @BindView(R.id.bottomLayout)
    LinearLayout bottomLayout;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    @BindView(R.id.exp_date_txt)
    TextView expDateTxt;
    @BindView(R.id.available_units_txt)
    TextView availableUnitsTxt;
    @BindView(R.id.unit_value_txt)
    TextView unitValueTxt;
    @BindView(R.id.price_units_txt)
    TextView priceUnitsTxt;
    @BindView(R.id.txtUnitPrice)
    TextView txtUnitPrice;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    PreOrderViewResponse preOrderViewResponse = new PreOrderViewResponse();
    Gson gson = new Gson();
    String pr_id;
    Button btnUpdateToCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_order_product_view);

        btnUpdateToCart = findViewById(R.id.btnUpdateToCart);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        checkInternet = NetworkChecking.isConnected(this);

        progressDialog = new ProgressDialog(PreOrderProductViewActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        ButterKnife.bind(this);

        if (getIntent() != null) {
            pr_id = getIntent().getStringExtra("pid");
        }


        getPreOrderViewData();


    }

    private void getPreOrderViewData() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();


            Call<PreOrderProductResponse> call= RetrofitClient.getInstance().getApi().PreOrderProducts(token,deviceId,pr_id,user_id);
            call.enqueue(new Callback<PreOrderProductResponse>() {
                @Override
                public void onResponse(Call<PreOrderProductResponse> call, retrofit2.Response<PreOrderProductResponse> response) {
                    if (response.isSuccessful());
                    PreOrderProductResponse preOrderProductResponse=response.body();
                    if (preOrderProductResponse.getStatus().equals("10100")){
                        progressDialog.dismiss();

                        PreOrderProductResponse.DataBean dataBean=preOrderProductResponse.getData();

                        pr_id = dataBean.getProduct_id();

                        Picasso.with(PreOrderProductViewActivity.this)
                                .load(dataBean.getPop_image())
                                .placeholder(R.drawable.no_image)
                                .into(productImg);

                        txtTitle.setText(dataBean.getProduct_name());
                        txtDescription.setText(dataBean.getPop_description());
                        txtDate.setText(dataBean.getPop_expected_delivery_date());
                        txtAvalibleUnits.setText(dataBean.getPop_available_units());
                        txtUnits.setText(dataBean.getPop_unit_value());
                        txtUnitPrice.setText("\u20B9 " + dataBean.getPop_unit_price());

                               /* String price = preOrderViewResponse.getData().getPopUnitPrice();
                                Double d = Double.parseDouble(price);
                                txtPrice.setText("" + Math.round(d) + "/-");*/

                        totalAmount = Float.valueOf(dataBean.getPop_unit_price());
                        maxavaliblUnits = Float.valueOf(dataBean.getPop_available_units());
                        unitPlusminus = Float.valueOf(dataBean.getQuantity());
                        txtQty.setText(""+Math.round(unitPlusminus));

                        double result = Math.round(totalAmount * unitPlusminus);
                        String priceResult = String.format("%.2f", result);
                        txtPrice.setText("" + priceResult);

                        // cart count label
                        cartCountTxt.setText("" + preOrderProductResponse.getCart_count());


                        if (unitPlusminus == 0) {
                            btnAddToCart.setEnabled(false);
                            btnViewCart.setEnabled(false);
                            txtDecrease.setEnabled(false);
                            btnAddToCart.setAlpha(.5f);
                            btnViewCart.setAlpha(.5f);
                            txtDecrease.setAlpha(.5f);

                        } else {
                            btnAddToCart.setText("UPDATE CART");
                            btnAddToCart.setEnabled(true);
                            btnViewCart.setEnabled(true);
                            txtDecrease.setEnabled(true);
                        }

                    }

                    if (preOrderProductResponse.getStatus().equals("10200")) {
                        progressDialog.dismiss();
                        Toast.makeText(PreOrderProductViewActivity.this, preOrderProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (preOrderProductResponse.getStatus().equals("10300")) {
                        progressDialog.dismiss();
                        Toast.makeText(PreOrderProductViewActivity.this, preOrderProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (preOrderProductResponse.getStatus().equals("10400")) {
                        progressDialog.dismiss();
                        Toast.makeText(PreOrderProductViewActivity.this, preOrderProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<PreOrderProductResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(PreOrderProductViewActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            progressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @OnClick({R.id.txtDecrease, R.id.txtIncrease, R.id.btnAddToCart, R.id.close_img, R.id.cart_img, R.id.btnViewCart})
    public void onViewClicked(View view) {


        switch (view.getId()) {

            case R.id.close_img:
                finish();
                break;

            case R.id.btnViewCart:
                addToCart(pr_id);
                Intent i1 = new Intent(PreOrderProductViewActivity.this, PreOrderCartActivity.class);
                startActivity(i1);
                break;

            case R.id.txtDecrease:

                float avalibleUnits2 = Float.parseFloat(txtAvalibleUnits.getText().toString());

                if (unitPlusminus >= 1) {

                    unitPlusminus = unitPlusminus - 1;
                    txtQty.setText("" + Math.round(unitPlusminus));

                    if (avalibleUnits2 <= maxavaliblUnits) {

                        avalibleUnits2 = avalibleUnits2 + 1;
                        txtAvalibleUnits.setText("" + Math.round(avalibleUnits2));


                    }


                }
                double result1 = Math.round(totalAmount * unitPlusminus);
                String priceResult1 = String.format("%.2f", result1);
                txtPrice.setText("" + priceResult1);

                if (txtQty.getText().equals("0")) {
                    btnAddToCart.setEnabled(false);
                    btnViewCart.setEnabled(false);
                    txtDecrease.setEnabled(false);

                    btnAddToCart.setAlpha(.5f);
                    btnViewCart.setAlpha(.5f);
                    txtDecrease.setAlpha(.5f);
                }

                break;

            case R.id.txtIncrease:

                float avalibleUnits = Float.parseFloat(txtAvalibleUnits.getText().toString());

                if (avalibleUnits > 0) {

                    unitPlusminus = unitPlusminus + 1;
                    txtQty.setText("" + Math.round(unitPlusminus));

                    avalibleUnits = avalibleUnits - 1;
                    txtAvalibleUnits.setText("" + Math.round(avalibleUnits));

                    double result = Math.round(totalAmount * unitPlusminus);
                    String priceResult = String.format("%.2f", result);
                    txtPrice.setText("" + priceResult);

                    btnAddToCart.setEnabled(true);
                    btnViewCart.setEnabled(true);
                    txtDecrease.setEnabled(true);
                    btnAddToCart.setAlpha(.9f);
                    btnViewCart.setAlpha(.9f);
                    txtDecrease.setAlpha(.9f);
                }

                break;
            case R.id.cart_img:

                if (cartCountTxt.getText().toString().equalsIgnoreCase("0")) {

                    Toast.makeText(PreOrderProductViewActivity.this, "No Items Found !", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(PreOrderProductViewActivity.this, PreOrderCartActivity.class);
                    startActivity(i);
                }
                break;

            case R.id.btnAddToCart:

                if (btnAddToCart.getText().toString().equalsIgnoreCase("UPDATE CART")) {
                    updateCart(pr_id);
                } else if (btnAddToCart.getText().toString().equalsIgnoreCase("ADD TO CART")){
                    addToCart(pr_id);
                }

                break;
        }
    }

    private void addToCart(final String pr_id) {
        progressDialog.show();

        Call<PreOrderAddCartResponse> cartResponseCall=RetrofitClient.getInstance().getApi().PreOrderAddCart(token,deviceId,user_id,pr_id,txtQty.getText().toString());
        cartResponseCall.enqueue(new Callback<PreOrderAddCartResponse>() {
            @Override
            public void onResponse(Call<PreOrderAddCartResponse> call, retrofit2.Response<PreOrderAddCartResponse> response) {
                if (response.isSuccessful());
                PreOrderAddCartResponse preOrderAddCartResponse=response.body();
                if (preOrderAddCartResponse.getStatus().equals("10100")){
                    progressDialog.dismiss();
                    cartCountTxt.setText(String.valueOf(preOrderAddCartResponse.getCart_count()));
                    makeFlyAnimation(txtPrice);
                    //getCartCount();
                    btnAddToCart.setText("UPDATE CART");

                    Toast.makeText(PreOrderProductViewActivity.this, preOrderAddCartResponse.getMessage(), Toast.LENGTH_LONG).show();

                }
                if (preOrderAddCartResponse.getStatus().equalsIgnoreCase("10200")) {
                    progressDialog.dismiss();
                    Toast.makeText(PreOrderProductViewActivity.this, preOrderAddCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

                if (preOrderAddCartResponse.getStatus().equalsIgnoreCase("10300")) {
                    progressDialog.dismiss();
                    Toast.makeText(PreOrderProductViewActivity.this, preOrderAddCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (preOrderAddCartResponse.getStatus().equalsIgnoreCase("10400")) {
                    progressDialog.dismiss();
                    Toast.makeText(PreOrderProductViewActivity.this, preOrderAddCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<PreOrderAddCartResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PreOrderProductViewActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void updateCart(final String pr_id) {

        progressDialog.show();

        Call<PreOrderUpdateCartResponse> cartResponseCall=RetrofitClient.getInstance().getApi().PreOrderUpdateCart(token,deviceId,user_id,pr_id,txtQty.getText().toString());
        cartResponseCall.enqueue(new Callback<PreOrderUpdateCartResponse>() {
            @Override
            public void onResponse(Call<PreOrderUpdateCartResponse> call, retrofit2.Response<PreOrderUpdateCartResponse> response) {
                if (response.isSuccessful());
                PreOrderUpdateCartResponse preOrderUpdateCartResponse=response.body();
                if (preOrderUpdateCartResponse.getStatus().equals("10100")){
                    progressDialog.dismiss();
                   // cartCountTxt.setText(String.valueOf(preOrderUpdateCartResponse.getCart_count()));
                    makeFlyAnimation(txtPrice);
                    //getCartCount();
                    btnAddToCart.setText("UPDATE CART");

                    Toast.makeText(PreOrderProductViewActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_LONG).show();

                }
                if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10200")) {
                    progressDialog.dismiss();
                    Toast.makeText(PreOrderProductViewActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

                if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10300")) {
                    progressDialog.dismiss();
                    Toast.makeText(PreOrderProductViewActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10400")) {
                    progressDialog.dismiss();
                    Toast.makeText(PreOrderProductViewActivity.this, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<PreOrderUpdateCartResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PreOrderProductViewActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreOrderViewData();
        //getCartCount();
    }


    /*private void getCartCount() {

        StringRequest string_request = new StringRequest(Request.Method.GET, AppUrls.API_URL + AppUrls.CARTCOUNT + user_id + "&browser_id=" + deviceId, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    int data = jsonObject.getInt("data");
                    cartCountTxt.setText("" + data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(string_request);
    }*/

    private void makeFlyAnimation(final TextView targetView) {

        ImageView destView = findViewById(R.id.cart_img);
        new CircleAnimationUtil().attachActivity(this).setTargetView(targetView)
                .setMoveDuration(1000).setDestView(destView).setAnimationListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                targetView.setVisibility(View.VISIBLE);
                //getCartCount();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();
    }
}
