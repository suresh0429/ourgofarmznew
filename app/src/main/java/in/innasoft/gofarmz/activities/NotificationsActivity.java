package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.NotificationResponse;
import in.innasoft.gofarmz.adapters.NitificationAdapter;
import in.innasoft.gofarmz.models.NotificationModel;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class NotificationsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img, no_notifictioon_img;
    TextView notify_toolbar_title;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    RotateLoading progress_indicator;
    RecyclerView notifction_recycleview;
    NitificationAdapter notificationAdapter;
    ArrayList<NotificationModel> notifiyList;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        pprogressDialog = new ProgressDialog(NotificationsActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        notifction_recycleview = findViewById(R.id.notifction_recycleview);

        no_notifictioon_img = findViewById(R.id.no_notifictioon_img);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        notify_toolbar_title = findViewById(R.id.notify_toolbar_title);


        if (user_id != null) {
            getNotification();
        } else {
            Intent intent = new Intent(NotificationsActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }

    public void getNotification() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<NotificationResponse> call = RetrofitClient.getInstance().getApi().notifications(token,deviceId,user_id);
            call.enqueue(new Callback<NotificationResponse>() {
                @Override
                public void onResponse(Call<NotificationResponse> call, retrofit2.Response<NotificationResponse> response) {
                    NotificationResponse loginResponse = response.body();

                    if (loginResponse.getStatus().equals("10100")) {


                        no_notifictioon_img.setVisibility(View.GONE);
                        pprogressDialog.dismiss();

                        List<NotificationResponse.DataBean> dataBeanList = loginResponse.getData();

                        notificationAdapter = new NitificationAdapter(dataBeanList, NotificationsActivity.this, R.layout.row_notification_list);
                        notifction_recycleview.setNestedScrollingEnabled(false);
                        notifction_recycleview.setLayoutManager(new LinearLayoutManager(NotificationsActivity.this, LinearLayoutManager.VERTICAL, false));
                        notifction_recycleview.setItemAnimator(new DefaultItemAnimator());
                        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(NotificationsActivity.this, R.drawable.recycler_view_divider));
                        notifction_recycleview.addItemDecoration(dividerItemDecoration);
                        notifction_recycleview.setAdapter(notificationAdapter);

                    } else {
                        pprogressDialog.dismiss();
                        no_notifictioon_img.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<NotificationResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                }
            });

        } else {
            progress_indicator.stop();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close_img) {
            finish();
        }
    }

}
