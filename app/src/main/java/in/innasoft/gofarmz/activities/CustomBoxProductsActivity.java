package in.innasoft.gofarmz.activities;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.victor.loading.rotate.RotateLoading;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.CartCountResponse;
import in.innasoft.gofarmz.Response.HomeBoxProductsResponse;
import in.innasoft.gofarmz.adapters.CustomBoxProductsAdapter;
import in.innasoft.gofarmz.database.GoFrmzDB;
import in.innasoft.gofarmz.models.CustomModel;
import in.innasoft.gofarmz.models.OptionsModel;
import in.innasoft.gofarmz.utils.CircleAnimationUtil;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class CustomBoxProductsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img, cart_img, buy_img;
    TextView tittle_txt, cart_count_txt, min_price_txt, total_price_txt;
    RecyclerView custom_recyclerview;
    CustomBoxProductsAdapter customBoxProductsAdapter;
    String productid = "",pid;
    ArrayList<Object> customBoxList = new ArrayList<Object>();
    ArrayList<CustomModel> productsList = new ArrayList<>();
    ArrayList<OptionsModel> optionsList = new ArrayList<>();
    RotateLoading progress_indicator;
    TextView progress_dialog_txt;
    GoFrmzDB goFormzDB;
    NetworkChecking networkChecking;
    private boolean checkInternet;

    UserSessionManager userSessionManager;
    String deviceId, user_id, token, main_prd_id;
    ProgressDialog pprogressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_box_products);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);

        pprogressDialog = new ProgressDialog(CustomBoxProductsActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        networkChecking = new NetworkChecking();

        String from = getIntent().getStringExtra("from");

        pid=getIntent().getStringExtra("pid");
        if(!from.equalsIgnoreCase("main")){
            productid = getIntent().getStringExtra("productid");
        }
        Log.v("FFF", "///" + pid);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        cart_img = findViewById(R.id.cart_img);
        cart_img.setOnClickListener(this);
        buy_img = findViewById(R.id.buy_img);
        buy_img.setOnClickListener(this);
      //  progress_indicator = findViewById(R.id.progress_indicator);
      //  progress_indicator.start();
        progress_dialog_txt = findViewById(R.id.progress_dialog_txt);

        tittle_txt = findViewById(R.id.tittle_txt);

        cart_count_txt = findViewById(R.id.cart_count_txt);
        min_price_txt = findViewById(R.id.min_price_txt);
        //min_price_txt.setTypeface(light);
        total_price_txt = findViewById(R.id.total_price_txt);
        //total_price_txt.setTypeface(light);

        goFormzDB = new GoFrmzDB(CustomBoxProductsActivity.this);
        custom_recyclerview = findViewById(R.id.custom_recyclerview);
        custom_recyclerview.setHasFixedSize(true);
        custom_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        custom_recyclerview.setItemAnimator(new DefaultItemAnimator());
        custom_recyclerview.setNestedScrollingEnabled(false);
        customBoxProductsAdapter = new CustomBoxProductsAdapter(CustomBoxProductsActivity.this, productsList, R.layout.row_custom_box_products, total_price_txt, productid,pid);

        getCartCount();

        getCustomBoxProducts();
    }

    private void getCartCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            pprogressDialog.show();
            Call<CartCountResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().CartCount(token, user_id, deviceId);
            cartCountResponseCall.enqueue(new Callback<CartCountResponse>() {
                @Override
                public void onResponse(Call<CartCountResponse> call, retrofit2.Response<CartCountResponse> response) {
                    if (response.isSuccessful()) ;
                    pprogressDialog.dismiss();
                    CartCountResponse cartCountResponse = response.body();
                    if (cartCountResponse.getStatus().equals("10100")) {
                        cart_count_txt.setText(String.valueOf(cartCountResponse.getData()));
                    } else if (cartCountResponse.getStatus().equals("10200")) {
                        Toast.makeText(getApplicationContext(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CartCountResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getCustomBoxProducts() {

        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            pprogressDialog.show();
            goFormzDB.emptyDBBucket();

            Call<HomeBoxProductsResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().HomeBoxProducts(token, pid,"COMBO_CUSTOM","1",user_id, deviceId);
            cartCountResponseCall.enqueue(new Callback<HomeBoxProductsResponse>() {
                @Override
                public void onResponse(Call<HomeBoxProductsResponse> call, retrofit2.Response<HomeBoxProductsResponse> response) {
                    if (response.isSuccessful()) ;
                    pprogressDialog.dismiss();
                    HomeBoxProductsResponse homeBoxProductsResponse = response.body();

                    if (homeBoxProductsResponse.getStatus().equals("10100")) {
                        progress_dialog_txt.setVisibility(View.GONE);

                        customBoxList.clear();
                        productsList.clear();

                        ArrayList<Object> data = (ArrayList<Object>) homeBoxProductsResponse.getData();
                        customBoxList.addAll(data);

                        Map<String, Object> boxPos = (Map<String, Object>) customBoxList.get(0);
                        main_prd_id = (String) boxPos.get("id");
                        String type = (String) boxPos.get("type");
                        String pdtName = (String) boxPos.get("pdtName");
                        String unitValue = (String) boxPos.get("unitValue");
                        String price = (String) boxPos.get("price");
                        String about = (String) boxPos.get("about");
                        String moreinfo = (String) boxPos.get("moreinfo");
                        String availability = (String) boxPos.get("availability");
                        String unitName = (String) boxPos.get("unitName");
                        String images = (String) boxPos.get("images");
                        ContentValues values = new ContentValues();
                        ArrayList<Object> tmpList = (ArrayList<Object>) boxPos.get("OPTIONAL");
                        for (int i = 0; i < tmpList.size(); i++) {
                            ArrayList<Object> custompricesarr = new ArrayList<>(tmpList.size());
                            CustomModel model = new CustomModel();
                            Map<String, Object> tmpObj = (Map<String, Object>) tmpList.get(i);
                            values.put(GoFrmzDB.ID, (String) tmpObj.get("id"));
                            values.put(GoFrmzDB.PDT_NAME, (String) tmpObj.get("pdtName"));
                            values.put(GoFrmzDB.IMAGES, (String) tmpObj.get("images"));
                            values.put(GoFrmzDB.AVAILABILITY, (String) tmpObj.get("availability"));
                            values.put(GoFrmzDB.OPTIONS, String.valueOf((ArrayList<Object>) tmpObj.get("options")));
                            goFormzDB.addFriendsList(values);
                            model.setId((String) tmpObj.get("id"));
                            model.setPdtName((String) tmpObj.get("pdtName"));
                            model.setImages((String) tmpObj.get("images"));
                            model.setAvailability((String) tmpObj.get("availability"));
                            ArrayList<Object> optionstmparr = (ArrayList<Object>) tmpObj.get("options");

                            for (int k = 0; k < optionstmparr.size(); k++) {
                                Map<String, Object> objectMap = new HashMap<>(tmpList.size());
                                Map<String, Object> priceObj1 = (Map<String, Object>) optionstmparr.get(k);
                                objectMap.put("optId", priceObj1.get("optId"));
                                objectMap.put("pdtId", priceObj1.get("pdtId"));
                                objectMap.put("optName", priceObj1.get("optName"));
                                objectMap.put("price", priceObj1.get("price"));
                                objectMap.put("qtyPrice", priceObj1.get("qtyPrice"));
                                objectMap.put("isInCart", priceObj1.get("isInCart"));
                                objectMap.put("cartQty", priceObj1.get("cartQty"));
                                custompricesarr.add(objectMap);
                                if (optionstmparr.size() == custompricesarr.size())
                                    model.setOptions(custompricesarr);
                                //Log.v("LLLLLLLLLL","///"+custompricesarr);
                            }
                            productsList.add(model);


                        }

                        //  productsList();
                        customBoxProductsAdapter = new CustomBoxProductsAdapter(CustomBoxProductsActivity.this, productsList, R.layout.row_custom_box_products, total_price_txt, productid, pid);
                        custom_recyclerview.setNestedScrollingEnabled(false);
                        custom_recyclerview.setAdapter(customBoxProductsAdapter);
                    }

                    }

                @Override
                public void onFailure(Call<HomeBoxProductsResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });



        } else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            productsList();
        }
    }

    private void productsList() {
        // customBoxList.clear();
        List<String> activityName = goFormzDB.getPdtName();
        if (activityName.size() > 0) {

            List<String> id = goFormzDB.getId();
            List<String> pdtName = goFormzDB.getPdtName();
            List<String> images = goFormzDB.getImages();
            List<String> availability = goFormzDB.getAvailability();
            List<String> option = goFormzDB.getOptions();


            for (int i = 0; i < activityName.size(); i++) {
                CustomModel model = new CustomModel();
                model.setId(id.get(i));
                model.setPdtName(pdtName.get(i));
                model.setImages(images.get(i));
                model.setAvailability(availability.get(i));
                model.setOption(option.get(i));
                productsList.add(model);
            }
            customBoxProductsAdapter = new CustomBoxProductsAdapter(CustomBoxProductsActivity.this, productsList, R.layout.row_custom_box_products, total_price_txt, productid,pid);
            custom_recyclerview.setNestedScrollingEnabled(false);
            custom_recyclerview.setAdapter(customBoxProductsAdapter);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(CustomBoxProductsActivity.this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onClick(View v) {

        if (v == close_img) {
            Intent intent = new Intent(CustomBoxProductsActivity.this, MainActivity.class);
             startActivity(intent);
             finish();
        }

        if (v == cart_img) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent intent = new Intent(CustomBoxProductsActivity.this, MyCartActivity.class);
                intent.putExtra("from", "custom");
                intent.putExtra("pid", pid);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == buy_img) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                String totalprice = total_price_txt.getText().toString();
                String[] split = totalprice.split(":");
             /*   int total = Integer.parseInt(split[1].trim());
                if (total >= 300)*/
                    addToCart(customBoxProductsAdapter.cartarr);
//                else
//                    Toast.makeText(this, "Minimun order is 300/-", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void addToCart(final ArrayList<String> cartarr) {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {


            for (int i = 0; i < cartarr.size(); i++) {
                 cartarr.get(i);
            }

            Call<BaseResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().addtocart(token,deviceId, user_id,main_prd_id, deviceId,"1",cartarr);
            cartCountResponseCall.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                    if (response.isSuccessful()) ;
                    BaseResponse cartCountResponse = response.body();
                    if (cartCountResponse.getStatus().equals("10100")) {
                        makeFlyAnimation(total_price_txt);
                        Toast.makeText(CustomBoxProductsActivity.this, "Add to Cart Successfully ", Toast.LENGTH_LONG).show();
                    } else if (cartCountResponse.getStatus().equals("10200")) {
                        Toast.makeText(getApplicationContext(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void makeFlyAnimation(TextView targetView) {

        //FrameLayout destView = findViewById(R.id.root);
        ImageView destView = findViewById(R.id.cart_img);

        new CircleAnimationUtil().attachActivity(this).setTargetView(targetView).setMoveDuration(1000).setDestView(destView).setAnimationListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
            pprogressDialog.dismiss();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                pprogressDialog.dismiss();
                total_price_txt.setVisibility(View.VISIBLE);
                getCartCount();
                //Toast.makeText(CustomBoxProductsActivity.this, "Continue Shopping...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();
    }
}
