package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.MyOrderResponse;
import in.innasoft.gofarmz.Response.MyPreOrderResponse;
import in.innasoft.gofarmz.Response.PreOrderHistoryResponse;
import in.innasoft.gofarmz.adapters.OrderHistoryAdapter;
import in.innasoft.gofarmz.adapters.PreOrderHistoryAdapter;
import in.innasoft.gofarmz.models.OrderHistoryModel;
import in.innasoft.gofarmz.models.PreOrderHistoryModel;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class OrdersHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close,no_order_img;
    TextView allsponsors_toolbar_title;
    RecyclerView order_history_recyclerview,preOrder_history_recyclerview;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    OrderHistoryAdapter myCartAdapter;
    ArrayList<OrderHistoryModel> recordList;
    PreOrderHistoryAdapter preOrderAdapter;
    ArrayList<PreOrderHistoryModel> preRecordList;
    private boolean checkInternet;
    String payurl="",Order_id,r_id;
    ProgressDialog pprogressDialog;
    RadioGroup orders_rg;
    RadioButton orders_rb,preOrders_rb;
    Gson gson;
    PreOrderHistoryResponse preOrderHistoryResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_history);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        pprogressDialog = new ProgressDialog(OrdersHistoryActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        no_order_img = findViewById(R.id.no_order_img);
        close = findViewById(R.id.close_img);
        close.setOnClickListener(this);

        orders_rg = findViewById(R.id.orders_rg);
        orders_rb = findViewById(R.id.orders_rb);
        orders_rb.setOnClickListener(this);
        preOrders_rb = findViewById(R.id.preOrders_rb);
        preOrders_rb.setOnClickListener(this);

        allsponsors_toolbar_title = findViewById(R.id.allsponsors_toolbar_title);

        order_history_recyclerview = findViewById(R.id.order_history_recyclerview);
        preOrder_history_recyclerview = findViewById(R.id.preOrder_history_recyclerview);


//        getOrderHisotryData();
        if (getIntent() != null) {
            r_id = getIntent().getStringExtra("R_id");
//        String orderr_id=getIntent().getStringExtra("order_R_id");
        }
        if (r_id.equals("preorders")){
            preOrders_rb.setChecked(true);
            order_history_recyclerview.setVisibility(View.GONE);
            preOrder_history_recyclerview.setVisibility(View.VISIBLE);
            getPreOrderHisotryData();

        }
        else if (r_id.equals("orders")){
            orders_rb.setChecked(true);
            preOrder_history_recyclerview.setVisibility(View.GONE);
            order_history_recyclerview.setVisibility(View.VISIBLE);
            getOrderHisotryData();
        }


        payurl=getIntent().getStringExtra("payurl");
        Order_id=getIntent().getStringExtra("ORDERID");
        if(payurl!=null && payurl.length()>0){

            Log.d("PPPPPAY",payurl+"///"+Order_id);
            Intent order_detail= new Intent(OrdersHistoryActivity.this,PaymentWebViewActivity.class);
            order_detail.putExtra("payurl",payurl);
            order_detail.putExtra("ORDERID",Order_id);
            startActivity(order_detail);
            /*Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(payurl));
            startActivity(myIntent);*/
        }

        //getOrderHisotryData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrderHisotryData();
    }

    private void getOrderHisotryData() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            pprogressDialog.show();

            Call<MyOrderResponse> call= RetrofitClient.getInstance().getApi().MyOrders(token,user_id);
            call.enqueue(new Callback<MyOrderResponse>() {
                @Override
                public void onResponse(Call<MyOrderResponse> call, retrofit2.Response<MyOrderResponse> response) {
                    if (response.isSuccessful());
                    MyOrderResponse myOrderResponse=response.body();
                    if (myOrderResponse.getStatus().equals("10100")){
                        pprogressDialog.dismiss();
                        MyOrderResponse.DataBean dataBean=myOrderResponse.getData();
                        List<MyOrderResponse.DataBean.RecordDataBean> recordDataBeanList=dataBean.getRecordData();

                        myCartAdapter = new OrderHistoryAdapter(recordDataBeanList, getApplicationContext(), R.layout.row_order_history);
                        order_history_recyclerview.setNestedScrollingEnabled(false);
                        order_history_recyclerview.setLayoutManager(new LinearLayoutManager(OrdersHistoryActivity.this, LinearLayoutManager.VERTICAL, false));
                        order_history_recyclerview.setItemAnimator(new DefaultItemAnimator());
                        order_history_recyclerview.setAdapter(myCartAdapter);
                    }
                    else if (myOrderResponse.getStatus().equals("10200")) {
                        pprogressDialog.dismiss();
                        no_order_img.setVisibility(View.VISIBLE);
                        order_history_recyclerview.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                    } else if (myOrderResponse.getStatus().equalsIgnoreCase("10300")) {
                        pprogressDialog.dismiss();
                        no_order_img.setVisibility(View.VISIBLE);
                        order_history_recyclerview.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onFailure(Call<MyOrderResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(OrdersHistoryActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }


        else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getPreOrderHisotryData() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<MyPreOrderResponse> call=RetrofitClient.getInstance().getApi().MyPreOrders(token,user_id);
            call.enqueue(new Callback<MyPreOrderResponse>() {
                @Override
                public void onResponse(Call<MyPreOrderResponse> call, Response<MyPreOrderResponse> response) {
                    if (response.isSuccessful());
                    MyPreOrderResponse myPreOrderResponse=response.body();
                    if (myPreOrderResponse.getStatus().equals("10100")){
                        MyPreOrderResponse.DataBean dataBean=myPreOrderResponse.getData();
                        List<MyPreOrderResponse.DataBean.RecordDataBean> recordDataBeanList=dataBean.getRecordData();

                        no_order_img.setVisibility(View.GONE);
                        order_history_recyclerview.setVisibility(View.VISIBLE);
                        preOrderAdapter = new PreOrderHistoryAdapter(recordDataBeanList, OrdersHistoryActivity.this);
                        order_history_recyclerview.setNestedScrollingEnabled(false);
                        order_history_recyclerview.setLayoutManager(new LinearLayoutManager(OrdersHistoryActivity.this, LinearLayoutManager.VERTICAL, false));
                        order_history_recyclerview.setItemAnimator(new DefaultItemAnimator());
                        order_history_recyclerview.setAdapter(preOrderAdapter);


                    }
                }

                @Override
                public void onFailure(Call<MyPreOrderResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(OrdersHistoryActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        }

        else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(OrdersHistoryActivity.this, MainActivity.class);
        startActivity(it);
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.close_img){
            Intent it=new Intent(OrdersHistoryActivity.this, MainActivity.class);
            startActivity(it);
            finish();
        }

        if (view == orders_rb){
            preOrder_history_recyclerview.setVisibility(View.GONE);
            order_history_recyclerview.setVisibility(View.VISIBLE);
            getOrderHisotryData();
        }

        if (view == preOrders_rb){
            order_history_recyclerview.setVisibility(View.GONE);
            preOrder_history_recyclerview.setVisibility(View.VISIBLE);
            getPreOrderHisotryData();
        }

    }
}
