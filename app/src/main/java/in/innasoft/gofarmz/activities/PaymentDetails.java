package in.innasoft.gofarmz.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.AddressGetResponse;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.CartResponse;
import in.innasoft.gofarmz.Response.CheckoutDataResponse;
import in.innasoft.gofarmz.Response.PlaceOrderResponse;
import in.innasoft.gofarmz.Response.PreOrderCheckoutDataResponse;
import in.innasoft.gofarmz.Response.PreOrderCheckoutResponse;
import in.innasoft.gofarmz.Response.PreOrderPlaceResponse;
import in.innasoft.gofarmz.Response.WalletAmountResponse;
import in.innasoft.gofarmz.adapters.MyProductDetailsAdapter;
import in.innasoft.gofarmz.adapters.PaymentTypeAdapter;
import in.innasoft.gofarmz.adapters.PreOrderPaymentTypeAdapter;
import in.innasoft.gofarmz.adapters.PreOrderProductdetailsAdapter;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class PaymentDetails extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG ="payment" ;
    TextView manage_address_txt, addAddress_txt, delivery_title_txt, delivery_detail_txt, product_title_txt, payment_title_txt, add_coupan_code, price_title, pricetxt, coupan_code_txt, discount_title,
            discounttxt, delivery_title, dileverytxt, deliverytext_nocharge, totlaPrice_title, totlaPricetxt, payment_type_txt, promo_code_txt, allsponsors_toolbar_title, expected_delevry_title, expected_delevry_date;
    Button next_btn, total_btn;
    RecyclerView product_details_recyclerview, payment_details_recyclerview;
    MyProductDetailsAdapter myProductDetailsAdapter;
    PreOrderProductdetailsAdapter preOrderProductdetailsAdapter;
    PreOrderPaymentTypeAdapter preOrderPaymentTypeAdapter;
    ArrayList<Object> recordList = new ArrayList<Object>();
    ArrayList<Object> defaultAddressList = new ArrayList<Object>();
    ArrayList<Object> recordChildList = new ArrayList<Object>();
    UserSessionManager userSessionManager;
    String activity, id, promoStatus, deviceId, user_id, token, coupon_code,coupon, preOrderActivity, cartActivity;
   // String wallet_Used="1";
    String wallet_Used="0";
    String paymentgatewayId ="" +
            "" +
            "";
    private boolean checkInternet,isWallet= true;
    Dialog dialog;
    RelativeLayout delivery_ll;
    ArrayList<Object> orderDetailProduct = new ArrayList<>();
    RecyclerView payment_type_recyclerview;
    PaymentTypeAdapter paymentTypeAdapter;
    Double final_amount, final_discount, Totalprice,walletPrice;
    String delivery_charges, adress_id, appliedCoupon_amount;
    ImageView close_img, remove_coupon_code;

    TableRow row_coupons;
    Double total_price_after_coupon, total_after_remove_coupan;
    SweetAlertDialog sd, sd_not_verify, sd_orderClose;
    String order_id, mobile_verifyed, contact_no, expected_delevry, users_mobile, orderCloseStatText, slotId, addressid, expected_slot,Instructions;
    int orderCloseStatus;
    ProgressDialog pprogressDialog;
    LinearLayout expected_delv_ll,payment_ll;
    Gson gson;
    PreOrderCheckoutResponse preOrderCheckoutResponse;
    Float result;
    Float d_charges;
    List<CartResponse.DataBean.RecordsBean> recordsBeanList;
    EditText edt_instructions;
    PlaceOrderResponse.DataBean dataBean;
    PreOrderPlaceResponse.DataBean preOrderPlaceResponseData;
    CheckBox cbWallet;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_payment_details);
        setContentView(R.layout.dummy_payment_detail_detail);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        pprogressDialog = new ProgressDialog(PaymentDetails.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        checkInternet = NetworkChecking.isConnected(this);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        expected_delv_ll = findViewById(R.id.expected_delv_ll);
        payment_ll = findViewById(R.id.payment_ll);

        edt_instructions = findViewById(R.id.edt_instructions);
        cbWallet = findViewById(R.id.cbWallet);

        promo_code_txt = findViewById(R.id.promo_code_txt);
        promo_code_txt.setPaintFlags(promo_code_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        promo_code_txt.setOnClickListener(this);

        row_coupons = findViewById(R.id.row_coupons);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        activity = bundle.getString("activity");
        //  preOrderActivity = bundle.getString("PreOrderActivity");
        // cartActivity=bundle.getString("MyCartActivity");
        assert activity != null;
        if (activity.equalsIgnoreCase("DeliverySlotActivity")) {

            expected_delv_ll.setVisibility(View.VISIBLE);
            promo_code_txt.setVisibility(View.VISIBLE);
            row_coupons.setVisibility(View.VISIBLE);

            addressid = bundle.getString("ADD_ID");
            slotId = bundle.getString("SLOT_ID");
            contact_no = bundle.getString("contact_no");
            expected_slot = bundle.getString("SlotString");

            Log.d("DDJJ", addressid + "\n" + slotId + "\n" + contact_no + "\n" + expected_slot);

            //delAddress();
        } else if (activity.equalsIgnoreCase("PreOrderCartActivity") || activity.equalsIgnoreCase("PreOrder")) {

            expected_delv_ll.setVisibility(View.GONE);
            promo_code_txt.setVisibility(View.GONE);
            row_coupons.setVisibility(View.GONE);

            addressid = bundle.getString("ADD_ID");
            slotId = bundle.getString("SLOT_ID");
            contact_no = bundle.getString("contact_no");
            expected_slot = bundle.getString("SlotString");
            Log.d("DDJJ", addressid + "\n" + slotId + "\n" + contact_no + "\n" + expected_slot);

        }


        delivery_ll = findViewById(R.id.delivery_ll);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        allsponsors_toolbar_title = findViewById(R.id.allsponsors_toolbar_title);

        delivery_title_txt = findViewById(R.id.delivery_title_txt);

        delivery_detail_txt = findViewById(R.id.delivery_detail_txt);

        product_title_txt = findViewById(R.id.product_title_txt);

        payment_title_txt = findViewById(R.id.payment_title_txt);

        payment_type_txt = findViewById(R.id.payment_type_txt);


        expected_delevry_title = findViewById(R.id.expected_delevry_title);

        expected_delevry_date = findViewById(R.id.expected_delevry_date);


        manage_address_txt = findViewById(R.id.manage_address_txt);

        manage_address_txt.setOnClickListener(this);

        addAddress_txt = findViewById(R.id.addAddress_txt);

        addAddress_txt.setOnClickListener(this);

        total_btn = findViewById(R.id.total_btn);

        next_btn = findViewById(R.id.next_btn);
        next_btn.setOnClickListener(this);

        price_title = findViewById(R.id.price_title);
        pricetxt = findViewById(R.id.pricetxt);
        add_coupan_code = findViewById(R.id.add_coupan_code);
        //add_coupan_code.setOnClickListener(this);
        coupan_code_txt = findViewById(R.id.coupan_code_txt);
        discount_title = findViewById(R.id.discount_title);
        discounttxt = findViewById(R.id.discounttxt);
        delivery_title = findViewById(R.id.delivery_title);
        dileverytxt = findViewById(R.id.dileverytxt);
        deliverytext_nocharge = findViewById(R.id.deliverytext_nocharge);
        totlaPrice_title = findViewById(R.id.totlaPrice_title);
        totlaPricetxt = findViewById(R.id.totlaPricetxt);

        product_details_recyclerview = findViewById(R.id.product_details_recyclerview);
        payment_type_recyclerview = findViewById(R.id.payment_type_recyclerview);

        if (activity.equalsIgnoreCase("PreOrderCartActivity") || activity.equalsIgnoreCase("PreOrder")) {

            getPreOrderCheckOutData();
        } else {
            product_details_recyclerview.setHasFixedSize(true);
            product_details_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            product_details_recyclerview.setItemAnimator(new DefaultItemAnimator());
            myProductDetailsAdapter = new MyProductDetailsAdapter(PaymentDetails.this, recordsBeanList, R.layout.row_product_payment_detail);

            getWalletAmount("activity", 0.00);
            getCartData();


            if (activity.equalsIgnoreCase("PromoCouponListActivity")) {
                promoStatus = bundle.getString("promoStatus");

                addressid = bundle.getString("ADD_ID");
                slotId = bundle.getString("SLOT_ID");

                if (promoStatus.equalsIgnoreCase("0")) {
                    promo_code_txt.setVisibility(View.VISIBLE);
                    row_coupons.setVisibility(View.GONE);
                } else {
                    promo_code_txt.setVisibility(View.GONE);
                    row_coupons.setVisibility(View.VISIBLE);
                }
            }


            coupan_code_txt.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (coupan_code_txt.getRight() - coupan_code_txt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // your action here
                            promo_code_txt.setVisibility(View.VISIBLE);
                            // remove_coupon_code.setVisibility(View.GONE);
                            row_coupons.setVisibility(View.GONE);
                            Log.d("ASSSSS", "" + appliedCoupon_amount + "///////" + total_price_after_coupon);
                            total_after_remove_coupan = total_price_after_coupon + Double.parseDouble(appliedCoupon_amount);


                           /* if (isWallet){
                                // wallet calculation
                                if (walletPrice>0.0){
                                    cbWallet.setChecked(true);

                                    if (walletPrice>total_after_remove_coupan){
                                        total_after_remove_coupan = total_after_remove_coupan+0.00;
                                        paymentgatewayId = "4";
                                        payment_ll.setVisibility(View.GONE);

                                    }else if (walletPrice<total_after_remove_coupan){
                                        total_after_remove_coupan = total_after_remove_coupan-walletPrice;
                                        paymentgatewayId = "";
                                        payment_ll.setVisibility(View.VISIBLE);
                                    }

                                }
                            }else {
                                paymentgatewayId = "";
                                payment_ll.setVisibility(View.VISIBLE);
                            }*/


                          //  totlaPricetxt.setText("" + new DecimalFormat("##.##").format(total_after_remove_coupan) + "/-");
                          //  total_btn.setText("Total: " + new DecimalFormat("##.##").format(total_after_remove_coupan) + "/-");

                            getWalletAmount("activityRemoveCoupan", total_after_remove_coupan);
                            return true;
                        }
                    }
                    return true;
                }
            });
        }


    }

    private void getPreOrderCheckOutData() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<PreOrderCheckoutDataResponse> checkoutDataResponseCall=RetrofitClient.getInstance().getApi().PreOrderCheckoutData(token,deviceId,user_id);
            checkoutDataResponseCall.enqueue(new Callback<PreOrderCheckoutDataResponse>() {
                @Override
                public void onResponse(Call<PreOrderCheckoutDataResponse> call, retrofit2.Response<PreOrderCheckoutDataResponse> response) {
                    if (response.isSuccessful());
                    PreOrderCheckoutDataResponse preOrderCheckoutDataResponse=response.body();
                    if (preOrderCheckoutDataResponse.getStatus().equals("10100")){
                        pprogressDialog.dismiss();

                        PreOrderCheckoutDataResponse.DataBean dataBean=preOrderCheckoutDataResponse.getData();
                        List<PreOrderCheckoutDataResponse.DataBean.AddressBean> addressBeanList=dataBean.getAddress();
                        List<PreOrderCheckoutDataResponse.DataBean.ProductsBean> productsBeanList=dataBean.getProducts();
                        List<PreOrderCheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeanList=dataBean.getPaymentGateway();

                        product_details_recyclerview.setHasFixedSize(true);
                        product_details_recyclerview.setLayoutManager(new LinearLayoutManager(PaymentDetails.this, LinearLayoutManager.VERTICAL, false));
                        product_details_recyclerview.setItemAnimator(new DefaultItemAnimator());
                        preOrderProductdetailsAdapter = new PreOrderProductdetailsAdapter(PaymentDetails.this, productsBeanList);
                        product_details_recyclerview.setAdapter(preOrderProductdetailsAdapter);

                        payment_type_recyclerview.setHasFixedSize(true);
                        payment_type_recyclerview.setLayoutManager(new LinearLayoutManager(PaymentDetails.this, LinearLayoutManager.VERTICAL, false));
                        payment_type_recyclerview.setItemAnimator(new DefaultItemAnimator());
                        preOrderPaymentTypeAdapter = new PreOrderPaymentTypeAdapter(PaymentDetails.this,paymentGatewayBeanList,paymentgatewayId);
                        payment_type_recyclerview.setAdapter(preOrderPaymentTypeAdapter);

                        pricetxt.setText("" + dataBean.getFinalTotal());

                        mobile_verifyed = dataBean.getMobileVerifyStatus();

                        row_coupons.setVisibility(View.VISIBLE);
                        for (int i = 0; i < addressBeanList.size(); i++) {

                            d_charges = Float.parseFloat(addressBeanList.get(i).getDeliveryCharges());
                            dileverytxt.setText("" + d_charges);


                        }

                        double result = Math.round(d_charges + Float.parseFloat(String.valueOf(dataBean.getFinalTotal())));
                        String priceResult = String.format("%.2f", result);
                        totlaPricetxt.setText("" + priceResult);
                        total_btn.setText("Total: " + priceResult);

                        mobile_verifyed = dataBean.getMobileVerifyStatus();


                    }
                    else if (preOrderCheckoutDataResponse.getStatus().equals("10200")){
                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, preOrderCheckoutDataResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PreOrderCheckoutDataResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(PaymentDetails.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        } else {

            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();

        }
    }

    private void delAddress() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<AddressGetResponse> call=RetrofitClient.getInstance().getApi().GetAddress(token,deviceId,user_id,id);
            call.enqueue(new Callback<AddressGetResponse>() {
                @Override
                public void onResponse(Call<AddressGetResponse> call, retrofit2.Response<AddressGetResponse> response) {
                    if (response.isSuccessful());
                    AddressGetResponse addressGetResponse=response.body();
                    if (addressGetResponse.getStatus().equals("10100")){
                        pprogressDialog.dismiss();
                        delivery_ll.setVisibility(View.VISIBLE);

                        AddressGetResponse.DataBean dataBean=addressGetResponse.getData();

                        String deliveryaddress = dataBean.getName() + "\n" + dataBean.getAddress_line1() + "," + dataBean.getAddress_line2() + "," + dataBean.getArea() + "," + dataBean.getCity() + "," + dataBean.getCity() + "," + dataBean.getCity() + "," + dataBean.getPincode() + "\n" + dataBean.getContact_no() + "\n" + dataBean.getAlternate_contact_no();
                        delivery_detail_txt.setText(deliveryaddress);
                        dileverytxt.setText(dataBean.getDelivery_charges());
                        SharedPreferences preferences = getSharedPreferences("amount", 0);
                        int amount = preferences.getInt("finalamount", 0);
                        final_amount = Double.valueOf(amount);
                        delivery_charges = dataBean.getDelivery_charges();
                        Double total = final_amount + Double.parseDouble(delivery_charges);   //after applied coupon add delivery chages
                        totlaPricetxt.setText("" + new DecimalFormat("##.##").format(total) + "/-");
                        total_btn.setText("Total: " + new DecimalFormat("##.##").format(total) + "/-");


                    }

                    if (addressGetResponse.getStatus().equals("10200")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                    }

                    if (addressGetResponse.getStatus().equals("10300")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "No Data Found..", Toast.LENGTH_SHORT).show();
                    }

                    if (addressGetResponse.getStatus().equals("10400")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddressGetResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(PaymentDetails.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == close_img) {
            onBackPressed();
            finish();

        }

        if (v == promo_code_txt) {
            String deliveryaddress = delivery_detail_txt.getText().toString();
            if (deliveryaddress != null && deliveryaddress.length() > 0) {
                promo_code_txt.setVisibility(View.GONE);
                row_coupons.setVisibility(View.VISIBLE);
                coupan_code_txt.setVisibility(View.GONE);
                Intent promolist = new Intent(PaymentDetails.this, PromoCouponListActivity.class);
                promolist.putExtra("FINAL_AMOUNT", final_amount);
                promolist.putExtra("ADD_ID", addressid);
                promolist.putExtra("SLOT_ID", slotId);

                startActivityForResult(promolist, 1);
            } else {

                Toast.makeText(PaymentDetails.this, "Select Your delivery details", Toast.LENGTH_SHORT).show();
            }

        }

        if (v == next_btn) {
            if (checkInternet) {
                Log.e("MOBILEVERIFIED", "----" + mobile_verifyed);
                if (mobile_verifyed != null && mobile_verifyed.equalsIgnoreCase("0")) {
                    //  contact_no = users_mobile;
                    // Toast.makeText(PaymentDetails.this, "not verified", Toast.LENGTH_SHORT).show();
                    sd_not_verify = new SweetAlertDialog(PaymentDetails.this, SweetAlertDialog.NORMAL_TYPE);
                    sd_not_verify.setTitleText("Mobile Verification");
                    sd_not_verify.setContentText("Please verify your mobile number -- " + contact_no);
                    sd_not_verify.setConfirmText("OK");
                    sd_not_verify.showCancelButton(true);
                    sd_not_verify.setCanceledOnTouchOutside(false);
                    sd_not_verify.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sweetAlertDialog) {
                            getOTPMobileVerified();
                            Intent it = new Intent(PaymentDetails.this, AccountVerificationActivity.class);
                            it.putExtra("activity", "PAYMENTDETAIL");
                            it.putExtra("mobile", contact_no);
                            startActivity(it);
                            finish();
                        }
                    });
                    sd_not_verify.show();
                } else {
                   /* next_btn.setClickable(false);
                    next_btn.setFocusable(false);
                    next_btn.setFocusableInTouchMode(false);
*/
                    if (activity.equalsIgnoreCase("PreOrderCartActivity") || activity.equalsIgnoreCase("PreOrder")) {

                        doPreOrderCheckoutPayment();
                    } else {
                        doCheckoutPayment();

                    }
                    Log.d("ASF", "DASFDS");
                }
            } else {
                Toast.makeText(PaymentDetails.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
            }
        }

      /*  if(v==remove_coupon_code)
        {
            promo_code_txt.setVisibility(View.VISIBLE);
            remove_coupon_code.setVisibility(View.GONE);
            row_coupons.setVisibility(View.GONE);
            Log.d("ASSSSS",""+appliedCoupon_amount+"///////"+total_price_after_coupon);
            total_after_remove_coupan=total_price_after_coupon+Double.parseDouble(appliedCoupon_amount);
            totlaPricetxt.setText(""  +total_after_remove_coupan+ "/-");


        }*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {


                coupon_code = data.getStringExtra("result");
                Log.d("dsafds", coupon_code);
                if (coupon_code.equalsIgnoreCase("2")) {
                    promo_code_txt.setVisibility(View.VISIBLE);
                    add_coupan_code.setVisibility(View.GONE);
                } else {
                    add_coupan_code.setVisibility(View.VISIBLE);
                    appliedCoupon_amount = data.getStringExtra("applied_amt");
                    Log.d("CODESSS", coupon_code + "///" + appliedCoupon_amount);
                    add_coupan_code.setText("Applied Code " + coupon_code);
                    coupan_code_txt.setVisibility(View.VISIBLE);
                    // remove_coupon_code.setVisibility(View.VISIBLE);
                    coupan_code_txt.setText(appliedCoupon_amount + "/-");
                    Double appliedCoupAmt = final_amount - Double.parseDouble(appliedCoupon_amount);
                    total_price_after_coupon = appliedCoupAmt + Double.parseDouble(delivery_charges);   //after applied coupon add delivery chages

                    PromoCodeCalculation(total_price_after_coupon);
                    getWalletAmount("onactivity",total_price_after_coupon);
                }




            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }


    private void getWalletAmount(String activity, Double total_price_after_coupon) {
        Log.d(TAG, "getWalletAmount: "+activity);
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<WalletAmountResponse> call= RetrofitClient.getInstance().getApi().getWalletAmount(token,deviceId,user_id);
            call.enqueue(new Callback<WalletAmountResponse>() {
                @Override
                public void onResponse(Call<WalletAmountResponse> call, retrofit2.Response<WalletAmountResponse> response) {
                    if (response.isSuccessful());
                    WalletAmountResponse addressGetResponse=response.body();
                    if (addressGetResponse.getStatus().equals("10100")){
                        pprogressDialog.dismiss();


                        if (Double.parseDouble(addressGetResponse.getData().getAmount())==0.0){
                            cbWallet.setEnabled(false);
                            cbWallet.setText("\u20B9" + "0.00");
                            walletPrice =0.00;
                        }
                        else if (Double.parseDouble(addressGetResponse.getData().getAmount())>0.0){
                            cbWallet.setEnabled(true);
                            cbWallet.setChecked(true);
                            cbWallet.setText("\u20B9" + addressGetResponse.getData().getAmount());
                            walletPrice = Double.valueOf(addressGetResponse.getData().getAmount());

                        }
                        getCheckOutData(true, activity,total_price_after_coupon);
                        cbWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                                if (isChecked){
                                    getCheckOutData(true,activity, total_price_after_coupon);
                                    isWallet = true;
                                    wallet_Used = "1";
                                }else {

                                    getCheckOutData(false,activity, total_price_after_coupon);
                                    isWallet = false;
                                    wallet_Used = "0";
                                }
                            }
                        });




                    }

                    if (addressGetResponse.getStatus().equals("10200")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if (addressGetResponse.getStatus().equals("10300")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), addressGetResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<WalletAmountResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(PaymentDetails.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getCartData() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();
            Call<CartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().CartData(token, user_id, deviceId);
            cartResponseCall.enqueue(new Callback<CartResponse>() {
                @Override
                public void onResponse(Call<CartResponse> call, retrofit2.Response<CartResponse> response) {
                    if (response.isSuccessful()) ;
                    CartResponse cartResponse = response.body();
                    if (cartResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();
                        product_details_recyclerview.setVisibility(View.VISIBLE);
                        CartResponse.DataBean dataBean = cartResponse.getData();
                        recordsBeanList = dataBean.getRecords();

                        myProductDetailsAdapter = new MyProductDetailsAdapter(PaymentDetails.this, recordsBeanList, R.layout.row_product_payment_detail);
                        product_details_recyclerview.setAdapter(myProductDetailsAdapter);
                        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(PaymentDetails.this, R.drawable.recycler_view_divider));
                        product_details_recyclerview.addItemDecoration(dividerItemDecoration);
                        myProductDetailsAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<CartResponse> call, Throwable t) {

                }
            });


        } else {
            Toast.makeText(PaymentDetails.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    public void refreshCartPage() {

        pprogressDialog.dismiss();
        getCartData();
    }

    public void getCheckOutData(boolean isWallet, String activity, double total_price_after_coupon) {

        Log.d(TAG, "getCheckOutData: "+activity);
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<CheckoutDataResponse> checkoutDataResponseCall = RetrofitClient.getInstance().getApi().CheckoutData(token, user_id,addressid);
            checkoutDataResponseCall.enqueue(new Callback<CheckoutDataResponse>() {
                @Override
                public void onResponse(Call<CheckoutDataResponse> call, retrofit2.Response<CheckoutDataResponse> response) {
                    if (response.isSuccessful()) ;
                    CheckoutDataResponse checkoutDataResponse = response.body();
                    if (checkoutDataResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();
                        CheckoutDataResponse.DataBean dataBean = checkoutDataResponse.getData();
                        List<CheckoutDataResponse.DataBean.AddressBean> addressBeanList = dataBean.getAddress();
                        List<CheckoutDataResponse.DataBean.ProductsBean> productsBeanList = dataBean.getProducts();
                        List<CheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeanList = dataBean.getPaymentGateway();


                        if (dataBean.getAddress().equals(false)) {
                            addAddress_txt.setVisibility(View.VISIBLE);
                        } else {
                            delivery_ll.setVisibility(View.VISIBLE);

                            if (PaymentDetails.this.activity.equalsIgnoreCase("ChooseAddressAdapter")) {

                                delAddress();
                            } else {
                                for (int i = 0; i < addressBeanList.size(); i++) {
                                    if (addressBeanList.get(i).getDeliveryCharges().equals("0"))
                                        delivery_charges = addressBeanList.get(i).getDeliveryCharges();
                                    else {
                                        Double charge = Double.parseDouble(addressBeanList.get(i).getDeliveryCharges());
                                        delivery_charges = String.valueOf(charge);
                                    }
                                    dileverytxt.setText(delivery_charges);
                                    Log.d(TAG, "onResponse: "+delivery_charges+"___"+addressBeanList.get(i).getId());
                                    delivery_detail_txt.setText(addressBeanList.get(i).getName() + "\n" + addressBeanList.get(i).getAddressLine1() + "," + addressBeanList.get(i).getAddressLine2() + "," + addressBeanList.get(i).getArea() + "," + addressBeanList.get(i).getCity() + "," + addressBeanList.get(i).getState() + "," + addressBeanList.get(i).getPincode() + "\n" + contact_no + "\n" + addressBeanList.get(i).getAlternateContactNo());

                                }

                            }




                            orderCloseStatus = dataBean.getEnableOrderClosing();
                            orderCloseStatText = dataBean.getOrderClosingTxt();
                            if (orderCloseStatus == 1) {
                                sd_orderClose = new SweetAlertDialog(PaymentDetails.this, SweetAlertDialog.ERROR_TYPE);
                                sd_orderClose.setTitleText("Oop's Sorry....!");
                                sd_orderClose.setContentText(orderCloseStatText);
                                sd_orderClose.setConfirmText("OK");
                                sd_orderClose.showCancelButton(true);
                                sd_orderClose.setCanceledOnTouchOutside(false);
                                sd_orderClose.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                        Intent order_close = new Intent(PaymentDetails.this, MainActivity.class);
                                        startActivity(order_close);
                                        finish();
                                    }
                                });
                                sd_orderClose.show();

                            }

                            expected_delevry_date.setText(expected_slot);

                            final_amount = Double.parseDouble(String.valueOf(dataBean.getFinalTotal()));   //200.00
                            SharedPreferences preferences = getSharedPreferences("amount", 0);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putInt("finalamount", final_amount.intValue());
                            editor.apply();

                            final_discount = Double.parseDouble(String.valueOf(dataBean.getFinalDiscount()));
                            pricetxt.setText("" + Math.round(final_amount) + "/-");
                            discounttxt.setText(String.valueOf(final_discount));



                            if (activity.equalsIgnoreCase("onactivity")){

                                PromoCodeCalculation(total_price_after_coupon);
                            }else if (activity.equalsIgnoreCase("activityRemoveCoupan")){
                                removeCoupenCode(total_price_after_coupon);
                            }else {
                                NormalCalculation();
                            }

                            paymentTypeAdapter = new PaymentTypeAdapter(PaymentDetails.this, paymentGatewayBeanList, R.layout.row_pay_type,paymentgatewayId);
                            payment_type_recyclerview.setHasFixedSize(true);

                            payment_type_recyclerview.setLayoutManager(new LinearLayoutManager(PaymentDetails.this, LinearLayoutManager.VERTICAL, false));
                            payment_type_recyclerview.setItemAnimator(new DefaultItemAnimator());
                            payment_type_recyclerview.setAdapter(paymentTypeAdapter);

                        }

                    } else if (checkoutDataResponse.getStatus().equals("10200")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, checkoutDataResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CheckoutDataResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    Toast.makeText(PaymentDetails.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(PaymentDetails.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    private  void  removeCoupenCode(double total_after_remove_coupan){
        if (isWallet){
            // wallet calculation
            if (walletPrice>0.0){
                cbWallet.setChecked(true);

                if (walletPrice>total_after_remove_coupan){
                    total_after_remove_coupan = total_after_remove_coupan+0.00;
                    paymentgatewayId = "4";
                    payment_ll.setVisibility(View.GONE);

                }else if (walletPrice<total_after_remove_coupan){
                    total_after_remove_coupan = total_after_remove_coupan-walletPrice;
                    paymentgatewayId = "";
                    payment_ll.setVisibility(View.VISIBLE);
                }

            }
        }else {
            paymentgatewayId = "";
            payment_ll.setVisibility(View.VISIBLE);
        }
        Totalprice = total_after_remove_coupan;
        totlaPricetxt.setText("" + new DecimalFormat("##.##").format(Totalprice) + "/-");
        total_btn.setText("Total: " + new DecimalFormat("##.##").format(Totalprice) + "/-");

    }

    private void PromoCodeCalculation(Double total_price_after_coupon){
        if (isWallet){
            // wallet calculation
            if (walletPrice>0.0){
                cbWallet.setChecked(true);

                if (walletPrice> total_price_after_coupon){
                    total_price_after_coupon = total_price_after_coupon +0.00;
                    paymentgatewayId = "4";
                    payment_ll.setVisibility(View.GONE);

                }else if (walletPrice< total_price_after_coupon){
                    total_price_after_coupon = total_price_after_coupon -walletPrice;
                    paymentgatewayId = "";
                    payment_ll.setVisibility(View.VISIBLE);
                }

            }
        }else {
            paymentgatewayId = "";
            payment_ll.setVisibility(View.VISIBLE);
        }
        Totalprice = total_price_after_coupon;
        totlaPricetxt.setText("" + new DecimalFormat("##.##").format(Totalprice) + "/-");                   //
        total_btn.setText("Total: " + new DecimalFormat("##.##").format(Totalprice) + "/-");
    }

    private void NormalCalculation(){



        if (delivery_charges != null)
            Totalprice = (final_amount + Double.valueOf(delivery_charges)) ;
        else
            Totalprice = (final_amount + Double.valueOf(0)) ;


        if (isWallet){
            // wallet calculation
            if (walletPrice>0.0){
                cbWallet.setChecked(true);

                if (walletPrice>Totalprice){
                    Totalprice = Totalprice+0.00;
                    paymentgatewayId = "4";
                    payment_ll.setVisibility(View.GONE);

                }else if (walletPrice<Totalprice){
                    Totalprice = Totalprice-walletPrice;
                    paymentgatewayId = "";
                    payment_ll.setVisibility(View.VISIBLE);
                }

            }
        }else {
            paymentgatewayId = "";
            payment_ll.setVisibility(View.VISIBLE);
        }


        totlaPricetxt.setText("" + new DecimalFormat("##.##").format(Totalprice) + "/-");
        total_btn.setText("Total: " + new DecimalFormat("##.##").format(Totalprice) + "/-");

    }

    private void doCheckoutPayment() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            if (coupon_code == null) {
                coupon = "";
            } else {
                coupon=coupon_code;
            }

            if (paymentgatewayId.equalsIgnoreCase("4")){

                paymentgatewayId = "4";
            }
            else {
                paymentgatewayId = paymentTypeAdapter.pay_id;
            }

            Instructions=edt_instructions.getText().toString().trim();
            Log.d(TAG, "doCheckoutPayment: "+paymentgatewayId);
            Call<PlaceOrderResponse> call = RetrofitClient.getInstance().getApi().PlaceOrder(token,coupon, paymentgatewayId, "ANDROID", user_id, addressid, slotId,Instructions,wallet_Used);
            call.enqueue(new Callback<PlaceOrderResponse>() {
                @Override
                public void onResponse(Call<PlaceOrderResponse> call, retrofit2.Response<PlaceOrderResponse> response) {
                    if (response.isSuccessful()) ;
                    final PlaceOrderResponse placeOrderResponse = response.body();
                    dataBean=placeOrderResponse.getData();
                    if (placeOrderResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();
                        sd = new SweetAlertDialog(PaymentDetails.this, SweetAlertDialog.SUCCESS_TYPE);
                        sd.setTitleText("Thank You!!");
                        sd.setContentText("Order has been confirmed.!");
                        sd.setConfirmText("OK");
                        sd.showCancelButton(true);
                        sd.setCanceledOnTouchOutside(false);
                        sd.setConfirmClickListener(sweetAlertDialog -> {
                            Intent order_detail = new Intent(PaymentDetails.this, OrderHistoryDetailActivity.class);
                            order_detail.putExtra("ORDERID", dataBean.getOrderId());
                            startActivity(order_detail);
                            finish();
                        });
                        sd.show();

                    }
                    else if (placeOrderResponse.getStatus().equals("10300")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, "Unable to process your request..! ", Toast.LENGTH_LONG).show();
                    }

                    else if (placeOrderResponse.getStatus().equals("10400")) {
                        pprogressDialog.dismiss();

                            if (paymentTypeAdapter.pay_id.equalsIgnoreCase("6")) {
                            Log.d(TAG, "onResponse:" + dataBean.getPaymentUrl());
                            Intent order_detail = new Intent(PaymentDetails.this, RazerPayActivity.class);
                            //order_detail.putExtra("payurl", dataBean.getPayment_url());
                            order_detail.putExtra("ORDERID", dataBean.getOrderId());
                            // order_detail.putExtra("TOTAL", dataBean.getAmount());
                            order_detail.putExtra("TOTAL", Totalprice);
                            order_detail.putExtra("ENCRYPTORDERID", dataBean.getEncryptedOrderid());
                            order_detail.putExtra("ORDER_REF_NO", dataBean.getOrderRefNo());
                            order_detail.putExtra("ACTIVITY", "OrderActivity");
                            Log.d(TAG, "onResponse: " + dataBean.getEncryptedOrderid());
                            startActivity(order_detail);
                            finish();
                        }else {
                            Intent order_detail = new Intent(PaymentDetails.this, PaymentWebViewActivity.class);
                            order_detail.putExtra("payurl", dataBean.getPaymentUrl());
                            order_detail.putExtra("ORDERID", dataBean.getOrderId());
                            // order_detail.putExtra("TOTAL", dataBean.getAmount());
                            order_detail.putExtra("TOTAL", Totalprice);
                            Log.d(TAG, "onResponse: " + dataBean.getEncryptedOrderid());
                            startActivity(order_detail);
                            finish();
                        }
                    }

                    else if (placeOrderResponse.getStatus().equals("10500")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, "" + placeOrderResponse.getMessage(), Toast.LENGTH_LONG).show();

                    } else if (placeOrderResponse.getStatus().equals("10600")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, "Coupon is invalid..!", Toast.LENGTH_LONG).show();
                    } else if (placeOrderResponse.getStatus().equals("10700")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, placeOrderResponse.getMessage(), Toast.LENGTH_LONG).show();
                    } else if (placeOrderResponse.getStatus().equals("10110")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, placeOrderResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<PlaceOrderResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    Toast.makeText(PaymentDetails.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(PaymentDetails.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }


    private void doPreOrderCheckoutPayment() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            pprogressDialog.show();

            if (coupon_code == null) {
                coupon = "";
            } else {
                coupon=coupon_code;
            }

            if (paymentgatewayId.equalsIgnoreCase("4")){

                paymentgatewayId = "4";
            }
            else {
                paymentgatewayId = preOrderPaymentTypeAdapter.pay_id;
            }

            Call<PreOrderPlaceResponse> call=RetrofitClient.getInstance().getApi().PreOrderPlace(token,deviceId,user_id,addressid,coupon,paymentgatewayId,"ANDROID");
            call.enqueue(new Callback<PreOrderPlaceResponse>() {
                @Override
                public void onResponse(Call<PreOrderPlaceResponse> call, retrofit2.Response<PreOrderPlaceResponse> response) {
                    if (response.isSuccessful());
                    PreOrderPlaceResponse preOrderPlaceResponse=response.body();
                    if (preOrderPlaceResponse.getStatus().equals("10100")){
                        pprogressDialog.dismiss();

                        preOrderPlaceResponseData=preOrderPlaceResponse.getData();

                        sd = new SweetAlertDialog(PaymentDetails.this, SweetAlertDialog.SUCCESS_TYPE);
                        sd.setTitleText("Thank You!!");
                        sd.setContentText("Order has been confirmed.!");
                        sd.setConfirmText("OK");
                        sd.showCancelButton(true);
                        sd.setCanceledOnTouchOutside(false);
                        sd.setConfirmClickListener(sweetAlertDialog -> {
                            Intent order_detail = new Intent(PaymentDetails.this, PreOrderHistoryDetailsActivity.class);
                            order_detail.putExtra("PREORDERID", String.valueOf(preOrderPlaceResponseData.getOrderId()));

                            startActivity(order_detail);
                            finish();
                        });
                        sd.show();

                    }

                    else if (preOrderPlaceResponse.getStatus().equals("10300")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, preOrderPlaceResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    else if (preOrderPlaceResponse.getStatus().equals("10400")) {

                        pprogressDialog.dismiss();
                        if (paymentgatewayId.equalsIgnoreCase("6")) {
                            preOrderPlaceResponseData=preOrderPlaceResponse.getData();

                            Totalprice  = (double) preOrderPlaceResponseData.getAmount();
                            Log.d(TAG, "onResponse: "+preOrderPlaceResponseData.getAmount());
                            Intent order_detail = new Intent(PaymentDetails.this, RazerPayActivity.class);
                            order_detail.putExtra("ORDERID", preOrderPlaceResponseData.getOrderId());
                            order_detail.putExtra("TOTAL", Totalprice);
                            order_detail.putExtra("ENCRYPTORDERID", preOrderPlaceResponseData.getEncryptedOrderid());
                            order_detail.putExtra("ORDER_REF_NO", preOrderPlaceResponseData.getOrderRefNo());
                           // order_detail.putExtra("ACTIVITY", "PaymentDetails");
                            order_detail.putExtra("ACTIVITY", "PreOrderActivity");
                            Log.d(TAG, "onResponse: " + preOrderPlaceResponseData.getEncryptedOrderid());
                            startActivity(order_detail);
                            finish();
                        }
                    }
                    else if (preOrderPlaceResponse.getStatus().equals("10500")) {
                        Toast.makeText(PaymentDetails.this, "" + preOrderPlaceResponse.getMessage(), Toast.LENGTH_LONG).show();

                    } else if (preOrderPlaceResponse.getStatus().equals("10600")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, preOrderPlaceResponse.getMessage(), Toast.LENGTH_LONG).show();
                    } else if (preOrderPlaceResponse.getStatus().equals("10700")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, preOrderPlaceResponse.getMessage(), Toast.LENGTH_LONG).show();
                    } else if (preOrderPlaceResponse.getStatus().equals("10110")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(PaymentDetails.this, preOrderPlaceResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<PreOrderPlaceResponse> call, Throwable t) {

                }
            });

        } else {
            Toast.makeText(PaymentDetails.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }


    private void getOTPMobileVerified() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            Call<BaseResponse> call1 = RetrofitClient.getInstance().getApi().ResendOtpRequest(deviceId, contact_no);
            call1.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                    if (response.isSuccessful()) ;
                    BaseResponse resendOtpResponse = response.body();

                    if (resendOtpResponse.getStatus().equals("10100")) {

                        Toast.makeText(PaymentDetails.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (resendOtpResponse.getStatus().equals("10200")) {

                        Toast.makeText(PaymentDetails.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (resendOtpResponse.getStatus().equals("10300")) {

                        Toast.makeText(PaymentDetails.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (resendOtpResponse.getStatus().equals("10400")) {

                        Toast.makeText(PaymentDetails.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {

                    Toast.makeText(PaymentDetails.this, t.getMessage(), Toast.LENGTH_SHORT).show();


                }
            });
        }
        else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
