package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.PreOrderCartResponse;
import in.innasoft.gofarmz.adapters.CartRecordChild;
import in.innasoft.gofarmz.adapters.PreOrderCartAdapter;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class PreOrderCartActivity extends AppCompatActivity implements View.OnClickListener {

    Button total_btn, proceed_btn;
    RecyclerView preorder_cart_recyclerview;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    Double finalTotal;
    PreOrderCartAdapter preOrderCartAdapter;
    ImageView close_img, no_order_img;
    ArrayList<Object> recordList = new ArrayList<Object>();
    ArrayList<Object> recordChildList = new ArrayList<Object>();
    CartRecordChild cartRecordChild;
    String from = "", pid = "", PreOrder;
    private boolean checkInternet;
    RotateLoading progress_indicator;
    TextView progress_dialog_txt, former_title_txt;
    ProgressDialog pprogressDialog;
    Gson gson;
    PreOrderCartResponse preOrderCartResponse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preorder_cart);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        pprogressDialog = new ProgressDialog(PreOrderCartActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        progress_dialog_txt = findViewById(R.id.progress_dialog_txt);
        no_order_img = findViewById(R.id.no_order_img);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        former_title_txt = findViewById(R.id.former_title_txt);
        total_btn = findViewById(R.id.total_btn);
        //total_btn.setTypeface(bold);
        proceed_btn = findViewById(R.id.proceed_btn);
        //proceed_btn.setTypeface(bold);
        proceed_btn.setOnClickListener(this);
        from = getIntent().getStringExtra("from");
        pid = getIntent().getStringExtra("pid");

        preorder_cart_recyclerview = findViewById(R.id.preorder_cart_recyclerview);
        getPreOrderCartData();

    }

    public void getPreOrderCartData() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<PreOrderCartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().PreOrderCartData(token, deviceId, user_id);
            cartResponseCall.enqueue(new Callback<PreOrderCartResponse>() {
                @Override
                public void onResponse(Call<PreOrderCartResponse> call, retrofit2.Response<PreOrderCartResponse> response) {
                    if (response.isSuccessful()) ;
                    PreOrderCartResponse preOrderCartResponse = response.body();
                    if (preOrderCartResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();
                        no_order_img.setVisibility(View.GONE);

                        List<PreOrderCartResponse.DataBean> dataBeanList = preOrderCartResponse.getData();

                        total_btn.setText("Total : " + String.valueOf(preOrderCartResponse.getTotal()));

                        preorder_cart_recyclerview.setHasFixedSize(true);
                        preorder_cart_recyclerview.setLayoutManager(new LinearLayoutManager(PreOrderCartActivity.this, LinearLayoutManager.VERTICAL, false));
                        preorder_cart_recyclerview.setItemAnimator(new DefaultItemAnimator());
                        preOrderCartAdapter = new PreOrderCartAdapter(PreOrderCartActivity.this, dataBeanList, R.layout.dummy_new_row_preorder_cart);
                        preorder_cart_recyclerview.setAdapter(preOrderCartAdapter);


                    }
                    else if (preOrderCartResponse.getStatus().equals("10200")) {
                        no_order_img.setVisibility(View.VISIBLE);

                        Toast.makeText(getApplicationContext(),preOrderCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (preOrderCartResponse.getStatus().equals("10300")) {

                        pprogressDialog.dismiss();
                        preOrderCartAdapter.notifyDataSetChanged();
                        preorder_cart_recyclerview.setVisibility(View.GONE);
                        no_order_img.setVisibility(View.VISIBLE);
                         Toast.makeText(getApplicationContext(), preOrderCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (preOrderCartResponse.getStatus().equals("10400")) {
                        no_order_img.setVisibility(View.VISIBLE);
                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), preOrderCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<PreOrderCartResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(PreOrderCartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == proceed_btn) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
               /* Intent intent = new Intent(MyCartActivity.this, PaymentDetails.class);
                intent.putExtra("activity", "MyCartActivity");
                startActivity(intent);*/

                String totalprice = total_btn.getText().toString();
                Log.d("TOTOTO", totalprice);
                String[] split = totalprice.split(":");
                int total = Integer.parseInt(split[1].trim());

                /*if (total >= 300)
                {*/
                Intent intent = new Intent(PreOrderCartActivity.this, DeliverySlotsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("activity", "PreOrderCartActivity");
                //intent.putExtra("activity", "PreOrderProductViewActivity");
                startActivity(intent);
                /*}
                else {
                    Toast.makeText(this, "Minimun order is 300/-", Toast.LENGTH_SHORT).show();
                }*/


            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == close_img) {
            onBackPressed();
        }

    }

    public void refreshCartPage() {
        preOrderCartAdapter.notifyDataSetChanged();
        getPreOrderCartData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreOrderCartData();
    }

}
