package in.innasoft.gofarmz.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.AddressGetResponse;
import in.innasoft.gofarmz.Response.DeliverSlotResponse;
import in.innasoft.gofarmz.adapters.DeliverySlotAdapter;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class DeliverySlotsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "DeliverySlotsActivity";
    ImageView close_img;
    TextView textDelivry_slot, delivery_op_toolbar_title, addAddress_txt, delivery_title_txt, delivery_detail_txt, manage_address_txt,
            delivry_slot_title_txt, delivry_charge_txt, delivry_chargetitle_txt;
    RelativeLayout delivery_ll;
    Button proceed_toPay_btn;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, delivery_charges, adress_id, activity, id;
    private boolean checkInternet;
    ProgressDialog pDialog;
    ArrayList<Object> defaultAddressList = new ArrayList<Object>();
    ArrayList<Object> delivrySlot = new ArrayList<>();
    ArrayList<String> simplesendList = new ArrayList<String>();
    AlertDialog dialog;
    DeliverySlotAdapter delivrySlotAdapter;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    String from = "", contact_no, sendidd, sendstartTime, sendendTime, senddate, slotString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_slots);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        checkInternet = NetworkChecking.isConnected(this);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        from = getIntent().getStringExtra("from");
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);
        checkInternet = NetworkChecking.isConnected(this);
        pDialog = new ProgressDialog(DeliverySlotsActivity.this);
        pDialog.setMessage("Please wait......");
        pDialog.setProgressStyle(R.style.DialogTheme);


        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        delivery_op_toolbar_title = findViewById(R.id.delivery_op_toolbar_title);


        proceed_toPay_btn = findViewById(R.id.proceed_toPay_btn);
        proceed_toPay_btn.setOnClickListener(this);



        delivery_ll = findViewById(R.id.delivery_ll);

        textDelivry_slot = findViewById(R.id.textDelivry_slot);
        textDelivry_slot.setOnClickListener(this);

        addAddress_txt = findViewById(R.id.addAddress_txt);

        addAddress_txt.setOnClickListener(this);
        delivery_title_txt = findViewById(R.id.delivery_title_txt);

        delivery_detail_txt = findViewById(R.id.delivery_detail_txt);

        manage_address_txt = findViewById(R.id.manage_address_txt);

        manage_address_txt.setOnClickListener(this);
        delivry_slot_title_txt = findViewById(R.id.delivry_slot_title_txt);


        delivry_chargetitle_txt = findViewById(R.id.delivry_chargetitle_txt);


        delivry_charge_txt = findViewById(R.id.delivry_charge_txt);


        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        activity = bundle.getString("activity");
        Log.d("ActivityName", activity);
        assert activity != null;
        if (activity.equalsIgnoreCase("ChooseAddressAdapter")) {

            delivery_ll.setVisibility(View.VISIBLE);
            id = bundle.getString("id");
            contact_no = bundle.getString("contact_no");
            //    addID = bundle.getString("id");
            //delAddress();
            Log.d("DeliverySlotOrder", id + "/" + contact_no);
        }

        if (activity.equalsIgnoreCase("MyCartActivity")) {

            delivry_slot_title_txt.setVisibility(View.VISIBLE);
            textDelivry_slot.setVisibility(View.VISIBLE);
            delivery_ll.setVisibility(View.VISIBLE);
        }

        if (activity.equalsIgnoreCase("PreOrderCartActivity") || activity.equalsIgnoreCase("PreOrderProductViewActivity")) {

            Log.d("DeliverySlotPreOrder", id + "/" + contact_no);
            delivry_slot_title_txt.setVisibility(View.GONE);
            delivry_slot_title_txt.setVisibility(View.GONE);
            textDelivry_slot.setVisibility(View.GONE);
        }

        if (activity.equalsIgnoreCase("Order")) {
            delivry_slot_title_txt.setVisibility(View.VISIBLE);
            textDelivry_slot.setVisibility(View.VISIBLE);
            addAddress_txt.setVisibility(View.GONE);
            delivery_ll.setVisibility(View.VISIBLE);
        }
        if (activity.equalsIgnoreCase("PreOrder")) {
            delivry_slot_title_txt.setVisibility(View.GONE);
            textDelivry_slot.setVisibility(View.GONE);
            addAddress_txt.setVisibility(View.GONE);
            delivery_ll.setVisibility(View.GONE);

            id = bundle.getString("id");
            adress_id = id;
            contact_no = bundle.getString("contact_no");
        }

        if (activity.equalsIgnoreCase("ChooseAddress")) {
            /*delivry_slot_title_txt.setVisibility(View.GONE);
            textDelivry_slot.setVisibility(View.GONE);
            addAddress_txt.setVisibility(View.GONE);
            delivery_ll.setVisibility(View.GONE);*/

            id = bundle.getString("id");
            contact_no = bundle.getString("contact_no");
        }


        getDeliveryOptData();
    }

    public void getDeliveryOptData() {
        if (checkInternet) {
            pDialog.show();

            Call<DeliverSlotResponse> call = RetrofitClient.getInstance().getApi().deliverySlot(token,deviceId,user_id);
            call.enqueue(new Callback<DeliverSlotResponse>() {
                @Override
                public void onResponse(Call<DeliverSlotResponse> call, retrofit2.Response<DeliverSlotResponse> response) {
                    pDialog.dismiss();

                    DeliverSlotResponse loginResponse = response.body();

                    if (loginResponse.getStatus().equals("10100")) {

                      //  List<DeliverSlotResponse.DataBean.AddressBean> addressBeanList = loginResponse.getData().getAddress();
                        //List<DeliverSlotResponse.DataBean.AvailableDeliverySlotsBean> available_delivery_slots = loginResponse.getData().getAvailable_delivery_slots();


                        if (loginResponse.getData().getAddress() instanceof Boolean){

                            addAddress_txt.setVisibility(View.VISIBLE);
                        }else {

                            delivery_ll.setVisibility(View.VISIBLE);
                            defaultAddressList = (ArrayList<Object>) loginResponse.getData().getAddress();
                            Map<String, Object> recordPos = (Map<String, Object>) defaultAddressList.get(0);
                            adress_id = (String) recordPos.get("id");
                            String name = (String) recordPos.get("name");
                            String address_line1 = (String) recordPos.get("address_line1");
                            String address_line2 = (String) recordPos.get("address_line2");
                            String area = (String) recordPos.get("area");
                            String city = (String) recordPos.get("city");
                            String state = (String) recordPos.get("state");
                            String pincode = (String) recordPos.get("pincode");
                            contact_no = (String) recordPos.get("contact_no");
                            String alternate_contact_no = (String) recordPos.get("alternate_contact_no");
                            boolean delivery_status = (Boolean) recordPos.get("delivery_status");


                            if (activity.equalsIgnoreCase("ChooseAddressAdapter") ||
                                    activity.equalsIgnoreCase("ChooseAddress") || activity.equalsIgnoreCase("PreOrder")) {
                                delAddress();
                            } else {


                                if (recordPos.get("delivery_charges") instanceof String)
                                    delivery_charges = (String) recordPos.get("delivery_charges");
                                else {
                                    Double charge = (Double) recordPos.get("delivery_charges");
                                    delivery_charges = String.valueOf(charge);
                                }
                                delivry_charge_txt.setText(delivery_charges + "/-");
                                delivery_detail_txt.setText(name + "\n" + address_line1 + "," + address_line2 + "," + area + "," + city + "," + state + "," + pincode + "\n" + contact_no + "\n" + alternate_contact_no);
                            }
                        }

                        ArrayList<Object> tmpList = (ArrayList<Object>) loginResponse.getData().getAvailable_delivery_slots();
                        Log.d("XXXX", tmpList.toString());
                        delivrySlot =tmpList ;


                    } else  if (loginResponse.getStatus().equalsIgnoreCase("10200")) {

                        pDialog.dismiss();
                        Toast.makeText(DeliverySlotsActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                    }
                    else if (loginResponse.getStatus().equalsIgnoreCase("10400")) {
                        pDialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<DeliverSlotResponse> call, Throwable t) {
                    pDialog.dismiss();
                }
            });

        } else {
            Toast.makeText(DeliverySlotsActivity.this, "No Internet COnnection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {
        if (v == close_img) {
         /*  if (activity.equalsIgnoreCase("PreOrderCartActivity")){
               Intent intent = new Intent(DeliverySlotsActivity.this, PreOrderCartActivity.class);
               startActivity(intent);
           }
           else {
               Intent intent = new Intent(DeliverySlotsActivity.this, MyCartActivity.class);
               intent.putExtra("from","Home");
               startActivity(intent);
           }*/
            onBackPressed();

        }


        if (v == textDelivry_slot) {
            getDataDialog();
        }

        if (v == proceed_toPay_btn) {

            if (delivery_detail_txt.getText().toString().equalsIgnoreCase(" ") ||
                    delivery_detail_txt.getText().toString() == null ||
                    delivery_detail_txt.getText().toString().isEmpty()) {

                Toast.makeText(this, "Please Select Address..!", Toast.LENGTH_SHORT).show();

            } else if (activity.equalsIgnoreCase("PreOrderCartActivity") || activity.equalsIgnoreCase("PreOrder")) {

                delivry_slot_title_txt.setVisibility(View.GONE);
                textDelivry_slot.setVisibility(View.GONE);

                Intent intent = new Intent(DeliverySlotsActivity.this, PaymentDetails.class);
                intent.putExtra("SLOT_ID", sendidd);
                intent.putExtra("SlotString", slotString);
                intent.putExtra("activity", activity);

                if (activity.equalsIgnoreCase("ChooseAddressAdapter")) {
                    intent.putExtra("ADD_ID", id);
                    intent.putExtra("contact_no", contact_no);

                } else if (activity.equalsIgnoreCase("PreOrder") || activity.equalsIgnoreCase("ChooseAddress")) {
                    intent.putExtra("ADD_ID", id);
                    intent.putExtra("contact_no", contact_no);
                } else {
                    intent.putExtra("ADD_ID", adress_id);
                    intent.putExtra("contact_no", contact_no);
                }
                startActivity(intent);

            } else {

                if (addAddress_txt.getVisibility() == View.VISIBLE) {
                    Toast.makeText(DeliverySlotsActivity.this, "Add Your delivery Address..!", Toast.LENGTH_SHORT).show();
                } else if (textDelivry_slot.getText().toString() == "" || textDelivry_slot.getText().equals("Select Slots")) {
                    Toast.makeText(DeliverySlotsActivity.this, "Please Select Delivery Slot...!", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(DeliverySlotsActivity.this, PaymentDetails.class);
                    intent.putExtra("activity", "DeliverySlotActivity");
                    intent.putExtra("SLOT_ID", sendidd);  //slot_id
                    intent.putExtra("SlotString", slotString);

                    if (activity.equalsIgnoreCase("ChooseAddressAdapter")) {
                        intent.putExtra("ADD_ID", id);
                        intent.putExtra("contact_no", contact_no);
                    } else {
                        intent.putExtra("ADD_ID", adress_id);
                        intent.putExtra("contact_no", contact_no);
                    }
                    startActivity(intent);
                }
            }


        }
        if (v == manage_address_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                if (checkLocationPermission()) {
                    Intent intent = new Intent(DeliverySlotsActivity.this, ChooseAddressActivity.class);
                    intent.putExtra("activity", activity);
                    startActivity(intent);
                }
            } else {
                Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == addAddress_txt) {
            if (checkLocationPermission()) {
                if (activity.equalsIgnoreCase("ChooseAddressAdapter")) {
                    Intent intent = new Intent(DeliverySlotsActivity.this, TestActivity.class);
                    intent.putExtra("activity", "ChooseAddressAdapter");
                    startActivity(intent);
                } else if (activity.equalsIgnoreCase("PreOrderCartActivity") ||
                        activity.equalsIgnoreCase("PreOrderProductViewActivity")) {
                    Intent intent = new Intent(DeliverySlotsActivity.this, TestActivity.class);
                    intent.putExtra("activity", "PreOrderCartActivity");
                    startActivity(intent);
                } else if (activity.equalsIgnoreCase("MyCartActivity")) {
                    Intent intent = new Intent(DeliverySlotsActivity.this, TestActivity.class);
                    intent.putExtra("activity", "MyCartActivity");
                    startActivity(intent);
                }
                /*Intent intent = new Intent(DeliverySlotsActivity.this, TestActivity.class);
                startActivity(intent);*/
            }
        }
    }

    private void getDataDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(DeliverySlotsActivity.this);
        LayoutInflater inflater = DeliverySlotsActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.delivery_slot_dialog, null);


        final ImageView close_dialog = dialog_layout.findViewById(R.id.close_dialog);
        RecyclerView delivery_slot_recyclerview = dialog_layout.findViewById(R.id.delivery_slot_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        delivery_slot_recyclerview.setLayoutManager(layoutManager);

        Log.d("LISTHOME", delivrySlot.toString());
        delivrySlotAdapter = new DeliverySlotAdapter(DeliverySlotsActivity.this, delivrySlot, R.layout.row_delivery_slot);
        delivery_slot_recyclerview.setAdapter(delivrySlotAdapter);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        builder.setView(dialog_layout);

        dialog = builder.create();
        dialog.show();


    }


    public void sendValue(String idd, String strtTime, String endTime, String date) {
        dialog.dismiss();

        sendidd = idd;
        sendstartTime = strtTime;
        sendendTime = endTime;
        senddate = date;
        textDelivry_slot.setText(" " + senddate + "   " + sendstartTime + " -- " + sendendTime);
        slotString = " " + senddate + "   " + sendstartTime + " -- " + sendendTime;
        Log.d("DATAAAT", sendidd + "//////" + sendstartTime + "/////" + sendendTime + "////" + senddate);
    }

    private boolean checkLocationPermission() {
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }


        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);

            return false;
        }
        return true;
    }

    private void delAddress() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pDialog.show();

            Call<AddressGetResponse> call=RetrofitClient.getInstance().getApi().GetAddress(token,deviceId,user_id,id);
            call.enqueue(new Callback<AddressGetResponse>() {
                @Override
                public void onResponse(Call<AddressGetResponse> call, retrofit2.Response<AddressGetResponse> response) {
                    if (response.isSuccessful());
                    AddressGetResponse addressGetResponse=response.body();
                    if (addressGetResponse.getStatus().equals("10100")){
                        pDialog.dismiss();
                        delivery_ll.setVisibility(View.VISIBLE);

                        AddressGetResponse.DataBean dataBean=addressGetResponse.getData();

                        String deliveryaddress = dataBean.getName() + "\n" + dataBean.getAddress_line1() + "," + dataBean.getAddress_line2() + "," + dataBean.getArea() + "," + dataBean.getCity() + "," + dataBean.getCity() + "," + dataBean.getCity() + "," + dataBean.getPincode() + "\n" + dataBean.getContact_no() + "\n" + dataBean.getAlternate_contact_no();
                        delivery_detail_txt.setText(deliveryaddress);
                        delivry_charge_txt.setText(dataBean.getDelivery_charges());


                    }

                    if (addressGetResponse.getStatus().equals("10200")) {

                        pDialog.dismiss();
                        delivery_ll.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                    }

                    if (addressGetResponse.getStatus().equals("10300")) {

                        pDialog.dismiss();
                        delivery_ll.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "No Data Found..", Toast.LENGTH_SHORT).show();
                    }

                    if (addressGetResponse.getStatus().equals("10400")) {

                        pDialog.dismiss();
                        delivery_ll.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddressGetResponse> call, Throwable t) {
                    pDialog.dismiss();
                    Toast.makeText(DeliverySlotsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);

                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);

                    if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (activity.equalsIgnoreCase("ChooseAddressAdapter")) {
                            Intent intent = new Intent(DeliverySlotsActivity.this, TestActivity.class);
                            intent.putExtra("activity", "ChooseAddressAdapter");
                            startActivity(intent);
                        } else if (activity.equalsIgnoreCase("PreOrderCartActivity") ||
                                activity.equalsIgnoreCase("PreOrderProductViewActivity")) {
                            Intent intent = new Intent(DeliverySlotsActivity.this, TestActivity.class);
                            intent.putExtra("activity", "PreOrderCartActivity");
                            startActivity(intent);
                        }

                    } else {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK("Location Permissions Required For This App", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkLocationPermission();
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            break;
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(this, "Goto Settings And Enable Permissions", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(this).setMessage(message).setPositiveButton("OK", okListener).create().show();
    }
}
