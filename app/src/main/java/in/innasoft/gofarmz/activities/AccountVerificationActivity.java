package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.victor.loading.rotate.RotateLoading;

import java.util.HashMap;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.LoginResponse;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class AccountVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView otp_txt, txtresend;
    PinView pinView;
    Button verify_btn;

    private boolean checkInternet;
    String send_mobile, deviceId,fromWhere;
    UserSessionManager userSessionManager;
    RotateLoading progress_indicator;
    SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        send_mobile = bundle.getString("mobile");
        fromWhere = bundle.getString("activity");
        Log.d("DDDDD","///"+send_mobile+"///"+fromWhere);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);


        progress_indicator = findViewById(R.id.progress_indicator);
      //  progress_indicator.start();

        otp_txt = findViewById(R.id.otp_txt);
        txtresend = findViewById(R.id.txtresend);
        txtresend.setPaintFlags(txtresend.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        txtresend.setOnClickListener(this);

        pinView = findViewById(R.id.pinView);


        verify_btn = findViewById(R.id.verify_btn);

        verify_btn.setOnClickListener(this);
        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = message.replaceAll("[^0-9]", "");
                pinView.setText(code);
                Log.v("message >>> ", message.replaceAll("[^0-9]", ""));
            }
        });
        //getOTP();
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }


   /* @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }*/

    private void getOTP() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            Call<BaseResponse> call1=RetrofitClient.getInstance().getApi().ResendOtpRequest(deviceId,send_mobile);
            call1.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                    if (response.isSuccessful());
                    BaseResponse resendOtpResponse=response.body();

                    if (resendOtpResponse.getStatus().equals("10100")){
                        progress_indicator.stop();
                        Toast.makeText(AccountVerificationActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (resendOtpResponse.getStatus().equals("10200")){
                        progress_indicator.stop();
                        Toast.makeText(AccountVerificationActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (resendOtpResponse.getStatus().equals("10300")){
                        progress_indicator.stop();
                        Toast.makeText(AccountVerificationActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (resendOtpResponse.getStatus().equals("10400")){
                        progress_indicator.stop();
                        Toast.makeText(AccountVerificationActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    progress_indicator.stop();
                    Toast.makeText(AccountVerificationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();


                }
            });


        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {

        if (v == verify_btn) {
            verifyOtp();
        }

        if (v == txtresend) {
            getOTP();
        }
    }

    private boolean validate() {
        boolean result = true;
        String otp = pinView.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            pinView.setError("Invalid OTP");
            result = false;
        }
        return result;
    }

    public void verifyOtp() {
        checkInternet = NetworkChecking.isConnected(this);
        if (validate()) {

            if (checkInternet) {

                Call<LoginResponse> call= RetrofitClient.getInstance().getApi().userOtpRequest(deviceId,send_mobile,pinView.getText().toString(),deviceId);
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {

                        if (response.isSuccessful());
                        LoginResponse verifyOtpResponse=response.body();

                        if (verifyOtpResponse.getStatus().equals("10100")){

                            progress_indicator.stop();

                            userSessionManager.createUserLoginSession(verifyOtpResponse.getData().getJwt()
                                    , verifyOtpResponse.getData().getUser_id()
                                    , verifyOtpResponse.getData().getUser_name()
                                    , verifyOtpResponse.getData().getEmail()
                                    , verifyOtpResponse.getData().getMobile());


                            if(fromWhere.equalsIgnoreCase("PAYMENTDETAIL"))
                            {
                                Intent intent = new Intent(AccountVerificationActivity.this, PaymentDetails.class);
                                intent.putExtra("activity",fromWhere);
                                startActivity(intent);
                            }
                            else
                            {
                                Intent intent = new Intent(AccountVerificationActivity.this, MainActivity.class);
                                startActivity(intent);
                            }

                            Toast.makeText(getApplicationContext(),"You have been logged in successfully.",Toast.LENGTH_SHORT).show();
                        }

                        else if (verifyOtpResponse.getStatus().equals("10200")){
                            progress_indicator.stop();
                            Toast.makeText(AccountVerificationActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        else if (verifyOtpResponse.getStatus().equals("10300")){
                            progress_indicator.stop();
                            Toast.makeText(AccountVerificationActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        else if (verifyOtpResponse.getStatus().equals("10400")){
                            progress_indicator.stop();
                            Toast.makeText(AccountVerificationActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        progress_indicator.stop();
                        Toast.makeText(AccountVerificationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

            } else {
                Toast.makeText(AccountVerificationActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }
}
