package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.victor.loading.rotate.RotateLoading;

import java.util.HashMap;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.ProfileResponse;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    EditText picode_edt, email_edt;
    RadioGroup rg;
    RadioButton male_rb, female_rb;
    ImageView close_img;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String deviceId, token, user_id, mobile_verufy_status, mobile, email, gender, name, gender_value;
    TextView address_text, user_name, mobile_no, gender_title, logout_text, change_password_text, edit_profile, verify_mob_text;
    Button save_btn;
    ImageView verf_mob_iv;
    RotateLoading progress_indicator;
    RelativeLayout location_layout;
    String fromWhere;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dummy_activity_profile);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();

        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        fromWhere = bundle.getString("activity");

        pprogressDialog = new ProgressDialog(ProfileActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        //  progress_indicator = findViewById(R.id.progress_indicator);
        // progress_indicator.start();

        picode_edt = findViewById(R.id.picode_edt);
        email_edt = findViewById(R.id.email_edt);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        rg = findViewById(R.id.rg);
        rg.setOnCheckedChangeListener(this);
        male_rb = findViewById(R.id.male_rb);
        female_rb = findViewById(R.id.female_rb);

        logout_text = findViewById(R.id.logout_text);
        logout_text.setOnClickListener(this);
        change_password_text = findViewById(R.id.change_password_text);
        change_password_text.setOnClickListener(this);
        edit_profile = findViewById(R.id.edit_profile);
        edit_profile.setOnClickListener(this);

        gender_title = findViewById(R.id.gender_title);
        location_layout = findViewById(R.id.location_layout);
        address_text = findViewById(R.id.address_text);
        user_name = findViewById(R.id.user_name);
        mobile_no = findViewById(R.id.mobile_no);
        mobile_no.setOnClickListener(this);

        save_btn = findViewById(R.id.save_btn);
        save_btn.setOnClickListener(this);

        verf_mob_iv = findViewById(R.id.verf_mob_iv);
        verify_mob_text = findViewById(R.id.verify_mob_text);
        verify_mob_text.setOnClickListener(this);

        getProfileDetails();
    }

    private void getProfileDetails() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<ProfileResponse> call1= RetrofitClient.getInstance().getApi().getUserProfile(token,deviceId,user_id);
            call1.enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(Call<ProfileResponse> call, retrofit2.Response<ProfileResponse> response) {
                    if (response.isSuccessful());
                    pprogressDialog.dismiss();
                    ProfileResponse profileResponse=response.body();

                    if (profileResponse.getStatus().equals("10100")){

                        user_name.setText(profileResponse.getData().getName());
                        if (profileResponse.getData().getGender().equalsIgnoreCase("Male")) {
                            male_rb.setChecked(true);
                            gender_value = "Male";
                        } else if (profileResponse.getData().getGender().equalsIgnoreCase("Female")) {
                            female_rb.setChecked(true);
                            gender_value = "Female";
                        }
                        name = profileResponse.getData().getName();
                        email = profileResponse.getData().getEmail();
                        mobile = profileResponse.getData().getMobile();
                        mobile_verufy_status=profileResponse.getData().getMobile_verify_status();

                        email_edt.setText(email);
                        if (profileResponse.getData().getMobile().equalsIgnoreCase("") || profileResponse.getData().getMobile().equalsIgnoreCase("null")) {
                            mobile_no.setText("Provide Mobile No");

                        } else {
                            mobile_no.setText(mobile);
                            verf_mob_iv.setVisibility(View.VISIBLE);
                            verify_mob_text.setVisibility(View.VISIBLE);

                        }

                        if (profileResponse.getData().getMobile_verify_status().equalsIgnoreCase("1")) {
                            verf_mob_iv.setImageResource(R.drawable.ic_verified);
                            verify_mob_text.setClickable(false);
                            verify_mob_text.setText("verified ");
                        }
                        else if (mobile_verufy_status.equalsIgnoreCase("0")) {
                            String mob = mobile_no.getText().toString();
                            if (!mob.equalsIgnoreCase("Provide Mobile No")) {
                                verify_mob_text.setVisibility(View.VISIBLE);
                                verf_mob_iv.setImageResource(R.drawable.ic_un_verified);
                            } else {

                            }


                        }


                        if (profileResponse.getData().getAddress() != null) {
                            location_layout.setVisibility(View.VISIBLE);
                            String address1 = profileResponse.getData().getAddress().getAddress_line1();
                            String address2 = profileResponse.getData().getAddress().getAddress_line2();
                            String area = profileResponse.getData().getAddress().getArea();
                            String city = profileResponse.getData().getAddress().getCity();
                            String state = profileResponse.getData().getAddress().getState();
                            String country = profileResponse.getData().getAddress().getCountry();
                            String pincode = profileResponse.getData().getAddress().getPincode();
                            address_text.setText(address1 + "," + address2 + "," + area + "," + city + "," + state + "," + country + "," + pincode);
                        }



                       // Toast.makeText(ProfileActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (profileResponse.getStatus().equals("10200")){

                        Toast.makeText(ProfileActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (profileResponse.getStatus().equals("10300")){

                        Toast.makeText(ProfileActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (profileResponse.getStatus().equals("10400")){

                        Toast.makeText(ProfileActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ProfileResponse> call, Throwable t) {

                    Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            pprogressDialog.dismiss();
            //  progress_indicator.stop();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(final View v) {

        if (v == logout_text) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                pprogressDialog.show();

                Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userLogout(token,deviceId,user_id,deviceId);
                call.enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {

                        if (response.isSuccessful()) ;
                        BaseResponse baseResponse = response.body();

                        if (baseResponse.getStatus().equals("10100")) {

                            SharedPreferences preferences = getApplicationContext().getSharedPreferences("LOCATION_PREF", 0);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.apply();
                            editor.clear();

                            userSessionManager.logoutUser();

                            pprogressDialog.dismiss();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                            Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();


                        } else if (baseResponse.getStatus().equals("10200")) {

                            Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        } else if (baseResponse.getStatus().equals("10300")) {

                            Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        } else if (baseResponse.getStatus().equals("10400")) {

                            Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });


            } else {
                Toast.makeText(this, "No Internet connection...!", Toast.LENGTH_SHORT).show();
            }
            //}
        }

        if (v == change_password_text) {
            Intent intent = new Intent(ProfileActivity.this, ChangePassword.class);
            startActivity(intent);
        }

        if (v == edit_profile || v == mobile_no) {
            String mob = mobile_no.getText().toString();
            Intent updateIntent = new Intent(ProfileActivity.this, UpdateProfileActivity.class);
            updateIntent.putExtra("name", name);
            updateIntent.putExtra("email", email);
            if (mob.equalsIgnoreCase("Provide Mobile No")) {
                updateIntent.putExtra("mobile", "");
            } else {
                updateIntent.putExtra("mobile", mobile);
            }


            if (gender_value != null && gender_value.length() > 0)
                updateIntent.putExtra("gender", gender_value);
            startActivity(updateIntent);

        }

        if (v == save_btn) {

            updateProfile();
        }
        if (v == close_img) {

            finish();
        }
        if (v == verify_mob_text) {
            getOTPMobileVerified();

            Intent intent = new Intent(ProfileActivity.this, AccountVerificationActivity.class);
            intent.putExtra("mobile", mobile);
            intent.putExtra("activity", fromWhere);
            startActivity(intent);
            //Resend OTP Service Call
        }
    }

    private void updateProfile() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
           String send_gender = "";
            if (male_rb.isChecked()) {
                send_gender = "Male";
            } else if (female_rb.isChecked()) {
                send_gender = "Female";
            }


            Call<BaseResponse> call = RetrofitClient.getInstance().getApi().updateProfile(token,deviceId,user_id,user_name.getText().toString(),email_edt.getText().toString(),
                    mobile_no.getText().toString(),send_gender);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {

                    if (response.isSuccessful()) ;
                    BaseResponse baseResponse = response.body();

                    if (baseResponse.getStatus().equals("10100")) {

                        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (baseResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.getStatus().equals("10300")) {

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getOTPMobileVerified() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            Call<BaseResponse> call1= RetrofitClient.getInstance().getApi().ResendOtpRequest(deviceId,mobile);
            call1.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                    if (response.isSuccessful());
                    BaseResponse resendOtpResponse=response.body();

                    if (resendOtpResponse.getStatus().equals("10100")){
                        progress_indicator.stop();
                        Toast.makeText(ProfileActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (resendOtpResponse.getStatus().equals("10200")){
                        progress_indicator.stop();
                        Toast.makeText(ProfileActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (resendOtpResponse.getStatus().equals("10300")){
                        progress_indicator.stop();
                        Toast.makeText(ProfileActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (resendOtpResponse.getStatus().equals("10400")){
                        progress_indicator.stop();
                        Toast.makeText(ProfileActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    progress_indicator.stop();
                    Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();


                }
            });


        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.male_rb) {
            male_rb.setChecked(true);
            gender_value = "Male";
        } else {
            female_rb.setChecked(true);
            gender_value = "Female";
        }

    }
}
