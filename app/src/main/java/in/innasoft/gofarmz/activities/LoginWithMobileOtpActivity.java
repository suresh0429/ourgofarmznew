package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.victor.loading.rotate.RotateLoading;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.LoginResponse;
import in.innasoft.gofarmz.utils.Config;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import in.innasoft.gofarmz.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginWithMobileOtpActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.progress_indicator)
    RotateLoading progressIndicator;
    @BindView(R.id.logo_img)
    ImageView logoImg;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.otp_view)
    OtpTextView otpView;
    @BindView(R.id.verify_btn)
    Button verifyBtn;
    @BindView(R.id.txtresend)
    TextView txtresend;
    private boolean checkInternet;
    String deviceId, refreshedToken, otpText,mobile;
    UserSessionManager userSessionManager;
    private String TAG = "LoginWithMobileOtpActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);

        Log.d("TOken ",""+FirebaseInstanceId.getInstance().getToken());
        FirebaseMessaging.getInstance().subscribeToTopic("allDevices");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        //progressIndicator.start();
        if (getIntent() != null) {
            mobile = getIntent().getStringExtra("Mobile_number");

            char first = mobile.charAt(0);
            String substring = mobile.substring(Math.max(mobile.length() - 2, 0));
            textView.setText("Please type the verification code sent to\n +91"+first+"XXXXXXX"+substring);


        }
        verifyBtn.setOnClickListener(this);
        txtresend.setOnClickListener(this);

        otpView.setOtpListener(new OTPListener() {
            @Override
            public void onInteractionListener() {
                // fired when user types something in the Otpbox
            }

            @Override
            public void onOTPComplete(String otp) {
                // fired when user has entered the OTP fully.
                otpText = otp;
               // Toast.makeText(LoginWithMobileOtpActivity.this, "The OTP is " + otp, Toast.LENGTH_SHORT).show();
            }
        });



    }


    @Override
    public void onClick(View view) {

        if (view == verifyBtn) {

            if ((otpText == null || otpText.equals("")) || otpText.length() != 6) {
                Toast.makeText(LoginWithMobileOtpActivity.this, "Enter OTP to Verify", Toast.LENGTH_SHORT).show();
                return;
            } else {

                checkInternet = NetworkChecking.isConnected(this);
                Log.d(TAG, "onClick: " + otpText);
                if (checkInternet) {

                    ProgressDialog progressDialog = new ProgressDialog(LoginWithMobileOtpActivity.this);
                    progressDialog.setMessage("Loading....");
                    progressDialog.show();
                    Call<LoginResponse> call = RetrofitClient.getInstance().getApi().LoginMobileWithOTP(mobile, deviceId, otpText);
                    call.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                            if (response.isSuccessful()) ;
                            LoginResponse verifyOtpResponse = response.body();

                            if (verifyOtpResponse.getStatus().equals("10100")) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "You have been logged in successfully.", Toast.LENGTH_SHORT).show();
                                //progressIndicator.stop();

                                userSessionManager.createUserLoginSession(verifyOtpResponse.getData().getJwt()
                                        , verifyOtpResponse.getData().getUser_id()
                                        , verifyOtpResponse.getData().getUser_name()
                                        , verifyOtpResponse.getData().getEmail()
                                        , verifyOtpResponse.getData().getMobile());

                                Log.d(TAG, "onResponse: " + verifyOtpResponse.getData().getUser_id());

                                Intent intent = new Intent(LoginWithMobileOtpActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                                setDefaults();
                                displayFirebaseRegId(verifyOtpResponse.getData().getUser_id(), verifyOtpResponse.getData().getJwt());

                            } else if (verifyOtpResponse.getStatus().equals("10200")) {
                                progressDialog.dismiss();
                                // progressIndicator.stop();
                                Toast.makeText(LoginWithMobileOtpActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            } else if (verifyOtpResponse.getStatus().equals("10300")) {
                                progressDialog.dismiss();
                                //progressIndicator.stop();
                                Toast.makeText(LoginWithMobileOtpActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            } else if (verifyOtpResponse.getStatus().equals("10400")) {
                                progressDialog.dismiss();
                                //progressIndicator.stop();
                                Toast.makeText(LoginWithMobileOtpActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // progressIndicator.stop();
                            Toast.makeText(LoginWithMobileOtpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                } else {

                    Toast.makeText(LoginWithMobileOtpActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                }
            }
        }

        if (view == txtresend) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {

                ProgressDialog progressDialog = new ProgressDialog(LoginWithMobileOtpActivity.this);
                progressDialog.setMessage("Loading....");
                progressDialog.show();
                Call<BaseResponse> call1 = RetrofitClient.getInstance().getApi().ResendOtpRequest(deviceId, mobile);
                call1.enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful()) ;
                        BaseResponse resendOtpResponse = response.body();

                        if (resendOtpResponse.getStatus().equals("10100")) {
                            progressDialog.dismiss();
                            //progress_indicator.stop();
                            Toast.makeText(LoginWithMobileOtpActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        } else if (resendOtpResponse.getStatus().equals("10200")) {
                            progressDialog.dismiss();
                            //progress_indicator.stop();
                            Toast.makeText(LoginWithMobileOtpActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        } else if (resendOtpResponse.getStatus().equals("10300")) {
                            progressDialog.dismiss();
                            //progress_indicator.stop();
                            Toast.makeText(LoginWithMobileOtpActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        } else if (resendOtpResponse.getStatus().equals("10400")) {
                            progressDialog.dismiss();
                            // progress_indicator.stop();
                            Toast.makeText(LoginWithMobileOtpActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        //progress_indicator.stop();
                        Toast.makeText(LoginWithMobileOtpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();


                    }
                });
            } else {

                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void setDefaults() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true);
        editor.apply();
    }

    private void displayFirebaseRegId(String user_id, String jwt) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        // String regId = pref.getString("regId", null);
        // Fcm Token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("TAG", "getInstanceId failed", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    refreshedToken = task.getResult().getToken();

                    // Log and toast

                    Log.d("TAG", refreshedToken);
                    // Toast.makeText(MainActivity.this, refreshedToken, Toast.LENGTH_SHORT).show();

                    Log.w("notificationToken", "" + refreshedToken);
                    if (!TextUtils.isEmpty(refreshedToken)) {
                        //Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
                        Log.d("FCM", refreshedToken);
                        sendFCMTokes(user_id, refreshedToken, "ANDROID", jwt);
                    } else {
                        Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
                    }

                });


    }

    public void sendFCMTokes(final String user_id, final String token_fcm, final String platform, String jwt) {
        boolean checkInternet = NetworkChecking.isConnected(LoginWithMobileOtpActivity.this);
        if (checkInternet) {

            Call<BaseResponse> call = RetrofitClient.getInstance().getApi().sendFcmToken(jwt, deviceId, user_id, token_fcm);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    BaseResponse loginResponse = response.body();
                    Log.d("Tag", "onResponseFCM: " + loginResponse.getStatus());


                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });



        } else {
            Toast.makeText(LoginWithMobileOtpActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


}
