package in.innasoft.gofarmz.activities;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.victor.loading.rotate.RotateLoading;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.PoliciesViewPagerAdapter;
import in.innasoft.gofarmz.fragments.PrivacyPolicyFragment;
import in.innasoft.gofarmz.fragments.TermsConditionFragment;

public class TermsConditionsAndPrivacy extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img;
    TextView term_condition_tittle_txt;
    RotateLoading progress_indicator;
    PoliciesViewPagerAdapter policiesViewPagerAdapter;
    ViewPager view_pager_faq_termscondition;
    String from;
    TabLayout tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        term_condition_tittle_txt = findViewById(R.id.term_condition_tittle_txt);
        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();

        tab = findViewById(R.id.tabLayout_faq_termscondition);
        view_pager_faq_termscondition = (ViewPager) findViewById(R.id.view_pager_faq_termscondition);

        from = getIntent().getStringExtra("from");
        setupViewPager(view_pager_faq_termscondition);
        tab.setupWithViewPager(view_pager_faq_termscondition);
    }


    private void setupViewPager(ViewPager view_pager_suggest) {
        policiesViewPagerAdapter = new PoliciesViewPagerAdapter(getSupportFragmentManager());


        if (from.equals("NORMAL")) {
            view_pager_suggest.setCurrentItem(1);
            policiesViewPagerAdapter.addFrag(new TermsConditionFragment(), "Terms And Codnitions");
            policiesViewPagerAdapter.addFrag(new PrivacyPolicyFragment(), "Privacy Policy");
        }

        if (from.equals("PrivacyPolicy")) {
            view_pager_suggest.setCurrentItem(2);
            policiesViewPagerAdapter.addFrag(new PrivacyPolicyFragment(), "Privacy Policy");
            policiesViewPagerAdapter.addFrag(new TermsConditionFragment(), "Terms And Codnitions");

        }

        view_pager_suggest.setAdapter(policiesViewPagerAdapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_faq_termscondition) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }

    @Override
    public void onClick(View v) {
        if (v == close_img) {

            finish();
        }
    }
}
