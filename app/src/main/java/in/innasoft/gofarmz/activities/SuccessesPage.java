package in.innasoft.gofarmz.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.LoginResponse;
import in.innasoft.gofarmz.utils.UserSessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class SuccessesPage extends AppCompatActivity implements View.OnClickListener {


    //ProgressDialog progressDialog;
    String user_id,name,deviceId,mobile,token,transaction_id,encryptorderid,activity;
    TextView order_info;
    ImageView close;
    int orderId;
    Double total;
    Button btnOk;
    ProgressBar progressBar2;
    UserSessionManager userSessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successes_page);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        order_info = findViewById(R.id.order_info);
        btnOk = findViewById(R.id.btnOk);
        close = findViewById(R.id.close);
        progressBar2 = findViewById(R.id.progressBar2);
        close.setOnClickListener(this);

        if (getIntent() != null){
            orderId = getIntent().getIntExtra("ORDERID",0);
            transaction_id = getIntent().getStringExtra("razorpayPaymentID");
            encryptorderid = getIntent().getStringExtra("ENCRYPTORDERID");
            total = getIntent().getDoubleExtra("amount",0);
            activity = getIntent().getStringExtra("ACTIVITY");

        }


        sendingToServerData();

    }

    private void sendingToServerData() {
        progressBar2.setVisibility(View.VISIBLE);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().paymentSuccess(encryptorderid,"Razorpay", transaction_id,"success","Order Successful","Online", String.valueOf(total),"cc");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                progressBar2.setVisibility(View.GONE);

              //  String text = "Your Transaction is :  <font color=#cc0029>"+transaction_id+"</font> \n\n Your Order Id : "+" <font color=#ffcc00>"+orderId+"</font>";

                order_info.setText(" Your Transaction Id : "+transaction_id+"\n\n Your Order Id : "+orderId);

                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (activity.equalsIgnoreCase("PreOrderActivity")) {
                            Intent intent = new Intent(SuccessesPage.this, PreOrderHistoryDetailsActivity.class);
                            intent.putExtra("ORDERID", String.valueOf(orderId));
                            startActivity(intent);
                        }
                        else if (activity.equalsIgnoreCase("OrderActivity")){
                            Intent intent = new Intent(SuccessesPage.this, OrderHistoryDetailActivity.class);
                            intent.putExtra("ORDERID", orderId);
                            startActivity(intent);
                        }
                    }
                });

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar2.setVisibility(View.GONE);
            }
        });


    }

    @Override
    public void onClick(View v) {
        if (v == close){
            if (activity.equalsIgnoreCase("PreOrderActivity")) {
                Intent intent = new Intent(SuccessesPage.this, PreOrderHistoryDetailsActivity.class);
                intent.putExtra("ORDERID", String.valueOf(orderId));
                startActivity(intent);
            }
            else if (activity.equalsIgnoreCase("OrderActivity")){
                Intent intent = new Intent(SuccessesPage.this, OrderHistoryDetailActivity.class);
                intent.putExtra("ORDERID", orderId);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (activity.equalsIgnoreCase("PreOrderActivity")) {
            Intent intent = new Intent(SuccessesPage.this, PreOrderHistoryDetailsActivity.class);
            intent.putExtra("ORDERID", String.valueOf(orderId));
            startActivity(intent);
        }
        else if (activity.equalsIgnoreCase("OrderActivity")){
            Intent intent = new Intent(SuccessesPage.this, OrderHistoryDetailActivity.class);
            intent.putExtra("ORDERID", orderId);
            startActivity(intent);
        }
    }
}
