package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.victor.loading.rotate.RotateLoading;

import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.MyPreOrderDetailsResponse;
import in.innasoft.gofarmz.adapters.OrderTrackingAdapter;
import in.innasoft.gofarmz.adapters.PreOrderHistoryDetailsAdapter;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class PreOrderHistoryDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recycycle_view_product, recycler_view_track;
    PreOrderHistoryDetailsAdapter preOrderHistoryDetailsAdapter;
    OrderTrackingAdapter orderTrackingAdapter;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, order_ID;
    TextView preorder_id, order_status_top, order_approv_title, order_approve_date, expected_delevry_title, expected_delevry_date, expected_delevry_slot, total_mrp_title, total_mrp_txt,
            discount_title, discounttxt, shipping_charges_tilte, shipping_charges_txt, final_price_title, final_price_txt, payment_mode, customer_name,
            customer_mobile, area_text, address_text, state_country, city_pincode, pro_detail_text, pro_sheepingdetail, pro_pricedetail, pro_trackOrder, toolbar_title;
    TableRow row_discount;
    private boolean mWithLinePadding;
    public final static String EXTRA_WITH_LINE_PADDING = "EXTRA_WITH_LINE_PADDING";
    RotateLoading progress_indicator;
    ImageView close_img;
    LinearLayout ll_expected_date;
    View running_view_line;
    ProgressDialog pprogressDialog;
    Gson gson;
    private boolean checkInternet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preorder_history_detail);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        pprogressDialog = new ProgressDialog(PreOrderHistoryDetailsActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        Bundle bundle = getIntent().getExtras();
        order_ID = bundle.getString("ORDERID");
        getOrderDetail(order_ID);


        mWithLinePadding = getIntent().getBooleanExtra(OrderHistoryDetailActivity.EXTRA_WITH_LINE_PADDING, false);
        recycler_view_track = findViewById(R.id.recycler_view_track);
        recycycle_view_product = findViewById(R.id.recycycle_view_product);
        row_discount = findViewById(R.id.row_discount);

        preorder_id = findViewById(R.id.order_id);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);

        ll_expected_date = findViewById(R.id.ll_expected_date);
        running_view_line = findViewById(R.id.running_view_line);

        order_status_top = findViewById(R.id.order_status_top);

        order_approv_title = findViewById(R.id.order_approv_title);

        order_approve_date = findViewById(R.id.order_approve_date);

        expected_delevry_title = findViewById(R.id.expected_delevry_title);

        expected_delevry_date = findViewById(R.id.expected_delevry_date);

        expected_delevry_slot = findViewById(R.id.expected_delevry_slot);

        total_mrp_title = findViewById(R.id.total_mrp_title);

        total_mrp_txt = findViewById(R.id.total_mrp_txt);
        //total_mrp_txt.setTypeface(light);
        discount_title = findViewById(R.id.discount_title);

        discounttxt = findViewById(R.id.discounttxt);
        //discounttxt.setTypeface(light);
        shipping_charges_tilte = findViewById(R.id.shipping_charges_tilte);

        shipping_charges_txt = findViewById(R.id.shipping_charges_txt);
        //shipping_charges_txt.setTypeface(light);
        final_price_title = findViewById(R.id.final_price_title);

        final_price_txt = findViewById(R.id.final_price_txt);
        //final_price_txt.setTypeface(light);
        payment_mode = findViewById(R.id.payment_mode);


        pro_detail_text = findViewById(R.id.pro_detail_text);

        pro_sheepingdetail = findViewById(R.id.pro_sheepingdetail);

        pro_pricedetail = findViewById(R.id.pro_pricedetail);

        pro_trackOrder = findViewById(R.id.pro_trackOrder);


        customer_name = findViewById(R.id.customer_name);

        customer_mobile = findViewById(R.id.customer_mobile);

        area_text = findViewById(R.id.area_text);

        address_text = findViewById(R.id.address_text);

        state_country = findViewById(R.id.state_country);

        city_pincode = findViewById(R.id.city_pincode);

    }

    private void getOrderDetail(String order_id) {


        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            pprogressDialog.show();
            Call<MyPreOrderDetailsResponse> call = RetrofitClient.getInstance().getApi().MyPreOrderDetails(token, user_id, order_id);
            call.enqueue(new Callback<MyPreOrderDetailsResponse>() {
                @Override
                public void onResponse(Call<MyPreOrderDetailsResponse> call, Response<MyPreOrderDetailsResponse> response) {
                    if (response.isSuccessful()) ;
                    MyPreOrderDetailsResponse myPreOrderDetailsResponse = response.body();
                    if (myPreOrderDetailsResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();
                        MyPreOrderDetailsResponse.DataBean dataBean = myPreOrderDetailsResponse.getData();

                        MyPreOrderDetailsResponse.DataBean.OrderDetailsBean orderDetailsBean = dataBean.getOrder_details();
                        MyPreOrderDetailsResponse.DataBean.AddressBean addressBean = dataBean.getAddress();
                        List<MyPreOrderDetailsResponse.DataBean.ProductsBean> productsBeanList = dataBean.getProducts();

                        preorder_id.setText(orderDetailsBean.getRef_no());
                        order_status_top.setText(orderDetailsBean.getPre_order_status());

                        total_mrp_txt.setText(orderDetailsBean.getTotal_price() + "/-");
                        shipping_charges_txt.setText(orderDetailsBean.getShipping_charges() + "/-");
                        final_price_txt.setText(orderDetailsBean.getGrand_total() + "/-");
                        order_approve_date.setText(orderDetailsBean.getOrder_date());

                        payment_mode.setText("Payment Mode - " + orderDetailsBean.getGetaway_name());
//
                        customer_name.setText(addressBean.getName());
                        customer_mobile.setText(addressBean.getMobile());
                        area_text.setText(addressBean.getArea());
                        address_text.setText(addressBean.getAddress());
                        state_country.setText(addressBean.getState() + "," + addressBean.getCountry());
                        city_pincode.setText(addressBean.getCity() + "-" + addressBean.getPincode() + ".");


                        preOrderHistoryDetailsAdapter = new PreOrderHistoryDetailsAdapter(PreOrderHistoryDetailsActivity.this, productsBeanList);
                        recycycle_view_product.setHasFixedSize(true);
                        recycycle_view_product.setLayoutManager(new LinearLayoutManager(PreOrderHistoryDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
                        recycycle_view_product.setItemAnimator(new DefaultItemAnimator());
                        recycycle_view_product.setAdapter(preOrderHistoryDetailsAdapter);


                    }
                }

                @Override
                public void onFailure(Call<MyPreOrderDetailsResponse> call, Throwable t) {

                }
            });

        } else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == close_img) {
            Intent it = new Intent(PreOrderHistoryDetailsActivity.this, OrdersHistoryActivity1.class);
            it.putExtra("R_id", "preorders");
            startActivity(it);
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(PreOrderHistoryDetailsActivity.this, OrdersHistoryActivity1.class);
        it.putExtra("R_id", "preorders");
        startActivity(it);
        finish();
    }
}
