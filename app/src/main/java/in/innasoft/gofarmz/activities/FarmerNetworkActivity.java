package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.victor.loading.rotate.RotateLoading;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.NetworkChecking;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.ContentValues.TAG;


public class FarmerNetworkActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img;
    TextView tittle_txt;
    WebView fn_web;
    RotateLoading progress_indicator;

    private boolean checkInternet;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_network);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        tittle_txt = findViewById(R.id.tittle_txt);


        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();

        fn_web = findViewById(R.id.fn_web);

        fn_web.setInitialScale(1);
        fn_web.getSettings().setJavaScriptEnabled(true);
        fn_web.getSettings().setLoadWithOverviewMode(true);
        fn_web.getSettings().setUseWideViewPort(true);
        fn_web.getSettings().setBuiltInZoomControls(true);
        fn_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        fn_web.setScrollbarFadingEnabled(false);
        fn_web.setWebViewClient(new WebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);
       // fn_web.loadUrl(AppUrls.API_URL+AppUrls.FARMER_NETWORK);

        fn_web.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

                progress_indicator.stop();

            }
        });

        loadDataInWebview();

    }

    private void loadDataInWebview() {
        // adSliderList.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

           // pprogressDialog.show();
            Call<ResponseBody> call = RetrofitClient.getInstance().getApi().formerNetwork();
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                   // pprogressDialog.dismiss();
                    try{
                        assert response.body() != null;
                        String html = response.body().string();
                        Document document = Jsoup.parse(html);
                        Log.d(TAG, "onResponse: "+document);
                        fn_web.loadData(String.valueOf(document),"text/html", null);

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    //pprogressDialog.dismiss();
                }
            });



        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == close_img) {
            Intent intent = new Intent(FarmerNetworkActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }
}
