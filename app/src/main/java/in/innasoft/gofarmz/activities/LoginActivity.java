package in.innasoft.gofarmz.activities;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.AppVersionResponse;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.LoginMobileResponse;
import in.innasoft.gofarmz.Response.LoginResponse;
import in.innasoft.gofarmz.utils.Config;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import in.innasoft.gofarmz.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private final static int RESOLVE_HINT = 1011;
    String mobNumber;
    private static final String TAG = "LoginActivity";
    TextView login_txt, reg_txt, forgotPassword_txt, or_txt, next_txt, reg_or_txt, reg_next_txt, t_c_textview;
    EditText username_edt, password_edt, name_edt, email_edt, mobile_edt, regpassword_edt;
    Button text_fbLogin, text_gplusLogin, reg_text_fbLogin, reg_text_gplusLogin;
    RelativeLayout log_rl, reg_rl;
    UserSessionManager userSessionManager;
    String deviceId, name, email, password, mobile,refreshedToken;
    private boolean checkInternet;
    CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    String screen, user = "", gmail = "";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String MOBILE_REGEX = "^[6789]\\d{9}$";
    String nm = "", lnm = "";
    String sendFBAccessToken, sendFBUserID, token = "", verified_data, fb_user_id_data, email_data, name_data, gender_data, user_type, checnkVeriosn, android_version;
    RotateLoading progress_indicator;
    CheckBox t_c_chk;
    SweetAlertDialog sd_update;
    LinearLayout linear_mobile;
    TextInputEditText edt_mobile;
    Button btn_continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializeFacebookSettings();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        PackageInfo eInfo = null;
        try {
            eInfo = getPackageManager().getPackageInfo("in.innasoft.gofarmz", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        checnkVeriosn = eInfo.versionName;
        Log.d("APPVERSION", checnkVeriosn);
        getVerionUpdateData(checnkVeriosn);

        deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d(TAG, "onCreate: " + deviceId);
        userSessionManager = new UserSessionManager(this);
        userSessionManager.createDeviceId(deviceId);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();

        login_txt = findViewById(R.id.login_txt);

        login_txt.setOnClickListener(this);
        reg_txt = findViewById(R.id.reg_txt);

        reg_txt.setOnClickListener(this);
        username_edt = findViewById(R.id.username_edt);

        password_edt = findViewById(R.id.password_edt);

        forgotPassword_txt = findViewById(R.id.forgotPassword_txt);

        //login with mobile number
        btn_continue = findViewById(R.id.btn_continue1);
        edt_mobile = findViewById(R.id.edt_mobile);
        linear_mobile = findViewById(R.id.linear_mobile);
        btn_continue.setOnClickListener(this);
        getPhone();

        btn_continue.setAlpha(.5f);
        btn_continue.setEnabled(false);
        edt_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = String.valueOf(charSequence.length());

                if (value.equals("10")) {

                    btn_continue.setEnabled(true);
                    btn_continue.setAlpha(.9f);
                    btn_continue.setText("CONTINUE");
                    btn_continue.setClickable(true);
                } else {
                    btn_continue.setAlpha(.5f);
                    btn_continue.setEnabled(false);
                    btn_continue.setText("ENTER MOBILE NUMBER");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        forgotPassword_txt.setOnClickListener(this);
        or_txt = findViewById(R.id.or_txt);

        text_fbLogin = findViewById(R.id.text_fbLogin);
        text_fbLogin.setOnClickListener(this);
        text_gplusLogin = findViewById(R.id.text_gplusLogin);
        text_gplusLogin.setOnClickListener(this);
        next_txt = findViewById(R.id.next_txt);

        next_txt.setOnClickListener(this);

        name_edt = findViewById(R.id.name_edt);

        email_edt = findViewById(R.id.email_edt);

        mobile_edt = findViewById(R.id.mobile_edt);

        regpassword_edt = findViewById(R.id.regpassword_edt);

        reg_or_txt = findViewById(R.id.reg_or_txt);


        reg_text_fbLogin = findViewById(R.id.reg_text_fbLogin);
        reg_text_gplusLogin = findViewById(R.id.reg_text_gplusLogin);
        reg_text_fbLogin.setOnClickListener(this);
        reg_text_gplusLogin.setOnClickListener(this);
        reg_next_txt = findViewById(R.id.reg_next_txt);
        reg_next_txt.setOnClickListener(this);

        t_c_chk = findViewById(R.id.t_c_chk);
        t_c_textview = findViewById(R.id.t_c_textview);


        log_rl = findViewById(R.id.log_rl);
        reg_rl = findViewById(R.id.reg_rl);
        checkInternet = NetworkChecking.isConnected(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        SpannableString ss = new SpannableString("I agree to Terms and Conditions");
        SpannableString ss1 = new SpannableString(" & Privacy policy .");

        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "in.innasoft.gofarmz",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent go = new Intent(LoginActivity.this, TermsConditionsAndPrivacy.class);
                go.putExtra("from", "NORMAL");
                startActivity(go);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ClickableSpan policyclickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent go = new Intent(LoginActivity.this, TermsConditionsAndPrivacy.class);
                go.putExtra("from", "PrivacyPolicy");
                startActivity(go);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 11, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new UnderlineSpan(), 11, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(Color.BLACK), 11, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss1.setSpan(policyclickableSpan, 3, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss1.setSpan(new UnderlineSpan(), 3, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss1.setSpan(new ForegroundColorSpan(Color.BLACK), 3, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        t_c_textview.setText(ss);
        t_c_textview.append(ss1);
        t_c_textview.setTextColor(Color.parseColor("#05914E"));
        t_c_textview.setMovementMethod(LinkMovementMethod.getInstance());
        //  t_c_textview.setHighlightColor(Color.MAGENTA);


        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("Login Sucessful", "Success Facebook Login");
                        JSONObject json = response.getJSONObject();
                        Log.d("LOGFBBB", json.toString());
                        if (json != null) {
                            try {
                                sendFBAccessToken = AccessToken.getCurrentAccessToken().getToken();
                                AccessToken token1 = AccessToken.getCurrentAccessToken();
                                if (token1 != null) {
                                    Log.d("ACCESSESTOKE", AccessToken.getCurrentAccessToken().getToken());
                                }
                                //  Log.d("ACCESSESTOKE", AccessToken.getCurrentAccessToken().getToken());

                                //  verified_data = json.getString("verified");
                                fb_user_id_data = json.getString("id");
                                sendFBUserID = fb_user_id_data;
                                email_data = json.getString("email");
                                name_data = json.getString("name");
                                //  gender_data = json.getString("gender");
                                String profile_ptah = "http://graph.facebook.com/" + fb_user_id_data + "/picture?type=large";
                                String provider = "Facebook";


                                String nm = "", lnm = "";

                                if (name_data.split("\\w+").length > 1) {

                                    lnm = name_data.substring(name_data.lastIndexOf(" ") + 1);
                                    nm = name_data.substring(0, name_data.lastIndexOf(' '));
                                } else {
                                    //  firstName = name;
                                }
                                if (screen.equalsIgnoreCase("Register")) {
                                    name_edt.setText(name_data);
                                    email_edt.setText(email_data);
                                } else {


                                    getSocialLoginStatus(fb_user_id_data, email_data, provider, lnm, nm);
                                }
                                Log.d("FACEBOOKPROFILE", nm + "   " + lnm + "    " + name_data + "    " + verified_data + "," + fb_user_id_data + "," + sendFBUserID + ", " + email_data + ", " + email_data + ", " + gender_data + ",," + profile_ptah);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,gender,link,email,verified,birthday");
                //  parameters.putString("fields", "id,email,first_name,last_name,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e("Login Canceled", "Canceled Facebook Login");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("Login Error", error.getMessage());

            }

        });

    }

    private void getVerionUpdateData(final String checnkVeriosn) {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            Call<AppVersionResponse> call = RetrofitClient.getInstance().getApi().CheckAppUpdate(token);
            call.enqueue(new Callback<AppVersionResponse>() {
                @Override
                public void onResponse(Call<AppVersionResponse> call, retrofit2.Response<AppVersionResponse> response) {
                    if (response.isSuccessful()) ;
                    AppVersionResponse appVersionResponse = response.body();
                    if (appVersionResponse.getStatus().equals("10100")) {

                        android_version = appVersionResponse.getData().getAndroid_version();

                        Log.d(TAG, "onResponse: " + android_version + "---" + checnkVeriosn);

                        if (!android_version.equalsIgnoreCase(checnkVeriosn)) {

                            sd_update = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.NORMAL_TYPE);
                            sd_update.setTitleText("App Update");
                            sd_update.setContentText("Please update your app now to experience our latest enhancements..!");
                            sd_update.setConfirmText("Update Now");
                            sd_update.showCancelButton(true);
                            sd_update.setCanceledOnTouchOutside(false);
                            sd_update.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                    Intent intent = new Intent();
                                    intent.setAction(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en"));
                                    startActivity(intent);
                                    sd_update.cancel();
                                }
                            });
                            sd_update.show();
                        }

                    } else if (appVersionResponse.getStatus().equals("10200")) {
                        Toast.makeText(LoginActivity.this, appVersionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (appVersionResponse.getStatus().equals("10300")) {
                        Toast.makeText(LoginActivity.this, appVersionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<AppVersionResponse> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            progress_indicator.stop();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TT", "handleSignInResult" + result.isSuccess());
        if (result.isSuccess()) {

            GoogleSignInAccount acct = result.getSignInAccount();

            Log.d("detailaname", "" + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String email = acct.getEmail();
            String id = acct.getId();
            String profilePicPath = "";
            if (acct != null) {
                if (acct.getPhotoUrl() != null) {
                    profilePicPath = acct.getPhotoUrl().getPath();
                }
            }
            if (personName.split("\\w+").length > 1) {
                lnm = personName.substring(personName.lastIndexOf(" ") + 1);
                nm = personName.substring(0, personName.lastIndexOf(' '));
            } else {
                //  firstName = name;
            }
            String provider = "Google";

            user = personName;
            gmail = email;
            if (screen.equalsIgnoreCase("Register")) {
                name_edt.setText(personName);
                email_edt.setText(email);
            } else {
                getSocialLoginStatus(id, email, provider, lnm, nm);

            }
            Log.e("infoooo", "" + personName + ", email: " + email + ", Image: " + profilePicPath);
        } else {

            // Signed out, show unauthenticated UI.

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            progress_indicator.stop();

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d("TAG", "RESULLLL" + result);
            handleSignInResult(result);
        }
        else  if (requestCode == RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                com.google.android.gms.auth.api.credentials.Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                if (credential != null) {

                    mobNumber = credential.getId();
                    String newString = mobNumber.replace("+91", "");
                    edt_mobile.setText(newString);
                   // Toast.makeText(this, "" + newString, Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(this, "err", Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }

    @Override
    public void onClick(View v) {

        if (v == login_txt) {
            reg_rl.setVisibility(View.GONE);
            //log_rl.setVisibility(View.VISIBLE);
            linear_mobile.setVisibility(View.VISIBLE);

            reg_txt.setBackgroundColor(this.getResources().getColor(R.color.light_green));
            login_txt.setBackgroundColor(this.getResources().getColor(R.color.green));
        }

        if (v == next_txt) {
            next_txt.setBackgroundResource(R.drawable.memberdetail_textview_background_fill_bg);
            next_txt.setTextColor(Color.parseColor("#FFFFFF"));

            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {

                final String l_email = username_edt.getText().toString();
                final String l_password = password_edt.getText().toString();
                if (!isEmailValid(l_email) || l_email.length() == 0) {
                    Toast.makeText(LoginActivity.this, "Enter valid email address", Toast.LENGTH_SHORT).show();
                } else if (l_password.length() == 0) {
                    Toast.makeText(LoginActivity.this, "Enter password", Toast.LENGTH_SHORT).show();
                } else {
                    userLogin(l_email, l_password);

                }

            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == btn_continue) {

            String Mobile = edt_mobile.getText().toString().trim();
            if (Mobile.equals("")) {
                Toast.makeText(LoginActivity.this, "Enter Mobile Number...", Toast.LENGTH_SHORT).show();
            } else {

                ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Loading....");
                progressDialog.show();
                Call<LoginMobileResponse> call = RetrofitClient.getInstance().getApi().LoginMobile(Mobile, deviceId);
                call.enqueue(new Callback<LoginMobileResponse>() {
                    @Override
                    public void onResponse(Call<LoginMobileResponse> call, Response<LoginMobileResponse> response) {
                        if (response.isSuccessful()) ;
                        LoginMobileResponse loginMobileResponse = response.body();
                        if (loginMobileResponse.getStatus().equals("10100")) {
                            progressDialog.dismiss();
                            //progress_indicator.stop();
                            Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, LoginWithMobileOtpActivity.class);
                            intent.putExtra("Mobile_number", Mobile);
                            startActivity(intent);

                            setDefaults();

                        } else if (loginMobileResponse.getStatus().equals("10200")) {
                            progressDialog.dismiss();
                            //progress_indicator.stop();
                            Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        } else if (loginMobileResponse.getStatus().equals("10300")) {
                            progressDialog.dismiss();
                            Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                            intent.putExtra("Mobile_number", Mobile);
                            startActivity(intent);
                           // Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        } else if (loginMobileResponse.getStatus().equals("10400")) {
                            progressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<LoginMobileResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        //progress_indicator.stop();
                        Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });




            }


//            Toast.makeText(LoginActivity.this, "click...", Toast.LENGTH_SHORT).show();

            //loginMobileNumber(mobile);

//            checkInternet = NetworkChecking.isConnected(this);
//            if (checkInternet) {
//
//                final String mobile = edt_mobile.getText().toString();
//               // final String l_password = password_edt.getText().toString();
//                if(mobile.equals("")) {
//                    Toast.makeText(LoginActivity.this,"EnterMobile number",Toast.LENGTH_SHORT).show();
//                }else {
//
//
//                }
//
//            } else {
//                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
//            }

        }

        if (v == forgotPassword_txt) {
            Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
            intent.putExtra("mobile", mobile_edt.getText().toString());
            startActivity(intent);
        }

        if (v == reg_txt) {
            // log_rl.setVisibility(View.GONE);
            linear_mobile.setVisibility(View.GONE);
            reg_rl.setVisibility(View.VISIBLE);
            login_txt.setBackgroundColor(this.getResources().getColor(R.color.light_green));
            reg_txt.setBackgroundColor(this.getResources().getColor(R.color.green));
        }

        if (v == text_fbLogin) {
            screen = "Login";
            LoginManager.getInstance().logOut();
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
        }

        if (v == text_gplusLogin) {
            screen = "Login";
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            //  updateUI(null);
                            //Toast.makeText(LoginActivity.this,"Logout",Toast.LENGTH_SHORT).show();
                            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                            startActivityForResult(signInIntent, RC_SIGN_IN);
                        }
                    });

        }

        if (v == reg_text_fbLogin) {
            screen = "Register";
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
        }
        if (v == reg_text_gplusLogin) {
            screen = "Register";
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }

        if (v == reg_next_txt) {
            reg_next_txt.setBackgroundResource(R.drawable.memberdetail_textview_background_fill_bg);
            reg_next_txt.setTextColor(Color.parseColor("#FFFFFF"));

            if (checkInternet) {

                name = name_edt.getText().toString();
                email = email_edt.getText().toString();
                password = regpassword_edt.getText().toString();
                mobile = mobile_edt.getText().toString();
                if (name.length() == 0) {
                    Toast.makeText(LoginActivity.this, "Please enter name", Toast.LENGTH_SHORT).show();
                } else if (!isEmailValid(email) || email.length() == 0) {
                    Toast.makeText(LoginActivity.this, "Please enter valid email", Toast.LENGTH_SHORT).show();
                } else if (mobile.length() == 0 || !mobile.matches(MOBILE_REGEX)) {
                    Toast.makeText(LoginActivity.this, "Please enter valid moblie no", Toast.LENGTH_SHORT).show();
                } else if (password.length() == 0) {
                    Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                } else if (!t_c_chk.isChecked()) {
                    Toast.makeText(getApplicationContext(), "Please Check Terms & Conditions", Toast.LENGTH_SHORT).show();
                } else {
                    // user Registration
                    userRegistration(name, email, mobile, password);
                }

            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getPhone() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                .addApi(Auth.CREDENTIALS_API)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) LoginActivity.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) LoginActivity.this)
                .build();
        googleApiClient.connect();
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();
        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(),
                    RESOLVE_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }




    private void loginMobileNumber(String mobile) {
        //progress_indicator.start();


    }


    // user Registration
    private void userRegistration(String name, String email, String mobile, String password) {

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userRegistrationRequest(deviceId, name, email, mobile, password);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {

                BaseResponse registerResponse = response.body();
                if (registerResponse.getStatus().equalsIgnoreCase("10100")) {

                    progress_indicator.stop();

                    Intent intent = new Intent(LoginActivity.this, AccountVerificationActivity.class);
                    intent.putExtra("mobile", LoginActivity.this.mobile);
                    intent.putExtra("activity", "LOGIN");

                    startActivity(intent);


                } else if (registerResponse.getStatus().equals("10200")) {
                    progress_indicator.stop();
                    Toast.makeText(LoginActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (registerResponse.getStatus().equals("10300")) {
                    progress_indicator.stop();
                    Toast.makeText(LoginActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (registerResponse.getStatus().equals("10400")) {
                    progress_indicator.stop();
                    Toast.makeText(LoginActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progress_indicator.stop();
            }
        });
    }

    // retrofit login
    private void userLogin(String l_email, String l_password) {


        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(l_email, l_password, deviceId);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();

                if (loginResponse.getStatus().equals("10100")) {
                    progress_indicator.stop();

                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    userSessionManager.createUserLoginSession(loginResponse.getData().getJwt()
                            , loginResponse.getData().getUser_id()
                            , loginResponse.getData().getUser_name()
                            , loginResponse.getData().getEmail()
                            , loginResponse.getData().getMobile());

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                    setDefaults();


                } else if (loginResponse.getStatus().equals("10200")) {
                    progress_indicator.stop();
                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (loginResponse.getStatus().equals("10300")) {
                    progress_indicator.stop();
                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (loginResponse.getStatus().equals("10400")) {
                    progress_indicator.stop();
                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    int color = Color.RED;
                    // snackBar(loginResponse.getMessage(), color);

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progress_indicator.stop();
            }
        });
    }


    public void setDefaults() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true);

        editor.apply();
    }



    private void initializeFacebookSettings() {
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    // social Login
    private void getSocialLoginStatus(final String id, final String email, final String provider, final String l_name, final String f_name) {

        if (checkInternet) {

            Call<LoginResponse> call = RetrofitClient.getInstance().getApi().socialLogin(deviceId, f_name + " " + l_name, email, deviceId, provider, id);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {

                    LoginResponse loginResponse = response.body();


                    if (loginResponse.getStatus().equals("10100")) {
                        progress_indicator.stop();

                        Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        if (mobile != null && mobile.length() > 0)
                            userSessionManager.createUserLoginSession(loginResponse.getData().getJwt()
                                    , loginResponse.getData().getUser_id()
                                    , loginResponse.getData().getUser_name()
                                    , loginResponse.getData().getEmail()
                                    , loginResponse.getData().getMobile());

                        else
                            userSessionManager.createUserLoginSession(loginResponse.getData().getJwt()
                                    , loginResponse.getData().getUser_id()
                                    , loginResponse.getData().getUser_name()
                                    , loginResponse.getData().getEmail()
                                    , "");

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();



                    } else if (loginResponse.getStatus().equals("10200")) {
                        progress_indicator.stop();
                        Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (loginResponse.getStatus().equals("10300")) {
                        progress_indicator.stop();
                        log_rl.setVisibility(View.GONE);
                        reg_rl.setVisibility(View.VISIBLE);
                        login_txt.setBackgroundColor(getResources().getColor(R.color.light_green));
                        reg_txt.setBackgroundColor(getResources().getColor(R.color.green));
                        if (provider.equalsIgnoreCase("Facebook")) {
                            name_edt.setText(name_data);
                            email_edt.setText(email_data);
                        } else {
                            name_edt.setText(user);
                            email_edt.setText(gmail);
                        }
                        Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (loginResponse.getMessage().equals("10400")) {
                        progress_indicator.stop();
                        Toast.makeText(getApplicationContext(), "Account is deactivated, please contact Admin.!", Toast.LENGTH_SHORT).show();
                    } else if (loginResponse.getMessage().equals("10500")) {
                        progress_indicator.stop();
                        Toast.makeText(getApplicationContext(), "Account is blocked, please contact Admin.!", Toast.LENGTH_SHORT).show();
                    } else if (loginResponse.getMessage().equals("10600")) {
                        progress_indicator.stop();
                        Toast.makeText(getApplicationContext(), "Invalid Login...!", Toast.LENGTH_SHORT).show();
                    } else if (loginResponse.getMessage().equals("10700")) {
                        progress_indicator.stop();
                        Toast.makeText(getApplicationContext(), "It seems you are login from different device...!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    progress_indicator.stop();
                }
            });




        } else {

            Toast.makeText(getApplicationContext(), "No Internet Connection Please Check Your Internet Connection..!!", Toast.LENGTH_SHORT).show();

        }


    }


    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


   /* String MOBILE_REGEX = "^[789]\\d{9}$";

        if (!mobile.matches(MOBILE_REGEX)) {
        usermobile_til.setError("Invalid Mobile Number");
        result = false;
    }else {
        usermobile_til.setError(null);
    }*/

}
