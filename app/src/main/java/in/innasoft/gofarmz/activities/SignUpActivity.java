package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.LoginMobileResponse;
import in.innasoft.gofarmz.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {


    String deviceId, mobile,terms;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.linear_mobile)
    LinearLayout linearMobile;
    @BindView(R.id.mobile_edt)
    TextInputEditText mobileEdt;
    @BindView(R.id.name_edt)
    TextInputEditText nameEdt;
    @BindView(R.id.email_edt)
    TextInputEditText emailEdt;
    @BindView(R.id.t_c_chk)
    CheckBox tCChk;
    @BindView(R.id.t_c_textview)
    TextView tCTextview;
    @BindView(R.id.term_conditon_layout)
    RelativeLayout termConditonLayout;
    @BindView(R.id.btn_signup)
    Button btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        if (getIntent() != null) {
            mobile = getIntent().getStringExtra("Mobile_number");
            mobileEdt.setText(mobile);
        }

        btnSignup.setEnabled(false);
        btnSignup.setAlpha(0.5f);
        btnSignup.setClickable(false);
        terms = "";
        tCChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    btnSignup.setEnabled(true);
                    btnSignup.setAlpha(0.9f);
                    btnSignup.setClickable(true);
                    terms = "terms";
                   // Toast.makeText(getApplicationContext(),""+isChecked,Toast.LENGTH_SHORT).show();
                }else {
                    btnSignup.setEnabled(false);
                    btnSignup.setAlpha(0.5f);
                    btnSignup.setClickable(false);
                    terms = "";
                }
            }
        });


    }

    @OnClick({R.id.close_img, R.id.btn_signup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_signup:
                String mobile = mobileEdt.getText().toString();
                String name = nameEdt.getText().toString();
                String email = emailEdt.getText().toString();

                if (name.isEmpty()) {
                    nameEdt.setError("Enter Your Name");
                    return;
                }

                if (email.isEmpty()) {
                    emailEdt.setError("Enter Your Email");
                    return;
                }


                userRegistration(name, email, mobile);
                break;
        }
    }


    private void userRegistration(String name, String email, String mobile) {
        ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();
        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userRegistrationRequest(deviceId, name, email, mobile, "");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressDialog.dismiss();
                BaseResponse registerResponse = response.body();
                if (registerResponse.getStatus().equalsIgnoreCase("10100")) {

                   /* Intent intent = new Intent(SignUpActivity.this, LoginWithMobileOtpActivity.class);
                    intent.putExtra("Mobile_number", mobile);
                    startActivity(intent);*/

                    otpRequest(mobile);

                    // setDefaults();

                } else if (registerResponse.getStatus().equals("10200")) {

                    Toast.makeText(SignUpActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (registerResponse.getStatus().equals("10300")) {

                    Toast.makeText(SignUpActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (registerResponse.getStatus().equals("10400")) {

                    Toast.makeText(SignUpActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void otpRequest(String mobile) {
        ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();
        Call<LoginMobileResponse> call = RetrofitClient.getInstance().getApi().LoginMobile(mobile, deviceId);
        call.enqueue(new Callback<LoginMobileResponse>() {
            @Override
            public void onResponse(Call<LoginMobileResponse> call, Response<LoginMobileResponse> response) {
                if (response.isSuccessful()) ;
                LoginMobileResponse loginMobileResponse = response.body();
                if (loginMobileResponse.getStatus().equals("10100")) {
                    progressDialog.dismiss();
                    //progress_indicator.stop();
                    Toast.makeText(SignUpActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SignUpActivity.this, LoginWithMobileOtpActivity.class);
                    intent.putExtra("Mobile_number", mobile);
                    startActivity(intent);



                } else if (loginMobileResponse.getStatus().equals("10200")) {
                    progressDialog.dismiss();
                    //progress_indicator.stop();
                    Toast.makeText(SignUpActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();

                } else if (loginMobileResponse.getStatus().equals("10300")) {
                    progressDialog.dismiss();
                    /*Intent intent = new Intent(SignUpActivity.this, SignUpActivity.class);
                    intent.putExtra("Mobile_number", Mobile);
                    startActivity(intent);*/
                    // Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (loginMobileResponse.getStatus().equals("10400")) {
                    progressDialog.dismiss();
                    Toast.makeText(SignUpActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<LoginMobileResponse> call, Throwable t) {
                progressDialog.dismiss();
                //progress_indicator.stop();
                Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void setDefaults() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true);

        editor.apply();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}
