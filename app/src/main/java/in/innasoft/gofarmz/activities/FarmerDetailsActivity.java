package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.innasoft.gofarmz.R;

public class FarmerDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {
    SupportMapFragment mapFragment;
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.tittle_txt)
    TextView tittleTxt;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.imgview)
    ImageView imgview;
    @BindView(R.id.txtCategoery)
    TextView txtCategoery;
    @BindView(R.id.txtFarmerName)
    TextView txtFarmerName;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.txtWeight)
    TextView txtWeight;
    @BindView(R.id.txtType)
    TextView txtType;
    @BindView(R.id.txtFacility)
    TextView txtFacility;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.btncall)
    Button btncall;

    String image, product, name, category, mobile, price, weight, facilty, type, location;
    double latitude, longitude;
    @BindView(R.id.txtProductName)
    TextView txtProductName;
    @BindView(R.id.txtLocation)
    TextView txtLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_details);
        ButterKnife.bind(this);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        if (getIntent() != null) {

            image = getIntent().getStringExtra("IMAGE");
            product = getIntent().getStringExtra("PRODUCT");
            name = getIntent().getStringExtra("NAME");
            category = getIntent().getStringExtra("CATEGORY");
            mobile = getIntent().getStringExtra("MOBILE");
             price= getIntent().getStringExtra("PRICE");
            weight= getIntent().getStringExtra("WEIGHT");
            facilty = getIntent().getStringExtra("FACILITY");
            type = getIntent().getStringExtra("TYPE");
            location = getIntent().getStringExtra("LOCATION");
            latitude = getIntent().getDoubleExtra("LATITUDE", 0.0);
            longitude = getIntent().getDoubleExtra("LONGITUDE", 0);


        }

        Glide.with(FarmerDetailsActivity.this).load(image).into(imgview);
        txtCategoery.setText(category);
        txtProductName.setText(product);
        tittleTxt.setText(product);
        txtFacility.setText("Transport Facility : " + facilty);
        txtFarmerName.setText(name);
        txtPrice.setText("\u20b9" + price);
        txtWeight.setText("Weight : "+weight);
        txtType.setText("Type of Farming : " + type);
        txtLocation.setText("Location: " + location);


    }

    @OnClick({R.id.close_img, R.id.btncall})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.btncall:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mobile, null));
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title(location)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 6));
    }
}
