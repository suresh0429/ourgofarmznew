package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.PreOrderResponse;
import in.innasoft.gofarmz.adapters.PreOrderListAdapter;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class PreOrderActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img;
    TextView preorderproducts_txt;
    RecyclerView pre_recyclerview;
    @BindView(R.id.cart_img)
    ImageView cartImg;
    @BindView(R.id.cart_count_txt)
    TextView cartCountTxt;
    @BindView(R.id.imgNoProduct)
    ImageView imgNoProduct;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    PreOrderListAdapter preOrderListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_order);
        ButterKnife.bind(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        pprogressDialog = new ProgressDialog(PreOrderActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        checkInternet = NetworkChecking.isConnected(this);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        preorderproducts_txt = findViewById(R.id.preorderproducts_txt);
        pre_recyclerview = findViewById(R.id.pre_recyclerview);
        pre_recyclerview.setHasFixedSize(true);


        getPreOrderData();
    }

    private void getPreOrderData() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<PreOrderResponse> call = RetrofitClient.getInstance().getApi().PreOrders(token, deviceId, user_id);
            call.enqueue(new Callback<PreOrderResponse>() {
                @Override
                public void onResponse(Call<PreOrderResponse> call, retrofit2.Response<PreOrderResponse> response) {
                    if (response.isSuccessful()) ;
                    PreOrderResponse preOrderResponse = response.body();
                    if (preOrderResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();
                        imgNoProduct.setVisibility(View.GONE);
                        PreOrderResponse.DataBean dataBean = preOrderResponse.getData();
                        cartCountTxt.setText(String.valueOf(preOrderResponse.getCart_count()));

                        PreOrderResponse.VersionsBean versionsBeanList = preOrderResponse.getVersions();
                        List<PreOrderResponse.DataBean.ProductsBean> productsBeanList = dataBean.getProducts();

                        pre_recyclerview.setLayoutManager(new GridLayoutManager(PreOrderActivity.this, 2));
                        pre_recyclerview.setItemAnimator(new DefaultItemAnimator());
                        preOrderListAdapter = new PreOrderListAdapter(PreOrderActivity.this, productsBeanList);
                        pre_recyclerview.setAdapter(preOrderListAdapter);
                    } else if (preOrderResponse.getStatus().equals("10200")) {
                        pprogressDialog.cancel();
                        Toast.makeText(getApplicationContext(), preOrderResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (preOrderResponse.getStatus().equals("10300")) {

                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), preOrderResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        imgNoProduct.setVisibility(View.VISIBLE);
//                        preOrderListAdapter.notifyDataSetChanged();

                    } else if (preOrderResponse.getStatus().equals("10400")) {
                        pprogressDialog.cancel();
                        Toast.makeText(getApplicationContext(), preOrderResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        pprogressDialog.dismiss();
                        imgNoProduct.setVisibility(View.VISIBLE);
                        //preOrderListAdapter.notifyDataSetChanged();

                    }
                }

                @Override
                public void onFailure(Call<PreOrderResponse> call, Throwable t) {

                }
            });

        } else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreOrderData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(PreOrderActivity.this, MainActivity.class);
        startActivity(it);

    }

    @Override
    public void onClick(View view) {
        if (view == close_img) {
            Intent it = new Intent(PreOrderActivity.this, MainActivity.class);
            startActivity(it);
            finish();
        }
    }

    @OnClick(R.id.cart_img)
    public void onViewClicked() {

        if (cartCountTxt.getText().toString().equalsIgnoreCase("0")) {

            Toast.makeText(PreOrderActivity.this, "No Items Found !", Toast.LENGTH_SHORT).show();
        } else {
            Intent it = new Intent(PreOrderActivity.this, PreOrderCartActivity.class);
            startActivity(it);
        }


        /*Intent it = new Intent(PreOrderActivity.this, PreOrderCartActivity.class);
        startActivity(it);*/
    }
}
