package in.innasoft.gofarmz.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.HomeAreasResponse;
import in.innasoft.gofarmz.adapters.LocationAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AreasActivity extends AppCompatActivity {
    @BindView(R.id.locationRecycler)
    RecyclerView locationRecycler;
    @BindView(R.id.btnClose)
    Button btnClose;
    @BindView(R.id.close_img)
    ImageView closeImg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_location_lis_dialog);

        ButterKnife.bind(this);
        getAllAreas();

        closeImg.setOnClickListener(view -> finish());

    }

    private void getAllAreas() {
        Call<HomeAreasResponse> call = RetrofitClient.getInstance().getApi().HomeAreas();
        call.enqueue(new Callback<HomeAreasResponse>() {
            @Override
            public void onResponse(Call<HomeAreasResponse> call, Response<HomeAreasResponse> response) {

                HomeAreasResponse homeAreasResponse = response.body();

                if (response.isSuccessful()) {

                    if (homeAreasResponse.getStatus().equalsIgnoreCase("10100")) {

                        List<HomeAreasResponse.DataBean> dataBeanList = homeAreasResponse.getData();

                        LocationAdapter adapter = new LocationAdapter(AreasActivity.this, dataBeanList);
                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(AreasActivity.this, LinearLayoutManager.VERTICAL, false);
                        locationRecycler.setLayoutManager(mLayoutManager1);
                        locationRecycler.setItemAnimator(new DefaultItemAnimator());
                        locationRecycler.setHasFixedSize(true);
                        locationRecycler.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeAreasResponse> call, Throwable t) {
                Toast.makeText(AreasActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
