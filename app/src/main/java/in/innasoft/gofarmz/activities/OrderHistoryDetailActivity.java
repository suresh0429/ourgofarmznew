package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.DeliverSlotResponse;
import in.innasoft.gofarmz.Response.MyOrderDetailsResponse;
import in.innasoft.gofarmz.adapters.DeliverySlotAdapter;
import in.innasoft.gofarmz.adapters.DeliverySlotAdapter1;
import in.innasoft.gofarmz.adapters.OrderHisDetailAdapter;
import in.innasoft.gofarmz.adapters.OrderTrackingAdapter;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class OrderHistoryDetailActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recycycle_view_product, recycler_view_track;
    OrderHisDetailAdapter orderHisDetailAdapter;
    OrderTrackingAdapter orderTrackingAdapter;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token,status;
    int order_ID;
    Map<String, Object> orderDetailList = new HashMap<>();
    ArrayList<Object> orderDetailProduct = new ArrayList<>();
    ArrayList<Object> orderTrackProduct = new ArrayList<>();
    TextView order_id, order_status_top, order_approv_title, order_approve_date, expected_delevry_title, expected_delevry_date, expected_delevry_slot, total_mrp_title, total_mrp_txt,
            discount_title, discounttxt, shipping_charges_tilte, shipping_charges_txt, final_price_title, final_price_txt, payment_mode, customer_name,
            customer_mobile, area_text, address_text, state_country, city_pincode, pro_detail_text, pro_sheepingdetail, pro_pricedetail, pro_trackOrder, toolbar_title;
    TableRow row_discount;
    private boolean mWithLinePadding;
    public final static String EXTRA_WITH_LINE_PADDING = "EXTRA_WITH_LINE_PADDING";
    RotateLoading progress_indicator;
    ImageView close_img;
    LinearLayout ll_expected_date;
    View running_view_line;
    ProgressDialog pprogressDialog;
    private boolean checkInternet;
    TextView changeDeliveryDate;
    ArrayList<Object> delivrySlot = new ArrayList<>();
    ArrayList<Object> defaultAddressList = new ArrayList<Object>();
    AlertDialog dialog;
    String from = "", contact_no, sendidd, sendstartTime, sendendTime, senddate, slotString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_detail);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        pprogressDialog = new ProgressDialog(OrderHistoryDetailActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);


        if (getIntent() != null) {
            order_ID = getIntent().getIntExtra("ORDERID", 0);
            status = getIntent().getStringExtra("Status");
        }
        getOrderDetail(order_ID);

        mWithLinePadding = getIntent().getBooleanExtra(OrderHistoryDetailActivity.EXTRA_WITH_LINE_PADDING, false);
        recycler_view_track = findViewById(R.id.recycler_view_track);
        recycycle_view_product = findViewById(R.id.recycycle_view_product);
        row_discount = findViewById(R.id.row_discount);

        order_id = findViewById(R.id.order_id);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);

        ll_expected_date = findViewById(R.id.ll_expected_date);
        running_view_line = findViewById(R.id.running_view_line);


        order_status_top = findViewById(R.id.order_status_top);
        order_approv_title = findViewById(R.id.order_approv_title);

        order_approve_date = findViewById(R.id.order_approve_date);
        expected_delevry_title = findViewById(R.id.expected_delevry_title);
        expected_delevry_date = findViewById(R.id.expected_delevry_date);
        expected_delevry_slot = findViewById(R.id.expected_delevry_slot);

        total_mrp_title = findViewById(R.id.total_mrp_title);

        total_mrp_txt = findViewById(R.id.total_mrp_txt);
        discount_title = findViewById(R.id.discount_title);

        discounttxt = findViewById(R.id.discounttxt);
        shipping_charges_tilte = findViewById(R.id.shipping_charges_tilte);
        shipping_charges_txt = findViewById(R.id.shipping_charges_txt);
        final_price_title = findViewById(R.id.final_price_title);
        final_price_txt = findViewById(R.id.final_price_txt);
        payment_mode = findViewById(R.id.payment_mode);

        pro_detail_text = findViewById(R.id.pro_detail_text);
        pro_sheepingdetail = findViewById(R.id.pro_sheepingdetail);
        pro_pricedetail = findViewById(R.id.pro_pricedetail);
        pro_trackOrder = findViewById(R.id.pro_trackOrder);

        customer_name = findViewById(R.id.customer_name);

        customer_mobile = findViewById(R.id.customer_mobile);

        area_text = findViewById(R.id.area_text);

        address_text = findViewById(R.id.address_text);

        state_country = findViewById(R.id.state_country);

        city_pincode = findViewById(R.id.city_pincode);

        getDeliveryOptData();


        changeDeliveryDate = findViewById(R.id.changeDeliveryDate);


        if (status== null || status.equalsIgnoreCase("Confirmed") || status.equalsIgnoreCase("Pending")){
            changeDeliveryDate.setVisibility(View.VISIBLE);
        }else {
            changeDeliveryDate.setVisibility(View.GONE);

        }

        changeDeliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataDialog();
            }
        });

    }

    public void getOrderDetail(int order_ID) {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<MyOrderDetailsResponse> call = RetrofitClient.getInstance().getApi().MyOrdersDetails(token, user_id, String.valueOf(order_ID));
            call.enqueue(new Callback<MyOrderDetailsResponse>() {
                @Override
                public void onResponse(Call<MyOrderDetailsResponse> call, Response<MyOrderDetailsResponse> response) {
                    if (response.isSuccessful()) ;
                    MyOrderDetailsResponse myOrderDetailsResponse = response.body();
                    if (myOrderDetailsResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();

                        MyOrderDetailsResponse.DataBean dataBean = myOrderDetailsResponse.getData();

                        MyOrderDetailsResponse.DataBean.OrderDetailsBean orderDetailsBean = dataBean.getOrder_details();
                        MyOrderDetailsResponse.DataBean.AddressBean addressBean = dataBean.getAddress();

                        List<MyOrderDetailsResponse.DataBean.StatusBean> statusBeanList = dataBean.getStatus();
                        List<MyOrderDetailsResponse.DataBean.ProductsBean> productsBeanList = dataBean.getProducts();

                        if (orderDetailsBean.getStatus().equalsIgnoreCase("1") || orderDetailsBean.getStatus().equalsIgnoreCase("4") || orderDetailsBean.getStatus().equalsIgnoreCase("6")) {
                            ll_expected_date.setVisibility(View.VISIBLE);
                            running_view_line.setVisibility(View.VISIBLE);
                            expected_delevry_date.setText(orderDetailsBean.getExpected_delivery());
                            expected_delevry_slot.setText(orderDetailsBean.getExpected_delivery_slot());
                        }

                        order_id.setText(orderDetailsBean.getOrder_confirmation_reference_id());
//
                        if (orderDetailsBean.getStatus().equalsIgnoreCase("Confirmed")) {
                            order_status_top.setTextColor(Color.parseColor("#05914E"));
                            order_status_top.setText(orderDetailsBean.getStatus());
                        } else if (orderDetailsBean.getStatus().equalsIgnoreCase("Pending")) {
                            order_status_top.setTextColor(Color.parseColor("#FF8000"));
                            order_status_top.setText(orderDetailsBean.getStatus());
                        } else if (orderDetailsBean.getStatus().equalsIgnoreCase("Failed")) {
                            order_status_top.setTextColor(Color.RED);
                            order_status_top.setText(orderDetailsBean.getStatus());
                        } else if (orderDetailsBean.getStatus().equalsIgnoreCase("Cancelled")) {
                            order_status_top.setTextColor(Color.RED);
                            order_status_top.setText(orderDetailsBean.getStatus());
                        } else if (orderDetailsBean.getStatus().equalsIgnoreCase("Dispatched")) {
                            order_status_top.setTextColor(Color.parseColor("#5CD199"));
                            order_status_top.setText(orderDetailsBean.getStatus());
                        } else if (orderDetailsBean.getStatus().equalsIgnoreCase("Shipped")) {
                            order_status_top.setTextColor(Color.parseColor("#5CD199"));
                            order_status_top.setText(orderDetailsBean.getStatus());
                        } else if (orderDetailsBean.getStatus().equalsIgnoreCase("Delivered")) {
                            order_status_top.setTextColor(Color.parseColor("#05914E"));
                            order_status_top.setText(orderDetailsBean.getStatus());
                        } else if (orderDetailsBean.getStatus().equalsIgnoreCase("Not Delivered")) {
                            order_status_top.setTextColor(Color.RED);
                            order_status_top.setText(orderDetailsBean.getStatus());
                        } else if (orderDetailsBean.getStatus().equalsIgnoreCase("Received")) {
                            order_status_top.setTextColor(Color.BLUE);
                            order_status_top.setText(orderDetailsBean.getStatus());
                        }

                        order_approve_date.setText(orderDetailsBean.getOrder_date());

                        total_mrp_txt.setText(orderDetailsBean.getTotal_mrp_price() + "/-");
                        int dis = orderDetailsBean.getDiscount().compareTo("0");
                        if (dis > 0) {
                            row_discount.setVisibility(View.VISIBLE);
                            discounttxt.setText(orderDetailsBean.getDiscount() + "/-");
                        }
                        shipping_charges_txt.setText(orderDetailsBean.getShipping_charges() + "/-");
                        final_price_txt.setText(orderDetailsBean.getFinal_price() + "/-");
                        payment_mode.setText("Payment Mode - " + orderDetailsBean.getGetaway_name());

                        //SHIPPING ADDRESS
                        customer_name.setText(addressBean.getName());
                        customer_mobile.setText(addressBean.getMobile() + " " + addressBean.getAlternate_contact_no());
                        area_text.setText(addressBean.getArea());
                        address_text.setText(addressBean.getAddress());
                        state_country.setText(addressBean.getState() + ", " + addressBean.getCountry());
                        city_pincode.setText(addressBean.getCity() + " - " + addressBean.getPincode());

                        //PRODUCTS LIST DETAIL
                        orderHisDetailAdapter = new OrderHisDetailAdapter(OrderHistoryDetailActivity.this, productsBeanList, R.layout.row_orderhistory_detail);
                        recycycle_view_product.setHasFixedSize(true);
                        recycycle_view_product.setLayoutManager(new LinearLayoutManager(OrderHistoryDetailActivity.this, LinearLayoutManager.VERTICAL, false));
                        recycycle_view_product.setItemAnimator(new DefaultItemAnimator());
                        recycycle_view_product.setAdapter(orderHisDetailAdapter);

                        //Track Status LIST DETAIL
                        orderTrackingAdapter = new OrderTrackingAdapter(OrderHistoryDetailActivity.this, statusBeanList, mWithLinePadding);
                        recycler_view_track.setHasFixedSize(true);
                        recycler_view_track.setLayoutManager(new LinearLayoutManager(OrderHistoryDetailActivity.this, LinearLayoutManager.VERTICAL, false));
                        recycler_view_track.setItemAnimator(new DefaultItemAnimator());
                        recycler_view_track.setAdapter(orderTrackingAdapter);


                    } else if (myOrderDetailsResponse.getStatus().equals("10200")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(OrderHistoryDetailActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (myOrderDetailsResponse.getStatus().equals("10300")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(OrderHistoryDetailActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (myOrderDetailsResponse.getStatus().equals("10400")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(OrderHistoryDetailActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<MyOrderDetailsResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(OrderHistoryDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getDeliveryOptData() {
        if (checkInternet) {
          //  pDialog.show();

            Call<DeliverSlotResponse> call = RetrofitClient.getInstance().getApi().deliverySlot(token,deviceId,user_id);
            call.enqueue(new Callback<DeliverSlotResponse>() {
                @Override
                public void onResponse(Call<DeliverSlotResponse> call, retrofit2.Response<DeliverSlotResponse> response) {
                  //  pDialog.dismiss();

                    DeliverSlotResponse loginResponse = response.body();

                    if (loginResponse.getStatus().equals("10100")) {

                        //  List<DeliverSlotResponse.DataBean.AddressBean> addressBeanList = loginResponse.getData().getAddress();
                        //List<DeliverSlotResponse.DataBean.AvailableDeliverySlotsBean> available_delivery_slots = loginResponse.getData().getAvailable_delivery_slots();


                        if (loginResponse.getData().getAddress() instanceof Boolean){

                            //addAddress_txt.setVisibility(View.VISIBLE);
                        }else {

                          //  delivery_ll.setVisibility(View.VISIBLE);
                            defaultAddressList = (ArrayList<Object>) loginResponse.getData().getAddress();
                            Map<String, Object> recordPos = (Map<String, Object>) defaultAddressList.get(0);
                            String adress_id = (String) recordPos.get("id");
                            String name = (String) recordPos.get("name");
                            String address_line1 = (String) recordPos.get("address_line1");
                            String address_line2 = (String) recordPos.get("address_line2");
                            String area = (String) recordPos.get("area");
                            String city = (String) recordPos.get("city");
                            String state = (String) recordPos.get("state");
                            String pincode = (String) recordPos.get("pincode");
                            String contact_no = (String) recordPos.get("contact_no");
                            String alternate_contact_no = (String) recordPos.get("alternate_contact_no");
                            boolean delivery_status = (Boolean) recordPos.get("delivery_status");



                        }

                        ArrayList<Object> tmpList = (ArrayList<Object>) loginResponse.getData().getAvailable_delivery_slots();
                        Log.d("XXXX", tmpList.toString());
                        delivrySlot =tmpList ;

                    } else  if (loginResponse.getStatus().equalsIgnoreCase("10200")) {

                       // pDialog.dismiss();
                        Toast.makeText(OrderHistoryDetailActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                    }
                    else if (loginResponse.getStatus().equalsIgnoreCase("10400")) {
                       // pDialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<DeliverSlotResponse> call, Throwable t) {
                   // pDialog.dismiss();
                }
            });

        } else {
            Toast.makeText(OrderHistoryDetailActivity.this, "No Internet COnnection...!", Toast.LENGTH_SHORT).show();
        }

    }

    private void getDataDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryDetailActivity.this);
        LayoutInflater inflater = OrderHistoryDetailActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.delivery_slot_dialog, null);


        final ImageView close_dialog = dialog_layout.findViewById(R.id.close_dialog);
        RecyclerView delivery_slot_recyclerview = dialog_layout.findViewById(R.id.delivery_slot_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        delivery_slot_recyclerview.setLayoutManager(layoutManager);

        Log.d("LISTHOME", delivrySlot.toString());
        DeliverySlotAdapter1 delivrySlotAdapter = new DeliverySlotAdapter1(OrderHistoryDetailActivity.this, delivrySlot, R.layout.row_delivery_slot);
        delivery_slot_recyclerview.setAdapter(delivrySlotAdapter);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        builder.setView(dialog_layout);

        dialog = builder.create();
        dialog.show();


    }


    public void sendValue(String idd, String strtTime, String endTime, String date) {
        dialog.dismiss();

        sendidd = idd;
        sendstartTime = strtTime;
        sendendTime = endTime;
        senddate = date;
        //textDelivry_slot.setText(" " + senddate + "   " + sendstartTime + " -- " + sendendTime);
        slotString = " " + senddate + "   " + sendstartTime + " -- " + sendendTime;
        Log.d("DATAAAT", sendidd + "//////" + sendstartTime + "/////" + sendendTime + "////" + senddate);

       getUpadteDate();

    }

    public void getUpadteDate() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<BaseResponse> call = RetrofitClient.getInstance().getApi().updateDeliverydate(token,deviceId ,user_id, String.valueOf(order_ID),sendidd,user_id, String.valueOf(order_ID),sendidd);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()) ;
                    BaseResponse myOrderDetailsResponse = response.body();
                    if (myOrderDetailsResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();

                        Toast.makeText(OrderHistoryDetailActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        getOrderDetail(order_ID);

                    } else if (myOrderDetailsResponse.getStatus().equals("10200")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(OrderHistoryDetailActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (myOrderDetailsResponse.getStatus().equals("10300")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(OrderHistoryDetailActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (myOrderDetailsResponse.getStatus().equals("10400")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(OrderHistoryDetailActivity.this, myOrderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(OrderHistoryDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {

        if (v == close_img) {
            Intent it = new Intent(OrderHistoryDetailActivity.this, OrdersHistoryActivity1.class);
            it.putExtra("R_id", "orders");
            startActivity(it);
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(OrderHistoryDetailActivity.this, OrdersHistoryActivity1.class);
        it.putExtra("R_id", "orders");
        startActivity(it);
        finish();
    }
}
