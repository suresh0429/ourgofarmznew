package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.CommunityResponse;
import in.innasoft.gofarmz.adapters.ComSpinAdapter;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmComActivity extends AppCompatActivity {

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.tittle_txt)
    TextView tittleTxt;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.spinfarmer_state)
    Spinner spinfarmerState;
    @BindView(R.id.etOtherCommunityName)
    EditText etOtherCommunityName;
    @BindView(R.id.etLeadName)
    EditText etLeadName;
    @BindView(R.id.etUserName)
    EditText etUserName;
    @BindView(R.id.inputCommunity)
    TextInputLayout inputCommunity;
    @BindView(R.id.inputLead)
    TextInputLayout inputLead;
    @BindView(R.id.inputUser)
    TextInputLayout inputUser;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    UserSessionManager userSessionManager;
    String deviceId, token, user_id, username;

    ComSpinAdapter comSpinAdapter;


    ProgressDialog pprogressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farm_com);
        ButterKnife.bind(this);

        pprogressDialog = new ProgressDialog(FarmComActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();

        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        username = userDetails.get(UserSessionManager.USER_NAME);

        communityData();

    }

    private void communityData() {
        pprogressDialog.show();
        Call<CommunityResponse> call1 = RetrofitClient.getInstance().getApi().getCommunityname(token, deviceId);
        call1.enqueue(new Callback<CommunityResponse>() {
            @Override
            public void onResponse(Call<CommunityResponse> call, Response<CommunityResponse> response) {
                if (response.isSuccessful()) {
                    pprogressDialog.hide();


                    CommunityResponse communityResponse = response.body();
                    List<CommunityResponse.DataBean> dataBeans = communityResponse != null ? communityResponse.getData() : null;

                    comSpinAdapter = new ComSpinAdapter(FarmComActivity.this, R.layout.spinner_rows, R.id.txtCommunityName, communityResponse.getData());
                    spinfarmerState.setAdapter(comSpinAdapter);
                    spinfarmerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            CommunityResponse.DataBean community = (CommunityResponse.DataBean) parent.getItemAtPosition(position);

                            if (community.getCommunity().equalsIgnoreCase("other")) {
                                inputCommunity.setVisibility(View.VISIBLE);
                                inputLead.setVisibility(View.GONE);
                            } else {
                                if (!community.getLeaderName().equalsIgnoreCase("")) {
                                    etLeadName.setText(community.getLeaderName());
                                    etUserName.setText(username);
                                    inputLead.setVisibility(View.VISIBLE);
                                } else {
                                    inputLead.setVisibility(View.GONE);
                                    etUserName.setText(username);
                                }
                                inputCommunity.setVisibility(View.GONE);
                            }


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }



               /* if (profileResponse.getStatus().equals("10100")){

                }
                else if (profileResponse.getStatus().equals("10200")){

                    Toast.makeText(FarmComActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (profileResponse.getStatus().equals("10300")){

                    Toast.makeText(FarmComActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (profileResponse.getStatus().equals("10400")){

                    Toast.makeText(FarmComActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }*/

            }

            @Override
            public void onFailure(Call<CommunityResponse> call, Throwable t) {

                Toast.makeText(FarmComActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                pprogressDialog.hide();

            }
        });
    }

    @OnClick({R.id.close_img, R.id.btnSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.btnSubmit:
                break;
        }
    }
}
