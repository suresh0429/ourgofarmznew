package in.innasoft.gofarmz.Response;

import java.util.List;

public class CartResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"currency":"Rs. ","records":[{"id":"2242","product_name":"Medium Basket","total_price":534,"grand_total":534,"product_id":"22","images":"VEGBOXSEASONAL.jpg","mrp_price":"534","type":"COMBO","purchase_quantity":"1","unit_name":"Kg","ESSENTIAL":[{"pdtId":"21","optId":"120","pdtName":"Tomato: 1.5 Kg","images":"tomato1.png","unitValue":"1.5","unitName":"Kg","price":"52.5"},{"pdtId":"23","optId":"119","pdtName":"Onions: 1.5 Kg","images":"onions.png","unitValue":"1.5","unitName":"Kg","price":"72"},{"pdtId":"24","optId":"34","pdtName":"Green Chillies: 250 gms","images":"green_chillies.png","unitValue":"250","unitName":"gms","price":"23"},{"pdtId":"25","optId":"0","pdtName":"Potato","images":"potato1.png","unitValue":"1","unitName":"Kg","price":"52"},{"pdtId":"26","optId":"20","pdtName":"Carrot: 500 gms","images":"carrot.png","unitValue":"500","unitName":"gms","price":"52"},{"pdtId":"27","optId":"132","pdtName":"Ginger: 200 Gms","images":"ginger.png","unitValue":"200","unitName":"gms","price":"24"},{"pdtId":"28","optId":"133","pdtName":"Garlic: 100 gms","images":"garlic.png","unitValue":"100","unitName":"gms","price":"22"},{"pdtId":"29","optId":"0","pdtName":"Lady Finger","images":"ladies_finger.png","unitValue":"1","unitName":"Kg","price":"68"},{"pdtId":"34","optId":"44","pdtName":"Curry Leaves: 1 Bunch","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"64","optId":"0","pdtName":"Brinjal Purple Round","images":"Brinjal_purple_Round.jpg","unitValue":"1","unitName":"Kg","price":"60"},{"pdtId":"75","optId":"136","pdtName":"Ridge Gourd (birakaya): 1.5 Kg","images":"Ridge_gourd.jpg","unitValue":"1.5","unitName":"Kg","price":"96"}],"discount":0},{"id":"2241","product_name":"Small Basket","total_price":1,"grand_total":1,"product_id":"20","images":"vegetable_box.png","mrp_price":"1","type":"COMBO","purchase_quantity":"1","unit_name":"Kg","ESSENTIAL":[{"pdtId":"21","optId":"0","pdtName":"Tomato","images":"tomato1.png","unitValue":"1","unitName":"Kg","price":"35"},{"pdtId":"23","optId":"0","pdtName":"Onions","images":"onions.png","unitValue":"1","unitName":"Kg","price":"48"},{"pdtId":"24","optId":"137","pdtName":"Green Chillies: 100 gms","images":"green_chillies.png","unitValue":"100","unitName":"gms","price":"9"},{"pdtId":"25","optId":"35","pdtName":"Potato: 500 gms","images":"potato1.png","unitValue":"500","unitName":"gms","price":"26"},{"pdtId":"26","optId":"37","pdtName":"Carrot: 250 gms","images":"carrot.png","unitValue":"250","unitName":"gms","price":"26"},{"pdtId":"27","optId":"138","pdtName":"Ginger: 100 gms","images":"ginger.png","unitValue":"100","unitName":"gms","price":"12"},{"pdtId":"28","optId":"139","pdtName":"Garlic: 50 gms","images":"garlic.png","unitValue":"50","unitName":"gms","price":"11"},{"pdtId":"29","optId":"51","pdtName":"Lady Finger: 500 gms","images":"ladies_finger.png","unitValue":"500","unitName":"gms","price":"34"},{"pdtId":"34","optId":"44","pdtName":"Curry Leaves: 1 Bunch","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"64","optId":"99","pdtName":"Brinjal Purple Round: 500 Gms","images":"Brinjal_purple_Round.jpg","unitValue":"500","unitName":"gms","price":"30"},{"pdtId":"75","optId":"0","pdtName":"Ridge Gourd (birakaya)","images":"Ridge_gourd.jpg","unitValue":"1","unitName":"Kg","price":"64"}],"discount":0}],"finalTotal":535,"finalDiscount":0}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * records : [{"id":"2242","product_name":"Medium Basket","total_price":534,"grand_total":534,"product_id":"22","images":"VEGBOXSEASONAL.jpg","mrp_price":"534","type":"COMBO","purchase_quantity":"1","unit_name":"Kg","ESSENTIAL":[{"pdtId":"21","optId":"120","pdtName":"Tomato: 1.5 Kg","images":"tomato1.png","unitValue":"1.5","unitName":"Kg","price":"52.5"},{"pdtId":"23","optId":"119","pdtName":"Onions: 1.5 Kg","images":"onions.png","unitValue":"1.5","unitName":"Kg","price":"72"},{"pdtId":"24","optId":"34","pdtName":"Green Chillies: 250 gms","images":"green_chillies.png","unitValue":"250","unitName":"gms","price":"23"},{"pdtId":"25","optId":"0","pdtName":"Potato","images":"potato1.png","unitValue":"1","unitName":"Kg","price":"52"},{"pdtId":"26","optId":"20","pdtName":"Carrot: 500 gms","images":"carrot.png","unitValue":"500","unitName":"gms","price":"52"},{"pdtId":"27","optId":"132","pdtName":"Ginger: 200 Gms","images":"ginger.png","unitValue":"200","unitName":"gms","price":"24"},{"pdtId":"28","optId":"133","pdtName":"Garlic: 100 gms","images":"garlic.png","unitValue":"100","unitName":"gms","price":"22"},{"pdtId":"29","optId":"0","pdtName":"Lady Finger","images":"ladies_finger.png","unitValue":"1","unitName":"Kg","price":"68"},{"pdtId":"34","optId":"44","pdtName":"Curry Leaves: 1 Bunch","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"64","optId":"0","pdtName":"Brinjal Purple Round","images":"Brinjal_purple_Round.jpg","unitValue":"1","unitName":"Kg","price":"60"},{"pdtId":"75","optId":"136","pdtName":"Ridge Gourd (birakaya): 1.5 Kg","images":"Ridge_gourd.jpg","unitValue":"1.5","unitName":"Kg","price":"96"}],"discount":0},{"id":"2241","product_name":"Small Basket","total_price":1,"grand_total":1,"product_id":"20","images":"vegetable_box.png","mrp_price":"1","type":"COMBO","purchase_quantity":"1","unit_name":"Kg","ESSENTIAL":[{"pdtId":"21","optId":"0","pdtName":"Tomato","images":"tomato1.png","unitValue":"1","unitName":"Kg","price":"35"},{"pdtId":"23","optId":"0","pdtName":"Onions","images":"onions.png","unitValue":"1","unitName":"Kg","price":"48"},{"pdtId":"24","optId":"137","pdtName":"Green Chillies: 100 gms","images":"green_chillies.png","unitValue":"100","unitName":"gms","price":"9"},{"pdtId":"25","optId":"35","pdtName":"Potato: 500 gms","images":"potato1.png","unitValue":"500","unitName":"gms","price":"26"},{"pdtId":"26","optId":"37","pdtName":"Carrot: 250 gms","images":"carrot.png","unitValue":"250","unitName":"gms","price":"26"},{"pdtId":"27","optId":"138","pdtName":"Ginger: 100 gms","images":"ginger.png","unitValue":"100","unitName":"gms","price":"12"},{"pdtId":"28","optId":"139","pdtName":"Garlic: 50 gms","images":"garlic.png","unitValue":"50","unitName":"gms","price":"11"},{"pdtId":"29","optId":"51","pdtName":"Lady Finger: 500 gms","images":"ladies_finger.png","unitValue":"500","unitName":"gms","price":"34"},{"pdtId":"34","optId":"44","pdtName":"Curry Leaves: 1 Bunch","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"64","optId":"99","pdtName":"Brinjal Purple Round: 500 Gms","images":"Brinjal_purple_Round.jpg","unitValue":"500","unitName":"gms","price":"30"},{"pdtId":"75","optId":"0","pdtName":"Ridge Gourd (birakaya)","images":"Ridge_gourd.jpg","unitValue":"1","unitName":"Kg","price":"64"}],"discount":0}]
         * finalTotal : 535
         * finalDiscount : 0
         */

        private String currency;
        private int finalTotal;
        private int finalDiscount;
        private List<RecordsBean> records;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getFinalTotal() {
            return finalTotal;
        }

        public void setFinalTotal(int finalTotal) {
            this.finalTotal = finalTotal;
        }

        public int getFinalDiscount() {
            return finalDiscount;
        }

        public void setFinalDiscount(int finalDiscount) {
            this.finalDiscount = finalDiscount;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class RecordsBean {
            /**
             * id : 2242
             * product_name : Medium Basket
             * total_price : 534
             * grand_total : 534
             * product_id : 22
             * images : VEGBOXSEASONAL.jpg
             * mrp_price : 534
             * type : COMBO
             * purchase_quantity : 1
             * unit_name : Kg
             * ESSENTIAL : [{"pdtId":"21","optId":"120","pdtName":"Tomato: 1.5 Kg","images":"tomato1.png","unitValue":"1.5","unitName":"Kg","price":"52.5"},{"pdtId":"23","optId":"119","pdtName":"Onions: 1.5 Kg","images":"onions.png","unitValue":"1.5","unitName":"Kg","price":"72"},{"pdtId":"24","optId":"34","pdtName":"Green Chillies: 250 gms","images":"green_chillies.png","unitValue":"250","unitName":"gms","price":"23"},{"pdtId":"25","optId":"0","pdtName":"Potato","images":"potato1.png","unitValue":"1","unitName":"Kg","price":"52"},{"pdtId":"26","optId":"20","pdtName":"Carrot: 500 gms","images":"carrot.png","unitValue":"500","unitName":"gms","price":"52"},{"pdtId":"27","optId":"132","pdtName":"Ginger: 200 Gms","images":"ginger.png","unitValue":"200","unitName":"gms","price":"24"},{"pdtId":"28","optId":"133","pdtName":"Garlic: 100 gms","images":"garlic.png","unitValue":"100","unitName":"gms","price":"22"},{"pdtId":"29","optId":"0","pdtName":"Lady Finger","images":"ladies_finger.png","unitValue":"1","unitName":"Kg","price":"68"},{"pdtId":"34","optId":"44","pdtName":"Curry Leaves: 1 Bunch","images":"curry_leaf.png","unitValue":"1","unitName":"Bunch","price":"10"},{"pdtId":"36","optId":"0","pdtName":"Bottle Gourd","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","price":"30"},{"pdtId":"64","optId":"0","pdtName":"Brinjal Purple Round","images":"Brinjal_purple_Round.jpg","unitValue":"1","unitName":"Kg","price":"60"},{"pdtId":"75","optId":"136","pdtName":"Ridge Gourd (birakaya): 1.5 Kg","images":"Ridge_gourd.jpg","unitValue":"1.5","unitName":"Kg","price":"96"}]
             * discount : 0
             */

            private String id;
            private String product_name;
            private int total_price;
            private int grand_total;
            private String product_id;
            private String images;
            private String mrp_price;
            private String type;
            private String purchase_quantity;
            private String unit_name;
            private int discount;
            private List<ESSENTIALBean> ESSENTIAL;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public int getTotal_price() {
                return total_price;
            }

            public void setTotal_price(int total_price) {
                this.total_price = total_price;
            }

            public int getGrand_total() {
                return grand_total;
            }

            public void setGrand_total(int grand_total) {
                this.grand_total = grand_total;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getMrp_price() {
                return mrp_price;
            }

            public void setMrp_price(String mrp_price) {
                this.mrp_price = mrp_price;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getPurchase_quantity() {
                return purchase_quantity;
            }

            public void setPurchase_quantity(String purchase_quantity) {
                this.purchase_quantity = purchase_quantity;
            }

            public String getUnit_name() {
                return unit_name;
            }

            public void setUnit_name(String unit_name) {
                this.unit_name = unit_name;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public List<ESSENTIALBean> getESSENTIAL() {
                return ESSENTIAL;
            }

            public void setESSENTIAL(List<ESSENTIALBean> ESSENTIAL) {
                this.ESSENTIAL = ESSENTIAL;
            }

            public static class ESSENTIALBean {
                /**
                 * pdtId : 21
                 * optId : 120
                 * pdtName : Tomato: 1.5 Kg
                 * images : tomato1.png
                 * unitValue : 1.5
                 * unitName : Kg
                 * price : 52.5
                 */

                private String pdtId;
                private String optId;
                private String pdtName;
                private String images;
                private String unitValue;
                private String unitName;
                private String price;

                public String getPdtId() {
                    return pdtId;
                }

                public void setPdtId(String pdtId) {
                    this.pdtId = pdtId;
                }

                public String getOptId() {
                    return optId;
                }

                public void setOptId(String optId) {
                    this.optId = optId;
                }

                public String getPdtName() {
                    return pdtName;
                }

                public void setPdtName(String pdtName) {
                    this.pdtName = pdtName;
                }

                public String getImages() {
                    return images;
                }

                public void setImages(String images) {
                    this.images = images;
                }

                public String getUnitValue() {
                    return unitValue;
                }

                public void setUnitValue(String unitValue) {
                    this.unitValue = unitValue;
                }

                public String getUnitName() {
                    return unitName;
                }

                public void setUnitName(String unitName) {
                    this.unitName = unitName;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }
            }
        }
    }
}
