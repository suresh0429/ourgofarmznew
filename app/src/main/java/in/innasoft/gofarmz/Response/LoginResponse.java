package in.innasoft.gofarmz.Response;

public class LoginResponse {


    /**
     * status : 10100
     * message : You have been logged in successfully.
     * data : {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NzA4NTU4MTIsImp0aSI6IlFQYks0cDlyYUFlUXlGUFwvcFN0VWRNRXNQbHZPMVZZYlwvaXZJU3AwOXVDQT0iLCJpc3MiOiJodHRwOlwvXC9nb2Zhcm16djIubGVhcm5pbmdzbG90LmluXC9waHAtanNvblwvIiwibmJmIjoxNTcwODU1ODEzLCJleHAiOjE2MDIzOTE4MTMsImRhdGEiOnsidXNlcl9pZCI6IjQxMSIsInVzZXJfbmFtZSI6IlN1cmVzaCIsImVtYWlsIjoic3VyZXNoQGlubmFzb2Z0LmluIiwibW9iaWxlIjoiODk4NTAxODEwMyIsImFjY291bnRfc3RhdHVzIjoiMSIsImJyb3dzZXJfc2Vzc2lvbl9pZCI6IlFQYks0cDlyYUFlUXlGUFwvcFN0VWRNRXNQbHZPMVZZYlwvaXZJU3AwOXVDQT0ifX0.nSL4OhnQ78xSd5eeejz1MtXhJ2NhD0WhiKNyWAKY1Xtutx6vyJ504fDXCownzr1bdOun_mChBAL2WDC6ybPk7Q","user_id":"411","user_name":"Suresh","email":"suresh@innasoft.in","mobile":"8985018103"}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * jwt : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NzA4NTU4MTIsImp0aSI6IlFQYks0cDlyYUFlUXlGUFwvcFN0VWRNRXNQbHZPMVZZYlwvaXZJU3AwOXVDQT0iLCJpc3MiOiJodHRwOlwvXC9nb2Zhcm16djIubGVhcm5pbmdzbG90LmluXC9waHAtanNvblwvIiwibmJmIjoxNTcwODU1ODEzLCJleHAiOjE2MDIzOTE4MTMsImRhdGEiOnsidXNlcl9pZCI6IjQxMSIsInVzZXJfbmFtZSI6IlN1cmVzaCIsImVtYWlsIjoic3VyZXNoQGlubmFzb2Z0LmluIiwibW9iaWxlIjoiODk4NTAxODEwMyIsImFjY291bnRfc3RhdHVzIjoiMSIsImJyb3dzZXJfc2Vzc2lvbl9pZCI6IlFQYks0cDlyYUFlUXlGUFwvcFN0VWRNRXNQbHZPMVZZYlwvaXZJU3AwOXVDQT0ifX0.nSL4OhnQ78xSd5eeejz1MtXhJ2NhD0WhiKNyWAKY1Xtutx6vyJ504fDXCownzr1bdOun_mChBAL2WDC6ybPk7Q
         * user_id : 411
         * user_name : Suresh
         * email : suresh@innasoft.in
         * mobile : 8985018103
         */

        private String jwt;
        private String user_id;
        private String user_name;
        private String email;
        private String mobile;

        public String getJwt() {
            return jwt;
        }

        public void setJwt(String jwt) {
            this.jwt = jwt;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
