package in.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

public class ImageNotificationResponse {


    /**
     * status : 10100
     * message : Notification details.
     * data : {"id":"775","type":"PUSH","msg_title":"cvcxvcv","msg_txt":"cvxcvcxv","msg_image":"","users":"7456","is_read":"0","response":"","created_on":"2020-01-28 15:38:59"}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 775
         * type : PUSH
         * msg_title : cvcxvcv
         * msg_txt : cvxcvcxv
         * msg_image :
         * users : 7456
         * is_read : 0
         * response :
         * created_on : 2020-01-28 15:38:59
         */

        @SerializedName("id")
        private String id;
        @SerializedName("type")
        private String type;
        @SerializedName("msg_title")
        private String msgTitle;
        @SerializedName("msg_txt")
        private String msgTxt;
        @SerializedName("msg_image")
        private String msgImage;
        @SerializedName("users")
        private String users;
        @SerializedName("is_read")
        private String isRead;
        @SerializedName("response")
        private String response;
        @SerializedName("created_on")
        private String createdOn;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMsgTitle() {
            return msgTitle;
        }

        public void setMsgTitle(String msgTitle) {
            this.msgTitle = msgTitle;
        }

        public String getMsgTxt() {
            return msgTxt;
        }

        public void setMsgTxt(String msgTxt) {
            this.msgTxt = msgTxt;
        }

        public String getMsgImage() {
            return msgImage;
        }

        public void setMsgImage(String msgImage) {
            this.msgImage = msgImage;
        }

        public String getUsers() {
            return users;
        }

        public void setUsers(String users) {
            this.users = users;
        }

        public String getIsRead() {
            return isRead;
        }

        public void setIsRead(String isRead) {
            this.isRead = isRead;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }
    }
}
