package in.innasoft.gofarmz.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WalletgetResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : [{"id":"5","title":"test","amount":"12.00","txn_type":"DEBIT","event_flag":"ORDER_PAID","internal_ref_no":"dff","created_on":"2019-12-09 00:00:00"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 5
         * title : test
         * amount : 12.00
         * txn_type : DEBIT
         * event_flag : ORDER_PAID
         * internal_ref_no : dff
         * created_on : 2019-12-09 00:00:00
         */

        @SerializedName("id")
        private String id;
        @SerializedName("title")
        private String title;
        @SerializedName("amount")
        private String amount;
        @SerializedName("txn_type")
        private String txnType;
        @SerializedName("event_flag")
        private String eventFlag;
        @SerializedName("internal_ref_no")
        private String internalRefNo;
        @SerializedName("created_on")
        private String createdOn;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getTxnType() {
            return txnType;
        }

        public void setTxnType(String txnType) {
            this.txnType = txnType;
        }

        public String getEventFlag() {
            return eventFlag;
        }

        public void setEventFlag(String eventFlag) {
            this.eventFlag = eventFlag;
        }

        public String getInternalRefNo() {
            return internalRefNo;
        }

        public void setInternalRefNo(String internalRefNo) {
            this.internalRefNo = internalRefNo;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }
    }
}
