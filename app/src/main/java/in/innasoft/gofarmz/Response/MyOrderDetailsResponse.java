package in.innasoft.gofarmz.Response;

import java.util.List;

public class MyOrderDetailsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"currency":"Rs. ","total_products":1,"order_details":{"reference_id":"REF20190614062530","order_confirmation_reference_id":"GFZ2019061406253177","total_mrp_price":"1","discount":"0","shipping_charges":"0","final_price":"1","cancel_charges":"0","refund_price":"0","pincode":"500090","medium":"WEB","order_status":"Confirmed","status":"1","getaway_name":"Cash on Delivery","ip_address":"183.82.123.1","expected_delivery":"15-06-2019","expected_delivery_slot":"10:00 AM - 12:00 PM","order_date":"14-06-2019 06:25 pm"},"address":{"name":"Jay","mobile":"9949905540","alternate_contact_no":"","country":"India","state":"Telangana","city":"Hyderabad","area":"Nizampet","address":"Unnamed Road ","pincode":"500090"},"status":[{"order_status":"Confirmed","message":"Order has been Confirmed","created_on":"2019-06-14 18:25:31"}],"products":[{"id":"604","orders_pid":"1144","product_sku_id":"PQQE85137","product_id":"20","product_name":"Small Basket","count_name":"","images":"vegetable_box.png","product_mrp_price":"1","purchase_quantity":"1","total_price":"1","grand_total":"1","status":"1","child_products":[{"type":"ESSENTIAL","product_name":"Tomato","unit_name":"Kg","unit_value":"1","price":"35","discount":"0","final_price":"35","quantity":"1","created_on":"2019-06-14 18:25:30","images":"tomato1.png"},{"type":"ESSENTIAL","product_name":"Onions","unit_name":"Kg","unit_value":"1","price":"48","discount":"0","final_price":"48","quantity":"1","created_on":"2019-06-14 18:25:30","images":"onions.png"},{"type":"ESSENTIAL","product_name":"Green Chillies (100 gms)","unit_name":"gms","unit_value":"100","price":"9","discount":"0","final_price":"9","quantity":"1","created_on":"2019-06-14 18:25:30","images":"green_chillies.png"},{"type":"ESSENTIAL","product_name":"Potato (500 gms)","unit_name":"gms","unit_value":"500","price":"26","discount":"0","final_price":"26","quantity":"1","created_on":"2019-06-14 18:25:30","images":"potato1.png"},{"type":"ESSENTIAL","product_name":"Carrot (250 gms)","unit_name":"gms","unit_value":"250","price":"26","discount":"0","final_price":"26","quantity":"1","created_on":"2019-06-14 18:25:30","images":"carrot.png"},{"type":"ESSENTIAL","product_name":"Ginger (100 gms)","unit_name":"gms","unit_value":"100","price":"12","discount":"0","final_price":"12","quantity":"1","created_on":"2019-06-14 18:25:30","images":"ginger.png"},{"type":"ESSENTIAL","product_name":"Garlic (50 gms)","unit_name":"gms","unit_value":"50","price":"11","discount":"0","final_price":"11","quantity":"1","created_on":"2019-06-14 18:25:30","images":"garlic.png"},{"type":"ESSENTIAL","product_name":"Lady Finger (500 gms)","unit_name":"gms","unit_value":"500","price":"34","discount":"0","final_price":"34","quantity":"1","created_on":"2019-06-14 18:25:30","images":"ladies_finger.png"},{"type":"ESSENTIAL","product_name":"Curry Leaves (1 Bunch)","unit_name":"Bunch","unit_value":"1","price":"10","discount":"0","final_price":"10","quantity":"1","created_on":"2019-06-14 18:25:30","images":"curry_leaf.png"},{"type":"ESSENTIAL","product_name":"Bottle Gourd","unit_name":"Pieces","unit_value":"1","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2019-06-14 18:25:30","images":"bottle_gourd.png"},{"type":"ESSENTIAL","product_name":"Brinjal Purple Round (500 Gms)","unit_name":"gms","unit_value":"500","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2019-06-14 18:25:30","images":"Brinjal_purple_Round.jpg"},{"type":"ESSENTIAL","product_name":"Ridge Gourd (birakaya)","unit_name":"Kg","unit_value":"1","price":"64","discount":"0","final_price":"64","quantity":"1","created_on":"2019-06-14 18:25:30","images":"Ridge_gourd.jpg"}]}]}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currency : Rs.
         * total_products : 1
         * order_details : {"reference_id":"REF20190614062530","order_confirmation_reference_id":"GFZ2019061406253177","total_mrp_price":"1","discount":"0","shipping_charges":"0","final_price":"1","cancel_charges":"0","refund_price":"0","pincode":"500090","medium":"WEB","order_status":"Confirmed","status":"1","getaway_name":"Cash on Delivery","ip_address":"183.82.123.1","expected_delivery":"15-06-2019","expected_delivery_slot":"10:00 AM - 12:00 PM","order_date":"14-06-2019 06:25 pm"}
         * address : {"name":"Jay","mobile":"9949905540","alternate_contact_no":"","country":"India","state":"Telangana","city":"Hyderabad","area":"Nizampet","address":"Unnamed Road ","pincode":"500090"}
         * status : [{"order_status":"Confirmed","message":"Order has been Confirmed","created_on":"2019-06-14 18:25:31"}]
         * products : [{"id":"604","orders_pid":"1144","product_sku_id":"PQQE85137","product_id":"20","product_name":"Small Basket","count_name":"","images":"vegetable_box.png","product_mrp_price":"1","purchase_quantity":"1","total_price":"1","grand_total":"1","status":"1","child_products":[{"type":"ESSENTIAL","product_name":"Tomato","unit_name":"Kg","unit_value":"1","price":"35","discount":"0","final_price":"35","quantity":"1","created_on":"2019-06-14 18:25:30","images":"tomato1.png"},{"type":"ESSENTIAL","product_name":"Onions","unit_name":"Kg","unit_value":"1","price":"48","discount":"0","final_price":"48","quantity":"1","created_on":"2019-06-14 18:25:30","images":"onions.png"},{"type":"ESSENTIAL","product_name":"Green Chillies (100 gms)","unit_name":"gms","unit_value":"100","price":"9","discount":"0","final_price":"9","quantity":"1","created_on":"2019-06-14 18:25:30","images":"green_chillies.png"},{"type":"ESSENTIAL","product_name":"Potato (500 gms)","unit_name":"gms","unit_value":"500","price":"26","discount":"0","final_price":"26","quantity":"1","created_on":"2019-06-14 18:25:30","images":"potato1.png"},{"type":"ESSENTIAL","product_name":"Carrot (250 gms)","unit_name":"gms","unit_value":"250","price":"26","discount":"0","final_price":"26","quantity":"1","created_on":"2019-06-14 18:25:30","images":"carrot.png"},{"type":"ESSENTIAL","product_name":"Ginger (100 gms)","unit_name":"gms","unit_value":"100","price":"12","discount":"0","final_price":"12","quantity":"1","created_on":"2019-06-14 18:25:30","images":"ginger.png"},{"type":"ESSENTIAL","product_name":"Garlic (50 gms)","unit_name":"gms","unit_value":"50","price":"11","discount":"0","final_price":"11","quantity":"1","created_on":"2019-06-14 18:25:30","images":"garlic.png"},{"type":"ESSENTIAL","product_name":"Lady Finger (500 gms)","unit_name":"gms","unit_value":"500","price":"34","discount":"0","final_price":"34","quantity":"1","created_on":"2019-06-14 18:25:30","images":"ladies_finger.png"},{"type":"ESSENTIAL","product_name":"Curry Leaves (1 Bunch)","unit_name":"Bunch","unit_value":"1","price":"10","discount":"0","final_price":"10","quantity":"1","created_on":"2019-06-14 18:25:30","images":"curry_leaf.png"},{"type":"ESSENTIAL","product_name":"Bottle Gourd","unit_name":"Pieces","unit_value":"1","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2019-06-14 18:25:30","images":"bottle_gourd.png"},{"type":"ESSENTIAL","product_name":"Brinjal Purple Round (500 Gms)","unit_name":"gms","unit_value":"500","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2019-06-14 18:25:30","images":"Brinjal_purple_Round.jpg"},{"type":"ESSENTIAL","product_name":"Ridge Gourd (birakaya)","unit_name":"Kg","unit_value":"1","price":"64","discount":"0","final_price":"64","quantity":"1","created_on":"2019-06-14 18:25:30","images":"Ridge_gourd.jpg"}]}]
         */

        private String currency;
        private int total_products;
        private OrderDetailsBean order_details;
        private AddressBean address;
        private List<StatusBean> status;
        private List<ProductsBean> products;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getTotal_products() {
            return total_products;
        }

        public void setTotal_products(int total_products) {
            this.total_products = total_products;
        }

        public OrderDetailsBean getOrder_details() {
            return order_details;
        }

        public void setOrder_details(OrderDetailsBean order_details) {
            this.order_details = order_details;
        }

        public AddressBean getAddress() {
            return address;
        }

        public void setAddress(AddressBean address) {
            this.address = address;
        }

        public List<StatusBean> getStatus() {
            return status;
        }

        public void setStatus(List<StatusBean> status) {
            this.status = status;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public static class OrderDetailsBean {
            /**
             * reference_id : REF20190614062530
             * order_confirmation_reference_id : GFZ2019061406253177
             * total_mrp_price : 1
             * discount : 0
             * shipping_charges : 0
             * final_price : 1
             * cancel_charges : 0
             * refund_price : 0
             * pincode : 500090
             * medium : WEB
             * order_status : Confirmed
             * status : 1
             * getaway_name : Cash on Delivery
             * ip_address : 183.82.123.1
             * expected_delivery : 15-06-2019
             * expected_delivery_slot : 10:00 AM - 12:00 PM
             * order_date : 14-06-2019 06:25 pm
             */

            private String reference_id;
            private String order_confirmation_reference_id;
            private String total_mrp_price;
            private String discount;
            private String shipping_charges;
            private String final_price;
            private String cancel_charges;
            private String refund_price;
            private String pincode;
            private String medium;
            private String order_status;
            private String status;
            private String getaway_name;
            private String ip_address;
            private String expected_delivery;
            private String expected_delivery_slot;
            private String order_date;

            public String getReference_id() {
                return reference_id;
            }

            public void setReference_id(String reference_id) {
                this.reference_id = reference_id;
            }

            public String getOrder_confirmation_reference_id() {
                return order_confirmation_reference_id;
            }

            public void setOrder_confirmation_reference_id(String order_confirmation_reference_id) {
                this.order_confirmation_reference_id = order_confirmation_reference_id;
            }

            public String getTotal_mrp_price() {
                return total_mrp_price;
            }

            public void setTotal_mrp_price(String total_mrp_price) {
                this.total_mrp_price = total_mrp_price;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getShipping_charges() {
                return shipping_charges;
            }

            public void setShipping_charges(String shipping_charges) {
                this.shipping_charges = shipping_charges;
            }

            public String getFinal_price() {
                return final_price;
            }

            public void setFinal_price(String final_price) {
                this.final_price = final_price;
            }

            public String getCancel_charges() {
                return cancel_charges;
            }

            public void setCancel_charges(String cancel_charges) {
                this.cancel_charges = cancel_charges;
            }

            public String getRefund_price() {
                return refund_price;
            }

            public void setRefund_price(String refund_price) {
                this.refund_price = refund_price;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }

            public String getOrder_status() {
                return order_status;
            }

            public void setOrder_status(String order_status) {
                this.order_status = order_status;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getGetaway_name() {
                return getaway_name;
            }

            public void setGetaway_name(String getaway_name) {
                this.getaway_name = getaway_name;
            }

            public String getIp_address() {
                return ip_address;
            }

            public void setIp_address(String ip_address) {
                this.ip_address = ip_address;
            }

            public String getExpected_delivery() {
                return expected_delivery;
            }

            public void setExpected_delivery(String expected_delivery) {
                this.expected_delivery = expected_delivery;
            }

            public String getExpected_delivery_slot() {
                return expected_delivery_slot;
            }

            public void setExpected_delivery_slot(String expected_delivery_slot) {
                this.expected_delivery_slot = expected_delivery_slot;
            }

            public String getOrder_date() {
                return order_date;
            }

            public void setOrder_date(String order_date) {
                this.order_date = order_date;
            }
        }

        public static class AddressBean {
            /**
             * name : Jay
             * mobile : 9949905540
             * alternate_contact_no :
             * country : India
             * state : Telangana
             * city : Hyderabad
             * area : Nizampet
             * address : Unnamed Road
             * pincode : 500090
             */

            private String name;
            private String mobile;
            private String alternate_contact_no;
            private String country;
            private String state;
            private String city;
            private String area;
            private String address;
            private String pincode;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getAlternate_contact_no() {
                return alternate_contact_no;
            }

            public void setAlternate_contact_no(String alternate_contact_no) {
                this.alternate_contact_no = alternate_contact_no;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }
        }

        public static class StatusBean {
            /**
             * order_status : Confirmed
             * message : Order has been Confirmed
             * created_on : 2019-06-14 18:25:31
             */

            private String order_status;
            private String message;
            private String created_on;

            public String getOrder_status() {
                return order_status;
            }

            public void setOrder_status(String order_status) {
                this.order_status = order_status;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getCreated_on() {
                return created_on;
            }

            public void setCreated_on(String created_on) {
                this.created_on = created_on;
            }
        }

        public static class ProductsBean {
            /**
             * id : 604
             * orders_pid : 1144
             * product_sku_id : PQQE85137
             * product_id : 20
             * product_name : Small Basket
             * count_name :
             * images : vegetable_box.png
             * product_mrp_price : 1
             * purchase_quantity : 1
             * total_price : 1
             * grand_total : 1
             * status : 1
             * child_products : [{"type":"ESSENTIAL","product_name":"Tomato","unit_name":"Kg","unit_value":"1","price":"35","discount":"0","final_price":"35","quantity":"1","created_on":"2019-06-14 18:25:30","images":"tomato1.png"},{"type":"ESSENTIAL","product_name":"Onions","unit_name":"Kg","unit_value":"1","price":"48","discount":"0","final_price":"48","quantity":"1","created_on":"2019-06-14 18:25:30","images":"onions.png"},{"type":"ESSENTIAL","product_name":"Green Chillies (100 gms)","unit_name":"gms","unit_value":"100","price":"9","discount":"0","final_price":"9","quantity":"1","created_on":"2019-06-14 18:25:30","images":"green_chillies.png"},{"type":"ESSENTIAL","product_name":"Potato (500 gms)","unit_name":"gms","unit_value":"500","price":"26","discount":"0","final_price":"26","quantity":"1","created_on":"2019-06-14 18:25:30","images":"potato1.png"},{"type":"ESSENTIAL","product_name":"Carrot (250 gms)","unit_name":"gms","unit_value":"250","price":"26","discount":"0","final_price":"26","quantity":"1","created_on":"2019-06-14 18:25:30","images":"carrot.png"},{"type":"ESSENTIAL","product_name":"Ginger (100 gms)","unit_name":"gms","unit_value":"100","price":"12","discount":"0","final_price":"12","quantity":"1","created_on":"2019-06-14 18:25:30","images":"ginger.png"},{"type":"ESSENTIAL","product_name":"Garlic (50 gms)","unit_name":"gms","unit_value":"50","price":"11","discount":"0","final_price":"11","quantity":"1","created_on":"2019-06-14 18:25:30","images":"garlic.png"},{"type":"ESSENTIAL","product_name":"Lady Finger (500 gms)","unit_name":"gms","unit_value":"500","price":"34","discount":"0","final_price":"34","quantity":"1","created_on":"2019-06-14 18:25:30","images":"ladies_finger.png"},{"type":"ESSENTIAL","product_name":"Curry Leaves (1 Bunch)","unit_name":"Bunch","unit_value":"1","price":"10","discount":"0","final_price":"10","quantity":"1","created_on":"2019-06-14 18:25:30","images":"curry_leaf.png"},{"type":"ESSENTIAL","product_name":"Bottle Gourd","unit_name":"Pieces","unit_value":"1","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2019-06-14 18:25:30","images":"bottle_gourd.png"},{"type":"ESSENTIAL","product_name":"Brinjal Purple Round (500 Gms)","unit_name":"gms","unit_value":"500","price":"30","discount":"0","final_price":"30","quantity":"1","created_on":"2019-06-14 18:25:30","images":"Brinjal_purple_Round.jpg"},{"type":"ESSENTIAL","product_name":"Ridge Gourd (birakaya)","unit_name":"Kg","unit_value":"1","price":"64","discount":"0","final_price":"64","quantity":"1","created_on":"2019-06-14 18:25:30","images":"Ridge_gourd.jpg"}]
             */

            private String id;
            private String orders_pid;
            private String product_sku_id;
            private String product_id;
            private String product_name;
            private String count_name;
            private String images;
            private String product_mrp_price;
            private String purchase_quantity;
            private String total_price;
            private String grand_total;
            private String status;
            private List<ChildProductsBean> child_products;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getOrders_pid() {
                return orders_pid;
            }

            public void setOrders_pid(String orders_pid) {
                this.orders_pid = orders_pid;
            }

            public String getProduct_sku_id() {
                return product_sku_id;
            }

            public void setProduct_sku_id(String product_sku_id) {
                this.product_sku_id = product_sku_id;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getCount_name() {
                return count_name;
            }

            public void setCount_name(String count_name) {
                this.count_name = count_name;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getProduct_mrp_price() {
                return product_mrp_price;
            }

            public void setProduct_mrp_price(String product_mrp_price) {
                this.product_mrp_price = product_mrp_price;
            }

            public String getPurchase_quantity() {
                return purchase_quantity;
            }

            public void setPurchase_quantity(String purchase_quantity) {
                this.purchase_quantity = purchase_quantity;
            }

            public String getTotal_price() {
                return total_price;
            }

            public void setTotal_price(String total_price) {
                this.total_price = total_price;
            }

            public String getGrand_total() {
                return grand_total;
            }

            public void setGrand_total(String grand_total) {
                this.grand_total = grand_total;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public List<ChildProductsBean> getChild_products() {
                return child_products;
            }

            public void setChild_products(List<ChildProductsBean> child_products) {
                this.child_products = child_products;
            }

            public static class ChildProductsBean {
                /**
                 * type : ESSENTIAL
                 * product_name : Tomato
                 * unit_name : Kg
                 * unit_value : 1
                 * price : 35
                 * discount : 0
                 * final_price : 35
                 * quantity : 1
                 * created_on : 2019-06-14 18:25:30
                 * images : tomato1.png
                 */

                private String type;
                private String product_name;
                private String unit_name;
                private String unit_value;
                private String price;
                private String discount;
                private String final_price;
                private String quantity;
                private String created_on;
                private String images;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getProduct_name() {
                    return product_name;
                }

                public void setProduct_name(String product_name) {
                    this.product_name = product_name;
                }

                public String getUnit_name() {
                    return unit_name;
                }

                public void setUnit_name(String unit_name) {
                    this.unit_name = unit_name;
                }

                public String getUnit_value() {
                    return unit_value;
                }

                public void setUnit_value(String unit_value) {
                    this.unit_value = unit_value;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getDiscount() {
                    return discount;
                }

                public void setDiscount(String discount) {
                    this.discount = discount;
                }

                public String getFinal_price() {
                    return final_price;
                }

                public void setFinal_price(String final_price) {
                    this.final_price = final_price;
                }

                public String getQuantity() {
                    return quantity;
                }

                public void setQuantity(String quantity) {
                    this.quantity = quantity;
                }

                public String getCreated_on() {
                    return created_on;
                }

                public void setCreated_on(String created_on) {
                    this.created_on = created_on;
                }

                public String getImages() {
                    return images;
                }

                public void setImages(String images) {
                    this.images = images;
                }
            }
        }
    }
}
