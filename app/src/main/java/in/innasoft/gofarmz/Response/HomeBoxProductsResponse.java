package in.innasoft.gofarmz.Response;

import java.util.List;

public class HomeBoxProductsResponse {


    /**
     * status : 10100
     * content : Our Vegetable boxes come in three variants, Small, Medium and Large recommended according to your family size. You also have an option to customize the box as per your requirement.
     * versions : {"android_version":"1.9.13","ios_version":"1.4.3"}
     * message : Data fetch successfully
     * data : [{"id":"20","type":"COMBO","pdtName":"Small Basket","unitValue":"6","price":"1","about":"Small veggie box is designed to cater the needs of a small family of two people. You can customize the box by adding other items you require.","moreinfo":"","availability":"available","unitName":"Kg","images":"vegetable_box.png","isInCart":0,"cartQty":0,"ESSENTIAL":[{"pdtId":"21","optId":"0","childPdtName":"Tomato","optName":"1 Kg","images":"tomato1.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"23","optId":"0","childPdtName":"Onions","optName":"1 Kg","images":"onions.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"24","optId":"137","childPdtName":"Green Chillies","optName":"100 gms","images":"green_chillies.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"25","optId":"35","childPdtName":"Potato","optName":"500 gms","images":"potato1.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"26","optId":"37","childPdtName":"Carrot","optName":"250 gms","images":"carrot.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"27","optId":"138","childPdtName":"Ginger","optName":"100 gms","images":"ginger.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"28","optId":"139","childPdtName":"Garlic","optName":"50 gms","images":"garlic.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"29","optId":"51","childPdtName":"Lady Finger","optName":"500 gms","images":"ladies_finger.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"34","optId":"44","childPdtName":"Curry Leaves","optName":"1 Bunch","images":"curry_leaf.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"36","optId":"0","childPdtName":"Bottle Gourd","optName":"1 Pieces","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","isInCart":1,"cartQty":1},{"pdtId":"64","optId":"99","childPdtName":"Brinjal Purple Round","optName":"500 Gms","images":"Brinjal_purple_Round.jpg","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"75","optId":"0","childPdtName":"Ridge Gourd (birakaya)","optName":"1 Kg","images":"Ridge_gourd.jpg","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1}]},{"id":"22","type":"COMBO","pdtName":"Medium Basket","unitValue":"8","price":"534","about":"Medium veggie box is designed to cater the needs of a family of around four people. You can customize the box by adding other items you require.","moreinfo":"","availability":"available","unitName":"Kg","images":"VEGBOXSEASONAL.jpg","isInCart":0,"cartQty":0,"ESSENTIAL":[{"pdtId":"21","optId":"120","childPdtName":"Tomato","optName":"1.5 Kg","images":"tomato1.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"23","optId":"119","childPdtName":"Onions","optName":"1.5 Kg","images":"onions.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"24","optId":"34","childPdtName":"Green Chillies","optName":"250 gms","images":"green_chillies.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"25","optId":"0","childPdtName":"Potato","optName":"1 Kg","images":"potato1.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"26","optId":"20","childPdtName":"Carrot","optName":"500 gms","images":"carrot.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"27","optId":"132","childPdtName":"Ginger","optName":"200 Gms","images":"ginger.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"28","optId":"133","childPdtName":"Garlic","optName":"100 gms","images":"garlic.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"29","optId":"0","childPdtName":"Lady Finger","optName":"1 Kg","images":"ladies_finger.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"34","optId":"44","childPdtName":"Curry Leaves","optName":"1 Bunch","images":"curry_leaf.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"36","optId":"0","childPdtName":"Bottle Gourd","optName":"1 Pieces","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","isInCart":1,"cartQty":1},{"pdtId":"64","optId":"0","childPdtName":"Brinjal Purple Round","optName":"1 Kg","images":"Brinjal_purple_Round.jpg","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"75","optId":"136","childPdtName":"Ridge Gourd (birakaya)","optName":"1.5 Kg","images":"Ridge_gourd.jpg","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1}]},{"id":"32","type":"COMBO","pdtName":"Large Basket","unitValue":"13","price":"865","about":"Large veggie box is designed to cater the needs of a big family of around six to eight people. You can customize the box by adding other items you require.","moreinfo":"","availability":"available","unitName":"Kg","images":"vegetable_box1.png","isInCart":0,"cartQty":0,"ESSENTIAL":[{"pdtId":"21","optId":"141","childPdtName":"Tomato","optName":"2 Kg","images":"tomato1.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"23","optId":"124","childPdtName":"Onions","optName":"2 Kg","images":"onions.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"24","optId":"33","childPdtName":"Green Chillies","optName":"500 gms","images":"green_chillies.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"26","optId":"0","childPdtName":"Carrot","optName":"1 Kg","images":"carrot.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"27","optId":"39","childPdtName":"Ginger","optName":"250 gms","images":"ginger.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"28","optId":"130","childPdtName":"Garlic","optName":"200 gms","images":"garlic.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"29","optId":"0","childPdtName":"Lady Finger","optName":"1 Kg","images":"ladies_finger.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"34","optId":"128","childPdtName":"Curry Leaves","optName":"2 Bunch","images":"curry_leaf.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"36","optId":"0","childPdtName":"Bottle Gourd","optName":"1 Pieces","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","isInCart":1,"cartQty":1},{"pdtId":"37","optId":"0","childPdtName":"Little Gourd Donda","optName":"1 Kg","images":"little_gourd.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"64","optId":"0","childPdtName":"Brinjal Purple Round","optName":"1 Kg","images":"Brinjal_purple_Round.jpg","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"75","optId":"144","childPdtName":"Ridge Gourd (birakaya)","optName":"2 Kg","images":"Ridge_gourd.jpg","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1}]}]
     */

    private String status;
    private String content;
    private VersionsBean versions;
    private String message;
    private List<Object> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public VersionsBean getVersions() {
        return versions;
    }

    public void setVersions(VersionsBean versions) {
        this.versions = versions;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public static class VersionsBean {
        /**
         * android_version : 1.9.13
         * ios_version : 1.4.3
         */

        private String android_version;
        private String ios_version;

        public String getAndroid_version() {
            return android_version;
        }

        public void setAndroid_version(String android_version) {
            this.android_version = android_version;
        }

        public String getIos_version() {
            return ios_version;
        }

        public void setIos_version(String ios_version) {
            this.ios_version = ios_version;
        }
    }

    public static class DataBean {
        /**
         * id : 20
         * type : COMBO
         * pdtName : Small Basket
         * unitValue : 6
         * price : 1
         * about : Small veggie box is designed to cater the needs of a small family of two people. You can customize the box by adding other items you require.
         * moreinfo :
         * availability : available
         * unitName : Kg
         * images : vegetable_box.png
         * isInCart : 0
         * cartQty : 0
         * ESSENTIAL : [{"pdtId":"21","optId":"0","childPdtName":"Tomato","optName":"1 Kg","images":"tomato1.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"23","optId":"0","childPdtName":"Onions","optName":"1 Kg","images":"onions.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"24","optId":"137","childPdtName":"Green Chillies","optName":"100 gms","images":"green_chillies.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"25","optId":"35","childPdtName":"Potato","optName":"500 gms","images":"potato1.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"26","optId":"37","childPdtName":"Carrot","optName":"250 gms","images":"carrot.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"27","optId":"138","childPdtName":"Ginger","optName":"100 gms","images":"ginger.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"28","optId":"139","childPdtName":"Garlic","optName":"50 gms","images":"garlic.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"29","optId":"51","childPdtName":"Lady Finger","optName":"500 gms","images":"ladies_finger.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"34","optId":"44","childPdtName":"Curry Leaves","optName":"1 Bunch","images":"curry_leaf.png","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"36","optId":"0","childPdtName":"Bottle Gourd","optName":"1 Pieces","images":"bottle_gourd.png","unitValue":"1","unitName":"Pieces","isInCart":1,"cartQty":1},{"pdtId":"64","optId":"99","childPdtName":"Brinjal Purple Round","optName":"500 Gms","images":"Brinjal_purple_Round.jpg","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1},{"pdtId":"75","optId":"0","childPdtName":"Ridge Gourd (birakaya)","optName":"1 Kg","images":"Ridge_gourd.jpg","unitValue":"1","unitName":"Kg","isInCart":1,"cartQty":1}]
         */

        private String id;
        private String type;
        private String pdtName;
        private String unitValue;
        private String price;
        private String about;
        private String moreinfo;
        private String availability;
        private String unitName;
        private String images;
        private int isInCart;
        private int cartQty;
        private List<ESSENTIALBean> ESSENTIAL;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPdtName() {
            return pdtName;
        }

        public void setPdtName(String pdtName) {
            this.pdtName = pdtName;
        }

        public String getUnitValue() {
            return unitValue;
        }

        public void setUnitValue(String unitValue) {
            this.unitValue = unitValue;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public String getMoreinfo() {
            return moreinfo;
        }

        public void setMoreinfo(String moreinfo) {
            this.moreinfo = moreinfo;
        }

        public String getAvailability() {
            return availability;
        }

        public void setAvailability(String availability) {
            this.availability = availability;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public int getIsInCart() {
            return isInCart;
        }

        public void setIsInCart(int isInCart) {
            this.isInCart = isInCart;
        }

        public int getCartQty() {
            return cartQty;
        }

        public void setCartQty(int cartQty) {
            this.cartQty = cartQty;
        }

        public List<ESSENTIALBean> getESSENTIAL() {
            return ESSENTIAL;
        }

        public void setESSENTIAL(List<ESSENTIALBean> ESSENTIAL) {
            this.ESSENTIAL = ESSENTIAL;
        }

        public static class ESSENTIALBean {
            /**
             * pdtId : 21
             * optId : 0
             * childPdtName : Tomato
             * optName : 1 Kg
             * images : tomato1.png
             * unitValue : 1
             * unitName : Kg
             * isInCart : 1
             * cartQty : 1
             */

            private String pdtId;
            private String optId;
            private String childPdtName;
            private String optName;
            private String images;
            private String unitValue;
            private String unitName;
            private int isInCart;
            private int cartQty;

            public String getPdtId() {
                return pdtId;
            }

            public void setPdtId(String pdtId) {
                this.pdtId = pdtId;
            }

            public String getOptId() {
                return optId;
            }

            public void setOptId(String optId) {
                this.optId = optId;
            }

            public String getChildPdtName() {
                return childPdtName;
            }

            public void setChildPdtName(String childPdtName) {
                this.childPdtName = childPdtName;
            }

            public String getOptName() {
                return optName;
            }

            public void setOptName(String optName) {
                this.optName = optName;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getUnitValue() {
                return unitValue;
            }

            public void setUnitValue(String unitValue) {
                this.unitValue = unitValue;
            }

            public String getUnitName() {
                return unitName;
            }

            public void setUnitName(String unitName) {
                this.unitName = unitName;
            }

            public int getIsInCart() {
                return isInCart;
            }

            public void setIsInCart(int isInCart) {
                this.isInCart = isInCart;
            }

            public int getCartQty() {
                return cartQty;
            }

            public void setCartQty(int cartQty) {
                this.cartQty = cartQty;
            }
        }
    }
}
