package in.innasoft.gofarmz.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


import com.victor.loading.rotate.RotateLoading;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.NetworkChecking;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class FAQFragment extends Fragment {

    View view;
    WebView faq_web;
   RotateLoading progress_indicator;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;

    public FAQFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_faq, container, false);

        faq_web = view.findViewById(R.id.faq_web);
       progress_indicator = view.findViewById(R.id.progress_indicator);
   //    progress_indicator.start();

        pprogressDialog = new ProgressDialog(getContext());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        faq_web.setInitialScale(1);
        faq_web.getSettings().setJavaScriptEnabled(true);
        faq_web.getSettings().setLoadWithOverviewMode(true);
        faq_web.getSettings().setBuiltInZoomControls(true);
        faq_web.getSettings().setUseWideViewPort(true);
        faq_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        faq_web.setScrollbarFadingEnabled(false);
        faq_web.setWebViewClient(new WebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);

       // faq_web.loadUrl(AppUrls.API_URL+AppUrls.FAQ);

        faq_web.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

         //   progress_indicator.stop();

            }
        });


loadDataInWebview();

        return view;




    }

    private void loadDataInWebview() {
        // adSliderList.clear();
        checkInternet = NetworkChecking.isConnected(getContext());
        if (checkInternet) {

            pprogressDialog.show();
            Call<ResponseBody> call = RetrofitClient.getInstance().getApi().faq();
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    pprogressDialog.dismiss();
                    try{
                        assert response.body() != null;
                        String html = response.body().string();
                        Document document = Jsoup.parse(html);
                        Log.d(TAG, "onResponse: "+document);
                        faq_web.loadData(String.valueOf(document),"text/html", null);

                    }catch (Exception e){
                        e.printStackTrace();
                    }




                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    pprogressDialog.dismiss();
                }
            });


        } else {
            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }


}
