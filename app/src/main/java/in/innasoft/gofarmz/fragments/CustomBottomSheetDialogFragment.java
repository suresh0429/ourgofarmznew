package in.innasoft.gofarmz.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.AreasActivity;
import in.innasoft.gofarmz.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class CustomBottomSheetDialogFragment extends BottomSheetDialogFragment {

    private BottomSheetBehavior mBehavior;
    ImageView img_cancel;
    TextView txtClickHere;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        View view = View.inflate(getContext(), R.layout.bottomsheet, null);

        img_cancel=view.findViewById(R.id.img_cancel);

        txtClickHere=view.findViewById(R.id.txtClickHere);
        txtClickHere.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences(Utilities.PREFS_LOCATION, 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
                editor.apply();

                dismiss();
            }
        });
        txtClickHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getActivity().getSharedPreferences(Utilities.PREFS_LOCATION, 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
                editor.apply();
                dialog.dismiss();
                Intent intent=new Intent(getActivity(), AreasActivity.class);
                startActivity(intent);

            }
        });
        dialog.setContentView(view);
        mBehavior = BottomSheetBehavior.from((View) view.getParent());
        return dialog;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences preferences = getActivity().getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
        editor.apply();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        SharedPreferences preferences = getActivity().getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
        editor.apply();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SharedPreferences preferences = getActivity().getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
        editor.apply();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        SharedPreferences preferences = getActivity().getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
        editor.apply();

    }
}