package in.innasoft.gofarmz.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devolper on 08-Nov-17.
 */

public class PoliciesViewPagerAdapter extends FragmentPagerAdapter
{

    private final List<Fragment> gFragmentList = new ArrayList<>();
    private final List<String> gFragmentTitleList = new ArrayList<>();

    public PoliciesViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return gFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return gFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        gFragmentList.add(fragment);
        gFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return gFragmentTitleList.get(position);
    }
}
