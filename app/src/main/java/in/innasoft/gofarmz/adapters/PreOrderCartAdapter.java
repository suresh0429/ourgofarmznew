package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.PreOrderCartResponse;
import in.innasoft.gofarmz.Response.PreOrderDeleteCartResponse;
import in.innasoft.gofarmz.Response.PreOrderUpdateCartResponse;
import in.innasoft.gofarmz.Response.UpdatePreOrderCardDeleteResponse;
import in.innasoft.gofarmz.Response.UpdatePreOrderCartResponse;
import in.innasoft.gofarmz.activities.PreOrderActivity;
import in.innasoft.gofarmz.activities.PreOrderCartActivity;
import in.innasoft.gofarmz.holders.PreOrderCartHolder;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class PreOrderCartAdapter extends RecyclerView.Adapter<PreOrderCartHolder> {

    private PreOrderCartActivity context;
    private LayoutInflater li;
    private int resource;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, price, p_id;
    SweetAlertDialog sd;
    List<PreOrderCartResponse.DataBean> dataBeanList;
    String qtystr;
    Gson gson;
    UpdatePreOrderCartResponse updatePreOrderCartResponse;
    UpdatePreOrderCardDeleteResponse updatePreOrderCardDeleteResponse;

    public PreOrderCartAdapter(PreOrderCartActivity context, List<PreOrderCartResponse.DataBean> dataBeanList, int resource) {
        this.context = context;
        this.dataBeanList = dataBeanList;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);
    }

    @NonNull
    @Override
    public PreOrderCartHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layout = li.inflate(resource, viewGroup, false);
        PreOrderCartHolder slh = new PreOrderCartHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(@NonNull final PreOrderCartHolder holder, final int i) {

        holder.product_name.setText(dataBeanList.get(i).getProduct_name());

        String price = dataBeanList.get(i).getTotal_price();
        Double d = Double.parseDouble(price);
        holder.price_txt.setText("" + Math.round(d));

        holder.cart_qty_txt.setText(dataBeanList.get(i).getQuantity());

        Picasso.with(context)
                .load(dataBeanList.get(i).getPop_image())
                .placeholder(R.drawable.cart)
                .into(holder.product_img);



        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });


        holder.delete_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    sd = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);

                    sd.setTitleText("Are you sure?");
                    sd.setContentText("This product will be delete from cart!");
                    sd.setCancelText("No,cancel pls!");
                    sd.setConfirmText("Yes,delete it!");
                    sd.showCancelButton(true);

                    sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sweetAlertDialog) {
                            p_id = dataBeanList.get(i).getProduct_id();

                            Call<PreOrderDeleteCartResponse> cartResponseCall= RetrofitClient.getInstance().getApi().PreOrderDeleteCart(token,deviceId,user_id,p_id);
                            cartResponseCall.enqueue(new Callback<PreOrderDeleteCartResponse>() {
                                @Override
                                public void onResponse(Call<PreOrderDeleteCartResponse> call, retrofit2.Response<PreOrderDeleteCartResponse> response) {
                                    if (response.isSuccessful());
                                    PreOrderDeleteCartResponse preOrderDeleteCartResponse=response.body();
                                    if (preOrderDeleteCartResponse.getStatus().equals("10100")){
                                        sweetAlertDialog.cancel();
                                        dataBeanList.remove(i);
                                        context.refreshCartPage();

                                        if (dataBeanList.size() == 0) {
                                            Intent intent = new Intent(context, PreOrderActivity.class);
                                            context.startActivity(intent);
                                        }

                                        Toast.makeText(context, preOrderDeleteCartResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                    }
                                    if (preOrderDeleteCartResponse.getStatus().equals("10200")) {
                                        sweetAlertDialog.cancel();
                                        Toast.makeText(context, preOrderDeleteCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<PreOrderDeleteCartResponse> call, Throwable t) {
                                    sweetAlertDialog.cancel();
                                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            });

                        }
                    });


                    sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    sd.show();
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.cart_add_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String qtystr = holder.cart_qty_txt.getText().toString();
                p_id = dataBeanList.get(i).getProduct_id();
                int qty = Integer.parseInt(qtystr);
                if (qty != 0)
                    qty++;
                Log.d("increasequantity", String.valueOf(qty));
//                    holder.cart_qty_txt.setText(String.valueOf(qty));
                increaseQuatity(p_id, qty);
            }
        });

        holder.cart_minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p_id = dataBeanList.get(i).getProduct_id();
                qtystr = holder.cart_qty_txt.getText().toString();
                int qty = Integer.parseInt(qtystr);
                if (qty > 1)
                    qty--;
                Log.d("decreasequantity", String.valueOf(qty));
//                holder.cart_qty_txt.setText(String.valueOf(qty));
                increaseQuatity(p_id, qty);
            }
        });

    }


    private void increaseQuatity(final String p_id, final int qty) {

        boolean checkInternet = NetworkChecking.isConnected(context);
     //   String URL = AppUrls.API_URL + AppUrls.PREORDERUPDATECART;
        if (checkInternet) {

            Call<PreOrderUpdateCartResponse> cartResponseCall=RetrofitClient.getInstance().getApi().PreOrderUpdateCart1(token,deviceId,user_id,p_id,String.valueOf(qty));
            cartResponseCall.enqueue(new Callback<PreOrderUpdateCartResponse>() {
                @Override
                public void onResponse(Call<PreOrderUpdateCartResponse> call, retrofit2.Response<PreOrderUpdateCartResponse> response) {
                    if (response.isSuccessful());
                    PreOrderUpdateCartResponse preOrderUpdateCartResponse=response.body();
                    if (preOrderUpdateCartResponse.getStatus().equals("10100")){
                       // progressDialog.dismiss();
                        // cartCountTxt.setText(String.valueOf(preOrderUpdateCartResponse.getCart_count()));
                        context.getPreOrderCartData();

                        Toast.makeText(context, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_LONG).show();

                    }
                    if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10200")) {
                      //  progressDialog.dismiss();
                        Toast.makeText(context, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10300")) {
                    //    progressDialog.dismiss();
                        Toast.makeText(context, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (preOrderUpdateCartResponse.getStatus().equalsIgnoreCase("10400")) {
                      //  progressDialog.dismiss();
                        Toast.makeText(context, preOrderUpdateCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<PreOrderUpdateCartResponse> call, Throwable t) {
                   // progressDialog.dismiss();
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return this.dataBeanList.size();
    }
}
