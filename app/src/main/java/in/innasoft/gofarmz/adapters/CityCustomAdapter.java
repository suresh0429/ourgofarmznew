package in.innasoft.gofarmz.adapters;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.models.CitiesModel;

public class CityCustomAdapter extends ArrayAdapter<CitiesModel> {
    private List<CitiesModel> stateList = new ArrayList<>();

    public CityCustomAdapter(@NonNull Context context, int resource, int spinnerText, @NonNull List<CitiesModel> stateList) {
        super(context, resource, spinnerText, stateList);
        this.stateList = stateList;
    }

    @Override
    public CitiesModel getItem(int position) {
        return stateList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position);

    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position);
    }

    private View initView(int position) {
        CitiesModel cirty = getItem(position);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.row_cities_list, null);
        TextView textView = v.findViewById(R.id.city_txt);
        textView.setText(cirty.getName());
        return v;

    }
}



