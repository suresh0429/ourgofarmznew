package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.Response.AddressDeleteResponse;
import in.innasoft.gofarmz.Response.AddressListResponse;
import in.innasoft.gofarmz.activities.ChooseAddressActivity;
import in.innasoft.gofarmz.activities.DeliverySlotsActivity;
import in.innasoft.gofarmz.activities.EditAddressActivity;
import in.innasoft.gofarmz.holders.AddressesHolder;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class AddressesAdapter extends RecyclerView.Adapter<AddressesHolder> {

    List<AddressListResponse.DataBean> dataBeanList;
    private ChooseAddressActivity context;
    private LayoutInflater layoutInflater;
    private int resource;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile, activity;
    SweetAlertDialog sd;

    public AddressesAdapter(List<AddressListResponse.DataBean> dataBeanList, ChooseAddressActivity context, int resource, String activity) {
        this.dataBeanList = dataBeanList;
        this.context = context;
        this.resource = resource;
        this.activity = activity;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);
    }

    @Override
    public AddressesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = layoutInflater.inflate(resource, parent, false);
        AddressesHolder slh = new AddressesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AddressesHolder holder, final int position) {


        holder.name_txt.setText(dataBeanList.get(position).getName());

        holder.address_one_txt.setText(dataBeanList.get(position).getAddress_line1());


        holder.address_two_txt.setText(dataBeanList.get(position).getAddress_line2());


        holder.area_txt.setText(dataBeanList.get(position).getArea());


        holder.city_txt.setText(dataBeanList.get(position).getCity());


        holder.state_txt.setText(dataBeanList.get(position).getState());


        holder.pincode_txt.setText(dataBeanList.get(position).getPincode());

        holder.mobile_txt.setText(dataBeanList.get(position).getContact_no() + ", " + dataBeanList.get(position).getAlternate_contact_no());
        final String id=dataBeanList.get(position).getId();

        holder.edit_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    Intent intent = new Intent(context, EditAddressActivity.class);
                    intent.putExtra("id", id);
                    intent.putExtra("name", dataBeanList.get(position).getName());
                    intent.putExtra("addressline1", dataBeanList.get(position).getAddress_line1());
                    intent.putExtra("addressline2", dataBeanList.get(position).getAddress_line2());
                    intent.putExtra("area", dataBeanList.get(position).getArea());
                    intent.putExtra("city", dataBeanList.get(position).getCity());
                    intent.putExtra("state", dataBeanList.get(position).getState());
                    intent.putExtra("pincode", dataBeanList.get(position).getPincode());
                    intent.putExtra("contactNo", dataBeanList.get(position).getContact_no());
                    intent.putExtra("alternatecontactNo", dataBeanList.get(position).getAlternate_contact_no());
                    intent.putExtra("latitude", dataBeanList.get(position).getLatitude());
                    intent.putExtra("longitude", dataBeanList.get(position).getLongitude());
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.delete_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    sd = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                    sd.setTitleText("Are you sure?");
                    sd.setContentText("This product will be delete from cart!");
                    sd.setCancelText("No,cancel pls!");
                    sd.setConfirmText("Yes,delete it!");
                    sd.showCancelButton(true);

                    sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sweetAlertDialog) {

                            Call<AddressDeleteResponse> call= RetrofitClient.getInstance().getApi().DeleteAddress(token,deviceId,user_id,id);
                            call.enqueue(new Callback<AddressDeleteResponse>() {
                                @Override
                                public void onResponse(Call<AddressDeleteResponse> call, retrofit2.Response<AddressDeleteResponse> response) {
                                    if (response.isSuccessful());
                                    AddressDeleteResponse addressDeleteResponse=response.body();
                                    if (addressDeleteResponse.getStatus().equals("10100")){
                                        sweetAlertDialog.cancel();
                                        dataBeanList.remove(position);
                                        //context.getAddresses();
                                        notifyDataSetChanged();

                                        Toast.makeText(context, addressDeleteResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                    }
                                    else if (addressDeleteResponse.getStatus().equals("10200")){
                                        sweetAlertDialog.cancel();
                                        Toast.makeText(context, "Invalid Input..", Toast.LENGTH_SHORT).show();

                                    }
                                    else if (addressDeleteResponse.getStatus().equals("10300")){
                                        sweetAlertDialog.cancel();
                                        Toast.makeText(context, addressDeleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                    else if (addressDeleteResponse.getStatus().equals("10400")){
                                        sweetAlertDialog.cancel();
                                        Toast.makeText(context, addressDeleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddressDeleteResponse> call, Throwable t) {
                                    sweetAlertDialog.cancel();
                                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                    });


                    sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    sd.show();

                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

       /* holder.setItemClickListener(new AddressesItemClickListener() {
            @Override
            public void onItemClick(View v, int pos)
            {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    Intent intent = new Intent(context, PaymentDetails.class);
                    intent.putExtra("activity", "ChooseAddressAdapter");
                    intent.putExtra("id", addressesModels.get(position).getId());
                    intent.putExtra("contact_no", addressesModels.get(position).getContact_no());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        holder.selectAddText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {

                    if (activity.equalsIgnoreCase("PreOrderCartActivity") ||
                            activity.equalsIgnoreCase("PreOrderProductViewActivity")) {
                        Intent intent = new Intent(context, DeliverySlotsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("activity", "PreOrder");
                        intent.putExtra("id", dataBeanList.get(position).getId());
                        intent.putExtra("contact_no", dataBeanList.get(position).getContact_no());
                        Log.d("PreOrderADDRESS",dataBeanList.get(position).getId()+"/"+dataBeanList.get(position).getContact_no());
                        context.startActivity(intent);
                    }  else if (activity.equalsIgnoreCase("ChooseAddress")){
                        Intent intent = new Intent(context, DeliverySlotsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("activity", "ChooseAddress");
                        intent.putExtra("id", dataBeanList.get(position).getId());
                        intent.putExtra("contact_no", dataBeanList.get(position).getContact_no());
                        Log.d("CHOOSEADDRESS",dataBeanList.get(position).getId()+"/"+dataBeanList.get(position).getContact_no());
                        context.startActivity(intent);
                    } else{
                        Intent intent = new Intent(context, DeliverySlotsActivity.class);
                        //intent.putExtra("activity", "ChooseAddressActivity");
                        intent.putExtra("activity", "ChooseAddressAdapter");
                        intent.putExtra("id", dataBeanList.get(position).getId());
                        intent.putExtra("contact_no", dataBeanList.get(position).getContact_no());
                        context.startActivity(intent);
                    }

                    /*Intent intent = new Intent(context, DeliverySlotsActivity.class);
                    intent.putExtra("activity", "ChooseAddressAdapter");
                    intent.putExtra("id", addressesModels.get(position).getId());
                    intent.putExtra("contact_no", addressesModels.get(position).getContact_no());
                    context.startActivity(intent);*/
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }
}
