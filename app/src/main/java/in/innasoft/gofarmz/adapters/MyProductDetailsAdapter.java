package in.innasoft.gofarmz.adapters;

import android.app.Dialog;
import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.CartResponse;
import in.innasoft.gofarmz.activities.PaymentDetails;
import in.innasoft.gofarmz.holders.MyCartHolder;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

import static in.innasoft.gofarmz.Api.RetrofitClient.IMAGE_URL;


public class MyProductDetailsAdapter extends RecyclerView.Adapter<MyCartHolder> {

//    private ArrayList<Object> cartList;
    private ArrayList<Object> carChldtList = new ArrayList<>();
    private PaymentDetails context;
    private LayoutInflater li;
    private int resource;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, price,TYPE;
    Dialog dialog;
    List<CartResponse.DataBean.RecordsBean> recordsBeanList;

    public MyProductDetailsAdapter(PaymentDetails context, List<CartResponse.DataBean.RecordsBean> recordsBeanList, int resource) {
        this.recordsBeanList = recordsBeanList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);
    }

    @Override
    public MyCartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        MyCartHolder slh = new MyCartHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final MyCartHolder holder, final int position) {

//        final Map<String, Object> productObject = (Map<String, Object>) cartList.get(position);

        holder.delete_img.setVisibility(View.GONE);
        holder.cart_qty_ll.setVisibility(View.GONE);

        holder.product_name.setText(recordsBeanList.get(position).getProduct_name());

        TYPE=recordsBeanList.get(position).getType();
        TYPE = TYPE.replaceAll("_", " ").toLowerCase();
        holder.product_type.setText(TYPE);

        String price=String.valueOf(recordsBeanList.get(position).getTotal_price());
        Double d=Double.parseDouble(price);
        holder.price_txt.setText(""+Math.round(d));

        holder.qty_btn.setText("Qty : " + recordsBeanList.get(position).getPurchase_quantity());

        Picasso.with(context)
                .load(IMAGE_URL + "70x70/" + recordsBeanList.get(position).getImages())
                .placeholder(R.drawable.cart)
                .into(holder.product_img);

        holder.delete_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {

                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.view_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
//                    String type = (String) productObject.get("type");
//                    String name=productObject.get("product_name").toString();
//                    if (type.equalsIgnoreCase("COMBO_CUSTOM"))
//                        carChldtList = (ArrayList<Object>) productObject.get("ADDONS");
//                    else
//                        carChldtList = (ArrayList<Object>) productObject.get("ESSENTIAL");
//                    Intent it=new Intent(context, ViewDetails.class);
//                    it.putExtra("productlist",carChldtList);
//                    it.putExtra("TYPE",type);
//                    context.startActivity(it);

                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });
    }

    @Override
    public int getItemCount() {

        return this.recordsBeanList.size();
    }
}
