package in.innasoft.gofarmz.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.PreOrderCheckoutDataResponse;
import in.innasoft.gofarmz.activities.PaymentDetails;

public class PreOrderProductdetailsAdapter extends RecyclerView.Adapter<PreOrderProductdetailsAdapter.Holder> {

    List<PreOrderCheckoutDataResponse.DataBean.ProductsBean> productsBeanList;
    Context context;

    public PreOrderProductdetailsAdapter(PaymentDetails context, List<PreOrderCheckoutDataResponse.DataBean.ProductsBean> productsBeanList) {
        this.context=context;
        this.productsBeanList=productsBeanList;
    }

    @NonNull
    @Override
    public PreOrderProductdetailsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_preorder_product_payment_details, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PreOrderProductdetailsAdapter.Holder holder, int i) {
        holder.product_name.setText(productsBeanList.get(i).getProductName());
        holder.price_txt.setText(""+productsBeanList.get(i).getTotalPrice());
        holder.qty_btn.setText("Qty : "+productsBeanList.get(i).getQuantity());
    }

    @Override
    public int getItemCount() {
        return productsBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder{

        TextView product_name,qty_btn,price_txt;
        public Holder(@NonNull View itemView) {
            super(itemView);
            price_txt=itemView.findViewById(R.id.price_txt);
            product_name=itemView.findViewById(R.id.product_name);
            qty_btn=itemView.findViewById(R.id.qty_btn);
        }
    }
}
