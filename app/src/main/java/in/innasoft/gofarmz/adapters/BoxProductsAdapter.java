package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

import in.innasoft.gofarmz.R;

import static in.innasoft.gofarmz.Api.RetrofitClient.IMAGE_URL;

public class BoxProductsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Object> boxProducts = new ArrayList<>();

    public BoxProductsAdapter(Context ctx, ArrayList<Object> tmp) {
        context = ctx;
        boxProducts = tmp;

       }

    @Override
    public int getCount() {
        return boxProducts.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.row_box_products, null, true);

        Map<String, Object> tmp = (Map<String, Object>) boxProducts.get(position);

        ImageView product_img = view.findViewById(R.id.product_img);
        Picasso.with(context)
                .load(IMAGE_URL + "70x70/" + (String) tmp.get("images"))
                .placeholder(R.drawable.cart)
                .into(product_img);

        TextView product_name_txt = view.findViewById(R.id.product_name_txt);

        product_name_txt.setText((String) tmp.get("childPdtName"));

        TextView qty_txt = view.findViewById(R.id.qty_txt);

        qty_txt.setText((String) tmp.get("optName")); //+ (String) tmp.get("unitName"));

        return view;
    }
}
