package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.ViewCartIncreaseResponse;
import in.innasoft.gofarmz.Response.ViewDetailsResponse;
import in.innasoft.gofarmz.activities.ViewDetails;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;

import static in.innasoft.gofarmz.Api.RetrofitClient.IMAGE_URL;


public class CartRecordChild extends BaseAdapter {

    private ViewDetails context;
    List<ViewDetailsResponse.DataBean.RecordsBean> recordsBeanList;
    String type_get, IDD, deviceId, user_id, token;
    UserSessionManager userSessionManager;

    public CartRecordChild(ViewDetails ctx, List<ViewDetailsResponse.DataBean.RecordsBean> recordsBean, String type) {
        context = ctx;
        recordsBeanList =recordsBean;
        type_get = type;
        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);
    }

    @Override
    public int getCount() {
        return recordsBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.row_cart_child_record_list, null, true);

//        final Map<String, Object> tmp = (Map<String, Object>) cartRecordChild.get(position);

        ImageView child_img = view.findViewById(R.id.child_img);
        Picasso.with(context)
                .load(IMAGE_URL + "70x70/" +recordsBeanList.get(position).getImages())
                .placeholder(R.drawable.cart)
                .into(child_img);

        LinearLayout cart_qty_ll = view.findViewById(R.id.cart_qty_ll);
        final TextView qty_txt = view.findViewById(R.id.qty_txt);
        final ImageView cart_minus_img = view.findViewById(R.id.cart_minus_img);
        ImageView cart_add_img = view.findViewById(R.id.cart_add_img);
        ImageView delete_img = view.findViewById(R.id.delete_img);

        TextView child_name_txt = view.findViewById(R.id.child_name_txt);
        child_name_txt.setText(recordsBeanList.get(position).getPdtName());
        TextView child_price_txt = view.findViewById(R.id.child_price_txt);
        child_price_txt.setText(recordsBeanList.get(position).getPrice() + "/-");
        TextView child_qty_txt = view.findViewById(R.id.child_qty_txt);
        child_qty_txt.setText(recordsBeanList.get(position).getUnitValue() + recordsBeanList.get(position).getUnitName());

        if (type_get.equalsIgnoreCase("COMBO")) {

        } else {

            cart_qty_ll.setVisibility(View.VISIBLE);
            delete_img.setVisibility(View.VISIBLE);

            qty_txt.setText(recordsBeanList.get(position).getQuantity());

            cart_add_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String qtystr = qty_txt.getText().toString();
                    int qty = Integer.parseInt(qtystr);
                    String id = recordsBeanList.get(position).getCart_product_addon_id();
                    if (qty != 0)
                        qty++;

                    Log.d("ADDDETAIL", id + "///" + qty);
                    increaseQuatity(id, qty);


                }
            });


            cart_minus_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String qtystr = qty_txt.getText().toString();
                    int qty = Integer.parseInt(qtystr);
                    String id =  recordsBeanList.get(position).getCart_product_addon_id();

                    if (qty > 1)
                        qty--;

                    Log.d("DETAILMINUS", id + "///" + qty);
                    increaseQuatity(id, qty);
                    //  qty_txt.setText(String.valueOf(qty));

                }
            });


            delete_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String delete_id =  recordsBeanList.get(position).getCart_product_addon_id();
                    // deleteItem(delete_id);
                    boolean checkInternet = NetworkChecking.isConnected(context);
                    if (checkInternet) {

                        Call<BaseResponse> categoriesResponseCall = RetrofitClient.getInstance().getApi().deleteCart(token,deviceId,delete_id);
                        categoriesResponseCall.enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                                if (response.isSuccessful()) ;
                                BaseResponse homeCategoriesResponse = response.body();
                                if (homeCategoriesResponse.getStatus().equals("10100")) {
                                    recordsBeanList.remove(position);
                                    Toast.makeText(context, "Item Deleted..!", Toast.LENGTH_SHORT).show();
                                    context.getViewDetailChild();
                                    notifyDataSetChanged();
                                } else if (homeCategoriesResponse.getStatus().equals("10300")) {
                                    //pprogressDialog.dismiss();
                                    Toast.makeText(context, homeCategoriesResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                        });
                    } else {
                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

        return view;
    }

    private void increaseQuatity(final String id, final int qty) {
        boolean checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {

            Call<ViewCartIncreaseResponse> cartIncreaseResponseCall=RetrofitClient.getInstance().getApi().ViewCartIncreaseData(token,deviceId,id,String.valueOf(qty));
            cartIncreaseResponseCall.enqueue(new Callback<ViewCartIncreaseResponse>() {
                @Override
                public void onResponse(Call<ViewCartIncreaseResponse> call, retrofit2.Response<ViewCartIncreaseResponse> response) {
                    if (response.isSuccessful());
                    ViewCartIncreaseResponse viewCartIncreaseResponse=response.body();
                    if (viewCartIncreaseResponse.getStatus().equals("10100")){
                        context.getViewDetailChild();
                    }
                    else if (viewCartIncreaseResponse.getStatus().equals("10200")){
                        Toast.makeText(context, viewCartIncreaseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ViewCartIncreaseResponse> call, Throwable t) {
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

}