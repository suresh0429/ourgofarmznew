package in.innasoft.gofarmz.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.FormarResponse;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;

public class FormarAdapter extends RecyclerView.Adapter<FormarAdapter.FormarNameHolder> {
    Context context;
    List<FormarResponse.DataBean> detailList;
    private LayoutInflater li;
    private int resource;
    String  Type;

    public FormarAdapter(Context context, List<FormarResponse.DataBean> detailList, int resource) {
        this.context = context;
        this.detailList = detailList;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public FormarAdapter.FormarNameHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layout = li.inflate(resource, viewGroup, false);
        FormarNameHolder slh = new FormarNameHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(@NonNull FormarAdapter.FormarNameHolder holder, int i) {

        Type = detailList.get(i).getType();
        holder.farmer_name_text.setText(detailList.get(i).getFarmer_name());
        holder.farm_name_text.setText(Html.fromHtml(detailList.get(i).getFarm_name()));


        if (Type.equalsIgnoreCase("Supplier")) {
            holder.farmername_titletext.setText("Supplier Name : ");
            holder.farm_name_title_text.setText("Store Name : ");
        } else {

        }


        holder.type_text.setText(Html.fromHtml(detailList.get(i).getType()));

       /* Log.d("FORMER", "onBindViewHolder: "+detailList.get(i).getFarm_photo());
        if (detailList.get(i).getFarm_photo() != null || !detailList.get(i).getFarm_photo().equalsIgnoreCase("") || !detailList.get(i).getFarm_photo().isEmpty()) {
            Picasso.with(context)
                    .load(detailList.get(i).getFarm_photo())
                    .placeholder(R.drawable.farmer_dummy)
                    .into(holder.farmer_image);

        }*/

        if (detailList.get(i).getFarm_photo().isEmpty()) {
            holder.farmer_image.setImageResource(R.drawable.farmer_dummy);
        } else{
            Picasso.with(context).load(detailList.get(i).getFarm_photo()).into(holder.farmer_image);
        }
    }

    @Override
    public int getItemCount() {
        return detailList.size();
    }

    public class FormarNameHolder extends RecyclerView.ViewHolder {

        public TextView farmername_titletext,farmer_name_text,farm_name_title_text,farm_name_text,type_text;

        public ImageView farmer_image;

        MyCartItemClickListener citiesItemClickListener;

        public FormarNameHolder(View itemView) {
            super(itemView);

            farmername_titletext = itemView.findViewById(R.id.farmername_titletext);
            farmer_name_text = itemView.findViewById(R.id.farmer_name_text);
            farm_name_title_text = itemView.findViewById(R.id.farm_name_title_text);
            farm_name_text = itemView.findViewById(R.id.farm_name_text);

            type_text = itemView.findViewById(R.id.type_text);

            farmer_image = itemView.findViewById(R.id.farmer_image);

        }
    }
}
