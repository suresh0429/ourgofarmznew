package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.HomeItem;
import in.innasoft.gofarmz.activities.FarmerDetailsActivity;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {
    List<HomeItem> model;
    Context context;



    public Adapter(List<HomeItem> model, Context context) {
        this.model = model;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imagProduct)
        ImageView imagProduct;
        @BindView(R.id.txtProduct)
        TextView txtProduct;
        @BindView(R.id.txtWeight)
        TextView txtWeight;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.txtType)
        TextView txtType;
        @BindView(R.id.txtFacility)
        TextView txtFacility;
        @BindView(R.id.txtCategoery)
        TextView txtCategoery;
        @BindView(R.id.fabcall)
        FloatingActionButton fabcall;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(model.get(position).getImage()).into(holder.imagProduct);
        Log.d(TAG, "onBindViewHolder: " + model.get(position).getImage());
        holder.txtProduct.setText(model.get(position).getProductName());
        holder.txtPrice.setText("\u20B9" + model.get(position).getPrice());
        holder.txtType.setText("Type of Farming : "+model.get(position).getType_of_Farming());
        holder.txtFacility.setText("Transport Facility : "+model.get(position).getTransport_Facility());
        holder.txtWeight.setText(model.get(position).getWeight());
        holder.txtCategoery.setText(model.get(position).getCategory());
        holder.fabcall.setOnClickListener(v -> {
            String phone = "+34666777888";
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", model.get(position).getMobile(), null));
            context.startActivity(intent);
        });

        holder.itemView.setOnClickListener(v -> {

            Intent data = new Intent(context, FarmerDetailsActivity.class);
            data.putExtra("IMAGE", model.get(position).getImage());
            data.putExtra("PRODUCT", model.get(position).getProductName());
            data.putExtra("NAME", model.get(position).getName());
            data.putExtra("CATEGORY", model.get(position).getCategory());
            data.putExtra("MOBILE", model.get(position).getMobile());
            data.putExtra("PRICE", model.get(position).getPrice());
            data.putExtra("WEIGHT", model.get(position).getWeight());
            data.putExtra("FACILITY", model.get(position).getTransport_Facility());
            data.putExtra("TYPE", model.get(position).getType_of_Farming());
            data.putExtra("LOCATION", model.get(position).getLocation());
            data.putExtra("LATITUDE", model.get(position).getLatitude());
            data.putExtra("LONGITUDE", model.get(position).getLongitude());
            context.startActivity(data);
                }
        );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
