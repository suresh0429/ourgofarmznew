package in.innasoft.gofarmz.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.CartDeleteResponse;
import in.innasoft.gofarmz.Response.CartIncreaseResponse;
import in.innasoft.gofarmz.Response.CartResponse;
import in.innasoft.gofarmz.activities.CustomBoxProductsActivity;
import in.innasoft.gofarmz.activities.MyCartActivity;
import in.innasoft.gofarmz.activities.ViewDetails;
import in.innasoft.gofarmz.holders.MyCartHolder;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static in.innasoft.gofarmz.Api.RetrofitClient.IMAGE_URL;


public class MyCartAdapter extends RecyclerView.Adapter<MyCartHolder> {

    //    private ArrayList<Object> cartList;
    private ArrayList<Object> carChldtList = new ArrayList<>();
    private MyCartActivity context;
    private LayoutInflater li;
    private int resource;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, price;
    Dialog dialog;
    String from, pid = "", TYPE;
    SweetAlertDialog sd;
    List<CartResponse.DataBean.RecordsBean> recordsBeanList;

    public MyCartAdapter(MyCartActivity context, List<CartResponse.DataBean.RecordsBean> recordsBeanList, int resource, String from, String pid) {
        this.recordsBeanList = recordsBeanList;
        this.context = context;
        this.resource = resource;
        this.from = from;
        this.pid = pid;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);
    }

    @Override
    public MyCartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        MyCartHolder slh = new MyCartHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final MyCartHolder holder, final int position) {

        holder.product_name.setText(recordsBeanList.get(position).getProduct_name());

        TYPE = recordsBeanList.get(position).getType();
        TYPE = TYPE.replaceAll("_", " ").toLowerCase();
        holder.product_type.setText(TYPE);
        String price = String.valueOf(recordsBeanList.get(position).getTotal_price());
        Double d = Double.parseDouble(price);
        holder.price_txt.setText("" + Math.round(d));
        holder.cart_qty_txt.setText(recordsBeanList.get(position).getPurchase_quantity());

        Picasso.with(context)
                .load(IMAGE_URL + "70x70/" + recordsBeanList.get(position).getImages())
                .placeholder(R.drawable.cart)
                .into(holder.product_img);

//        Log.d("LIMKKNKKK",AppUrls.IMAGE_URL + "70x70/" + productObject.get("images").toString());


        holder.delete_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    sd = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);

                    sd.setTitleText("Are you sure?");
                    sd.setContentText("This product will be delete from cart!");
                    sd.setCancelText("No,cancel pls!");
                    sd.setConfirmText("Yes,delete it!");
                    sd.showCancelButton(true);

                    sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sweetAlertDialog) {

                            Call<CartDeleteResponse> cartDeleteResponseCall = RetrofitClient.getInstance().getApi().CartDelete(user_id, recordsBeanList.get(position).getId(), deviceId);
                            cartDeleteResponseCall.enqueue(new Callback<CartDeleteResponse>() {
                                @Override
                                public void onResponse(Call<CartDeleteResponse> call, retrofit2.Response<CartDeleteResponse> response) {
                                    if (response.isSuccessful()) ;
                                    CartDeleteResponse cartDeleteResponse = response.body();
                                    if (cartDeleteResponse.getStatus().equals("10100")) {
                                        sweetAlertDialog.cancel();
                                        recordsBeanList.remove(position);
                                        String message = cartDeleteResponse.getMessage();

                                        if (recordsBeanList.size() == 0) {
                                            if (from.equalsIgnoreCase("Home")) {
                                                Intent intent = new Intent(context, MainActivity.class);
                                                context.startActivity(intent);
                                            } else {
                                                Intent intent = new Intent(context, CustomBoxProductsActivity.class);
                                                intent.putExtra("pid", pid);
                                                intent.putExtra("from", "main");
                                                context.startActivity(intent);
                                            }
                                        }

                                        context.refreshCartPage();
                                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                    }

                                    if (cartDeleteResponse.getStatus().equals("10200")) {

                                        Toast.makeText(context, "Invalid Input..", Toast.LENGTH_SHORT).show();
                                    }
//


                                }

                                @Override
                                public void onFailure(Call<CartDeleteResponse> call, Throwable t) {

                                }
                            });

                        }
                    });


                    sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    sd.show();
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.view_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    String type = recordsBeanList.get(position).getType();
                    String name=recordsBeanList.get(position).getProduct_name();
                    Intent it=new Intent(context, ViewDetails.class);
//                    it.putExtra("productlist",recordsBeanList);
                    it.putExtra("TYPE",type);
                    it.putExtra("IDD",recordsBeanList.get(position).getId());
                    context.startActivity(it);

                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.cart_add_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               String qtystr=holder.cart_qty_txt.getText().toString();
               if(qtystr.equals("8"))
               {
                   Toast.makeText(context,"Sorry,You Can't add more qunatity!!!",Toast.LENGTH_LONG).show();
               }
               else
               {
                   int qty=Integer.parseInt(qtystr);
                   final String id =recordsBeanList.get(position).getId();
                   if(qty!=0)
                       qty++;
                   increaseQuatity(id,qty);
               }


            }
        });
        holder.cart_minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final Map<String, Object> productObject = (Map<String, Object>) cartList.get(position);
                final String id = recordsBeanList.get(position).getId();
                String qtystr=holder.cart_qty_txt.getText().toString();
                int qty = Integer.parseInt(qtystr);
                if (qty > 1)
                    qty--;
                increaseQuatity(id,qty);
            }
        });


    }

    private void increaseQuatity(final String id, final int qty) {
        boolean checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {

            Call<CartIncreaseResponse> cartIncreaseResponseCall=RetrofitClient.getInstance().getApi().CartIncrease(qty,id);
            cartIncreaseResponseCall.enqueue(new Callback<CartIncreaseResponse>() {
                @Override
                public void onResponse(Call<CartIncreaseResponse> call, Response<CartIncreaseResponse> response) {
                    if (response.isSuccessful());
                    CartIncreaseResponse cartIncreaseResponse=response.body();
                    if (cartIncreaseResponse.getStatus().equals("10100")){
                        context.getCartData();
                    }
                    else if (cartIncreaseResponse.getStatus().equals("10200")){
                        Toast.makeText(context, cartIncreaseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<CartIncreaseResponse> call, Throwable t) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public int getItemCount() {

        return this.recordsBeanList.size();
    }
}
