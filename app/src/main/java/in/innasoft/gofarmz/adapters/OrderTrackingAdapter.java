package in.innasoft.gofarmz.adapters;

import android.app.Dialog;
import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.MyOrderDetailsResponse;
import in.innasoft.gofarmz.activities.OrderHistoryDetailActivity;
import in.innasoft.gofarmz.holders.TimeLineViewHolder;
import in.innasoft.gofarmz.utils.VectorDrawableUtils;

public class OrderTrackingAdapter extends RecyclerView.Adapter<TimeLineViewHolder> {

//    private ArrayList<Object> orderTrackingList;
    private OrderHistoryDetailActivity context;
    private LayoutInflater li;
    private int resource;
    String deviceId, user_id, token, price;
    Dialog dialog;
    private Context mContext;
    private boolean mWithLinePadding;
    private LayoutInflater mLayoutInflater;
    List<MyOrderDetailsResponse.DataBean.StatusBean> statusBeanList;

    public OrderTrackingAdapter(OrderHistoryDetailActivity context,List<MyOrderDetailsResponse.DataBean.StatusBean> statusBeanList, boolean withLinePadding) {
        this.statusBeanList = statusBeanList;
        this.context = context;
        mWithLinePadding = withLinePadding;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;
        view = mLayoutInflater.inflate(mWithLinePadding ? R.layout.item_timeline_line_padding : R.layout.item_timeline, parent, false);
        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final TimeLineViewHolder holder, final int position) {

        holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_active, R.color.green));
        holder.mMessage.setText(statusBeanList.get(position).getMessage());
        holder.status.setText(statusBeanList.get(position).getOrder_status());
        holder.mDate.setText(statusBeanList.get(position).getCreated_on());

    }

    @Override
    public int getItemCount() {

        return statusBeanList.size();
    }
}
