package in.innasoft.gofarmz.adapters;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.CheckoutDataResponse;
import in.innasoft.gofarmz.activities.PaymentDetails;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.NetworkChecking;

import static com.google.firebase.remoteconfig.FirebaseRemoteConfig.TAG;

import static in.innasoft.gofarmz.Api.RetrofitClient.BASE_URL;
import static in.innasoft.gofarmz.Api.RetrofitClient.IMAGE_URL;

public class PaymentTypeAdapter extends RecyclerView.Adapter<PaymentTypeAdapter.PayTypeHolder> {

//    private ArrayList<Object> orderHistDetailList;
    private PaymentDetails context;
    private LayoutInflater li;
    private int resource;
    private RadioButton lastCheckedRB = null;
    public String send_check_Value, pay_id,paymentgatewayId;
    private boolean checkInternet;
    List<CheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeanList;


    public PaymentTypeAdapter(PaymentDetails context, List<CheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeanList, int resource, String paymentgatewayId) {

        this.paymentGatewayBeanList = paymentGatewayBeanList;
        this.context = context;
        this.resource = resource;
        this.paymentgatewayId = paymentgatewayId;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PayTypeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        PayTypeHolder slh = new PayTypeHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final PayTypeHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder:"+paymentGatewayBeanList.get(position).getLogo());
        Picasso.with(context)
                .load(BASE_URL+ paymentGatewayBeanList.get(position).getLogo())

                .placeholder(R.drawable.cart)
                .into(holder.pay_logo);

        String str_reason = paymentGatewayBeanList.get(position).getName();
        String converted_string = str_reason.substring(0, 1).toUpperCase() + str_reason.substring(1);
        holder.pay_radio_dynamic_button.setText(converted_string);


        if (!paymentgatewayId.equalsIgnoreCase("") ) {
            if (paymentgatewayId.equalsIgnoreCase("4")) {
                holder.pay_radio_dynamic_button.setChecked(false);
                lastCheckedRB = holder.pay_radio_dynamic_button;
            }
        } else {
            if (position == 0) {
                if (lastCheckedRB == null) {
                    pay_id = paymentGatewayBeanList.get(position).getId();
                    holder.pay_radio_dynamic_button.setChecked(true);
                    lastCheckedRB = holder.pay_radio_dynamic_button;
                    Log.d("eeee", "" + pay_id);
                }
            }
        }

        View.OnClickListener rbClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    RadioButton checked_rb = (RadioButton) v;
                    int pos = (int) v.getTag();

                    if (lastCheckedRB != null) {

                        lastCheckedRB.setChecked(false);
                        pay_id = paymentGatewayBeanList.get(position).getId();
                        Log.d("PayId", "" + pay_id);
                        send_check_Value = holder.pay_radio_dynamic_button.getText().toString();
                    }
                    lastCheckedRB = checked_rb;
                    pay_id = paymentGatewayBeanList.get(position).getId();
                    send_check_Value = holder.pay_radio_dynamic_button.getText().toString();
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        };

        holder.pay_radio_dynamic_button.setOnClickListener(rbClick);
        holder.pay_radio_dynamic_button.setTag(position);

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });

    }

    @Override
    public int getItemCount() {

        return this.paymentGatewayBeanList.size();
    }

    public class PayTypeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public RadioButton pay_radio_dynamic_button;
        public ImageView pay_logo;
        MyCartItemClickListener citiesItemClickListener;

        public PayTypeHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            pay_radio_dynamic_button = itemView.findViewById(R.id.pay_radio_dynamic_button);
            pay_logo = itemView.findViewById(R.id.pay_logo);

        }

        @Override
        public void onClick(View view) {

            this.citiesItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(MyCartItemClickListener ic) {
            this.citiesItemClickListener = ic;
        }
    }
}
