package in.innasoft.gofarmz.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.MyOrderDetailsResponse;
import in.innasoft.gofarmz.activities.OrderChildeViewDetails;
import in.innasoft.gofarmz.activities.OrderHistoryDetailActivity;
import in.innasoft.gofarmz.holders.OrderHisDetailHolder;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.models.ViewChildItemsModel;

import static in.innasoft.gofarmz.Api.RetrofitClient.IMAGE_URL;

public class OrderHisDetailAdapter extends RecyclerView.Adapter<OrderHisDetailHolder> {

    private ArrayList<Object> orderHistDetailList;
    private ArrayList<ViewChildItemsModel> orderChildHistDetailList = new ArrayList<>();
    private OrderHistoryDetailActivity context;
    private LayoutInflater li;
    private int resource;
    String deviceId, user_id, token, price;
    Dialog dialog;
    private boolean checkInternet;
    List<MyOrderDetailsResponse.DataBean.ProductsBean> productsBeanList;

    public OrderHisDetailAdapter(OrderHistoryDetailActivity context, List<MyOrderDetailsResponse.DataBean.ProductsBean> productsBeanList, int resource) {
        this.productsBeanList = productsBeanList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public OrderHisDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        OrderHisDetailHolder slh = new OrderHisDetailHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final OrderHisDetailHolder holder, final int position) {

        holder.product_name.setText(productsBeanList.get(position).getProduct_name());
        holder.price_txt.setText(productsBeanList.get(position).getTotal_price());
        holder.qty_btn.setText("Qty: "+productsBeanList.get(position).getPurchase_quantity());

        Picasso.with(context)
                .load(IMAGE_URL + "70x70/" + productsBeanList.get(position).getImages())
                .placeholder(R.drawable.cart)
                .into(holder.product_img);

        holder.view_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                orderChildHistDetailList.clear();
                List<MyOrderDetailsResponse.DataBean.ProductsBean.ChildProductsBean> childProductsBeanList=productsBeanList.get(position).getChild_products();

                for (int i=0;i<childProductsBeanList.size();i++){
                    orderChildHistDetailList.add(new ViewChildItemsModel(childProductsBeanList.get(i).getType(),childProductsBeanList.get(i).getProduct_name(),
                            childProductsBeanList.get(i).getUnit_name(),childProductsBeanList.get(i).getUnit_value(),
                            childProductsBeanList.get(i).getPrice(),childProductsBeanList.get(i).getDiscount(),
                            childProductsBeanList.get(i).getFinal_price(),childProductsBeanList.get(i).getQuantity(),
                            childProductsBeanList.get(i).getCreated_on(),childProductsBeanList.get(i).getImages()));
                }

                Intent it=new Intent(context, OrderChildeViewDetails.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST",(Serializable)orderChildHistDetailList);
                it.putExtra("BUNDLE",args);
                it.putExtra("TITLE",productsBeanList.get(position).getProduct_name());
                context.startActivity(it);
            }
        });

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });

    }

    @Override
    public int getItemCount() {

        return this.productsBeanList.size();
    }
}
