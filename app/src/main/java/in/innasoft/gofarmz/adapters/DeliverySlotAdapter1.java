package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.DeliverySlotsActivity;
import in.innasoft.gofarmz.activities.OrderHistoryDetailActivity;
import in.innasoft.gofarmz.itemClickListerners.StatesItemClickListener;

public class DeliverySlotAdapter1 extends RecyclerView.Adapter<DeliverySlotAdapter1.SlotHolder> {

    OrderHistoryDetailActivity context;
    ArrayList<Object> daysarr;
    SlotListAdapter delivrySlotAdapter;
    String  idd,startTime,endTime,date;
    private LayoutInflater li;
    private int resource;

    public DeliverySlotAdapter1(OrderHistoryDetailActivity context, ArrayList<Object> daysarr, int resource) {
        this.daysarr = daysarr;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public DeliverySlotAdapter1.SlotHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        DeliverySlotAdapter1.SlotHolder slh = new DeliverySlotAdapter1.SlotHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final DeliverySlotAdapter1.SlotHolder holder, final int position)
    {

        final Map<String, Object> tmpObj = (Map<String, Object>) daysarr.get(position);

        Log.d("DAAYS", tmpObj.toString());

        holder.day_text.setText((String) tmpObj.get("date"));//+" ("+(String)tmpObj.get("day")+")");
// spinDelivry_slot.setPrompt((String) tmpObj.get("date"));
        final ArrayList<Object> slotarr = (ArrayList<Object>) tmpObj.get("slots");

        Log.d("GEGEGE", slotarr.toString());


        delivrySlotAdapter=new SlotListAdapter( context, slotarr);
        holder.listSlot.setAdapter(delivrySlotAdapter);
        setListViewHeightBasedOnChildren(holder.listSlot);


        holder.listSlot.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Map<String, Object> slottmpObj = (Map<String, Object>) slotarr.get(position);
                idd=(String) slottmpObj.get("id");
                startTime=(String) slottmpObj.get("start_time");
                endTime=(String) slottmpObj.get("end_time");
                date=(String) tmpObj.get("date");
                context.sendValue(idd,startTime,endTime,date);
            }});


    }

    @Override
    public int getItemCount() {

        return this.daysarr.size();
    }
    public static void setListViewHeightBasedOnChildren(ListView listView)
    {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    //Holder
    public class SlotHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView day_text;
        public ListView listSlot;

        StatesItemClickListener slotItemClickListener;

        public SlotHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            day_text =   itemView.findViewById(R.id.day_text);
            listSlot =  itemView.findViewById(R.id.listSlot);
        }

        @Override
        public void onClick(View view) {

            // this.slotItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(StatesItemClickListener ic) {
            this.slotItemClickListener = ic;
        }


    }



}
