package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.WalletgetResponse;
import in.innasoft.gofarmz.models.PostItem;
import in.innasoft.gofarmz.utils.BaseViewHolder;
import in.innasoft.gofarmz.utils.TimeAgo;
import in.innasoft.gofarmz.utils.UserSessionManager;

import static com.google.firebase.remoteconfig.FirebaseRemoteConfig.TAG;

public class WalletAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    Context context;
    List<PostItem> mPostItems;


    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;


    public WalletAdapter(Context context, List<PostItem> mPostItems) {
        this.context = context;
        this.mPostItems = mPostItems;

    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       /* View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_wallet, null);
        return new Holder(view);*/

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.row_wallet, parent, false));
            case VIEW_TYPE_LOADING:
                return new ProgressHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false));
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, final int position) {

        holder.onBind(position);


    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == mPostItems.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public int getItemCount() {
        return mPostItems == null ? 0 : mPostItems.size();
    }

    public void addItems(List<PostItem> postItems) {
        mPostItems.addAll(postItems);
        notifyDataSetChanged();
    }

    public void addLoading() {
        isLoaderVisible = true;
        mPostItems.add(new PostItem());
        notifyItemInserted(mPostItems.size() - 1);
    }

    public void removeLoading() {
        isLoaderVisible = false;
        int position = mPostItems.size() - 1;
        PostItem item = getItem(position);
        if (item != null) {
            mPostItems.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        mPostItems.clear();
        notifyDataSetChanged();
    }

    PostItem getItem(int position) {
        return mPostItems.get(position);
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtAmount)
        TextView txtAmount;
        @BindView(R.id.txtRefno)
        TextView txtRefno;
        @BindView(R.id.txtType)
        TextView txtType;
        @BindView(R.id.txtDate)
        TextView txtDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @Override
        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);
            PostItem item = mPostItems.get(position);

            txtAmount.setText("\u20B9"+item.getAmount());
            txtRefno.setText("Ref No : "+item.getInternal_ref_no());
            txtTitle.setText(item.getTitle());


            TimeAgo timeAgo2 = new TimeAgo();
            String timeAgo = timeAgo2.covertTimeToText(item.getCreated_on());
            txtDate.setText(timeAgo);

            if (item.getTxt_type().equalsIgnoreCase("DEBIT")){
                txtType.setText(item.getTxt_type());
                txtType.setTextColor(Color.RED);
            }else {
                txtType.setText(item.getTxt_type());
                txtType.setTextColor(Color.parseColor("#05914E"));
            }


        }

    }



    public class ProgressHolder extends BaseViewHolder {
        ProgressHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {
        }
    }
}