package in.innasoft.gofarmz.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.MyPreOrderDetailsResponse;

public class PreOrderHistoryDetailsAdapter extends RecyclerView.Adapter<PreOrderHistoryDetailsAdapter.Holder> {

    Context context;
    List<MyPreOrderDetailsResponse.DataBean.ProductsBean> productsBeanList;

    public PreOrderHistoryDetailsAdapter(Context context,  List<MyPreOrderDetailsResponse.DataBean.ProductsBean> productsBeanList) {
        this.context=context;
        this.productsBeanList=productsBeanList;
    }

    @NonNull
    @Override
    public PreOrderHistoryDetailsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_preorderhistory_detail, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PreOrderHistoryDetailsAdapter.Holder holder, int i) {
        holder.price_txt.setText(productsBeanList.get(i).getTotal_price()+"/-");
        holder.product_name.setText(productsBeanList.get(i).getProduct_name());
        Picasso.with(context).load(productsBeanList.get(i).getImage()) .placeholder(R.drawable.no_image).into(holder.product_img);
        holder.qty_btn.setText("Qty : "+productsBeanList.get(i).getProduct_quantity());

    }

    @Override
    public int getItemCount() {
        return productsBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder{
        ImageView product_img;
        TextView qty_btn,product_name,price_txt;

        public Holder(@NonNull View itemView) {
            super(itemView);

            price_txt=itemView.findViewById(R.id.price_txt);
            product_name=itemView.findViewById(R.id.product_name);
            qty_btn=itemView.findViewById(R.id.qty_btn);
            product_img=itemView.findViewById(R.id.product_img);

        }
    }
}
