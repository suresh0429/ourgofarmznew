package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.MyPreOrderResponse;
import in.innasoft.gofarmz.activities.PreOrderHistoryDetailsActivity;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.NetworkChecking;

public class PreOrderHistoryAdapter extends RecyclerView.Adapter<PreOrderHistoryAdapter.Holder> {

    Context context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    List<MyPreOrderResponse.DataBean.RecordDataBean> preOrderHistoryResponse;

    public PreOrderHistoryAdapter(List<MyPreOrderResponse.DataBean.RecordDataBean> recordData, Context context) {
        this.preOrderHistoryResponse=recordData;
        this.context=context;
    }

    @Override
    public PreOrderHistoryAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pre_order_history, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int i) {

        holder.order_id.setText(preOrderHistoryResponse.get(i).getRef_no());
        holder.order_date.setText(preOrderHistoryResponse.get(i).getCreated_date_time());
        holder.order_status.setText(preOrderHistoryResponse.get(i).getPre_order_status());

        holder.ord_his_price_txt.setText(preOrderHistoryResponse.get(i).getGrand_total());

        String ord_his_product_img = preOrderHistoryResponse.get(i).getImages();

        Picasso.with(context)
                .load(ord_his_product_img)
                .placeholder(R.drawable.cart)
                .into(holder.ord_his_product_img);

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    Intent intent = new Intent(context, PreOrderHistoryDetailsActivity.class);
                   // intent.putExtra("PREORDERID", preOrderHistoryResponse.get(i).getId());
                    intent.putExtra("ORDERID", preOrderHistoryResponse.get(i).getId());
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.preOrderHistoryResponse.size();
    }


    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ord_his_product_img;
        public TextView order_date_title, coupan_apply_title, order_status_title, order_id, order_date, coupan_apply, order_status, ord_his_price_txt, seriel_no;
        MyCartItemClickListener myCartItemClickListener;
        LinearLayout coup_ll;

        public Holder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            ord_his_product_img = itemView.findViewById(R.id.ord_his_product_img);
            order_date_title = itemView.findViewById(R.id.order_date_title);
            coupan_apply_title = itemView.findViewById(R.id.coupan_apply_title);
            order_status_title = itemView.findViewById(R.id.order_status_title);
            order_id = itemView.findViewById(R.id.order_id);
            order_date = itemView.findViewById(R.id.order_date);
            coupan_apply = itemView.findViewById(R.id.coupan_apply);
            order_status = itemView.findViewById(R.id.order_status);
            seriel_no = itemView.findViewById(R.id.seriel_no);
            coup_ll = itemView.findViewById(R.id.coup_ll);

            ord_his_price_txt = itemView.findViewById(R.id.ord_his_price_txt);
        }

        @Override
        public void onClick(View view) {
            this.myCartItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(MyCartItemClickListener ic) {
            this.myCartItemClickListener = ic;
        }
    }


}
