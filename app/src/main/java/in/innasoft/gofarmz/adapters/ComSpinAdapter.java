package in.innasoft.gofarmz.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.CommunityResponse;

public class ComSpinAdapter extends ArrayAdapter<CommunityResponse.DataBean> {

    LayoutInflater flater;

    public ComSpinAdapter(Activity context, int resouceId, int textviewId, List<CommunityResponse.DataBean> list) {

        super(context, resouceId, textviewId, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return rowview(convertView, position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView, position);
    }

    private View rowview(View convertView, int position) {

        CommunityResponse.DataBean rowItem = getItem(position);

        viewHolder holder;
        View rowview = convertView;
        if (rowview == null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.spinner_rows, null, false);

            holder.txtTitle = (TextView) rowview.findViewById(R.id.txtCommunityName);
          //  holder.imageView = (ImageView) rowview.findViewById(R.id.icon);
            rowview.setTag(holder);
        } else {
            holder = (viewHolder) rowview.getTag();
        }
      //  holder.imageView.setImageResource(rowItem.getImageId());
        holder.txtTitle.setText(rowItem.getCommunity());

        return rowview;
    }

    private class viewHolder {
        TextView txtTitle;
        //ImageView imageView;
    }
}


