package in.innasoft.gofarmz.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.HomeAreasResponse;
import in.innasoft.gofarmz.utils.Utilities;

import static in.innasoft.gofarmz.utils.Utilities.capitalize;


public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.MyViewHolder> {


    private Context mContext;
    List<HomeAreasResponse.DataBean> dataBeanList;
    private AlertDialog alertDialog;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtLocation)
        TextView txtLocation;
        @BindView(R.id.parentLayout)
        LinearLayout parentLayout;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public LocationAdapter(Context mContext, List<HomeAreasResponse.DataBean> dataBeanList) {
        this.mContext = mContext;
        this.dataBeanList = dataBeanList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final HomeAreasResponse.DataBean home = dataBeanList.get(position);

        holder.txtLocation.setText(capitalize(home.getArea())+" ("+home.getPincode()+")");

      /*  holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String area=home.getArea();
                Intent intent = new Intent(mContext, MainActivity.class);
//                intent.putExtra("loc_Area",area);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);

                alertDialog.dismiss();

                // update location preferences
                SharedPreferences preferences = mContext.getSharedPreferences(Utilities.PREFS_LOCATION, 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
                editor.putString(Utilities.KEY_AREA,home.getArea());
                editor.putString(Utilities.KEY_PINCODE,home.getPincode());
                editor.apply();

            }

        });
*/

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }


}
