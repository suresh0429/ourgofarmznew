package in.innasoft.gofarmz.adapters;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.activities.EditAddressActivity;
import in.innasoft.gofarmz.filters.CountriesFilter;
import in.innasoft.gofarmz.holders.CountriesHolder;
import in.innasoft.gofarmz.itemClickListerners.CountriesItemClickListener;
import in.innasoft.gofarmz.models.CountriesModel;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class CountriesAdapter extends RecyclerView.Adapter<CountriesHolder> implements Filterable {

    public ArrayList<CountriesModel> countriesModels,filterList;
    CountriesFilter filter;
    private EditAddressActivity context;
    private LayoutInflater layoutInflater;
    private int resource;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile;

    public CountriesAdapter(ArrayList<CountriesModel> countriesModels, EditAddressActivity context, int resource) {
        this.countriesModels = countriesModels;
        this.filterList = countriesModels;
        this.context = context;
        this.resource = resource;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);
    }

    @Override
    public CountriesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = layoutInflater.inflate(resource, parent, false);
        CountriesHolder slh = new CountriesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final CountriesHolder holder, final int position) {

        holder.country_txt.setText(countriesModels.get(position).getName());

        holder.setItemClickListener(new CountriesItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
            }
        });

    }

    @Override
    public int getItemCount() {
        return countriesModels.size();
    }

    @Override
    public Filter getFilter()
    {
        if(filter==null)
        {
            filter=new CountriesFilter(filterList,this);
        }

        return filter;
    }

}
