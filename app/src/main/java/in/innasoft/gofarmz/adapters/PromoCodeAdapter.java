package in.innasoft.gofarmz.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import java.util.List;

import in.innasoft.gofarmz.Response.PromoCodeResponse;
import in.innasoft.gofarmz.activities.PromoCouponListActivity;
import in.innasoft.gofarmz.holders.PromoCodeHolder;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;

public class PromoCodeAdapter extends RecyclerView.Adapter<PromoCodeHolder> {

    public List<PromoCodeResponse.DataBean> promolistModels;
    PromoCouponListActivity context;
    LayoutInflater li;
    int resource;
    private RadioButton lastCheckedRB = null;
    public String send_code_Value;

    public PromoCodeAdapter(List<PromoCodeResponse.DataBean> promolistModels, PromoCouponListActivity context, int resource) {
        this.promolistModels = promolistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PromoCodeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        PromoCodeHolder slh = new PromoCodeHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final PromoCodeHolder holder, final int position) {

        send_code_Value = promolistModels.get(position).getCode();

        holder.pro_coupon_code.setText(send_code_Value);

        holder.pro_descrption.setText(Html.fromHtml(promolistModels.get(position).getDescription()));

        holder.apply_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.setCode(promolistModels.get(position).getCode());
                context.validateCouponCode(promolistModels.get(position).getCode());
            }
        });


        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.promolistModels.size();
    }
}
