package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.models.ViewChildItemsModel;
import in.innasoft.gofarmz.utils.UserSessionManager;

import static in.innasoft.gofarmz.Api.RetrofitClient.IMAGE_URL;

public class ChildItemDetailsAdapter extends RecyclerView.Adapter<ChildItemDetailsAdapter.Holder> {

    Context context;
    ArrayList<ViewChildItemsModel> productsBeanList;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, price;

    public ChildItemDetailsAdapter(Context context, ArrayList<ViewChildItemsModel> productsBeanList) {
        this.context=context;
        this.productsBeanList = productsBeanList;

    }

    @NonNull
    @Override
    public ChildItemDetailsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_order_hist_child_record_list, null);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ChildItemDetailsAdapter.Holder holder, final int i) {


        holder.child_name_txt.setText(productsBeanList.get(i).getProduct_name());
        Picasso.with(context).load(IMAGE_URL + "70x70/"+productsBeanList.get(i).getImages()).into(holder.child_img);

        if(productsBeanList.get(i).getType().equalsIgnoreCase("ADDON"))
        {
            holder.child_finalprice_txt.setVisibility(View.VISIBLE);
            holder.child_finalprice_txt.setText(productsBeanList.get(i).getFinal_price());


            holder.child_qty_txt.setVisibility(View.VISIBLE);
            holder.child_qty_txt.setText("Quantity - "+productsBeanList.get(i).getQuantity());

            holder.mrp_price_txt.setVisibility(View.VISIBLE);
            holder.mrp_price_txt.setText("Rs. "+productsBeanList.get(i).getPrice()+"/-");

            holder.child_unit_txt.setVisibility(View.GONE);

        }

        holder.child_unit_txt.setVisibility(View.VISIBLE);
        holder.child_unit_txt.setText(productsBeanList.get(i).getUnit_value() + productsBeanList.get(i).getUnit_name());

        holder.child_qty_txt.setVisibility(View.VISIBLE);
        holder.child_qty_txt.setText("Quantity - "+productsBeanList.get(i).getQuantity());

        holder.mrp_price_txt.setVisibility(View.VISIBLE);
        holder.mrp_price_txt.setText("Rs. "+productsBeanList.get(i).getPrice()+"/-");

        holder.child_finalprice_txt.setVisibility(View.VISIBLE);
        holder.child_finalprice_txt.setText(productsBeanList.get(i).getFinal_price());

    }

    @Override
    public int getItemCount() {
        return productsBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView child_name_txt,child_finalprice_txt,child_unit_txt,child_qty_txt,mrp_price_txt;
        ImageView child_img;

        public Holder(@NonNull View itemView) {
            super(itemView);

            child_name_txt=itemView.findViewById(R.id.child_name_txt);
            child_finalprice_txt=itemView.findViewById(R.id.child_finalprice_txt);
            child_unit_txt=itemView.findViewById(R.id.child_unit_txt);
            child_qty_txt=itemView.findViewById(R.id.child_qty_txt);
            mrp_price_txt=itemView.findViewById(R.id.mrp_price_txt);

            child_img=itemView.findViewById(R.id.child_img);




        }



    }
}
