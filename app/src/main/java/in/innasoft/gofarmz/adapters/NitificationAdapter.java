package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.NotificationResponse;
import in.innasoft.gofarmz.activities.ImageNotificationActivity;
import in.innasoft.gofarmz.activities.NotificationsActivity;
import in.innasoft.gofarmz.holders.NotificationHolder;
import in.innasoft.gofarmz.itemClickListerners.AreasItemClickListener;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;
import retrofit2.Call;
import retrofit2.Callback;



public class NitificationAdapter extends RecyclerView.Adapter<NotificationHolder> {

    NotificationsActivity context;
    private List<NotificationResponse.DataBean> notificationList;
    private LayoutInflater li;
    private int resource;
    String deviceId, user_id, token;
    SweetAlertDialog sd;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String read_status,path;

    public NitificationAdapter(List<NotificationResponse.DataBean> notificationList, NotificationsActivity context, int resource) {
        this.notificationList = notificationList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        NotificationHolder slh = new NotificationHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final NotificationHolder holder, final int position) {


        read_status=   notificationList.get(position).getIsRead();


//        Log.d("asd",read_status);


        if(read_status.equals("0"))
        {

            holder.linear_layout_row.setBackgroundColor(Color.parseColor("#d1f2e2"));
        }
        else
        {
           //nothing
        }


        holder.notify_title_name.setText(notificationList.get(position).getMsgTitle());

        holder.notify_description.setText(notificationList.get(position).getMsgTxt());

        String timedate=parseDateToddMMyyyy(notificationList.get(position).getCreatedOn());
        holder.notify_dateandtime.setText(Html.fromHtml(timedate));


         holder.linear_layout_row.setOnClickListener(new View.OnClickListener()
         {
             @Override
             public void onClick(View v)
             {
                 path=  notificationList.get(position).getMsgImage();
                 Log.d("RRRRRRRR",path);
                 if(path != null && !path.isEmpty())
                   {

                       String send_id=notificationList.get(position).getId();
                       String notification_id=notificationList.get(position).getId();
                       sendReadStatus(send_id);
                       Intent intent = new Intent(context, ImageNotificationActivity.class);
                       intent.putExtra("GETDATANOTFY",notification_id);
                       context.startActivity(intent);
                   }
                   else
                   {

                       sd = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                       sd.setTitleText(""+(notificationList.get(position).getMsgTitle()));
                       sd.setContentText(""+notificationList.get(position).getMsgTxt());
                       sd.setConfirmText("OK");
                       sd.showCancelButton(true);
                       sd.setCanceledOnTouchOutside(false);
                       sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                           @Override
                           public void onClick(final SweetAlertDialog sweetAlertDialog)
                           {

                               String is_read=notificationList.get(position).getIsRead();
                               if(is_read.equals("0"))
                                                                                                                           {
                                   String send_id=notificationList.get(position).getId();
                                   sendReadStatus(send_id);
                                   Intent intent = new Intent(context, MainActivity.class);
                                   context.startActivity(intent);

                               }
                               else
                               {
                                   sd.cancel();
                                   //nothin
                               }
                           }
                       });
                       sd.show();
                   }
             }
         });


        holder.setItemClickListener(new AreasItemClickListener() {
            @Override
            public void onItemClick(View v, int pos)
            {

            }
        });

    }

    private void sendReadStatus(String send_id)
    {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {

            Call<BaseResponse> call = RetrofitClient.getInstance().getApi().readNotifications(token,deviceId,user_id,send_id);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                    BaseResponse loginResponse = response.body();

                    if (loginResponse.getStatus().equals("10100"))
                    {
                        // sd.cancel();
                        context.getNotification();

                    }
                    if (loginResponse.getStatus().equals("10200")) {

                        Toast.makeText(context, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                   // progress_indicator.stop();
                }
            });


        } else {
            Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {

        return this.notificationList.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy  HH:mm ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
