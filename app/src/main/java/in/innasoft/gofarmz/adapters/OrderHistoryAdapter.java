package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.MyOrderResponse;
import in.innasoft.gofarmz.activities.OrderHistoryDetailActivity;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.NetworkChecking;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.OrderHistoryHolder> {

//    public ArrayList<OrderHistoryModel> orderHisListModels;
    Context context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    List<MyOrderResponse.DataBean.RecordDataBean> recordDataBeanList;

//    public OrderHistoryAdapter(ArrayList<OrderHistoryModel> orderHisListModels, Context context, int resource) {
//
//        this.orderHisListModels = orderHisListModels;
//        this.context = context;
//        this.resource = resource;
//        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//    }

    public OrderHistoryAdapter(List<MyOrderResponse.DataBean.RecordDataBean> recordDataBeanList, Context context, int resource) {

        this.recordDataBeanList = recordDataBeanList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }



    @Override
    public OrderHistoryAdapter.OrderHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        OrderHistoryAdapter.OrderHistoryHolder slh = new OrderHistoryAdapter.OrderHistoryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final OrderHistoryAdapter.OrderHistoryHolder holder, final int position) {

        holder.order_id.setText(Html.fromHtml(recordDataBeanList.get(position).getOrder_id()));

        String coupn = recordDataBeanList.get(position).getCoupon_code();

        if (coupn != null)
        {
            holder.coup_ll.setVisibility(View.VISIBLE);
            holder.coupan_apply.setVisibility(View.VISIBLE);
            holder.coupan_apply.setText(Html.fromHtml(coupn) + "  Coupon Applied");
        }


        holder.order_date.setText(Html.fromHtml(recordDataBeanList.get(position).getOrder_date()));

        if (recordDataBeanList.get(position).getStatus().equalsIgnoreCase("0")) {
            holder.order_status.setTextColor(Color.parseColor("#FF8000"));
            holder.order_status.setText("Pending");
        } else if (recordDataBeanList.get(position).getStatus().equalsIgnoreCase("1")) {
            holder.order_status.setTextColor(Color.parseColor("#05914E"));
            holder.order_status.setText("Confirmed");
        } else if (recordDataBeanList.get(position).getStatus().equalsIgnoreCase("2")) {
            holder.order_status.setTextColor(Color.RED);
            holder.order_status.setText("Failed");
        } else if (recordDataBeanList.get(position).getStatus().equalsIgnoreCase("3")) {
            holder.order_status.setTextColor(Color.RED);
            holder.order_status.setText("Cancelled");
        } else if (recordDataBeanList.get(position).getStatus().equalsIgnoreCase("4")) {
            holder.order_status.setTextColor(Color.parseColor("#FF8000"));
            holder.order_status.setText("Dispatched");
        } else if (recordDataBeanList.get(position).getStatus().equalsIgnoreCase("6")) {
            holder.order_status.setTextColor(Color.parseColor("#5CD199"));
            holder.order_status.setText("Shipped");
        } else if (recordDataBeanList.get(position).getStatus().equalsIgnoreCase("7")) {
            holder.order_status.setTextColor(Color.parseColor("#05914E"));
            holder.order_status.setText("Delivered");
        } else if (recordDataBeanList.get(position).getStatus().equalsIgnoreCase("8")) {
            holder.order_status.setTextColor(Color.RED);
            holder.order_status.setText("Not Delivered");
        } else if (recordDataBeanList.get(position).getStatus().equalsIgnoreCase("11")) {
            holder.order_status.setTextColor(Color.BLUE);
            holder.order_status.setText("Received");
        }

        holder.ord_his_price_txt.setText(Html.fromHtml(recordDataBeanList.get(position).getFinal_price()));

//        String ord_his_product_img = orderHisListModels.get(position).images;

        Picasso.with(context).load(recordDataBeanList.get(position).getImages()).placeholder(R.drawable.cart).into(holder.ord_his_product_img);

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    Intent intent = new Intent(context, OrderHistoryDetailActivity.class);
                    intent.putExtra("ORDERID", Integer.parseInt(recordDataBeanList.get(position).getId()));
                    intent.putExtra("Status", recordDataBeanList.get(position).getOrder_status());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.recordDataBeanList.size();
    }

    //HOLDER CLASS

    public class OrderHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public ImageView ord_his_product_img;
        public TextView order_date_title, coupan_apply_title, order_status_title, order_id, order_date, coupan_apply, order_status, ord_his_price_txt, seriel_no;
        MyCartItemClickListener myCartItemClickListener;
        LinearLayout coup_ll;

        public OrderHistoryHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            ord_his_product_img = itemView.findViewById(R.id.ord_his_product_img);
            order_date_title = itemView.findViewById(R.id.order_date_title);
            coupan_apply_title = itemView.findViewById(R.id.coupan_apply_title);
            order_status_title = itemView.findViewById(R.id.order_status_title);
            order_id = itemView.findViewById(R.id.order_id);
            order_date = itemView.findViewById(R.id.order_date);
            coupan_apply = itemView.findViewById(R.id.coupan_apply);
            order_status = itemView.findViewById(R.id.order_status);
            seriel_no = itemView.findViewById(R.id.seriel_no);
            coup_ll = itemView.findViewById(R.id.coup_ll);

            ord_his_price_txt = itemView.findViewById(R.id.ord_his_price_txt);

        }

        @Override
        public void onClick(View view) {
            this.myCartItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(MyCartItemClickListener ic) {
            this.myCartItemClickListener = ic;
        }
    }


}
