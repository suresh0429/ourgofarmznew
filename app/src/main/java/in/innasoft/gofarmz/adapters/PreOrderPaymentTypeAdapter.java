package in.innasoft.gofarmz.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.PreOrderCheckoutDataResponse;
import in.innasoft.gofarmz.activities.PaymentDetails;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.NetworkChecking;

import static in.innasoft.gofarmz.Api.RetrofitClient.BASE_URL;

public class PreOrderPaymentTypeAdapter extends RecyclerView.Adapter<PreOrderPaymentTypeAdapter.Holder> {

    Context context;
    List<PreOrderCheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeanList;
    private RadioButton lastCheckedRB = null;
    public String send_check_Value, pay_id,paymentgatewayId;
    private boolean checkInternet;

    public PreOrderPaymentTypeAdapter(PaymentDetails context,List<PreOrderCheckoutDataResponse.DataBean.PaymentGatewayBean> paymentGatewayBeanList, String paymentgatewayId) {
        this.context=context;
        this.paymentGatewayBeanList=paymentGatewayBeanList;
        this.paymentgatewayId = paymentgatewayId;
    }

    @NonNull
    @Override
    public PreOrderPaymentTypeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_pay_type, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PreOrderPaymentTypeAdapter.Holder holder, final int i) {

        Picasso.with(context)
                .load(BASE_URL+ paymentGatewayBeanList.get(i).getLogo())
                .placeholder(R.drawable.cart)
                .into(holder.pay_logo);

        String str_reason=paymentGatewayBeanList.get(i).getName();
        String converted_string = str_reason.substring(0, 1).toUpperCase() + str_reason.substring(1);
        holder.pay_radio_dynamic_button.setText(converted_string);

        if (!paymentgatewayId.equalsIgnoreCase("") ) {
            if (paymentgatewayId.equalsIgnoreCase("4")) {
                holder.pay_radio_dynamic_button.setChecked(false);
                lastCheckedRB = holder.pay_radio_dynamic_button;
            }
        } else {
            if (i == 0) {
                if (lastCheckedRB == null) {
                    pay_id = paymentGatewayBeanList.get(i).getId();
                    holder.pay_radio_dynamic_button.setChecked(true);
                    lastCheckedRB = holder.pay_radio_dynamic_button;
                    Log.d("eeee", "" + pay_id);
                }
            }
        }

        View.OnClickListener rbClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    RadioButton checked_rb = (RadioButton) v;
                    int pos = (int) v.getTag();
                    if (lastCheckedRB != null) {

                        lastCheckedRB.setChecked(false);
                        pay_id = paymentGatewayBeanList.get(i).getId();
                        Log.d("prePayId", "" + pay_id);
                        send_check_Value = holder.pay_radio_dynamic_button.getText().toString();
                    }
                    lastCheckedRB = checked_rb;
                    pay_id =paymentGatewayBeanList.get(i).getId();
                    send_check_Value = holder.pay_radio_dynamic_button.getText().toString();
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        };
        holder.pay_radio_dynamic_button.setOnClickListener(rbClick);
        holder.pay_radio_dynamic_button.setTag(i);

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return paymentGatewayBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView pay_logo;
        RadioButton pay_radio_dynamic_button;
        MyCartItemClickListener citiesItemClickListener;

        public Holder(@NonNull View itemView) {
            super(itemView);
            pay_radio_dynamic_button=itemView.findViewById(R.id.pay_radio_dynamic_button);
            pay_logo=itemView.findViewById(R.id.pay_logo);


        }

        @Override
        public void onClick(View v) {
            this.citiesItemClickListener.onItemClick(v, getLayoutPosition());
        }
        public void setItemClickListener(MyCartItemClickListener ic) {
            this.citiesItemClickListener = ic;
        }
    }
}
