package in.innasoft.gofarmz.adapters;

import android.content.Intent;

import androidx.viewpager.widget.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.Response.BottomOfferAddResponse;
import in.innasoft.gofarmz.activities.PreOrderActivity;
import in.innasoft.gofarmz.activities.ReferFarmerActivity;

public class CustomPageAdapter extends PagerAdapter {

    private MainActivity context;
    List<BottomOfferAddResponse.DataBean> dataBeanList;
    private LayoutInflater layoutInflater;
    SweetAlertDialog sd;

    public CustomPageAdapter(MainActivity context, List<BottomOfferAddResponse.DataBean> dataBeanList) {
        this.context = context;
        this.dataBeanList = dataBeanList;
        this.layoutInflater = (LayoutInflater)this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataBeanList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View)object);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {

        View view = this.layoutInflater.inflate(R.layout.row_ad_refer_bootom, container, false);


        TextView title =  view.findViewById(R.id.title);
        title.setText(dataBeanList.get(position).getTitle());



        TextView textdescr =  view.findViewById(R.id.textdescr);
        textdescr.setText(Html.fromHtml(dataBeanList.get(position).getDescription()));

        TextView do_txt = view.findViewById(R.id.do_txt);

        Button view_detail_btn =  view.findViewById(R.id.view_detail_btn);

        view_detail_btn.setText("View Details");
        view_detail_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                /*if(position==0)
                {
                    Intent referfarmer=new Intent(context, PreOrderActivity.class);
                    context.startActivity(referfarmer);
                }
              else*/ if(position==0)
              {
                  Intent referfarmer=new Intent(context, ReferFarmerActivity.class);
                  context.startActivity(referfarmer);
              }
              else
              {
                  sd = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                  sd.setContentText(String.valueOf(Html.fromHtml(dataBeanList.get(position).getDescription())));
                  sd.setConfirmText("OK");
                  sd.showCancelButton(true);
                  sd.setCanceledOnTouchOutside(false);
                  sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                      @Override
                      public void onClick(final SweetAlertDialog sweetAlertDialog)
                      {
                         sd.cancel();
                      }
                  });
                  sd.show();
                /*
                   Intent offerdetail=new Intent(context, OfferDetailACtivity.class);
                  context.startActivity(offerdetail);*/
              }
            }
        });

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


}
