package in.innasoft.gofarmz;

import android.animation.Animator;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.reflect.TypeToken;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.Api.RetrofitClient;
import in.innasoft.gofarmz.Response.BaseResponse;
import in.innasoft.gofarmz.Response.BottomOfferAddResponse;
import in.innasoft.gofarmz.Response.CartCountResponse;
import in.innasoft.gofarmz.Response.HomeBoxProductsResponse;
import in.innasoft.gofarmz.Response.HomeCategoriesResponse;
import in.innasoft.gofarmz.Response.SliderTopBannerResponse;
import in.innasoft.gofarmz.activities.BulkBuyerActivity;
import in.innasoft.gofarmz.activities.CustomBoxProductsActivity;
import in.innasoft.gofarmz.activities.FarmComActivity;
import in.innasoft.gofarmz.activities.FarmerNetworkActivity;
import in.innasoft.gofarmz.activities.FormarActivity;
import in.innasoft.gofarmz.activities.KYFActivity;
import in.innasoft.gofarmz.activities.LoginActivity;
import in.innasoft.gofarmz.activities.MyCartActivity;
import in.innasoft.gofarmz.activities.NotificationsActivity;
import in.innasoft.gofarmz.activities.OrdersHistoryActivity1;
import in.innasoft.gofarmz.activities.OurStoryActivity;
import in.innasoft.gofarmz.activities.PreOrderActivity;
import in.innasoft.gofarmz.activities.ProfileActivity;
import in.innasoft.gofarmz.activities.ReferFarmerActivity;
import in.innasoft.gofarmz.activities.SupportActivity;
import in.innasoft.gofarmz.activities.TermsConditionsAndPrivacy;
import in.innasoft.gofarmz.activities.WalletActivity;
import in.innasoft.gofarmz.adapters.BoxNameAdapter;
import in.innasoft.gofarmz.adapters.BoxProductsAdapter;
import in.innasoft.gofarmz.adapters.CategoryAdapter;
import in.innasoft.gofarmz.adapters.CustomPageAdapter;
import in.innasoft.gofarmz.adapters.GoCustomBoxProductsAdapter;
import in.innasoft.gofarmz.animation.GuillotineAnimation;
import in.innasoft.gofarmz.database.CategoryDbHelper;
import in.innasoft.gofarmz.fragments.CustomBottomSheetDialogFragment;
import in.innasoft.gofarmz.models.CategoryModel;
import in.innasoft.gofarmz.models.CustomModel;
import in.innasoft.gofarmz.models.SliderModel;
import in.innasoft.gofarmz.utils.CircleAnimationUtil;
import in.innasoft.gofarmz.utils.Config;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.ScrollableGridView;
import in.innasoft.gofarmz.utils.UserSessionManager;
import in.innasoft.gofarmz.utils.Utilities;
import in.innasoft.gofarmz.utils.viewpager.EnchantedViewPager;
import retrofit2.Call;
import retrofit2.Callback;


import static in.innasoft.gofarmz.Api.RetrofitClient.API_URL;
import static in.innasoft.gofarmz.Api.RetrofitClient.BASE_URL;



public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private static final long RIPPLE_DURATION = 250;
    FrameLayout root;
    RelativeLayout toolbar;
    ImageView contentHamburger, cart_img, no_product_img, viewpager_image;
    String productid = "";
    String selectedType = "";
    RelativeLayout go_box_bottom, layoutBottom, rotate_rl;
    ImageView rupee_img, proceed_img, veg_img, kyf_img, story_img, fn_img, support_img, buy_img, terms_faq_policies_img;
    TextView cart_count_txt, price_txt, box_name_txt, kyf_txt, story_txt, fn_txt, support_txt, terms_faq_policies_txt, profile_txt,bulk_txt, orders_txt, share_txt,farmcom_txt, notifications_txt, ref_farmer_menu, preorders_txt,wallet_txt, notification_menu;
    LinearLayout price_rl, box_ll, kyf_group, story_group, fn_group, support_group, terms_faq_policies, refer_farmer, notification,share;
    Button custom_btn;

    TextView total_price_txt;
    ScrollableGridView box_products_grid;
    ArrayList<CustomModel> productsList = new ArrayList<>();
    RecyclerView cat_recyclerview, box_recyclerview, go_box_recyclerview;
    LinearLayout comobo_liner_layout,relative_areas;
    RelativeLayout rllll;
    CategoryAdapter categoryAdapter;
    ArrayList<CategoryModel> categoryModels = new ArrayList<CategoryModel>();
    private Boolean exit = false;
    BoxNameAdapter boxNameAdapter;
    GoCustomBoxProductsAdapter customBoxProductsAdapter;
    ArrayList<Object> boxNameList = new ArrayList<>();
    private boolean checkInternet;
    BoxProductsAdapter boxProductsAdapter;
    ArrayList<Object> boxProductList = new ArrayList<>();
    ArrayList<SliderModel> slidermodel = new ArrayList<>();
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    SliderLayout sliderLayout;
    /*Database*/
    CategoryDbHelper categoryDbHelper;


    TextView desp_txt,txt_loc_area;
    String pr_id, android_version, ios_version, checnkVeriosn;

    SweetAlertDialog sd_update;


    public String user_image,loc_area;
    View guillotineMenu;

    CustomPageAdapter mCustomPagerAdapter;
    private EnchantedViewPager viewPager;
    List<BottomOfferAddResponse.DataBean> dataBeanList;
    ProgressDialog pprogressDialog;
    ImageView rotateImage;

    private FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String, Object> firebaseDefaultMap;
    private static final String TAG = "MainActivity";
    private static final String VERSION_CODE_KEY = "GofarmzVersionCode";
    String refreshedToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        PackageInfo eInfo = null;
        try {
            eInfo = getPackageManager().getPackageInfo("in.innasoft.gofarmz", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        checnkVeriosn = eInfo.versionName;


        /*Start Firebase Remote Config*/
        //This is default Map
        //Setting the Default Map Value with the current version code
        firebaseDefaultMap = new HashMap<>();
        firebaseDefaultMap.put(VERSION_CODE_KEY, getCurrentVersionCode());
        mFirebaseRemoteConfig.setDefaults(firebaseDefaultMap);


        //Setting that default Map to Firebase Remote Config

        //Setting Developer Mode enabled to fast retrieve the values
        mFirebaseRemoteConfig.setConfigSettings(
                new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(BuildConfig.DEBUG)
                        .build());

        //Fetching the values here
        mFirebaseRemoteConfig.fetch(0).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mFirebaseRemoteConfig.activateFetched();
                    Log.d(TAG, "Fetched value: " + mFirebaseRemoteConfig.getString(VERSION_CODE_KEY));
                    //calling function to check if new version is available or not
                    checkForUpdate();
                } /*else {
                   Toast.makeText(HomeActivity.this, "Something went wrong please try again",
                           Toast.LENGTH_SHORT).show();
               }*/
            }

        });

        Log.d(TAG, "Default value: " + mFirebaseRemoteConfig.getString(VERSION_CODE_KEY));

        /*End Firebase Remote Config*/


        comobo_liner_layout = findViewById(R.id.comobo_liner_layout);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token + "///////" + checnkVeriosn);

        pprogressDialog = new ProgressDialog(MainActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        no_product_img = findViewById(R.id.no_product_img);
        viewpager_image = findViewById(R.id.viewpager_image);

        root = findViewById(R.id.root);
        toolbar = findViewById(R.id.toolbar);
        contentHamburger = findViewById(R.id.content_hamburger);
        checkInternet = NetworkChecking.isConnected(this);


        guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        root.addView(guillotineMenu);
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
        cart_img = findViewById(R.id.cart_img);

        cart_img.setOnClickListener(this);


        categoryDbHelper = new CategoryDbHelper(MainActivity.this);


        kyf_group = guillotineMenu.findViewById(R.id.kyf_group);
        kyf_img = guillotineMenu.findViewById(R.id.kyf_img);
        kyf_txt = guillotineMenu.findViewById(R.id.kyf_txt);
        //kyf_txt.setTypeface(bold);
        story_group = guillotineMenu.findViewById(R.id.story_group);
        story_img = guillotineMenu.findViewById(R.id.story_img);
        story_txt = guillotineMenu.findViewById(R.id.story_txt);
        //story_txt.setTypeface(bold);
        fn_group = guillotineMenu.findViewById(R.id.fn_group);
        fn_img = guillotineMenu.findViewById(R.id.fn_img);
        fn_txt = guillotineMenu.findViewById(R.id.fn_txt);
        //fn_txt.setTypeface(bold);
        support_group = guillotineMenu.findViewById(R.id.support_group);
        support_img = guillotineMenu.findViewById(R.id.support_img);
        support_txt = guillotineMenu.findViewById(R.id.support_txt);
        support_txt = guillotineMenu.findViewById(R.id.support_txt);
        //support_txt.setTypeface(bold);

        ref_farmer_menu = guillotineMenu.findViewById(R.id.ref_farmer_menu);
        //ref_farmer_menu.setTypeface(bold);

        notification_menu = guillotineMenu.findViewById(R.id.notification_menu);
        //notification_menu.setTypeface(bold);

        sliderLayout = findViewById(R.id.slider_image);
        sliderLayout.setIndicatorAnimation(SliderLayout.Animations.FILL);
        //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(2);


        refer_farmer = guillotineMenu.findViewById(R.id.refer_farmer);
        notification = guillotineMenu.findViewById(R.id.notification);
        share = guillotineMenu.findViewById(R.id.share);
        terms_faq_policies = guillotineMenu.findViewById(R.id.terms_faq_policies);
        terms_faq_policies_img = guillotineMenu.findViewById(R.id.terms_faq_policies_img);
        terms_faq_policies_txt = guillotineMenu.findViewById(R.id.terms_faq_policies_txt);
        terms_faq_policies_txt = guillotineMenu.findViewById(R.id.terms_faq_policies_txt);
        //terms_faq_policies_txt.setTypeface(bold);


        kyf_group.setOnClickListener(this);
        story_group.setOnClickListener(this);
        fn_group.setOnClickListener(this);
        support_group.setOnClickListener(this);
        terms_faq_policies.setOnClickListener(this);
        refer_farmer.setOnClickListener(this);
        notification.setOnClickListener(this);
        share.setOnClickListener(this);


        box_ll = findViewById(R.id.box_ll);


        rllll = findViewById(R.id.rllll);
        desp_txt = findViewById(R.id.desp_txt);

        cart_count_txt = findViewById(R.id.cart_count_txt);
        total_price_txt = findViewById(R.id.total_price_txt);
        price_rl = findViewById(R.id.price_rl);
        rotate_rl = findViewById(R.id.rotate_rl);
        rupee_img = findViewById(R.id.rupee_img);
        price_txt = findViewById(R.id.price_txt);
        proceed_img = findViewById(R.id.proceed_img);
        veg_img = findViewById(R.id.veg_img);
        layoutBottom = findViewById(R.id.layoutBottom);
        go_box_bottom = findViewById(R.id.go_box_bottom);
        box_name_txt = findViewById(R.id.box_name_txt);

        custom_btn = findViewById(R.id.custom_btn);

        custom_btn.setOnClickListener(this);

        buy_img = findViewById(R.id.buy_img);
        profile_txt = findViewById(R.id.profile_txt);
        profile_txt.setOnClickListener(this);
        bulk_txt = findViewById(R.id.bulk_txt);

        orders_txt = findViewById(R.id.orders_txt);

        share_txt = findViewById(R.id.share_txt);
        farmcom_txt = findViewById(R.id.farmcom_txt);

        notifications_txt = findViewById(R.id.notifications_txt);


        preorders_txt = findViewById(R.id.preorders_txt);
        wallet_txt = findViewById(R.id.wallet_txt);
        relative_areas=findViewById(R.id.relative_areas);
        txt_loc_area = findViewById(R.id.txt_loc_area);
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(900); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        txt_loc_area.startAnimation(anim);

        bulk_txt.setOnClickListener(this);
        orders_txt.setOnClickListener(this);
        share_txt.setOnClickListener(this);
        farmcom_txt.setOnClickListener(this);
        notifications_txt.setOnClickListener(this);
        preorders_txt.setOnClickListener(this);
        wallet_txt.setOnClickListener(this);
        buy_img.setOnClickListener(this);

        box_products_grid = findViewById(R.id.box_products_grid);
        box_products_grid.setOnItemClickListener(this);

        box_products_grid.setFocusable(false);

        cat_recyclerview = findViewById(R.id.cat_recyclerview);

        box_recyclerview = findViewById(R.id.box_recyclerview);
        box_recyclerview.setHasFixedSize(true);
        box_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        box_recyclerview.setItemAnimator(new DefaultItemAnimator());

        go_box_recyclerview = findViewById(R.id.go_box_recyclerview);
        go_box_recyclerview.setHasFixedSize(true);
        go_box_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        go_box_recyclerview.setItemAnimator(new DefaultItemAnimator());



        updateUIAd();

        getCartCount();
        getCategories();
        addBanners(); //for slider banner
        locationShowFirsttime();
       // getAllAreas();

        Animation rotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.android_rotate_animation);
        rotateImage = (ImageView) findViewById(R.id.rotate_image);
        rotateImage.startAnimation(rotateAnimation);
        // rotateImage.clearAnimation();




    }


    private void locationShowFirsttime() {
        //location prefences
        SharedPreferences locationPref = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        boolean locationPrefBoolean = locationPref.getBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);



        if (locationPrefBoolean) {

            CustomBottomSheetDialogFragment newFragment = new CustomBottomSheetDialogFragment();
            newFragment.show(MainActivity.this.getSupportFragmentManager(), "Dialog");
        }
    }


    /*Start Firebase Remote Config*/
    private void checkForUpdate() {
        int latestAppVersion = (int) mFirebaseRemoteConfig.getDouble(VERSION_CODE_KEY);
        Log.e("LATESTVERSION", "" + latestAppVersion + " CURRENTVERSION  " + getCurrentVersionCode());
        if (latestAppVersion > getCurrentVersionCode()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Update the App")
                    .setMessage("A new version of this app is available on play store. if you update click ok").setPositiveButton(
                    "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en");
                            startActivity(new Intent(Intent.ACTION_VIEW, marketUri));


                        }
                    }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    builder.setCancelable(true);
                }
            }).setCancelable(true).show();
        } /*else {

           Toast.makeText(this,"This app is already up to date", Toast.LENGTH_SHORT).show();
       }*/
    }

    private void clearAppData() {

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userLogout(token,deviceId,user_id,deviceId);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {

                if (response.isSuccessful()) ;
                BaseResponse baseResponse = response.body();

                if (baseResponse.getStatus().equals("10100")) {

                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("LOCATION_PREF", 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.apply();
                    editor.clear();

                    userSessionManager.logoutUser1();

                   /* pprogressDialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();*/

                   // Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();


                } else if (baseResponse.getStatus().equals("10200")) {

                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (baseResponse.getStatus().equals("10300")) {

                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (baseResponse.getStatus().equals("10400")) {

                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


       /* try {
            // clearing app data
            String packageName = getApplicationContext().getPackageName();
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear "+packageName);

        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private int getCurrentVersionCode() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /*End Firebase Remote Config*/

    private void bottomOffersAd() {
        // adSliderList.clear();
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            pprogressDialog.show();

            Call<BottomOfferAddResponse> call = RetrofitClient.getInstance().getApi().BottomOfferAdd(token);
            call.enqueue(new Callback<BottomOfferAddResponse>() {
                @Override
                public void onResponse(Call<BottomOfferAddResponse> call, retrofit2.Response<BottomOfferAddResponse> response) {
                    if (response.isSuccessful()) ;
                    BottomOfferAddResponse bottomOfferAddResponse = response.body();
                    if (bottomOfferAddResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();

                        dataBeanList = bottomOfferAddResponse.getData();
                        mCustomPagerAdapter = new CustomPageAdapter(MainActivity.this, dataBeanList);
                        viewPager.setAdapter(mCustomPagerAdapter);

                    }
                    else if (bottomOfferAddResponse.getStatus().equals("10200")){
                        pprogressDialog.dismiss();
                        Toast.makeText(MainActivity.this, bottomOfferAddResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BottomOfferAddResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void updateUIAd() {
        bottomOffersAd();
        viewPager = findViewById(R.id.viewpager);
        viewPager.setClipToPadding(true);
        viewPager.useScale();
        viewPager.setPageMargin(0);


        //viewPager.setCurrentItem(true);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                String path = API_URL +dataBeanList.get(position).getImage();

                Picasso.with(MainActivity.this)
                        .load(path)
                        .into(viewpager_image);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });
    }

    public void addBanners() {
        if (checkInternet) {
            pprogressDialog.show();

            Call<SliderTopBannerResponse> call = RetrofitClient.getInstance().getApi().TopBanners(token);
            call.enqueue(new Callback<SliderTopBannerResponse>() {
                @Override
                public void onResponse(Call<SliderTopBannerResponse> call, retrofit2.Response<SliderTopBannerResponse> response) {
                    if (response.isSuccessful()) ;
                    SliderTopBannerResponse sliderTopBannerResponse = response.body();
                    if (sliderTopBannerResponse.getStatus().equals("10100")) {
                        pprogressDialog.dismiss();
                        List<SliderTopBannerResponse.DataBean> dataBeanList = sliderTopBannerResponse.getData();
                        for (int i = 0; i < dataBeanList.size(); i++) {

                            SliderView sliderView = new SliderView(MainActivity.this);
                            user_image = BASE_URL + dataBeanList.get(i).getImage();
                            Log.d(TAG, "onResponse: "+user_image);
                            sliderView.setImageUrl(user_image);
                            sliderView.setImageScaleType(ImageView.ScaleType.FIT_CENTER);

                            sliderView.setDescription(dataBeanList.get(i).getHighlighted_text());
                            final int finalI = i;
                            //at last add this view in your layout :
                            sliderLayout.addSliderView(sliderView);

                        }
                    } else if (sliderTopBannerResponse.getStatus().equals("10200")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(MainActivity.this, sliderTopBannerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SliderTopBannerResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            pprogressDialog.dismiss();
            Toast.makeText(MainActivity.this, "Check Internet Connection", Toast.LENGTH_LONG);

        }

    }



    @Override
    public void onBackPressed() {
        //  guillotineMenu.clearAnimation();

        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
            if (guillotineMenu != null) {
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setStartDelay(RIPPLE_DURATION)
                        .setActionBarViewForAnimation(toolbar)
                        .setClosedOnStart(true)
                        .build();
            }
        }

    }

   
    private void getCartCount() {

        Call<CartCountResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().CartCount(token, user_id, deviceId);
        cartCountResponseCall.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, retrofit2.Response<CartCountResponse> response) {
                if (response.isSuccessful()) ;
                CartCountResponse cartCountResponse = response.body();
                if (cartCountResponse.getStatus().equals("10100")) {
                    cart_count_txt.setText(String.valueOf(cartCountResponse.getData()));
                } else if (cartCountResponse.getStatus().equals("10200")) {
                    Toast.makeText(MainActivity.this, cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getCategories() {
        pprogressDialog.show();

        Call<HomeCategoriesResponse> categoriesResponseCall = RetrofitClient.getInstance().getApi().HomeCategories(token);
        categoriesResponseCall.enqueue(new Callback<HomeCategoriesResponse>() {
            @Override
            public void onResponse(Call<HomeCategoriesResponse> call, retrofit2.Response<HomeCategoriesResponse> response) {
                if (response.isSuccessful()) ;
                HomeCategoriesResponse homeCategoriesResponse = response.body();
                if (homeCategoriesResponse.getStatus().equals("10100")) {
                    pprogressDialog.dismiss();

                    List<HomeCategoriesResponse.DataBean> dataBeanList = homeCategoriesResponse.getData();


                    //cat_recyclerview.setAdapter(categoryAdapter);
                    ContentValues values = new ContentValues();
                    for (int i = 0; i < dataBeanList.size(); i++) {

                        values.put(CategoryDbHelper.CATEGORY_ID, dataBeanList.get(i).getId());
                        values.put(CategoryDbHelper.CATEGORY_NAME, dataBeanList.get(i).getName());
                        values.put(CategoryDbHelper.CATEGORY_IMAGE, BASE_URL + dataBeanList.get(i).getImage());
                        values.put(CategoryDbHelper.TYPE_AVAIL, dataBeanList.get(i).getType_avail());
                        categoryDbHelper.addCategoryList(values);

                    }

                    getCategoryList();
                } else if (homeCategoriesResponse.getStatus().equals("10200")) {
                    pprogressDialog.dismiss();
                    Toast.makeText(MainActivity.this, homeCategoriesResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HomeCategoriesResponse> call, Throwable t) {
                pprogressDialog.dismiss();
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getCategoryList() {

        //categoryModels.clear();

        CategoryDbHelper db = new CategoryDbHelper(MainActivity.this);
        List<String> categoryIdList = db.getCategoryId();
        List<String> categoryNameList = db.getCategoryName();
        List<String> categoryImageList = db.getCategoryImage();
        List<String> type_avail = db.getTypeAvail();



        categoryModels.clear();
        for (int i = 0; i < 4; i++) {

            CategoryModel cm = new CategoryModel();
            cm.setId(categoryIdList.get(i));
            cm.setName(categoryNameList.get(i));
            cm.setImage(categoryImageList.get(i));
            cm.setType_avail(type_avail.get(i));

            categoryModels.add(cm);
        }

        Log.d(TAG, "onResponse: "+categoryModels.size());
        cat_recyclerview.setHasFixedSize(true);
        cat_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        cat_recyclerview.setItemAnimator(new DefaultItemAnimator());
        categoryAdapter = new CategoryAdapter(categoryModels, this, R.layout.row_category);
        // cat_recyclerview.setAdapter(categoryAdapter);
        categoryAdapter.selection(1);
        cat_recyclerview.setAdapter(categoryAdapter);
        db.close();
    }

    public void getBoxProducts(String id, final String type) {
        pprogressDialog.show();
        boxNameList.clear();
        selectedType = type;
        productid = id;

        Call<HomeBoxProductsResponse> cartCountResponseCall = RetrofitClient.getInstance().getApi().HomeBoxProducts(token, id,type,"1",user_id, deviceId);
        cartCountResponseCall.enqueue(new Callback<HomeBoxProductsResponse>() {
            @Override
            public void onResponse(Call<HomeBoxProductsResponse> call, retrofit2.Response<HomeBoxProductsResponse> response) {
                if (response.isSuccessful()) ;
                pprogressDialog.dismiss();
                HomeBoxProductsResponse homeBoxProductsResponse = response.body();

                if (!homeBoxProductsResponse.getVersions().getAndroid_version().equalsIgnoreCase(checnkVeriosn)) {
                    sd_update = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.NORMAL_TYPE);
                    sd_update.setTitleText("App Update");
                    sd_update.setContentText("Please update your app now to experience our latest enhancements..!");
                    sd_update.setConfirmText("Update Now");
                    sd_update.showCancelButton(true);
                    sd_update.setCanceledOnTouchOutside(false);
                    sd_update.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sweetAlertDialog) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en"));
                            startActivity(intent);
                            sd_update.cancel();

                            clearAppData();

                        }
                    });
                    sd_update.show();
                }

                if (homeBoxProductsResponse.getStatus().equalsIgnoreCase("10100")) {
                    no_product_img.setVisibility(View.GONE);
                    String content = homeBoxProductsResponse.getContent();
                    desp_txt.setText(content);
                    pprogressDialog.dismiss();
                    box_ll.setVisibility(View.VISIBLE);
                    price_rl.setVisibility(View.VISIBLE);
                    boxNameList.clear();
                    productsList.clear();
                    Type stringStringMap = new TypeToken<ArrayList<Object>>() {
                    }.getType();
                    if (type.equalsIgnoreCase("COMBO_CUSTOM")) {
                        go_box_recyclerview.setVisibility(View.VISIBLE);
                        go_box_bottom.setVisibility(View.VISIBLE);
                        comobo_liner_layout.setVisibility(View.GONE);
                        rotate_rl.setVisibility(View.GONE);
                        layoutBottom.setVisibility(View.GONE);
                        ArrayList<Object> data = (ArrayList<Object>) homeBoxProductsResponse.getData();
                        boxNameList.addAll(data);

                        Map<String, Object> boxPos = (Map<String, Object>) boxNameList.get(0);
                        // productid = (String) boxPos.get("id");
                        String type = (String) boxPos.get("type");
                        String pdtName = (String) boxPos.get("pdtName");
                        String unitValue = (String) boxPos.get("unitValue");
                        String price = (String) boxPos.get("price");
                        String about = (String) boxPos.get("about");
                        String moreinfo = (String) boxPos.get("moreinfo");
                        String availability = (String) boxPos.get("availability");
                        String unitName = (String) boxPos.get("unitName");
                        String images = (String) boxPos.get("images");
                        ContentValues values = new ContentValues();
                        ArrayList<Object> tmpList = (ArrayList<Object>) boxPos.get("OPTIONAL");
                        for (int i = 0; i < tmpList.size(); i++) {
                            ArrayList<Object> custompricesarr = new ArrayList<>(tmpList.size());
                            CustomModel model = new CustomModel();
                            Map<String, Object> tmpObj = (Map<String, Object>) tmpList.get(i);
                            model.setId((String) tmpObj.get("id"));
                            model.setPdtName((String) tmpObj.get("pdtName"));
                            model.setImages((String) tmpObj.get("images"));
                            model.setAvailability((String) tmpObj.get("availability"));
                            ArrayList<Object> optionstmparr = (ArrayList<Object>) tmpObj.get("options");

                            for (int k = 0; k < optionstmparr.size(); k++) {
                                Map<String, Object> objectMap = new HashMap<>(tmpList.size());
                                Map<String, Object> priceObj1 = (Map<String, Object>) optionstmparr.get(k);
                                objectMap.put("optId", priceObj1.get("optId"));
                                objectMap.put("pdtId", priceObj1.get("pdtId"));
                                objectMap.put("optName", priceObj1.get("optName"));
                                objectMap.put("price", priceObj1.get("price"));
                                objectMap.put("qtyprice", priceObj1.get("price"));
                                objectMap.put("qty", 1);

                                custompricesarr.add(objectMap);
                                if (optionstmparr.size() == custompricesarr.size())
                                    model.setOptions(custompricesarr);
                                //Log.v("LLLLLLLLLL","///"+custompricesarr);
                            }
                            productsList.add(model);


                        }

                        //  productsList();
                        customBoxProductsAdapter = new GoCustomBoxProductsAdapter(MainActivity.this, productsList, R.layout.row_custom_box_products, total_price_txt, "");
                        go_box_recyclerview.setNestedScrollingEnabled(false);
                        go_box_recyclerview.setAdapter(customBoxProductsAdapter);
                    } else {
                        comobo_liner_layout.setVisibility(View.VISIBLE);
                        rotate_rl.setVisibility(View.VISIBLE);
                        layoutBottom.setVisibility(View.VISIBLE);
                        go_box_recyclerview.setVisibility(View.GONE);
                        go_box_bottom.setVisibility(View.GONE);
                        ArrayList<Object> data = (ArrayList<Object>) homeBoxProductsResponse.getData();
                        boxNameList.addAll(data);
                        boxNameAdapter = new BoxNameAdapter(boxNameList, MainActivity.this, R.layout.row_box, box_products_grid);
                        box_recyclerview.setAdapter(boxNameAdapter);

                        Map<String, Object> boxPos = (Map<String, Object>) boxNameList.get(0);
                        boxNameAdapter.selection(0);
                        SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putInt("position", 0);
                        editor.apply();

                        price(boxPos.get("price").toString());
                        boxpdtName(boxPos.get("pdtName").toString());

                        boxProductList = (ArrayList<Object>) boxPos.get("ESSENTIAL");
                        boxProductsAdapter = new BoxProductsAdapter(MainActivity.this, boxProductList);
                        box_products_grid.setAdapter(boxProductsAdapter);
                        box_products_grid.setExpanded(true);
                    }


                }

                if (homeBoxProductsResponse.getStatus().equalsIgnoreCase("10300")) {
                    pprogressDialog.dismiss();
                    // progress_indicator.setVisibility(View.GONE);
                    box_ll.setVisibility(View.GONE);
                    price_rl.setVisibility(View.GONE);
                    no_product_img.setVisibility(View.VISIBLE);

                    //   Toast.makeText(MainActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HomeBoxProductsResponse> call, Throwable t) {
                pprogressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void price(String price) {
        price_txt.setText(price);
    }

    public void productId(String id) {

    }

    public void boxpdtName(String name) {
        box_name_txt.setText(name);
    }

    @Override
    public void onClick(View v) {

        if (v == cart_img) {
            String count = cart_count_txt.getText().toString();
            if (!count.equalsIgnoreCase("0")) {
                Intent intent = new Intent(MainActivity.this, MyCartActivity.class);
                intent.putExtra("from", "Home");
                startActivity(intent);
            } else {
                Toast.makeText(this, "Please add atleast one product", Toast.LENGTH_SHORT).show();
            }

        }
        if (v == kyf_group) {
            Intent intent = new Intent(MainActivity.this, KYFActivity.class);
            startActivity(intent);
        }
        if (v == story_group) {
            Intent intent = new Intent(MainActivity.this, OurStoryActivity.class);
            startActivity(intent);
        }
        if (v == fn_group) {
            Intent intent = new Intent(MainActivity.this, FarmerNetworkActivity.class);
            startActivity(intent);
        }
        if (v == support_group) {
            Intent intent = new Intent(MainActivity.this, SupportActivity.class);
            startActivity(intent);
        }
        if (v == refer_farmer) {
            Intent referFarmer = new Intent(MainActivity.this, ReferFarmerActivity.class);
            startActivity(referFarmer);
        }
        if (v == notification) {
            Intent notification = new Intent(MainActivity.this, NotificationsActivity.class);
            startActivity(notification);
        }

        if (v == share) {
            boolean checkInternet = NetworkChecking.isConnected(MainActivity.this);
            if (checkInternet) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "GoFarmz -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, Here is an excellent app which supplies genuine naturally grown food products, sourced from local farmers, straight to your doorstep. Download to Check it out.  " + "https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else {
                Toast.makeText(this, "No Internet connection...!", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == terms_faq_policies) {

            // buy_img.setClickable(false);
            Intent intent = new Intent(MainActivity.this, TermsConditionsAndPrivacy.class);
            intent.putExtra("from", "NORMAL");
            startActivity(intent);
        }

        if (v == custom_btn) {

            Intent intent = new Intent(MainActivity.this, CustomBoxProductsActivity.class);
            intent.putExtra("pid", productid);
            intent.putExtra("from", "main");
            startActivity(intent);

        }
        if (v == buy_img) {
            boolean checkInternet = NetworkChecking.isConnected(MainActivity.this);
            if (checkInternet) {
                SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                int shPre_val = sharedpreferences.getInt("position", 0);
                Map<String, Object> boxPos = (Map<String, Object>) boxNameList.get(shPre_val);
                pr_id = (String) boxPos.get("id");
                Log.d("ISSS", pr_id);
                if (selectedType.equalsIgnoreCase("COMBO_CUSTOM")) {
                    addToCart(pr_id);
                   /* String totalprice = total_price_txt.getText().toString();
                    String[] split = totalprice.split(":");
                    int total = Integer.parseInt(split[1].trim());
                    if (total >= 300)
                        addToCart(pr_id);
                    else
                        Toast.makeText(this, "Minimun order is 300/-", Toast.LENGTH_SHORT).show();*/
                } else {
                    addToCart(pr_id);
                }
            } else {
                Toast.makeText(MainActivity.this, "Check Internet Connection", Toast.LENGTH_LONG);
            }
           /* if(selectedType.equalsIgnoreCase("COMBO_CUSTOM")){

            }else {

                addToCart(selectedType);   //ADD TO CART METHOD
            }*/

        }

        if (v == profile_txt) {
            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            intent.putExtra("activity", "MAINACTIVITY");
            startActivity(intent);
        }
        if (v == bulk_txt) {
            Intent intent = new Intent(MainActivity.this, BulkBuyerActivity.class);
            //intent.putExtra("activity", "MAINACTIVITY");
            startActivity(intent);
        }
        if (v == orders_txt) {
            Intent intent = new Intent(MainActivity.this, OrdersHistoryActivity1.class);
            intent.putExtra("R_id", "orders");
            startActivity(intent);
        }
        if (v == share_txt) {
            boolean checkInternet = NetworkChecking.isConnected(MainActivity.this);
            if (checkInternet) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "GoFarmz -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, Here is an excellent app which supplies genuine naturally grown food products, sourced from local farmers, straight to your doorstep. Download to Check it out.  " + "https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else {
                Toast.makeText(this, "No Internet connection...!", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == farmcom_txt) {
            Intent intent = new Intent(MainActivity.this, FarmComActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }

        if (v == notifications_txt) {
            Intent intent = new Intent(MainActivity.this, NotificationsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }

        if (v == preorders_txt) {
            Intent intent = new Intent(MainActivity.this, PreOrderActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }


        if (v == wallet_txt) {
            Intent intent = new Intent(MainActivity.this, WalletActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }


    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Map<String, Object> mainproductObj = (Map<String, Object>) boxNameList.get(boxNameAdapter.selectedPos);
        ArrayList<Object> tmpboxproductlist = (ArrayList<Object>) mainproductObj.get("ESSENTIAL");
        Map<String, Object> itemPosition = (Map<String, Object>) tmpboxproductlist.get(position);
        itemPosition.put("pid", productid);
        Intent it = new Intent(MainActivity.this, FormarActivity.class);
        it.putExtra("productobj", new HashMap<>(itemPosition));
        it.putExtra("FROM", "NONBROUSE");
        startActivity(it);
        Log.d("Chlid Id", "///" + itemPosition.get("pdtId"));
       /* new AlertDialog.Builder(MainActivity.this)
                 .setMessage("")
                .setCancelable(false)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent it=new Intent(MainActivity.this, FormarActivity.class);
                        it.putExtra("productobj",new HashMap<>(itemPosition));
                        startActivity(it);
                        Log.d("Chlid Id","///" + itemPosition.get("pdtId"));

                    }
                }).show();
*/
    }

    private void addToCart(final String pr_id) {
        pprogressDialog.show();
        Call<BaseResponse> cartCountResponseCall;
        if (selectedType.equalsIgnoreCase("COMBO_CUSTOM")) {

            for (int i = 0; i < customBoxProductsAdapter.cartarr.size(); i++) {
                customBoxProductsAdapter.cartarr.get(i);  //Arrya values
            }

            cartCountResponseCall = RetrofitClient.getInstance().getApi().addtocart(token,deviceId, user_id,pr_id, deviceId,"1",customBoxProductsAdapter.cartarr);

        } else {

             cartCountResponseCall = RetrofitClient.getInstance().getApi().addtocart(token,deviceId, user_id,pr_id, deviceId,"1",null);

        }

        cartCountResponseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, retrofit2.Response<BaseResponse> response) {
                if (response.isSuccessful()) ;
                pprogressDialog.dismiss();
                BaseResponse cartCountResponse = response.body();
                if (cartCountResponse.getStatus().equals("10100")) {
                    if (selectedType.equalsIgnoreCase("COMBO_CUSTOM"))
                        makeFlyAnimation(total_price_txt);
                    else
                        makeFlyAnimation(price_txt);
                    getCartCount();

                    Toast.makeText(MainActivity.this, "Add to Cart Successfully ", Toast.LENGTH_LONG).show();
                } else if (cartCountResponse.getStatus().equals("10200")) {
                    Toast.makeText(getApplicationContext(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (cartCountResponse.getStatus().equalsIgnoreCase("10300")) {
                    pprogressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Sorry Try Again.", Toast.LENGTH_SHORT).show();
                }
                if (cartCountResponse.getStatus().equalsIgnoreCase("10400")) {
                    pprogressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Already added to cart", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                pprogressDialog.dismiss();
            }
        });


      /*  String url = AppUrls.API_URL + AppUrls.ADDTOCART;
        Log.d("ADDCARTURL", url);
        pprogressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pprogressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("ADDCARTPONSE:", response);

                            String status = jsonObject.getString("status");
                            // String message = jsonObject.getString("message");

                            if (status.equalsIgnoreCase("10100")) {

                                if (selectedType.equalsIgnoreCase("COMBO_CUSTOM"))
                                    makeFlyAnimation(total_price_txt);
                                else
                                    makeFlyAnimation(price_txt);
                                getCartCount();
                                Toast.makeText(MainActivity.this, "Add to Cart Successfully ", Toast.LENGTH_LONG).show();
                            }
                            if (status.equalsIgnoreCase("10200")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Invalid Input.", Toast.LENGTH_SHORT).show();
                            }

                            if (status.equalsIgnoreCase("10300")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Sorry Try Again.", Toast.LENGTH_SHORT).show();
                            }
                            if (status.equalsIgnoreCase("10400")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Already added to cart", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.getMessage();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("rtttrtttrt", selectedType);
                if (selectedType.equalsIgnoreCase("COMBO_CUSTOM")) {

                    params.put("user_id", user_id);
                    params.put("product_id", pr_id);
                    params.put("browser_id", deviceId);
                    params.put("purchase_quantity", "1");  //by default one quantity
                    for (int i = 0; i < customBoxProductsAdapter.cartarr.size(); i++)
                        params.put("addOnProducts[" + i + "]", customBoxProductsAdapter.cartarr.get(i));  //Arrya values
                } else {
                    params.put("user_id", user_id);
                    params.put("product_id", pr_id);
                    params.put("browser_id", deviceId);
                    params.put("purchase_quantity", "1");  //by default one quantity
                }

                Log.d("CARTPARAM", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Gofarmz-Api-Key", GOFARMZ_API_KEY);
                headers.put("Authorization-Basic", token);
                headers.put("x-device-id", deviceId);
                headers.put("x-device-platform", "ANDROID");
                Log.d("GETOTPEADER", "HEADDER " + headers.toString());
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);*/
    }


    private void makeFlyAnimation(final TextView targetView) {

        ImageView destView = findViewById(R.id.cart_img);
        new CircleAnimationUtil().attachActivity(this).setTargetView(targetView).setMoveDuration(1000).setDestView(destView).setAnimationListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                targetView.setVisibility(View.VISIBLE);
                getCartCount();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();
    }

    public void setImagePath(String imgPath) {

        Log.d("asdkfjsdf", imgPath);
        Picasso.with(MainActivity.this)
                .load(imgPath)
                .placeholder(R.drawable.vegetable_img)
                .into(veg_img);
    }
}
